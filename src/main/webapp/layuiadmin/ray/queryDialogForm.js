Vue.component("query-form",{
		template:`<el-dialog title="检索" :visible.sync="show" :before-close="handleClose" :with-header="true">
					<el-row :gutter="0">
						<el-form ref="queryForm" :inline="true" label-width="80px" size="mini" :model="queryForm" class="demo-form-inline">
							<el-col :span="item.type=='radio'||item.type=='checkbox'||item.type=='datetime'?24:12" v-for="(item,index) in columns" :key="item.id" v-if="item.is_query">
							  <el-form-item :label="item.cn" :prop="item.en">
							    <el-input v-if="item.type == 'input'" v-model.lazy="queryForm[item.en]"></el-input>
							    <el-select v-if="item.type=='select' || item.type=='radio'" v-model="queryForm[item.en]" filterable placeholder="请选择">
								    <el-option
								      label="全部"
								      value=""
								      selected>
								    </el-option>
								    <el-option
								      v-for="selectItem in selectList[index]"
								      :key="selectItem.id"
								      :label="selectItem.label"
								      :value="selectItem.value">
								    </el-option>
								</el-select>
								<el-date-picker
									v-if="item.type=='date'"
									v-model="queryForm[item.en]"
							        type="daterange"
							        value-format="yyyy-MM-dd"
							        range-separator="至"
							        start-placeholder="开始日期"
							        end-placeholder="结束日期">
							    </el-date-picker>
							    <el-date-picker
									v-if="item.type=='datetime'"
									v-model="queryForm[item.en]"
							        type="datetimerange"
							        value-format="yyyy-MM-dd HH:mm:ss"
							        range-separator="至"
							        start-placeholder="开始时间"
							        end-placeholder="结束时间">
							    </el-date-picker>
							  </el-form-item>
							  </el-col>
							</el-form>
							</el-row>
							 <span slot="footer" class="dialog-footer">
							    <el-button type="primary" @click="query">查询</el-button>
							  </span>
							</el-dialog>`,
		  props: {params: Object},
		  data () {
		    return {
		    	columns:null,
		    	selectList:null,
		    	is_query:false,
		    	queryForm:{},
		    	show:false,
		    }
		  },
		  watch: {
			params () {
			  const { columns, selectList,is_query,show} = this.params
		      this.columns = columns
		      this.selectList = selectList
		      this.is_query = is_query
		      this.show = show
		    }
		  },
		  methods: {
			  query(){
				  this.$parent.query(this.queryForm)
			  },
			  handleClose(done) {
	        	  this.$parent.closeDialog();
	          },
		  }
})