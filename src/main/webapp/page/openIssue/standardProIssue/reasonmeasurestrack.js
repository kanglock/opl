Vue.component("reasonmeasures",{
	template:`
		<vxe-modal title="原因分析及措施详情" fullscreen append-to-body @close='close'
			v-model="showReasonMeasure" id="myModal6" width="800" height="400" min-width="460" min-height="320" 
			show-zoom resize remember transfer>
    		<vxe-table border size="mini" ref="nTable" :data="measuresList" :cell-class-name="cellStatusNameMea" class="mytable-style">
                <vxe-table-column field="reason_analysis" title="原因分析 " width="200"  type="html"></vxe-table-column>
                <vxe-table-column field="measure_type" title="措施类型" width="80"  :formatter="measureTypeFormatter" ></vxe-table-column>
		        <vxe-table-column field="measure" title="解决措施（延期-黄色；已完成-绿色）"   type="html"></vxe-table-column>
		        <vxe-table-column field="meature_track" title="措施跟踪历史" width="200"  type="html"></vxe-table-column>
		        <vxe-table-column title="完成凭证" width="80">
					<template slot-scope="scope">
						<vxe-button  status="primary" size="mini" @click="finishPingzheng(scope.row)" v-if="scope.row.measure_fujian_id!=0">下载</vxe-button>
					</template>
				</vxe-table-column>
				<vxe-table-column title="跟踪" width="80"  fixed="right">
					<template slot-scope="scope">
						<vxe-button  status="primary" size="mini" @click="trackOPL(scope.row)" v-if="scope.row.id!=0">跟踪</vxe-button>
					</template>
				</vxe-table-column>
            </vxe-table>
            <!-- 跟踪 -->
			<el-dialog append-to-body :visible.sync="showTrack" title="OPL跟踪情况"  center  :show-close="true" :fullscreen="false">
	   			<el-form :model="trackData" label-width="130px" ref="trackData" size="mini">
	   				<el-form-item label="措施详情"  > 
	   					<el-input v-model="trackData.measure" type="textarea" :autosize="{ minRows: 2, maxRows: 4}" readonly></el-input>
					</el-form-item>
					<el-form-item label="是否驳回" v-if="trackData.isFinish==2" > 
	  					<el-radio v-model="trackData.isBack" label="1">否</el-radio>
	  					<el-radio v-model="trackData.isBack" label="2">是</el-radio>
	  				</el-form-item>
	  				<el-form-item label="措施跟踪情况" > 
	  					<el-input v-model="trackData.measure_track" type="textarea" :autosize="{ minRows: 2, maxRows: 4}" placeholder="请输入措施跟踪情况"  ></el-input>
	  				</el-form-item>
	  				<el-form-item label="补充说明记录" > 
	  					<el-input v-model="trackData.opl_track" type="textarea" :autosize="{ minRows: 2, maxRows: 4}" placeholder="请输入其他补充说明"  ></el-input>
	  				</el-form-item>
	  				<el-form-item label="附件" v-if="trackData.isFinish==2" style="width:48%;display:inline-block"> 
	  					<el-button type="warning" @click="downLoadFujian()" v-if="trackData.measure_fujian_id!=0">下载完成证据</el-button>
	  				</el-form-item>
	  			</el-form>
	  			<span slot="footer" class="dialog-footer">
	 				<el-button type="primary" @click="saveTrack()">确 定</el-button>
					<el-button @click="showTrack = false">取 消</el-button>
				</span>
			</el-dialog>
        </vxe-modal>
		
	`,
	props: {params: Object},
	data () {
		return {
			showReasonMeasure: false,
	  	  	measuresList:[],
	  	  	issueId: null,
	  	  	measureTypeList:[
	        	{ value:0, label:'临时措施' },
	        	{ value:1, label:'永久措施' },
	        ],
	  	  	//OPL跟踪
	       	showTrack: false,
	  		trackData:{
	  	  		issueId: null,
	  	  		measureId: null,
	  	  		measure_track: null,
	  	  		measure: null,
	  	  		opl_track: null,
	  	  		isBack:'1',
	  	  		isFinish:null,
	  	  		measure_fujian_id: null
	  	  	},
		}
	},
	methods: {
		measureTypeFormatter({ cellValue }) {
  			let item = this.measureTypeList.find(item => item.value === cellValue)
            return item ? item.label : ''
        },
		//跟踪
  	  	trackOPL(row){
	  	  	this.showTrack= true
	  		this.trackData={
	  	  		issueId: row.id,
	  	  		measureId: row.measure_id,
	  	  		measure: row.measure,
	  	  		measure_track: null,
	  	  		opl_track: null,
	  	  		isBack:'1',
	  	  		isFinish: row.measure_status,
	  	  		measure_fujian_id: row.measure_fujian_id
	  	  	}
  	  	},
  	  	//保存OPL跟踪
  	  	saveTrack(){
	  	  	axios({
	  	    	method:"post",
	  	    	url:'/myDutyProOPL/saveTrackIssue',
	  	    	params:{
		  	    	formData: this.trackData,
		  	    }
	  		}).then((res)=>{
	  			if(res.status==200){
	  		    	if(res.data.state=="ok"){
	  		    		this.$message({
			    			message: res.data.msg,
			    			type: 'success'
			    		});
			    		this.showTrack=false;
			    		this.getData(this.issueId)
	  				}else{
	  					this.$message.error(res.data.msg);
	  				}
	  			}else{
	  				this.$message.error('网络请求失败');
	  			}
			})
  	 	},
		show(id){
  	 		this.issueId = id
			this.showReasonMeasure = true;
			this.getData(id)
		},
		getData(id){
			axios({
	    		method:"post",
	    		url:"/myCreateProOPL/getOPLreasonMeasures",
	    		params:{id:id}
    		}).then((res)=>{
		    	if(res.status==200){
		    		if(res.data.state=="ok"){
		    			this.measuresList = res.data.data
				    }else{
				    	this.$message.error(res.data.msg);
					}
			  	}else{
			  		this.$message.error('网络请求失败');
			  	}
    		})
		},
		cellStatusNameMea({ row, rowIndex, column, columnIndex }){
			if(column.property === 'measure'){
				if (row.color =="yellow") {
					return 'col-yellow'
				}else if (row.color =="green") {
					return 'col-green'
                }
			} 
        },
        finishPingzheng(row){
        	location.href = "/comMethod/downLoad/"+row.measure_fujian_id;
        },
        downLoadFujian(){
        	location.href = "/comMethod/downLoad/"+this.trackData.measure_fujian_id;
        },
        close() {
        	this.$parent.getProIssue();
        },
	}
})