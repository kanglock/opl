Vue.component("reasonmeasures",{
	template:`
		<vxe-modal title="原因分析及措施详情" fullscreen
		v-model="showReasonMeasure" id="myModal6" width="800" height="400" min-width="460" min-height="320" 
		show-zoom resize remember transfer>
    		<vxe-table border size="mini" ref="nTable" :data="measuresList" :cell-class-name="cellStatusNameMea" class="mytable-style">
                <vxe-table-column field="reason_analysis" title="原因分析 " width="200"  type="html"></vxe-table-column>
                <vxe-table-column field="measure_type" title="措施类型" width="80"  :formatter="measureTypeFormatter" ></vxe-table-column>
		        <vxe-table-column field="measure" title="解决措施（延期-黄色；已完成-绿色）"   type="html"></vxe-table-column>
		        <vxe-table-column field="meature_track" title="措施跟踪历史" width="200"  type="html"></vxe-table-column>
		        <vxe-table-column title="完成凭证" width="80">
					<template slot-scope="scope">
						<vxe-button  status="primary" size="mini" @click="finishPingzheng(scope.row)" v-if="scope.row.measure_fujian_id!=0">下载</vxe-button>
					</template>
				</vxe-table-column>
            </vxe-table>
        </vxe-modal>
	`,
	props: {params: Object},
	data () {
		return {
			showReasonMeasure: false,
	  	  	measuresList:[],
	  	  	measureTypeList:[
	        	{ value:0, label:'临时措施' },
	        	{ value:1, label:'永久措施' },
	        ],
		}
	},
	methods: {
		measureTypeFormatter({ cellValue }) {
  			let item = this.measureTypeList.find(item => item.value === cellValue)
            return item ? item.label : ''
        },
		show(id){
			this.showReasonMeasure = true;
			this.getData(id)
		},
		getData(id){
			axios({
	    		method:"post",
	    		url:"/myCreateProOPL/getOPLreasonMeasures",
	    		params:{id:id}
    		}).then((res)=>{
		    	if(res.status==200){
		    		if(res.data.state=="ok"){
		    			this.measuresList = res.data.data
				    }else{
				    	this.$message.error(res.data.msg);
					}
			  	}else{
			  		this.$message.error('网络请求失败');
			  	}
    		})
		},
		cellStatusNameMea({ row, rowIndex, column, columnIndex }){
			if(column.property === 'measure'){
				if (row.color =="yellow") {
					return 'col-yellow'
				}else if (row.color =="green") {
					return 'col-green'
                }
			} 
        },
        finishPingzheng(row){
        	location.href = "/comMethod/downLoad/"+row.measure_fujian_id;
        },
	}
})