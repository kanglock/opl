package com.ray.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

import com.github.liaochong.myexcel.core.DefaultExcelBuilder;
import com.github.liaochong.myexcel.utils.FileExportUtil;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Record;

public class ExportExcelUtil{
	
	static Prop p = PropKit.use("config-dev.properties").appendIfExists("config-pro.properties");
	
	/**
	 * 导出excel到配置文件路径下
	 * @param headerMap 第一行映射关系
	 * @param fileLogo 文件标识
	 * @param deliverList 数据record集合
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String export(Map headerMap,String fileLogo,List<Record> deliverList ) {
		List<Map> dataMapList = new ArrayList<>();
		for (int i = 0; i < deliverList.size(); i++) {
			Map<String, Object> temp = deliverList.get(i).getColumns();
			dataMapList.add(temp);
		}
		List<String> titles = new ArrayList(headerMap.values());
		List<String> orders = new ArrayList(headerMap.keySet());
		Workbook workbook = DefaultExcelBuilder.of(Map.class)
		        .sheetName("导出")
		        .titles(titles)
		        .widths(10,20)
		        .fieldDisplayOrder(orders)
		        .fixedTitles()
		        .build(dataMapList);
		String filename = fileLogo +".xlsx";
		File file = new File(p.get("domin_path")+"/upload/"+filename);
	    try {
			FileExportUtil.export(workbook,file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return filename;
	}
}