package com.ray.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiDepartmentListParentDeptsByDeptRequest;
import com.dingtalk.api.request.OapiUserGetRequest;
import com.dingtalk.api.response.OapiDepartmentListParentDeptsByDeptResponse;
import com.dingtalk.api.response.OapiUserGetResponse;
import com.ray.common.ding.AccessTokenUtil;
import com.ray.common.model.HCauseDep;



/**
 * 公共方法引用
 * @author FL00024996
 *
 */
public class HyCommenMethods {
	/**
	 * 判断字符串是否为数字
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年3月11日 下午3:23:52
	 */
	public static boolean IsNum(String numString) {
		for (int i = 0; i < numString.length(); i++){
			if (!Character.isDigit(numString.charAt(i))){
				return false;
			}
		}
		return true;
	}
	/**
	 * 日期加天数
	 */
	public static Date addDate(Date oldDate,int day) {
		Calendar calendar=Calendar.getInstance();
		calendar.clear();
		calendar.setTime(oldDate);
		calendar.add(Calendar.DATE, day);
		Date newDate=calendar.getTime();
		return newDate;
	}
	/**
	 * 项目部门与钉钉部门转化
	 */
	public static Long dDepId(String proDepName) {
		long depId=0;
		HCauseDep dep=HCauseDep.dao.findFirst("select * from h_cause_dep where dep_name like '%"+proDepName+"%' and status=0");
		if (dep!=null) {
			depId=dep.getDepId();
		}
		return depId;
	}
	/**
	 * 获取用户二级部门
	 */
	public static List<Long> secondDepId(String userDingId) {
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/get");
		OapiUserGetRequest req = new OapiUserGetRequest();
		req.setUserid(userDingId);
		req.setHttpMethod("GET");
		OapiUserGetResponse rsp = null;
		try {
			rsp = client.execute(req,  AccessTokenUtil.getToken());
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<Long> userDepartments=rsp.getDepartment();
		List<Long> secondDepList=new ArrayList<Long>();
		for (int i = 0; i < userDepartments.size(); i++) {
			long firstDep=0;
			DingTalkClient clientDep = new DefaultDingTalkClient("https://oapi.dingtalk.com/department/list_parent_depts_by_dept");
			OapiDepartmentListParentDeptsByDeptRequest request = new OapiDepartmentListParentDeptsByDeptRequest();
			request.setId(userDepartments.get(i).toString());
			request.setHttpMethod("GET");
			OapiDepartmentListParentDeptsByDeptResponse response = null;
			try {
				response=clientDep.execute(request, AccessTokenUtil.getToken());
			} catch (Exception e) {
				e.printStackTrace();
			}
			List<Long>	depList	=response.getParentIds();
			if (depList.size()==1) {
				firstDep=depList.get(0);
			}else if (depList.size()==2) {
				firstDep=depList.get(1);
			}else {
				firstDep=depList.get(depList.size()-3);
			}
			secondDepList.add(firstDep);
		}
		return secondDepList;
	}
}
