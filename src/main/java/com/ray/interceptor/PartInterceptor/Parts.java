package com.ray.interceptor.PartInterceptor;

import java.util.List;
import com.ray.common.interceptor.AopContext;
import com.ray.common.interceptor.MetaObjectIntercept;
import com.ray.common.model.HParts;
import com.ray.common.model.HQualityIssue;

public class Parts extends MetaObjectIntercept{
	@Override
	public String queryBefore(AopContext ac) throws Exception {
		ac.sql = ac.sql+" and status=0 ";
		return ac.sql;
	}
	@Override
	public String addBefore(AopContext ac) throws Exception {
		String part_no=ac.record.getStr("part_no");
		List<HParts> hParts=HParts.dao.find("select * from h_parts where part_no='"+part_no+"' and status=0");
		if (hParts.isEmpty()) {
			return null;
		}else {
			return "零件号已存在！";
		}
	}
	@Override
	public String updateBefore(AopContext ac) throws Exception {
		int partId=ac.record.getInt("id");
		String part_no=ac.record.getStr("part_no");
		List<HParts> hParts=HParts.dao.find("select * from h_parts where part_no='"+part_no+"' and status=0 and id<>"+partId);
		if (hParts.isEmpty()) {
			return null;
		}else {
			return "零件号已存在！";
		}
	}
	@Override
	public String deleteBefore(AopContext ac) throws Exception {
		HParts hPart=HParts.dao.findById(ac.record.getInt("id"));
		//判断此零件号是否已经有OPL存在
		List<HQualityIssue> issues=HQualityIssue.dao.find("select * from h_quality_issue where part_id="+hPart.getId());
		if (issues.isEmpty()) {
			hPart.setStatus(1);
			hPart.update();
			return null;
		}else {
			return "此零件号已被OPL关联，不能删除！";
		}
	}

}
