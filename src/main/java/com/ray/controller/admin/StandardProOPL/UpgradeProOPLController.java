package com.ray.controller.admin.StandardProOPL;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.HProIssuePlantimeEditRecord;
import com.ray.common.model.HProIssueReasonAnalysis;
import com.ray.common.model.HProIssueTell;
import com.ray.common.model.HProIssueTrackRecord;
import com.ray.common.model.HProjectIssue;
import com.ray.common.model.HProjectMeasures;
import com.ray.common.model.HProoplUpgradeRecord;
import com.ray.common.model.User;
/**
 * 升级处理upgradeProOPL/toUpgradeOPL
 * @author FL00024996
 *
 */
public class UpgradeProOPLController extends Controller {
	public void toUpgradeOPL() {
		render("upgradeIssues.html");
	}
	/**
	 * 以OPL为单元行展示
	 */
	public void getUpgradeOPL() {
		Record loginUser=getSessionAttr("user");
		User user=User.dao.findById(loginUser.getInt("id"));
		StringBuilder sb=new StringBuilder();
		sb.append(" from h_project_issue a,h_cause_dep b where a.dep_id=b.dep_id and a.is_del=0 and a.status<>'G' and a.quality_issue_id is null and a.handle_issue_id is null ");
		sb.append(" and a.id in (select issue_id from h_proopl_upgrade_record where ((new_user_id="+user.getId()+" and sp_status<>2 ) or old_user_id="+user.getId()+") )");
		String selProCode=get("selProCode");
		if (!"".equals(selProCode)&& selProCode!=null) {
			sb.append(" and a.pro_code like '%"+selProCode+"%'");
		}
		String selProductName=get("selProductName");
		if (selProductName!=null&&!"".equals(selProductName)) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		String selIssueType=get("selIssueType");
		if (selIssueType!=null&&!"".equals(selIssueType)) {
			sb.append(" and a.issue_type='"+selIssueType+"'");
		}
		String selStatus=get("selStatus");
		if (selStatus!=null&&!"".equals(selStatus)) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name ",sb.toString()+" order by id desc");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			//问题、目标
			String issue_description=issueList.getList().get(i).getStr("issue_description");
			if(issue_description==null) {
				issue_description="";
			}
			String issue_target=issueList.getList().get(i).getStr("issue_target");
			if (issue_target==null) {
				issue_target="";
			}
			String description_target="问题："+issue_description+"<br>"+"目标："+issue_target;
			//知会人
			List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueId+" and status=0");
			List<Integer> tellUserId=new ArrayList<Integer>();
			String tellUserName=" ";
			for (int j = 0; j < tells.size(); j++) {
				tellUserId.add(tells.get(j).getTellUserId());
				tellUserName=tellUserName+tells.get(j).getTellUserName()+"<br>";
			}
			//OPL整体跟踪记录
			List<HProIssueTrackRecord> issueTrackList=HProIssueTrackRecord.dao.find("select * from h_pro_issue_track_record where issue_id="+issueId);
			String issueTracks=" ";
			for (int j = 0; j < issueTrackList.size(); j++) {
				issueTracks=issueTracks+(j+1)+"."+issueTrackList.get(j).getTrackRecord()+"-"+issueTrackList.get(j).getCreateUserName()+"-"+sdf1.format(issueTrackList.get(j).getCreateTime())+"<br>";
			}
			//实际完成
			Date realFinishDate=issueList.getList().get(i).getDate("real_finish_time");
			String real_finish_time="";
			if (realFinishDate==null) {
				real_finish_time="进行中";
			}else {
				real_finish_time=sdf.format(realFinishDate);
			}
			//计划完成时间修改记录
			List<HProIssuePlantimeEditRecord> editRecords=HProIssuePlantimeEditRecord.dao.find("select * from h_pro_issue_plantime_edit_record where issue_id="+issueId);
			String planTimeEditRecord=" ";
			for (int j = 0; j < editRecords.size(); j++) {
				String oldTime="空";
				String newTime="空";
				if (editRecords.get(j).getOldPlanTime()!=null) {
					oldTime=sdf.format(editRecords.get(j).getOldPlanTime());
				}
				if (editRecords.get(j).getNewPlanTime()!=null) {
					newTime=sdf.format(editRecords.get(j).getNewPlanTime());
				}
				planTimeEditRecord=planTimeEditRecord+oldTime+"->"+newTime+"-"+editRecords.get(j).getCreateUserName()+"<br>";
			}
			issueList.getList().get(i)
			.set("tell_users_name",tellUserName)
			.set("tell_users_id", tellUserId)
			.set("description_target",description_target)
			.set("issue_track_record", issueTracks)
			.set("real_finish_time", real_finish_time)
			.set("caozuo", " ")
			.set("plan_time_edit_record", planTimeEditRecord);
			//升级对象钉钉id
			User upgradeUser=User.dao.findById(issueList.getList().get(i).getInt("upgrade_user_id"));
			issueList.getList().get(i).set("upgrade_user_id", upgradeUser.getDingUserId());
			//判断是否拥有升级按钮 \ 审批按钮 \ 驳回确认按钮
			int canUp=0;
			int canPassBack=0;
			int canSure=0;
			HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where status=0 and issue_id="+issueList.getList().get(i).getLong("id"));
			if (upgradeRecord.getSpStatus()==0) {//待审批
				if (upgradeRecord.getNewUserId().intValue()==user.getId().intValue()) {//审批人为我
					canPassBack=1;//拥有审批按钮
				}
			}else if (upgradeRecord.getSpStatus()==1) {//审批通过
				if (upgradeRecord.getNewUserId().intValue()==user.getId().intValue()) {//审批人为我
					canUp=1;//拥有升级按钮
				}
			}else if (upgradeRecord.getSpStatus()==2) {//审批驳回
				if (upgradeRecord.getOldUserId().intValue()==user.getId().intValue()&&issueList.getList().get(i).getInt("issue_status")==2) {//发起升级人为我,且还没有确认驳回
					canSure=1;//拥有确认驳回按钮
				}else if (upgradeRecord.getOldUserId().intValue()==user.getId().intValue()&&issueList.getList().get(i).getInt("issue_status")==0) {//发起升级人为我，且已经确认驳回处理
					canUp=1;//拥有升级按钮
				}
			}
			//升级原因
			issueList.getList().get(i).set("can_up", canUp).set("can_pass_back", canPassBack).set("can_sure", canSure).set("upgrade_reason", upgradeRecord.getUpgradeReason());
			//升级记录
			List<HProoplUpgradeRecord> upgradeRecords=HProoplUpgradeRecord.dao.find("select * from h_proopl_upgrade_record where issue_id="+issueList.getList().get(i).getLong("id"));
			String upgrades="暂无升级记录";
			if (!upgradeRecords.isEmpty()) {
				upgrades="";
				for (int j = 0; j < upgradeRecords.size(); j++) {
					String upStatus=upgradeRecords.get(j).getSpStatus()==0?"待审批":upgradeRecords.get(j).getSpStatus()==1?"审批通过":"审批驳回";
					upgrades=upgrades+upgradeRecords.get(j).getOldUserName()+" 将问题升级给 "+upgradeRecords.get(j).getNewUserName()+" ，升级原因："+upgradeRecords.get(j).getUpgradeReason()+"，处理状态："+upStatus+"<br>";
				}
			}
			issueList.getList().get(i).set("upgrade_history", upgrades);
			//原因分析
			List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
			String reason_analysis="暂无原因分析";
			String temMeasures="暂无临时措施";
			String perMeasures="暂无永久措施";
			if (!reasonAnalysis.isEmpty()) {
				reason_analysis="";
				boolean haveTemMeasure = false;
				boolean havePerTemMeasure = false;
				String temMeasure1="";
				String perMeasure1="";
				for (int j = 0; j < reasonAnalysis.size(); j++) {
					reason_analysis=reason_analysis+(j+1)+" "+reasonAnalysis.get(j).getReasonAnalysis()+"<br>";
					//临时措施状态、内容
					List<HProjectMeasures> temMeasureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" and measure_type=0 and is_del=0 order by xuhao asc");
					if (!temMeasureList.isEmpty()) {
						haveTemMeasure = true;
						for (int k = 0; k < temMeasureList.size(); k++) {
							int measureStatus=temMeasureList.get(k).getStatus();
							String measure=(j+1)+"."+(k+1)+" ";
							if (measureStatus==1) {//进行中
								measure=measure+temMeasureList.get(k).getMeasures()+"-"+temMeasureList.get(k).getUserName()+"-计划："+sdf.format(temMeasureList.get(k).getPlanFinishDate());
								if (temMeasureList.get(k).getPlanFinishDate().before(new Date())) {
								}
							}else {
								measure=measure+temMeasureList.get(k).getMeasures()+"-"+temMeasureList.get(k).getUserName()+"-"+temMeasureList.get(k).getRealFinishDate();
							}
							temMeasure1=temMeasure1+measure+"<br>";
						}
					}
					//永久
					List<HProjectMeasures> perMeasureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" and measure_type=1 and is_del=0 order by xuhao asc");
					if (!perMeasureList.isEmpty()) {
						havePerTemMeasure = true;
						for (int k = 0; k < perMeasureList.size(); k++) {
							int measureStatus=perMeasureList.get(k).getStatus();
							String measure=(j+1)+"."+(k+1)+" ";
							if (measureStatus==1) {//进行中
								measure=measure+perMeasureList.get(k).getMeasures()+"-"+perMeasureList.get(k).getUserName()+"-计划："+sdf.format(perMeasureList.get(k).getPlanFinishDate());
								if (perMeasureList.get(k).getPlanFinishDate().before(new Date())) {
								}
							}else {
								measure=measure+perMeasureList.get(k).getMeasures()+"-"+perMeasureList.get(k).getUserName()+"-"+perMeasureList.get(k).getRealFinishDate();
							}
							perMeasure1=perMeasure1+measure+"<br>";
						}
					}
				}
				if (haveTemMeasure) {
					temMeasures=temMeasure1;
				}
				if (havePerTemMeasure) {
					perMeasures=perMeasure1;
				}
			}
			issueList.getList().get(i)
			.set("reason_analysis", reason_analysis)
			.set("tem_measure", temMeasures).set("per_measure", perMeasures);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("totalResult", issueList.getTotalRow());
		record.set("list", issueList.getList());
		renderJson(record);
	}
	/**
	 * 以措施为单元行展示
	 */
//	public void getUpgradeOPL() {
//		Record loginUser=getSessionAttr("user");
//		User user=User.dao.findById(loginUser.getInt("id"));
//		StringBuilder sb=new StringBuilder();
//		sb.append(" from h_project_issue a,h_cause_dep b where a.dep_id=b.dep_id and a.is_del=0 and a.status<>'G'");
//		sb.append(" and a.id in (select issue_id from h_proopl_upgrade_record where ((new_user_id="+user.getId()+" and sp_status<>2 )or old_user_id="+user.getId()+") )");
//		String selProCode=get("selProCode");
//		if (!"".equals(selProCode)&& selProCode!=null) {
//			sb.append(" and a.pro_code like '%"+selProCode+"%'");
//		}
//		String selProductName=get("selProductName");
//		if (selProductName!=null&&!"".equals(selProductName)) {
//			sb.append(" and a.product_name like '%"+selProductName+"%'");
//		}
//		String selIssueType=get("selIssueType");
//		if (selIssueType!=null&&!"".equals(selIssueType)) {
//			sb.append(" and a.issue_type='"+selIssueType+"'");
//		}
//		String selStatus=get("selStatus");
//		if (selStatus!=null&&!"".equals(selStatus)) {
//			sb.append(" and a.status='"+selStatus+"'");
//		}
//		int page=getInt("currentPage");
//		int limit=getInt("pageSize");
//		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name ",sb.toString()+" order by id desc");
//		List<Record> proIssueList=new ArrayList<Record>();
//		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		for (int i = 0; i < issueList.getList().size(); i++) {
//			long issueId=issueList.getList().get(i).getLong("id");
//			//问题、目标
//			String issue_description=issueList.getList().get(i).getStr("issue_description");
//			if(issue_description==null) {
//				issue_description="";
//			}
//			String issue_target=issueList.getList().get(i).getStr("issue_target");
//			if (issue_target==null) {
//				issue_target="";
//			}
//			String description_target="问题："+issue_description+"<br>"+"目标："+issue_target;
//			//知会人
//			List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueId+" and status=0");
//			List<Integer> tellUserId=new ArrayList<Integer>();
//			String tellUserName=" ";
//			for (int j = 0; j < tells.size(); j++) {
//				tellUserId.add(tells.get(j).getTellUserId());
//				tellUserName=tellUserName+tells.get(j).getTellUserName()+"<br>";
//			}
//			//OPL整体跟踪记录
//			List<HProIssueTrackRecord> issueTrackList=HProIssueTrackRecord.dao.find("select * from h_pro_issue_track_record where issue_id="+issueId);
//			String issueTracks=" ";
//			for (int j = 0; j < issueTrackList.size(); j++) {
//				issueTracks=issueTracks+(j+1)+"."+issueTrackList.get(j).getTrackRecord()+"-"+issueTrackList.get(j).getCreateUserName()+"-"+sdf1.format(issueTrackList.get(j).getCreateTime())+"<br>";
//			}
//			//实际完成
//			Date realFinishDate=issueList.getList().get(i).getDate("real_finish_time");
//			String real_finish_time="";
//			if (realFinishDate==null) {
//				real_finish_time="进行中";
//			}else {
//				real_finish_time=sdf.format(realFinishDate);
//			}
//			//计划完成时间修改记录
//			List<HProIssuePlantimeEditRecord> editRecords=HProIssuePlantimeEditRecord.dao.find("select * from h_pro_issue_plantime_edit_record where issue_id="+issueId);
//			String planTimeEditRecord=" ";
//			for (int j = 0; j < editRecords.size(); j++) {
//				String oldTime="空";
//				String newTime="空";
//				if (editRecords.get(j).getOldPlanTime()!=null) {
//					oldTime=sdf.format(editRecords.get(j).getOldPlanTime());
//				}
//				if (editRecords.get(j).getNewPlanTime()!=null) {
//					newTime=sdf.format(editRecords.get(j).getNewPlanTime());
//				}
//				planTimeEditRecord=planTimeEditRecord+oldTime+"->"+newTime+"-"+editRecords.get(j).getCreateUserName()+"<br>";
//			}
//			issueList.getList().get(i)
//			.set("tell_users_name",tellUserName)
//			.set("tell_users_id", tellUserId)
//			.set("description_target",description_target)
//			.set("issue_track_record", issueTracks)
//			.set("real_finish_time", real_finish_time)
//			.set("caozuo", " ")
//			.set("plan_time_edit_record", planTimeEditRecord);
//			//升级对象钉钉id
//			User upgradeUser=User.dao.findById(issueList.getList().get(i).getInt("upgrade_user_id"));
//			issueList.getList().get(i).set("upgrade_user_id", upgradeUser.getDingUserId());
//			//判断是否拥有升级按钮 \ 审批按钮 \ 驳回确认按钮
//			int canUp=0;
//			int canPassBack=0;
//			int canSure=0;
//			HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where status=0 and issue_id="+issueList.getList().get(i).getLong("id"));
//			if (upgradeRecord.getSpStatus()==0) {//待审批
//				if (upgradeRecord.getNewUserId().intValue()==user.getId().intValue()) {//审批人为我
//					canPassBack=1;//拥有审批按钮
//				}
//			}else if (upgradeRecord.getSpStatus()==1) {//审批通过
//				if (upgradeRecord.getNewUserId().intValue()==user.getId().intValue()) {//审批人为我
//					canUp=1;//拥有升级按钮
//				}
//			}else if (upgradeRecord.getSpStatus()==2) {//审批驳回
//				if (upgradeRecord.getOldUserId().intValue()==user.getId().intValue()&&issueList.getList().get(i).getInt("issue_status")==2) {//发起升级人为我,且还没有确认驳回
//					canSure=1;//拥有确认驳回按钮
//				}else if (upgradeRecord.getOldUserId().intValue()==user.getId().intValue()&&issueList.getList().get(i).getInt("issue_status")==0) {//发起升级人为我，且已经确认驳回处理
//					canUp=1;//拥有升级按钮
//				}
//			}
//			//升级原因
//			issueList.getList().get(i).set("can_up", canUp).set("can_pass_back", canPassBack).set("can_sure", canSure).set("upgrade_reason", upgradeRecord.getUpgradeReason());
//			List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
//			if (reasonAnalysis.isEmpty()) {
//				Record issueRecord= new Record();
//				issueRecord.setColumns(issueList.getList().get(i));					
//				issueRecord.set("reason_id",0)
//				.set("reason_analysis", "暂无原因分析")
//				.set("measure_id", 0)
//				.set("measure_fujian_id", 0)
//				.set("measure_status",0)
//				.set("measure", "暂无")
//				.set("meature_track", "");
//				proIssueList.add(issueRecord);
//			}else {
//				//原因分析
//				for (int j = 0; j < reasonAnalysis.size(); j++) {
//					//措施状态、内容
//					List<HProjectMeasures> measureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId());
//					if (!measureList.isEmpty()) {
//						for (int k = 0; k < measureList.size(); k++) {
//							int measureStatus=measureList.get(k).getStatus();
//							String measure=measureList.get(k).getXuhao()+" ";
//							String color="";//措施单元格颜色标注
//							if (measureStatus==1) {//进行中
//								measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-计划："+sdf.format(measureList.get(k).getPlanFinishDate());
//								if (measureList.get(k).getPlanFinishDate().before(new Date())) {
//									color="yellow";
//								}
//							}else {
//								measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-"+measureList.get(k).getRealFinishDate();
//								color="green";
//							}
//							//措施跟踪情况 
//							List<HProissueTrackHistory> measureTrackList =HProissueTrackHistory.dao.find("select * from h_proissue_track_history where measure_id="+measureList.get(k).getId());
//							String measureTracks="";
//							for (int l = 0; l < measureTrackList.size(); l++) {
//								measureTracks=measureTracks+(l+1)+"."+measureTrackList.get(l).getRemark()+"-"+measureTrackList.get(l).getTrackUserName()+"-"+sdf1.format(measureTrackList.get(l).getCreateTime())+"<br>";
//							}
//							Record issueRecord= new Record();
//							issueRecord.setColumns(issueList.getList().get(i));
//							issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
//							.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
//							.set("measure_id", measureList.get(k).getId())
//							.set("measure_fujian_id", measureList.get(k).getFujianId())
//							.set("measure_status",measureStatus)
//							.set("measure", measure)
//							.set("color", color)
//							.set("meature_track", measureTracks);
//							proIssueList.add(issueRecord); 
//						}
//					}else {
//						Record issueRecord= new Record();
//						issueRecord.setColumns(issueList.getList().get(i));					
//						issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
//						.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
//						.set("measure_id", 0)
//						.set("measure_fujian_id", 0)
//						.set("measure_status",0)
//						.set("measure", "暂无")
//						.set("meature_track", "");
//						proIssueList.add(issueRecord);
//					}
//				}
//			}
//		}
//		Record record=new Record();
//		record.set("code", 0);
//		record.set("msg", "获取成功");
//		record.set("totalResult", issueList.getTotalRow());
//		record.set("list", proIssueList);
//		renderJson(record);
//	}
	/**
	 * 同意B升A
	 */
	public void passUpgradeIssueToA() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("issueId");
				HProjectIssue projectIssue=HProjectIssue.dao.findById(issueId);
				HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where status=0 and issue_id="+issueId);
				//问题状态改变
				projectIssue.setIssueStatus(0);
				projectIssue.setSeverity("A");
				projectIssue.update();
				//升级记录状态
				upgradeRecord.setSpStatus(1);
				upgradeRecord.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "升级成功！"));
			}else {
				renderJson(Ret.fail("msg", "升级失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 同意升级
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月21日 下午1:53:09
	 */
	@SuppressWarnings({  "rawtypes" })
	public void passUpgradeProIssue() {
		try {
			boolean flag = Db.tx(() -> {
				long editId=getParaToLong("editId");
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				HProjectIssue projectIssue=HProjectIssue.dao.findById(editId);
				//相关人员获取
				String upgrade_user_id=map.get("upgrade_user_id").toString();
				User upUser=User.dao.findFirst("select * from user where ding_user_id='"+upgrade_user_id+"'");
				//严重度处理
				String newSeverity="";
				HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where status=0 and issue_id="+editId);
				if (upgradeRecord.getUpgradeType()==0) {//手动升级，等级取选择等级
					newSeverity=upgradeRecord.getSeverity();
				}else {//自动升级，等级自动累加
					String severity= projectIssue.getSeverity();
					if ("D".equals(severity)) {
						newSeverity="C";
					}else if ("C".equals(severity)) {
						newSeverity="B";
					}else if ("B".equals(severity)) {
						newSeverity="A";
					}
				}
				//问题状态改变
				projectIssue.setIssueStatus(0);
				projectIssue.setUpgradeUserId(upUser.getId().intValue()).setUpgradeUserName(upUser.getNickname());
				projectIssue.setSeverity(newSeverity);
				projectIssue.update();
				//升级记录状态
				upgradeRecord.setSpStatus(1);
				upgradeRecord.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "升级成功！"));
			}else {
				renderJson(Ret.fail("msg", "升级失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 驳回
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月21日 下午2:02:15
	 */
	public void backUpgrade() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("id");
				HProjectIssue issue=HProjectIssue.dao.findById(issueId);
				issue.setIssueStatus(2);
				issue.update();
				//升级记录处理
				HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where status=0 and issue_id="+issueId);
				upgradeRecord.setSpStatus(2);
				upgradeRecord.update();
				//钉钉消息通知升级对象
				User oldUpUser=User.dao.findById(upgradeRecord.getOldUserId().intValue());
				DingMessage.sendText(oldUpUser.getDingUserId(),"项目号\n【"+issue.getProCode()+"】关联的标准OPL升级被驳回，请重新编辑计划完成时间！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "5");//5-标准OPL措施升级处理页面
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "升级驳回成功！"));
			}else {
				renderJson(Ret.fail("msg", "升级驳回失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 确认驳回处理
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月21日 下午2:12:24
	 */
	@SuppressWarnings({ "rawtypes", "unchecked"})
	public void sureBackProIssue() {
		try {
			boolean flag = Db.tx(() -> {
				long editId=getParaToLong("editId");
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				HProjectIssue projectIssue=HProjectIssue.dao.findById(editId);
				Record loginUser=getSessionAttr("user");
				User sysUser=User.dao.findById(loginUser.getInt("id"));
				
				Date oldPlanTime=projectIssue.getPlanFinishTime();
				Date newPlanTime=null;
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				try {
					newPlanTime=sdf.parse(map.get("plan_finish_time").toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				//问题状态改变
				projectIssue.setIssueStatus(0);
				projectIssue._setAttrs(map);
				projectIssue.update();
				//计划时间修改记录
				HProIssuePlantimeEditRecord plantimeEdit=new HProIssuePlantimeEditRecord();
				plantimeEdit.setIssueId(getParaToLong("editId"))
				.setOldPlanTime(oldPlanTime).setNewPlanTime(newPlanTime)
				.setCreateTime(new Date() )
				.setCreateUserId(sysUser.getId()).setCreateUserName(sysUser.getNickname());
				plantimeEdit.save();
				//升级记录状态
				HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where status=0 and issue_id="+editId);
				upgradeRecord.setSpStatus(2);
				upgradeRecord.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "处理成功！"));
			}else {
				renderJson(Ret.fail("msg", "处理失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
}
