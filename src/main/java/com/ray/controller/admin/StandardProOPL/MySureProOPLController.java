package com.ray.controller.admin.StandardProOPL;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.model.HProIssuePlantimeEditRecord;
import com.ray.common.model.HProIssueReasonAnalysis;
import com.ray.common.model.HProIssueTell;
import com.ray.common.model.HProIssueTrackRecord;
import com.ray.common.model.HProissueTrackHistory;
import com.ray.common.model.HProjectIssue;
import com.ray.common.model.HProjectMeasures;
import com.ray.common.model.HProoplUpgradeRecord;
import com.ray.common.model.User;
/**
 * 我确认的标准OPL mySureProOPL/toMySureProOPL
 * @author FL00024996
 *
 */
public class MySureProOPLController extends Controller {
	public void toMySureProOPL() {
		render("mySureProOPL.html");
	}
	/**
	 * 以OPL为单元行展示
	 */
	public void getMySureProOPL() {
		StringBuilder sb=new StringBuilder();
		Record loginUser=getSessionAttr("user");
		int loginUserId=loginUser.getInt("id");
		sb.append(" from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.confirmer_id="+loginUserId+" and a.status<>'G' and a.quality_issue_id is null and a.handle_issue_id is null ");//我确认的
		String selProductName=get("selProductName");
		String selStatus=get("selStatus");
		String selIssueType=get("selIssueType");
		String selProCode=get("selProCode");
		String selIssueLevel=get("selIssueLevel");
		if (selIssueLevel!=null&&!"".equals(selIssueLevel)) {
			sb.append(" and a.severity='"+selIssueLevel+"'");
		}
		if (!"".equals(selProCode)&& selProCode!=null) {
			sb.append(" and a.pro_code like '%"+selProCode+"%'");
		}
		if (selProductName!=null&&!"".equals(selProductName)) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		if (selIssueType!=null&&!"".equals(selIssueType)) {
			sb.append(" and a.issue_type='"+selIssueType+"'");
		}
		if (selStatus!=null&&!"".equals(selStatus)) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name ",sb.toString()+" order by id desc");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			//问题、目标
			String issue_description=issueList.getList().get(i).getStr("issue_description");
			if(issue_description==null) {
				issue_description="";
			}
			String issue_target=issueList.getList().get(i).getStr("issue_target");
			if (issue_target==null) {
				issue_target="";
			}
			String description_target="问题："+issue_description+"<br>"+"目标："+issue_target;
			//知会人
			List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueList.getList().get(i).getLong("id")+" and status=0");
			List<Integer> tellUserId=new ArrayList<Integer>();
			String tellUserName=" ";
			for (int j = 0; j < tells.size(); j++) {
				tellUserId.add(tells.get(j).getTellUserId());
				tellUserName=tellUserName+tells.get(j).getTellUserName()+"<br>";
			}
			//OPL整体跟踪记录
			List<HProIssueTrackRecord> issueTrackList=HProIssueTrackRecord.dao.find("select * from h_pro_issue_track_record where issue_id="+issueId);
			String issueTracks=" ";
			for (int j = 0; j < issueTrackList.size(); j++) {
				issueTracks=issueTracks+(j+1)+"."+issueTrackList.get(j).getTrackRecord()+"-"+issueTrackList.get(j).getCreateUserName()+"-"+issueTrackList.get(j).getCreateTime()+"<br>";
			}
			//实际完成
			Date realFinishDate=issueList.getList().get(i).getDate("real_finish_time");
			String real_finish_time="";
			if (realFinishDate==null) {
				real_finish_time="进行中";
			}else {
				real_finish_time=sdf.format(realFinishDate);
			}
			//计划完成时间修改记录
			List<HProIssuePlantimeEditRecord> editRecords=HProIssuePlantimeEditRecord.dao.find("select * from h_pro_issue_plantime_edit_record where issue_id="+issueId);
			String planTimeEditRecord=" ";
			for (int j = 0; j < editRecords.size(); j++) {
				String oldTime="空";
				String newTime="空";
				if (editRecords.get(j).getOldPlanTime()!=null) {
					oldTime=sdf.format(editRecords.get(j).getOldPlanTime());
				}
				if (editRecords.get(j).getNewPlanTime()!=null) {
					newTime=sdf.format(editRecords.get(j).getNewPlanTime());
				}
				planTimeEditRecord=planTimeEditRecord+oldTime+"->"+newTime+"-"+editRecords.get(j).getCreateUserName()+"<br>";
			}
			//是否可关闭
			List<HProjectMeasures> inMeasures=HProjectMeasures.dao.find("select * from h_project_measures where issue_id="+issueId+" and is_del=0 and status=1");//进行中
			List<HProjectMeasures> allMeasures=HProjectMeasures.dao.find("select * from h_project_measures where issue_id="+issueId+" and is_del=0 ");
			int issueStatus=issueList.getList().get(i).getInt("issue_status");
			int canClose=0;
			if (inMeasures.isEmpty()&&!allMeasures.isEmpty()&&issueStatus==0) {//没有进行中措施&有措施&opl状态为0
				canClose=1;
			}
			issueList.getList().get(i)
			.set("tell_users_name",tellUserName)
			.set("tell_users_id", tellUserId)
			.set("description_target",description_target)
			.set("issue_track_record", issueTracks)
			.set("real_finish_time", real_finish_time)
			.set("caozuo", " ")
			.set("can_close", canClose)
			.set("plan_time_edit_record", planTimeEditRecord);
			//升级记录
			HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where status=0 and issue_id="+issueList.getList().get(i).getLong("id"));
			if (upgradeRecord==null) {
				issueList.getList().get(i).set("upgrade_reason", "未升级");
			}else {
				issueList.getList().get(i).set("upgrade_reason", upgradeRecord.getUpgradeReason());
			}
			//升级记录
			List<HProoplUpgradeRecord> upgradeRecords=HProoplUpgradeRecord.dao.find("select * from h_proopl_upgrade_record where issue_id="+issueList.getList().get(i).getLong("id"));
			String upgrades="暂无升级记录";
			if (!upgradeRecords.isEmpty()) {
				upgrades="";
				for (int j = 0; j < upgradeRecords.size(); j++) {
					String upStatus=upgradeRecords.get(j).getSpStatus()==0?"待审批":upgradeRecords.get(j).getSpStatus()==1?"审批通过":"审批驳回";
					upgrades=upgrades+upgradeRecords.get(j).getOldUserName()+" 将问题升级给 "+upgradeRecords.get(j).getNewUserName()+" ，升级原因："+upgradeRecords.get(j).getUpgradeReason()+"，处理状态："+upStatus+"<br>";
				}
			}
			issueList.getList().get(i).set("upgrade_history", upgrades);
			//原因分析
			List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0  and issue_id="+issueId);
			String reason_analysis="暂无原因分析";
			String temMeasures="暂无临时措施";
			String perMeasures="暂无永久措施";
			if (!reasonAnalysis.isEmpty()) {
				reason_analysis="";
				boolean haveTemMeasure = false;
				boolean havePerTemMeasure = false;
				String temMeasure1="";
				String perMeasure1="";
				for (int j = 0; j < reasonAnalysis.size(); j++) {
					reason_analysis=reason_analysis+(j+1)+" "+reasonAnalysis.get(j).getReasonAnalysis()+"<br>";
					//临时措施状态、内容
					List<HProjectMeasures> temMeasureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" and measure_type=0 and is_del=0 order by xuhao asc");
					if (!temMeasureList.isEmpty()) {
						haveTemMeasure = true;
						for (int k = 0; k < temMeasureList.size(); k++) {
							int measureStatus=temMeasureList.get(k).getStatus();
							String measure=(j+1)+"."+(k+1)+" ";
							if (measureStatus==1) {//进行中
								measure=measure+temMeasureList.get(k).getMeasures()+"-"+temMeasureList.get(k).getUserName()+"-计划："+sdf.format(temMeasureList.get(k).getPlanFinishDate());
								if (temMeasureList.get(k).getPlanFinishDate().before(new Date())) {
								}
							}else {
								measure=measure+temMeasureList.get(k).getMeasures()+"-"+temMeasureList.get(k).getUserName()+"-"+temMeasureList.get(k).getRealFinishDate();
							}
							temMeasure1=temMeasure1+measure+"<br>";
						}
					}
					//永久
					List<HProjectMeasures> perMeasureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" and measure_type=1 and is_del=0 order by xuhao asc");
					if (!perMeasureList.isEmpty()) {
						havePerTemMeasure = true;
						for (int k = 0; k < perMeasureList.size(); k++) {
							int measureStatus=perMeasureList.get(k).getStatus();
							String measure=(j+1)+"."+(k+1)+" ";
							if (measureStatus==1) {//进行中
								measure=measure+perMeasureList.get(k).getMeasures()+"-"+perMeasureList.get(k).getUserName()+"-计划："+sdf.format(perMeasureList.get(k).getPlanFinishDate());
								if (perMeasureList.get(k).getPlanFinishDate().before(new Date())) {
								}
							}else {
								measure=measure+perMeasureList.get(k).getMeasures()+"-"+perMeasureList.get(k).getUserName()+"-"+perMeasureList.get(k).getRealFinishDate();
							}
							perMeasure1=perMeasure1+measure+"<br>";
						}
					}
				}
				if (haveTemMeasure) {
					temMeasures=temMeasure1;
				}
				if (havePerTemMeasure) {
					perMeasures=perMeasure1;
				}
			}
			issueList.getList().get(i)
			.set("reason_analysis", reason_analysis)
			.set("tem_measure", temMeasures).set("per_measure", perMeasures);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", issueList.getList());
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**
	 * 以措施为单元行展示
	 */
//	public void getMySureProOPL() {
//		StringBuilder sb=new StringBuilder();
//		Record loginUser=getSessionAttr("user");
//		int loginUserId=loginUser.getInt("id");
//		sb.append(" from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.confirmer_id="+loginUserId+" and a.status<>'G'");//我确认的
//		String selProductName=get("selProductName");
//		String selStatus=get("selStatus");
//		String selIssueType=get("selIssueType");
//		String selProCode=get("selProCode");
//		String selIssueLevel=get("selIssueLevel");
//		if (selIssueLevel!=null&&!"".equals(selIssueLevel)) {
//			sb.append(" and a.severity='"+selIssueLevel+"'");
//		}
//		if (!"".equals(selProCode)&& selProCode!=null) {
//			sb.append(" and a.pro_code like '%"+selProCode+"%'");
//		}
//		if (selProductName!=null&&!"".equals(selProductName)) {
//			sb.append(" and a.product_name like '%"+selProductName+"%'");
//		}
//		if (selIssueType!=null&&!"".equals(selIssueType)) {
//			sb.append(" and a.issue_type='"+selIssueType+"'");
//		}
//		if (selStatus!=null&&!"".equals(selStatus)) {
//			sb.append(" and a.status='"+selStatus+"'");
//		}
//		int page=getInt("currentPage");
//		int limit=getInt("pageSize");
//		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name ",sb.toString()+" order by id desc");
//		List<Record> proIssueList=new ArrayList<Record>();
//		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//		for (int i = 0; i < issueList.getList().size(); i++) {
//			long issueId=issueList.getList().get(i).getLong("id");
//			//问题、目标
//			String issue_description=issueList.getList().get(i).getStr("issue_description");
//			if(issue_description==null) {
//				issue_description="";
//			}
//			String issue_target=issueList.getList().get(i).getStr("issue_target");
//			if (issue_target==null) {
//				issue_target="";
//			}
//			String description_target="问题："+issue_description+"<br>"+"目标："+issue_target;
//			//知会人
//			List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueList.getList().get(i).getLong("id")+" and status=0");
//			List<Integer> tellUserId=new ArrayList<Integer>();
//			String tellUserName=" ";
//			for (int j = 0; j < tells.size(); j++) {
//				tellUserId.add(tells.get(j).getTellUserId());
//				tellUserName=tellUserName+tells.get(j).getTellUserName()+"<br>";
//			}
//			//OPL整体跟踪记录
//			List<HProIssueTrackRecord> issueTrackList=HProIssueTrackRecord.dao.find("select * from h_pro_issue_track_record where issue_id="+issueId);
//			String issueTracks=" ";
//			for (int j = 0; j < issueTrackList.size(); j++) {
//				issueTracks=issueTracks+(j+1)+"."+issueTrackList.get(j).getTrackRecord()+"-"+issueTrackList.get(j).getCreateUserName()+"-"+issueTrackList.get(j).getCreateTime()+"<br>";
//			}
//			//实际完成
//			Date realFinishDate=issueList.getList().get(i).getDate("real_finish_time");
//			String real_finish_time="";
//			if (realFinishDate==null) {
//				real_finish_time="进行中";
//			}else {
//				real_finish_time=sdf.format(realFinishDate);
//			}
//			//计划完成时间修改记录
//			List<HProIssuePlantimeEditRecord> editRecords=HProIssuePlantimeEditRecord.dao.find("select * from h_pro_issue_plantime_edit_record where issue_id="+issueId);
//			String planTimeEditRecord=" ";
//			for (int j = 0; j < editRecords.size(); j++) {
//				String oldTime="空";
//				String newTime="空";
//				if (editRecords.get(j).getOldPlanTime()!=null) {
//					oldTime=sdf.format(editRecords.get(j).getOldPlanTime());
//				}
//				if (editRecords.get(j).getNewPlanTime()!=null) {
//					newTime=sdf.format(editRecords.get(j).getNewPlanTime());
//				}
//				planTimeEditRecord=planTimeEditRecord+oldTime+"->"+newTime+"-"+editRecords.get(j).getCreateUserName()+"<br>";
//			}
//			//是否可关闭
//			List<HProjectMeasures> inMeasures=HProjectMeasures.dao.find("select * from h_project_measures where issue_id="+issueId+" and is_del=0 and status=1");//进行中
//			List<HProjectMeasures> allMeasures=HProjectMeasures.dao.find("select * from h_project_measures where issue_id="+issueId+" and is_del=0 ");
//			int issueStatus=issueList.getList().get(i).getInt("issue_status");
//			int canClose=0;
//			if (inMeasures.isEmpty()&&!allMeasures.isEmpty()&&issueStatus==0) {//没有进行中措施&有措施&opl状态为0
//				canClose=1;
//			}
//			issueList.getList().get(i)
//			.set("tell_users_name",tellUserName)
//			.set("tell_users_id", tellUserId)
//			.set("description_target",description_target)
//			.set("issue_track_record", issueTracks)
//			.set("real_finish_time", real_finish_time)
//			.set("caozuo", " ")
//			.set("can_close", canClose)
//			.set("plan_time_edit_record", planTimeEditRecord);
//			//升级记录
//			HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where status=0 and issue_id="+issueList.getList().get(i).getLong("id"));
//			if (upgradeRecord==null) {
//				issueList.getList().get(i).set("upgrade_reason", "未升级");
//			}else {
//				issueList.getList().get(i).set("upgrade_reason", upgradeRecord.getUpgradeReason());
//			}
//			List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
//			if (reasonAnalysis.isEmpty()) {
//				Record issueRecord= new Record();
//				issueRecord.setColumns(issueList.getList().get(i));					
//				issueRecord.set("reason_id",0)
//				.set("reason_analysis", "暂无原因分析")
//				.set("measure_id", 0)
//				.set("measure_fujian_id", 0)
//				.set("measure_status",0)
//				.set("measure", "暂无")
//				.set("meature_track", "");
//				proIssueList.add(issueRecord);
//			}else {
//				//原因分析
//				for (int j = 0; j < reasonAnalysis.size(); j++) {
//					//措施状态、内容
//					List<HProjectMeasures> measureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId());
//					if (!measureList.isEmpty()) {
//						for (int k = 0; k < measureList.size(); k++) {
//							int measureStatus=measureList.get(k).getStatus();
//							String measure=measureList.get(k).getXuhao()+" ";
//							String color="";//措施单元格颜色标注
//							if (measureStatus==1) {//进行中
//								measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-计划："+sdf.format(measureList.get(k).getPlanFinishDate());
//								if (measureList.get(k).getPlanFinishDate().before(new Date())) {
//									color="yellow";
//								}
//							}else {
//								measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-"+measureList.get(k).getRealFinishDate();
//								color="green";
//							}
//							//措施跟踪情况 
//							List<HProissueTrackHistory> measureTrackList =HProissueTrackHistory.dao.find("select * from h_proissue_track_history where measure_id="+measureList.get(k).getId());
//							String measureTracks="";
//							for (int l = 0; l < measureTrackList.size(); l++) {
//								measureTracks=measureTracks+(l+1)+"."+measureTrackList.get(l).getRemark()+"-"+measureTrackList.get(l).getTrackUserName()+"-"+measureTrackList.get(l).getCreateTime()+"<br>";
//							}
//							Record issueRecord= new Record();
//							issueRecord.setColumns(issueList.getList().get(i));
//							issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
//							.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
//							.set("measure_id", measureList.get(k).getId())
//							.set("measure_fujian_id", measureList.get(k).getFujianId())
//							.set("measure_status",measureStatus)
//							.set("measure", measure)
//							.set("color", color)
//							.set("meature_track", measureTracks);
//							proIssueList.add(issueRecord); 
//						}
//					}else {
//						Record issueRecord= new Record();
//						issueRecord.setColumns(issueList.getList().get(i));					
//						issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
//						.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
//						.set("measure_id", 0)
//						.set("measure_fujian_id", 0)
//						.set("measure_status",0)
//						.set("measure", "暂无")
//						.set("meature_track", "");
//						proIssueList.add(issueRecord);
//					}
//				}
//			}
//		}
//		Record record=new Record();
//		record.set("code", 0);
//		record.set("msg", "获取成功");
//		record.set("list", proIssueList);
//		record.set("totalResult", issueList.getTotalRow());
//		renderJson(record);
//	}
	/**
	 * 关闭问题
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月25日 下午4:15:42
	 */
	@SuppressWarnings("rawtypes")
	public void closeIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issureId=Long.valueOf(map.get("issue_id").toString());
				//查询此OPL下是否有永久措施
				List<HProjectMeasures> perMeasureList=HProjectMeasures.dao.find("select * from h_project_measures where issue_id="+issureId+" and  measure_type=1 order by xuhao asc");
				if (perMeasureList.isEmpty()) {
					return false;
				}
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				Date closeTime=null;
				try {
					closeTime=sdf.parse(map.get("closeTime").toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Record loginUser=getSessionAttr("user");
				User user=User.dao.findById(loginUser.getInt("id"));
				HProjectIssue projectIssue=HProjectIssue.dao.findById(issureId);
				projectIssue.setRealFinishTime(closeTime).setStatus("G");
				projectIssue.update();
				//添加跟踪记录
				HProissueTrackHistory trackHistory=new HProissueTrackHistory();
				trackHistory.setIssueId(issureId)
				.setMeasureStatus(3).setRemark("问题关闭")
				.setTrackUserId(user.getId()).setTrackUserName(user.getNickname())
				.setCreateTime(new Date());
				trackHistory.save();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "问题关闭成功！"));
			}else {
				renderJson(Ret.fail("msg", "此问题没有永久性措施，请确认！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}

}
