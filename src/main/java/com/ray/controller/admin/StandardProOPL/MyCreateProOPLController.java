package com.ray.controller.admin.StandardProOPL;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.liaochong.myexcel.core.DefaultExcelReader;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.HDepLine;
import com.ray.common.model.HProIssuePlantimeEditRecord;
import com.ray.common.model.HProIssueReasonAnalysis;
import com.ray.common.model.HProIssueTell;
import com.ray.common.model.HProIssueTrackRecord;
import com.ray.common.model.HProissueEditRecord;
import com.ray.common.model.HProissueTrackHistory;
import com.ray.common.model.HProjectIssue;
import com.ray.common.model.HProjectMeasures;
import com.ray.common.model.HProoplUpgradeRecord;
import com.ray.common.model.User;
import com.ray.common.model.UserRole;
/**
 * 我创建的标准OPL myCreateProOPL
 * @author FL00024996
 *
 */
public class MyCreateProOPLController extends Controller {
	public void toMyCreateProOPL() {
		render("myCreateProOPL.html");
	}
	/**
	 * 以OPL为单元行展示
	 */
	public void getMyCreateProOPL() {
		StringBuilder sb=new StringBuilder();
		Record loginUser=getSessionAttr("user");
		int loginUserId=loginUser.getInt("id");
		UserRole userRole = UserRole.dao.findFirst("select a.* from user_role a, roles b where a.user_id="+loginUserId+" and a.role_id=b.id and (b.role_name='project_tixi' or b.role_name='admin')");
		if (userRole==null) {
			sb.append(" from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.create_user_id="+loginUserId+" and a.status<>'G'  and a.quality_issue_id is null and a.handle_issue_id is null ");//我创建的
		}else {
			sb.append(" from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.status<>'G'  and a.quality_issue_id is null and a.handle_issue_id is null ");
		}
		String selProductName=get("selProductName");
		String selStatus=get("selStatus");
		String selIssueType=get("selIssueType");
		String selProCode=get("selProCode");
		String selIssueLevel=get("selIssueLevel");
		if (selIssueLevel!=null&&!"".equals(selIssueLevel)) {
			sb.append(" and a.severity='"+selIssueLevel+"'");
		}
		if (!"".equals(selProCode)&& selProCode!=null) {
			sb.append(" and a.pro_code like '%"+selProCode+"%'");
		}
		if (selProductName!=null&&!"".equals(selProductName)) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		if (selIssueType!=null&&!"".equals(selIssueType)) {
			sb.append(" and a.issue_type='"+selIssueType+"'");
		}
		if (selStatus!=null&&!"".equals(selStatus)) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name ",sb.toString()+" order by id desc");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			//问题、目标
			String issue_description=issueList.getList().get(i).getStr("issue_description");
			if(issue_description==null) {
				issue_description=" ";
			}
			String issue_target=issueList.getList().get(i).getStr("issue_target");
			if (issue_target==null) {
				issue_target=" ";
			}
			String description_target="问题："+issue_description+"<br>"+"目标："+issue_target;
			//知会人
			List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueId+" and status=0");
			List<Integer> tellUserId=new ArrayList<Integer>();
			String tellUserName=" ";
			for (int j = 0; j < tells.size(); j++) {
				tellUserId.add(tells.get(j).getTellUserId());
				tellUserName=tellUserName+tells.get(j).getTellUserName()+"<br>";
			}
			//OPL整体跟踪记录
			List<HProIssueTrackRecord> issueTrackList=HProIssueTrackRecord.dao.find("select * from h_pro_issue_track_record where issue_id="+issueId);
			String issueTracks=" ";
			for (int j = 0; j < issueTrackList.size(); j++) {
				issueTracks=issueTracks+(j+1)+"."+issueTrackList.get(j).getTrackRecord()+"-"+issueTrackList.get(j).getCreateUserName()+"-"+sdf1.format(issueTrackList.get(j).getCreateTime())+"<br>";
			}
			//实际完成
			Date realFinishDate=issueList.getList().get(i).getDate("real_finish_time");
			String real_finish_time="";
			if (realFinishDate==null) {
				real_finish_time="进行中";
			}else {
				real_finish_time=sdf.format(realFinishDate);
			}
			//计划完成时间修改记录
			List<HProIssuePlantimeEditRecord> editRecords=HProIssuePlantimeEditRecord.dao.find("select * from h_pro_issue_plantime_edit_record where issue_id="+issueId);
			String planTimeEditRecord=" ";
			for (int j = 0; j < editRecords.size(); j++) {
				String oldTime="空";
				String newTime="空";
				if (editRecords.get(j).getOldPlanTime()!=null) {
					oldTime=sdf.format(editRecords.get(j).getOldPlanTime());
				}
				if (editRecords.get(j).getNewPlanTime()!=null) {
					newTime=sdf.format(editRecords.get(j).getNewPlanTime());
				}
				planTimeEditRecord=planTimeEditRecord+oldTime+"->"+newTime+"-"+editRecords.get(j).getCreateUserName()+"<br>";
			}
			issueList.getList().get(i)
			.set("tell_users_name",tellUserName)
			.set("tell_users_id", tellUserId)
			.set("description_target",description_target)
			.set("issue_track_record", issueTracks)
			.set("real_finish_time", real_finish_time)
			.set("caozuo", " ")
			.set("plan_time_edit_record", planTimeEditRecord);
			//升级记录
			HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where id in (select max(id) from h_proopl_upgrade_record where status=0 and issue_id="+issueId+")");
			if (upgradeRecord==null) {
				issueList.getList().get(i).set("upgrade_reason", "未升级");
			}else {
				issueList.getList().get(i).set("upgrade_reason", upgradeRecord.getUpgradeReason());
			}
			//升级记录
			List<HProoplUpgradeRecord> upgradeRecords=HProoplUpgradeRecord.dao.find("select * from h_proopl_upgrade_record where issue_id="+issueList.getList().get(i).getLong("id"));
			String upgrades="暂无升级记录";
			if (!upgradeRecords.isEmpty()) {
				upgrades="";
				for (int j = 0; j < upgradeRecords.size(); j++) {
					String upStatus=upgradeRecords.get(j).getSpStatus()==0?"待审批":upgradeRecords.get(j).getSpStatus()==1?"审批通过":"审批驳回";
					upgrades=upgrades+upgradeRecords.get(j).getOldUserName()+" 将问题升级给 "+upgradeRecords.get(j).getNewUserName()+" ，升级原因："+upgradeRecords.get(j).getUpgradeReason()+"，处理状态："+upStatus+"<br>";
				}
			}
			issueList.getList().get(i).set("upgrade_history", upgrades);
			//原因分析
			List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
			String reason_analysis="暂无原因分析";
			String temMeasures="暂无临时措施";
			String perMeasures="暂无永久措施";
			if (!reasonAnalysis.isEmpty()) {
				reason_analysis="";
				boolean haveTemMeasure = false;
				boolean havePerTemMeasure = false;
				String temMeasure1="";
				String perMeasure1="";
				for (int j = 0; j < reasonAnalysis.size(); j++) {
					reason_analysis=reason_analysis+(j+1)+" "+reasonAnalysis.get(j).getReasonAnalysis()+"<br>";
					//临时措施状态、内容
					List<HProjectMeasures> temMeasureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" and measure_type=0 and is_del=0 order by xuhao asc");
					if (!temMeasureList.isEmpty()) {
						haveTemMeasure = true;
						for (int k = 0; k < temMeasureList.size(); k++) {
							int measureStatus=temMeasureList.get(k).getStatus();
							String measure=(j+1)+"."+(k+1)+" ";
							if (measureStatus==1) {//进行中
								measure=measure+temMeasureList.get(k).getMeasures()+"-"+temMeasureList.get(k).getUserName()+"-计划："+sdf.format(temMeasureList.get(k).getPlanFinishDate());
								if (temMeasureList.get(k).getPlanFinishDate().before(new Date())) {
								}
							}else {
								measure=measure+temMeasureList.get(k).getMeasures()+"-"+temMeasureList.get(k).getUserName()+"-"+temMeasureList.get(k).getRealFinishDate();
							}
							temMeasure1=temMeasure1+measure+"<br>";
						}
					}
					//永久
					List<HProjectMeasures> perMeasureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" and measure_type=1 and is_del=0 order by xuhao asc");
					if (!perMeasureList.isEmpty()) {
						havePerTemMeasure = true;
						for (int k = 0; k < perMeasureList.size(); k++) {
							int measureStatus=perMeasureList.get(k).getStatus();
							String measure=(j+1)+"."+(k+1)+" ";
							if (measureStatus==1) {//进行中
								measure=measure+perMeasureList.get(k).getMeasures()+"-"+perMeasureList.get(k).getUserName()+"-计划："+sdf.format(perMeasureList.get(k).getPlanFinishDate());
								if (perMeasureList.get(k).getPlanFinishDate().before(new Date())) {
								}
							}else {
								measure=measure+perMeasureList.get(k).getMeasures()+"-"+perMeasureList.get(k).getUserName()+"-"+perMeasureList.get(k).getRealFinishDate();
							}
							perMeasure1=perMeasure1+measure+"<br>";
						}
					}
				}
				if (haveTemMeasure) {
					temMeasures=temMeasure1;
				}
				if (havePerTemMeasure) {
					perMeasures=perMeasure1;
				}
			}
			issueList.getList().get(i)
			.set("reason_analysis", reason_analysis)
			.set("tem_measure", temMeasures).set("per_measure", perMeasures);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", issueList.getList());
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**获取OPL原因分析及措施详情
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年12月21日 下午5:07:21
	 */
	public void getOPLreasonMeasures() {
		long issueId=getParaToLong("id");
		List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<Record> dataList=new ArrayList<Record>();
		if (!reasonAnalysis.isEmpty()) {
			//原因分析
			for (int j = 0; j < reasonAnalysis.size(); j++) {
				//措施状态、内容
				List<HProjectMeasures> measureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" and is_del=0 order by xuhao asc");
				if (!measureList.isEmpty()) {
					for (int k = 0; k < measureList.size(); k++) {
						int measureStatus=measureList.get(k).getStatus();
						String measure=measureList.get(k).getXuhao()+" ";
						String color="";//措施单元格颜色标注
						if (measureStatus==1) {//进行中
							measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-计划："+sdf.format(measureList.get(k).getPlanFinishDate());
							if (measureList.get(k).getPlanFinishDate().before(new Date())) {
								color="yellow";
							}
						}else {
							measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-"+measureList.get(k).getRealFinishDate();
							color="green";
						}
						//措施跟踪情况 
						List<HProissueTrackHistory> measureTrackList =HProissueTrackHistory.dao.find("select * from h_proissue_track_history where measure_id="+measureList.get(k).getId());
						String measureTracks="";
						for (int l = 0; l < measureTrackList.size(); l++) {
							measureTracks=measureTracks+(l+1)+"."+measureTrackList.get(l).getRemark()+"-"+measureTrackList.get(l).getTrackUserName()+"-"+sdf1.format(measureTrackList.get(l).getCreateTime())+"<br>";
						}
						Record issueRecord= new Record();
						issueRecord.set("id", issueId)
						.set("measure_type", measureList.get(k).getMeasureType().intValue())
						.set("reason_id", reasonAnalysis.get(j).getId())
						.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
						.set("measure_id", measureList.get(k).getId())
						.set("measure_fujian_id", measureList.get(k).getFujianId())
						.set("measure_status",measureStatus)
						.set("measure", measure)
						.set("meature_track", measureTracks)
						.set("color", color);
						dataList.add(issueRecord); 
					}
				}else {
					Record issueRecord=new Record();
					issueRecord.set("id", 0)
					.set("reason_id", reasonAnalysis.get(j).getId())
					.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
					.set("measure_fujian_id", 0)
					.set("measure_id", 0)
					.set("measure_status",0)
					.set("measure", "暂无")
					.set("meature_track", "");
					dataList.add(issueRecord);
				}
			}
		}
		renderJson(Ret.ok("data",dataList));
	}
	/**
	 * 以措施为单元行展示
	 */
//	public void getMyCreateProOPL() {
//		StringBuilder sb=new StringBuilder();
//		Record loginUser=getSessionAttr("user");
//		int loginUserId=loginUser.getInt("id");
//		sb.append(" from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.create_user_id="+loginUserId+" and a.status<>'G' ");//我创建的
//		String selProductName=get("selProductName");
//		String selStatus=get("selStatus");
//		String selIssueType=get("selIssueType");
//		String selProCode=get("selProCode");
//		String selIssueLevel=get("selIssueLevel");
//		if (selIssueLevel!=null&&!"".equals(selIssueLevel)) {
//			sb.append(" and a.severity='"+selIssueLevel+"'");
//		}
//		if (!"".equals(selProCode)&& selProCode!=null) {
//			sb.append(" and a.pro_code like '%"+selProCode+"%'");
//		}
//		if (selProductName!=null&&!"".equals(selProductName)) {
//			sb.append(" and a.product_name like '%"+selProductName+"%'");
//		}
//		if (selIssueType!=null&&!"".equals(selIssueType)) {
//			sb.append(" and a.issue_type='"+selIssueType+"'");
//		}
//		if (selStatus!=null&&!"".equals(selStatus)) {
//			sb.append(" and a.status='"+selStatus+"'");
//		}
//		int page=getInt("currentPage");
//		int limit=getInt("pageSize");
//		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name ",sb.toString()+" order by id desc");
//		List<Record> proIssueList=new ArrayList<Record>();
//		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		for (int i = 0; i < issueList.getList().size(); i++) {
//			long issueId=issueList.getList().get(i).getLong("id");
//			//问题、目标
//			String issue_description=issueList.getList().get(i).getStr("issue_description");
//			if(issue_description==null) {
//				issue_description=" ";
//			}
//			String issue_target=issueList.getList().get(i).getStr("issue_target");
//			if (issue_target==null) {
//				issue_target=" ";
//			}
//			String description_target="问题："+issue_description+"<br>"+"目标："+issue_target;
//			//知会人
//			List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueId+" and status=0");
//			List<Integer> tellUserId=new ArrayList<Integer>();
//			String tellUserName=" ";
//			for (int j = 0; j < tells.size(); j++) {
//				tellUserId.add(tells.get(j).getTellUserId());
//				tellUserName=tellUserName+tells.get(j).getTellUserName()+"<br>";
//			}
//			//OPL整体跟踪记录
//			List<HProIssueTrackRecord> issueTrackList=HProIssueTrackRecord.dao.find("select * from h_pro_issue_track_record where issue_id="+issueId);
//			String issueTracks=" ";
//			for (int j = 0; j < issueTrackList.size(); j++) {
//				issueTracks=issueTracks+(j+1)+"."+issueTrackList.get(j).getTrackRecord()+"-"+issueTrackList.get(j).getCreateUserName()+"-"+sdf1.format(issueTrackList.get(j).getCreateTime())+"<br>";
//			}
//			//实际完成
//			Date realFinishDate=issueList.getList().get(i).getDate("real_finish_time");
//			String real_finish_time="";
//			if (realFinishDate==null) {
//				real_finish_time="进行中";
//			}else {
//				real_finish_time=sdf.format(realFinishDate);
//			}
//			//计划完成时间修改记录
//			List<HProIssuePlantimeEditRecord> editRecords=HProIssuePlantimeEditRecord.dao.find("select * from h_pro_issue_plantime_edit_record where issue_id="+issueId);
//			String planTimeEditRecord=" ";
//			for (int j = 0; j < editRecords.size(); j++) {
//				String oldTime="空";
//				String newTime="空";
//				if (editRecords.get(j).getOldPlanTime()!=null) {
//					oldTime=sdf.format(editRecords.get(j).getOldPlanTime());
//				}
//				if (editRecords.get(j).getNewPlanTime()!=null) {
//					newTime=sdf.format(editRecords.get(j).getNewPlanTime());
//				}
//				planTimeEditRecord=planTimeEditRecord+oldTime+"->"+newTime+"-"+editRecords.get(j).getCreateUserName()+"<br>";
//			}
//			issueList.getList().get(i)
//			.set("tell_users_name",tellUserName)
//			.set("tell_users_id", tellUserId)
//			.set("description_target",description_target)
//			.set("issue_track_record", issueTracks)
//			.set("real_finish_time", real_finish_time)
//			.set("caozuo", " ")
//			.set("plan_time_edit_record", planTimeEditRecord);
//			//升级记录
//			HProoplUpgradeRecord upgradeRecord=HProoplUpgradeRecord.dao.findFirst("select * from h_proopl_upgrade_record where status=0 and issue_id="+issueList.getList().get(i).getLong("id"));
//			if (upgradeRecord==null) {
//				issueList.getList().get(i).set("upgrade_reason", "未升级");
//			}else {
//				issueList.getList().get(i).set("upgrade_reason", upgradeRecord.getUpgradeReason());
//			}
//			List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
//			if (!reasonAnalysis.isEmpty()) {
//				//原因分析
//				for (int j = 0; j < reasonAnalysis.size(); j++) {
//					//措施状态、内容
//					List<HProjectMeasures> measureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" order by xuhao asc");
//					if (!measureList.isEmpty()) {
//						for (int k = 0; k < measureList.size(); k++) {
//							int measureStatus=measureList.get(k).getStatus();
//							String measure=measureList.get(k).getXuhao()+" ";
//							String color="";//措施单元格颜色标注
//							if (measureStatus==1) {//进行中
//								measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-计划："+sdf.format(measureList.get(k).getPlanFinishDate());
//								if (measureList.get(k).getPlanFinishDate().before(new Date())) {
//									color="yellow";
//								}
//							}else {
//								measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-"+measureList.get(k).getRealFinishDate();
//								color="green";
//							}
//							//措施跟踪情况 
//							List<HProissueTrackHistory> measureTrackList =HProissueTrackHistory.dao.find("select * from h_proissue_track_history where measure_id="+measureList.get(k).getId());
//							String measureTracks="";
//							for (int l = 0; l < measureTrackList.size(); l++) {
//								measureTracks=measureTracks+(l+1)+"."+measureTrackList.get(l).getRemark()+"-"+measureTrackList.get(l).getTrackUserName()+"-"+sdf1.format(measureTrackList.get(l).getCreateTime())+"<br>";
//							}
//							Record issueRecord= new Record();
//							issueRecord.setColumns(issueList.getList().get(i));
//							issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
//							.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
//							.set("measure_id", measureList.get(k).getId())
//							.set("measure_fujian_id", measureList.get(k).getFujianId())
//							.set("measure_status",measureStatus)
//							.set("measure", measure)
//							.set("meature_track", measureTracks)
//							.set("color", color);
//							proIssueList.add(issueRecord); 
//						}
//					}else {
//						Record issueRecord=new Record();
//						issueRecord.setColumns(issueList.getList().get(i));
//						issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
//						.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
//						.set("measure_fujian_id", 0)
//						.set("measure_id", 0)
//						.set("measure_status",0)
//						.set("measure", "暂无")
//						.set("meature_track", "");
//						proIssueList.add(issueRecord);
//					}
//				}
//			}else {
//				Record issueRecord=new Record();
//				issueRecord.setColumns(issueList.getList().get(i));
//				issueRecord.set("reason_id", 0)
//				.set("reason_analysis", "暂无原因分析")
//				.set("measure_fujian_id", 0)
//				.set("measure_id", 0)
//				.set("measure_status",0)
//				.set("measure", "暂无")
//				.set("meature_track", "");
//				proIssueList.add(issueRecord);
//			}
//		}
//		Record record=new Record();
//		record.set("code", 0);
//		record.set("msg", "获取成功");
//		record.set("list", proIssueList);
//		record.set("totalResult", issueList.getTotalRow());
//		renderJson(record);
//	}
	/**
	 * 指向新增页面
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月14日 下午4:42:22
	 */
	public void toAddProStandOPL() {
		long issueId=getParaToLong("issueid");
		set("issueid", issueId);
		render("addEditProOPL.html");
	}
	/**
	 * 获取OPL信息
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月14日 下午4:52:54
	 */
	public void getIssueInfo() {
		try {
			long issueId=getParaToLong("issueId");
			HProjectIssue issue=HProjectIssue.dao.findById(issueId);
			//对应用户钉钉id
			User upgradeUser=User.dao.findById(issue.getUpgradeUserId());
			User dutyUser=User.dao.findById(issue.getDutyUserId());
			User confimerUser=User.dao.findById(issue.getConfirmerId());
			//知会人
			List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueId+" and status=0");
			String tellUserIds="";
			String tellUserName="";
			for (int j = 0; j < tells.size(); j++) {
				User tellUser=User.dao.findById(tells.get(j).getTellUserId());
				tellUserIds=tellUserIds+tellUser.getDingUserId()+",";
				tellUserName=tellUserName+tellUser.getNickname()+",";
			}
			Record record=new Record();
			record.set("issue", issue)
			.set("upgrade_user_id",upgradeUser.getDingUserId())
			.set("duty_user_id",dutyUser.getDingUserId())
			.set("confimer_user_id",confimerUser.getDingUserId())
			.set("tell_users_name",tellUserName)
			.set("tell_users_id", tellUserIds);
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 新增
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月8日 下午5:45:10
	 */
	@SuppressWarnings({ "rawtypes"})
	public void addProIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				HProjectIssue projectIssue=new HProjectIssue();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				//相关人员获取
				String upgrade_user_id=map.get("upgrade_user_id").toString();
				User upgradeUser=User.dao.findFirst("select * from user where ding_user_id='"+upgrade_user_id+"'");
				String duty_user_id=map.get("duty_user_id").toString();
				User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+duty_user_id+"'");
				String confirmer_id=map.get("confirmer_id").toString();
				User confirmer=User.dao.findFirst("select * from user where ding_user_id='"+confirmer_id+"'");
				//措施
				JSONArray measuresList=JSONArray.parseArray(getPara("measuresList"));
				String status="R";
				if (measuresList.size()!=0) {
					status="Y";
				}
				//项目信息获取部门
				Record proInfo=Db.use("bud").findById("file_pro_info", Integer.valueOf(map.get("pro_id").toString()));
				String proCode=proInfo.getStr("pro_code");
				long proDepId=Long.valueOf(map.get("dep_id").toString());
//				String depart_name=proInfo.getStr("depart_name");
//				long proDepId=com.ray.util.HyCommenMethods.dDepId(depart_name);
				//计划完成时间
				Date newPlanTime=null;
				if (map.get("plan_finish_time")!=null) {
					try {
						newPlanTime=sdf.parse(map.get("plan_finish_time").toString());
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				
				Record loginUser=getSessionAttr("user");
				User sysUser=User.dao.findById(loginUser.getInt("id"));
				projectIssue
				.setProId(proInfo.getInt("id")).setDepId(proDepId).setProCode(proCode)
				.setLineId(Integer.valueOf(map.get("line_id").toString()))
				.setProName(map.get("pro_name").toString())
				.setProductName(map.get("product_name").toString())
				.setStatus(status)
				.set("plan_finish_time",newPlanTime)
				.setStage(map.get("stage").toString())
				.set("propose_time",map.get("propose_time").toString())
				.setSeverity(map.get("severity").toString())
				.setIssueType(map.get("issue_type").toString())
				.setIssueSource(map.get("issue_source").toString())
				.setIssueDescription(map.get("issue_description").toString())
				.setIssueTarget(map.get("issue_target").toString())
				.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
				.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
				.setConfirmerId(confirmer.getId().intValue()).setConfirmerName(confirmer.getNickname())
				.setCreateTime(new Date()).setCreateUserId(sysUser.getId()).setCreateUserName(sysUser.getNickname())
				.setIsDel(0).setIssueStatus(0);
				projectIssue.save();
				DingMessage.sendText(duty_user_id, "有新的项目标准OPL创建并需要你负责，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "3");//3-标准OPL措施责任人页面
				DingMessage.sendText(confirmer_id, "有新的项目标准OPL创建并需要你确认，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "4");//4-标准OPL措施确认人页面
				//原因分析
				JSONArray reasonAnalysisList=JSONArray.parseArray(getPara("reasonAnalysisList"));
				for (int i = 0; i < reasonAnalysisList.size(); i++) {
					JSONObject reasonObject=reasonAnalysisList.getJSONObject(i);
					HProIssueReasonAnalysis reasonAnalysis=new HProIssueReasonAnalysis();
					reasonAnalysis.setIssueId(projectIssue.getId())
					.setReasonAnalysis(reasonObject.getString("reason_analysis"))
					.setLsIndex(reasonObject.getLongValue("id"))
					.setStatus(0);
					reasonAnalysis.save();
				}
				//新增措施
				for (int i = 0; i < measuresList.size(); i++) {
					JSONObject measureObject=measuresList.getJSONObject(i);
					HProIssueReasonAnalysis reasonAnalysis=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where issue_id="+projectIssue.getId()+" and status=0 and ls_index="+measureObject.getLongValue("reason_id"));
					if (reasonAnalysis!=null) {
						String user_id=measureObject.getString("user_id");
						User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+user_id+"'");
						HProjectMeasures measure=new HProjectMeasures();
						measure.setIssueId(projectIssue.getId()).setReasonId(reasonAnalysis.getId())
						.setXuhao(measureObject.getString("xuhao")).setMeasureType(measureObject.getInteger("measure_type"))
						.setMeasures(measureObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(measureObject.getDate("plan_finish_date"))
						.setStatus(1).setIsDel(0);//措施创建之后默认状态为1---进行中
						measure.save();
						DingMessage.sendText(user_id, "有新的项目标准OPL创建并需要你执行，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "2");//2-标准OPL措施执行页面
					}
				}
				//问题知会
				String tellUserIdstring=getPara("tellUserIds");
				if (tellUserIdstring!=null) {
					String[] tellUserIds=tellUserIdstring.split(",");
					if (tellUserIds.length>0) {
						for (int i = 0; i < tellUserIds.length; i++) {
							String tellUserId=tellUserIds[i];
							User tellUser=User.dao.findFirst("select * from user where ding_user_id='"+tellUserId+"'");
							HProIssueTell issueTell=new HProIssueTell();
							issueTell.setIssueId(projectIssue.getId())
							.setTellUserId(tellUser.getId().intValue()).setTellUserName(tellUser.getNickname())
							.setStatus(0);
							issueTell.save();
							DingMessage.sendText(tellUserId, "有新的项目标准OPL创建并知会了你，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "1");//1-标准OPL知会页面
						}
					}
				}
				//增加操作历史记录
				HProissueEditRecord editRecord=new HProissueEditRecord();
				editRecord.setIssueId(projectIssue.getId()).setContext(map.toString())
				.setCreateTime(new Date()).setCreateUserId(sysUser.getId()).setCreateUserName(sysUser.getNickname());
				editRecord.save();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "新增成功！"));
			}else {
				renderJson(Ret.fail("msg", "新增失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 编辑
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月8日 下午5:45:10
	 */
	@SuppressWarnings({  "rawtypes" })
	public void editProIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				HProjectIssue projectIssue=HProjectIssue.dao.findById(getParaToLong("editId"));
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				Date oldPlanTime=projectIssue.getPlanFinishTime();
				Date newPlanTime=null;
				Record loginUser=getSessionAttr("user");
				User sysUser=User.dao.findById(loginUser.getInt("id"));
				if (map.get("plan_finish_time")!=null) {
					try {
						newPlanTime=sdf.parse(map.get("plan_finish_time").toString());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					if (oldPlanTime==null) {
						HProIssuePlantimeEditRecord plantimeEdit=new HProIssuePlantimeEditRecord();
						plantimeEdit.setIssueId(getParaToLong("editId"))
						.setOldPlanTime(oldPlanTime).setNewPlanTime(newPlanTime)
						.setCreateTime(new Date() )
						.setCreateUserId(sysUser.getId()).setCreateUserName(sysUser.getNickname());
						plantimeEdit.save();
					}else if (oldPlanTime.compareTo(newPlanTime)!=0) {//计划完成时间变化
						HProIssuePlantimeEditRecord plantimeEdit=new HProIssuePlantimeEditRecord();
						plantimeEdit.setIssueId(getParaToLong("editId"))
						.setOldPlanTime(oldPlanTime).setNewPlanTime(newPlanTime)
						.setCreateTime(new Date() )
						.setCreateUserId(sysUser.getId()).setCreateUserName(sysUser.getNickname());
						plantimeEdit.save();
					}
				}
				//相关人员获取
				String upgrade_user_id=map.get("upgrade_user_id").toString();
				User upgradeUser=User.dao.findFirst("select * from user where ding_user_id='"+upgrade_user_id+"'");
				String duty_user_id=map.get("duty_user_id").toString();
				User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+duty_user_id+"'");
				String confirmer_id=map.get("confirmer_id").toString();
				User confirmer=User.dao.findFirst("select * from user where ding_user_id='"+confirmer_id+"'");
				//措施
				JSONArray measuresList=JSONArray.parseArray(getPara("measuresList"));
				String status="R";
				if (measuresList.size()!=0) {
					status="Y";
				}
				//项目信息获取部门
				Record proInfo=Db.use("bud").findById("file_pro_info", Integer.valueOf(map.get("pro_id").toString()));
				String proCode=proInfo.getStr("pro_code");
				long proDepId=Long.valueOf(map.get("dep_id").toString());
//				String depart_name=proInfo.getStr("depart_name");
//				long proDepId=com.ray.util.HyCommenMethods.dDepId(depart_name);
				projectIssue
				.setProId(proInfo.getInt("id")).setDepId(proDepId).setProCode(proCode)
				.setLineId(Integer.valueOf(map.get("line_id").toString()))
				.setProName(map.get("pro_name").toString()).setProCode(proCode)
				.setProductName(map.get("product_name").toString())
				.setStatus(status)
				.set("plan_finish_time",newPlanTime)
				.setStage(map.get("stage").toString())
				.set("propose_time",map.get("propose_time").toString())
				.setSeverity(map.get("severity").toString())
				.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
				.setIssueType(map.get("issue_type").toString())
				.setIssueSource(map.get("issue_source").toString())
				.setIssueDescription(map.get("issue_description").toString())
				.setIssueTarget(map.get("issue_target").toString())
				.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
				.setConfirmerId(confirmer.getId().intValue()).setConfirmerName(confirmer.getNickname());
				projectIssue.update();
				DingMessage.sendText(duty_user_id, "有新的项目标准OPL已重新编辑并需要你负责，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "3");//3-标准OPL措施责任人页面
				DingMessage.sendText(confirmer_id, "有新的项目标准OPL已重新编辑并需要你确认，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "4");//4-标准OPL措施确认人页面
				//原因分析
				Db.update("update h_pro_issue_reason_analysis set status=1 where issue_id="+projectIssue.getId());
				JSONArray reasonAnalysisList=JSONArray.parseArray(getPara("reasonAnalysisList"));
				for (int i = 0; i < reasonAnalysisList.size(); i++) {
					JSONObject reasonObject=reasonAnalysisList.getJSONObject(i);
					boolean isLinshi=reasonObject.getBooleanValue("isLinshi");
					if (isLinshi) {//临时数据，则新增
						HProIssueReasonAnalysis reasonAnalysis=new HProIssueReasonAnalysis();
						reasonAnalysis.setIssueId(projectIssue.getId())
						.setReasonAnalysis(reasonObject.getString("reason_analysis"))
						.setLsIndex(reasonObject.getLongValue("id"))
						.setStatus(0);
						reasonAnalysis.save();
					}else {
						HProIssueReasonAnalysis reasonAnalysis=HProIssueReasonAnalysis.dao.findById(reasonObject.getLongValue("id"));
						reasonAnalysis
						.setReasonAnalysis(reasonObject.getString("reason_analysis"))
						.setLsIndex(reasonObject.getLongValue("id"))
						.setStatus(0);
						reasonAnalysis.update();
					}
				}
				//措施
				Db.update("update h_project_measures set is_del=1 where issue_id="+projectIssue.getId());
				for (int i = 0; i < measuresList.size(); i++) {
					JSONObject measureObject=measuresList.getJSONObject(i);
					HProIssueReasonAnalysis reasonAnalysis=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where issue_id="+projectIssue.getId()+" and status=0 and ls_index="+measureObject.getLongValue("reason_id"));
					String user_id=measureObject.getString("user_id");
					User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+user_id+"'");
					if (reasonAnalysis!=null) {
						HProjectMeasures measure=HProjectMeasures.dao.findById(measureObject.getLong("id"));
						if (measure==null) {
							measure=new HProjectMeasures();
							measure
							.setXuhao(measureObject.getString("xuhao")).setMeasureType(measureObject.getInteger("measure_type"))
							.setIssueId(projectIssue.getId()).setReasonId(reasonAnalysis.getId())
							.setMeasures(measureObject.getString("measures"))
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(measureObject.getDate("plan_finish_date"))
							.setStatus(1).setIsDel(0);//措施创建之后默认状态为1---进行中
							measure.save();
							DingMessage.sendText(user_id, "有新的项目标准OPL创建并需要你执行，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "2");//2-标准OPL措施执行页面
						}else {
							measure
							.setXuhao(measureObject.getString("xuhao")).setMeasureType(measureObject.getInteger("measure_type"))
							.setMeasures(measureObject.getString("measures")).setReasonId(reasonAnalysis.getId())
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(measureObject.getDate("plan_finish_date"))
							.setIsDel(0);
							measure.update();
						}
					}
				}
				//问题知会
				Db.update("update h_pro_issue_tell set status=1 where issue_id="+projectIssue.getId());
				String tellUserIdstring=getPara("tellUserIds");
				if (tellUserIdstring!=null) {
					String[] tellUserIds=tellUserIdstring.split(",");
					if (tellUserIds.length>0) {
						for (int i = 0; i < tellUserIds.length; i++) {
							String tellUserId=tellUserIds[i];
							User tellUser=User.dao.findFirst("select * from user where ding_user_id='"+tellUserId+"'");
							HProIssueTell issueTell=HProIssueTell.dao.findFirst("select * from h_pro_issue_tell where issue_id="+projectIssue.getId()+" and tell_user_id="+tellUser.getId());
							if (issueTell==null) {
								issueTell=new HProIssueTell();
								issueTell.setIssueId(projectIssue.getId())
								.setTellUserId(tellUser.getId().intValue()).setTellUserName(tellUser.getNickname())
								.setStatus(0);
								issueTell.save();
								DingMessage.sendText(tellUserId, "有新的项目标准OPL已重新编辑并知会了你，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "1");//1-标准OPL知会页面
							}else {
								issueTell.setStatus(0);
								issueTell.update(); 
							}
						}
					}
				}
				//增加操作历史记录
				HProissueEditRecord editRecord=new HProissueEditRecord();
				editRecord.setIssueId(projectIssue.getId()).setContext(map.toString())
				.setCreateTime(new Date()).setCreateUserId(sysUser.getId()).setCreateUserName(sysUser.getNickname());
				editRecord.save();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "编辑成功！"));
			}else {
				renderJson(Ret.fail("msg", "编辑失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 删除问题
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月9日 下午3:57:02
	 */
	public void delIssue() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("id");
				HProjectIssue issue=HProjectIssue.dao.findById(issueId);
				issue.setIsDel(1);
				issue.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "删除成功！"));
			}else {
				renderJson(Ret.fail("msg", "删除失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 导出
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月30日 下午4:57:30
	 */
	@SuppressWarnings("resource")
	public void exportProStand() {
		Record req=new Record();
		Db.tx(() -> {
			try {
				HSSFWorkbook workbook = new HSSFWorkbook();
		        HSSFCellStyle style = workbook.createCellStyle();
		        style.setAlignment(HorizontalAlignment.CENTER);
		        style.setVerticalAlignment(VerticalAlignment.CENTER);
		        HSSFSheet sheet = workbook.createSheet("sheet");
		        
		        HSSFRow title = sheet.createRow(0);
		        HSSFCell cell_00 = title.createCell(0);
		        cell_00.setCellStyle(style);
		        cell_00.setCellValue("事业部");
		        HSSFCell cell_01 = title.createCell(1);
		        cell_01.setCellStyle(style);
		        cell_01.setCellValue("项目号");
		        HSSFCell cell_02 = title.createCell(2);
		        cell_02.setCellStyle(style);
		        cell_02.setCellValue("产品名称");
		        HSSFCell cell_03 = title.createCell(3);
		        cell_03.setCellStyle(style);
		        cell_03.setCellValue("状态");
		        HSSFCell cell_04 = title.createCell(4);
		        cell_04.setCellStyle(style);
		        cell_04.setCellValue("计划完成");
		        HSSFCell cell_05 = title.createCell(5);
		        cell_05.setCellStyle(style);
		        cell_05.setCellValue("实际完成");
		        HSSFCell cell_06 = title.createCell(6);
		        cell_06.setCellStyle(style);
		        cell_06.setCellValue("阶段");
		        HSSFCell cell_07 = title.createCell(7);
		        cell_07.setCellStyle(style);
		        cell_07.setCellValue("提出时间");
		        HSSFCell cell_08 = title.createCell(8);
		        cell_08.setCellStyle(style);
		        cell_08.setCellValue("严重度");
		        HSSFCell cell_09 = title.createCell(9);
		        cell_09.setCellStyle(style);
		        cell_09.setCellValue("升级对象");
		        HSSFCell cell_010 = title.createCell(10);
		        cell_010.setCellStyle(style);
		        cell_010.setCellValue("问题类别");
		        HSSFCell cell_011 = title.createCell(11);
		        cell_011.setCellStyle(style);
		        cell_011.setCellValue("问题来源");
		        HSSFCell cell_012 = title.createCell(12);
		        cell_012.setCellStyle(style);
		        cell_012.setCellValue("问题/目标");
		        HSSFCell cell_013 = title.createCell(13);
		        cell_013.setCellStyle(style);
		        cell_013.setCellValue("原因分析");
		        HSSFCell cell_014 = title.createCell(14);
		        cell_014.setCellStyle(style);
		        cell_014.setCellValue("临时解决措施");
		        HSSFCell cell_015 = title.createCell(15);
		        cell_015.setCellStyle(style);
		        cell_015.setCellValue("永久解决措施");
		        HSSFCell cell_016 = title.createCell(16);
		        cell_016.setCellStyle(style);
		        cell_016.setCellValue("创建人");
		        HSSFCell cell_017 = title.createCell(17);
		        cell_017.setCellStyle(style);
		        cell_017.setCellValue("责任人");
		        HSSFCell cell_018 = title.createCell(18);
		        cell_018.setCellStyle(style);
		        cell_018.setCellValue("确认人");
		        HSSFCell cell_019 = title.createCell(19);
		        cell_019.setCellStyle(style);
		        cell_019.setCellValue("知会人");
		        HSSFCell cell_020 = title.createCell(20);
		        cell_020.setCellStyle(style);
		        cell_020.setCellValue("跟踪确认记录");
				StringBuilder sb=new StringBuilder();
				Record loginUser=getSessionAttr("user");
				int loginUserId=loginUser.getInt("id");
				int downLoadType=getParaToInt("downLoadType");
				if (downLoadType==0) {//我创建
					sb.append("select a.*,b.dep_name  from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.create_user_id="+loginUserId);//我创建的
				}else if (downLoadType==1) {//我负责
					sb.append("select a.*,b.dep_name  from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.duty_user_id="+loginUserId);//我负责的
				}else if (downLoadType==2) {//我确认
					sb.append("select a.*,b.dep_name  from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.confirmer_id="+loginUserId);//我确认的
				}else if (downLoadType==3) {//全部
					User user=User.dao.findById(loginUser.getInt("id"));
					//判断是否超管、体系等
					List<Record> userRole=Db.use("bud").find("select b.role_name from user_role a, roles b where a.user_id='"+user.getDingUserId()+"' and a.role_id=b.id");
					int user_role=0;
					if (userRole.size()>0) {
						for (int i = 0; i < userRole.size(); i++) {
							String role_name=userRole.get(i).getStr("role_name");
							if ("公司经营层".equals(role_name)|| "项目体系".equals(role_name)||"财务".equals(role_name)||"管理员".equals(role_name)) {
								user_role=1;
							}
						}
					}
					sb.append("select a.*,b.dep_name  from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id ");
					if (user_role==0) {
						sb.append(" and (a.dep_id in (select first_dep_id from h_user_dep where user_id="+loginUserId+" and status=0  union "
								+ "select dep_id from h_project_issue where create_user_id="+loginUserId+" or duty_user_id="+loginUserId+" or confirmer_id="+loginUserId+") ) ");
					}
					String selCreateUser=get("selCreateUser");
					if (!"".equals(selCreateUser)&& selCreateUser!=null) {
						sb.append(" and a.create_user_name like '%"+selCreateUser+"%'");
					}
					String selDutyUser=get("selDutyUser");
					if (!"".equals(selDutyUser)&& selDutyUser!=null) {
						sb.append(" and a.duty_user_name like '%"+selDutyUser+"%'");
					}
					String selSureUser=get("selSureUser");
					if (!"".equals(selSureUser)&& selSureUser!=null) {
						sb.append(" and a.confirmer_name like '%"+selSureUser+"%'");
					}
					String dept_id=get("dept_id");
					boolean present = Optional.ofNullable(dept_id).isPresent();
					if (present) {
						sb.append(" and a.dep_id='"+dept_id+"'");
					}
					String selStage=get("selStage");
					if (selStage!=null&&!"".equals(selStage)) {
						sb.append(" and a.stage='"+selStage+"'");
					}
				}
				String selProductName=get("selProductName");
				String selStatus=get("selStatus");
				String selIssueType=get("selIssueType");
				String selProCode=get("selProCode");
				String selIssueLevel=get("selIssueLevel");
				if (selIssueLevel!=null&&!"".equals(selIssueLevel)) {
					sb.append(" and a.severity='"+selIssueLevel+"'");
				}
				if (!"".equals(selProCode)&& selProCode!=null) {
					sb.append(" and a.pro_code like '%"+selProCode+"%'");
				}
				if (selProductName!=null&&!"".equals(selProductName)) {
					sb.append(" and a.product_name like '%"+selProductName+"%'");
				}
				if (selIssueType!=null&&!"".equals(selIssueType)) {
					sb.append(" and a.issue_type='"+selIssueType+"'");
				}
				if (selStatus!=null&&!"".equals(selStatus)) {
					sb.append(" and a.status='"+selStatus+"'");
				}
				sb.append(" order by id desc");
				List<Record> issueList=Db.find(sb.toString());
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				List<Record> exportList=new ArrayList<Record>();
				for (int i = 0; i < issueList.size(); i++) {
					long issueId=issueList.get(i).getLong("id");
					//问题、目标
					String issue_description=issueList.get(i).getStr("issue_description");
					if(issue_description==null) {
						issue_description=" ";
					}
					String issue_target=issueList.get(i).getStr("issue_target");
					if (issue_target==null) {
						issue_target=" ";
					}
					String description_target="问题："+issue_description+"\n"+"目标："+issue_target;
					//知会人
					List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueId+" and status=0");
					List<Integer> tellUserId=new ArrayList<Integer>();
					String tellUserName=" ";
					for (int j = 0; j < tells.size(); j++) {
						tellUserId.add(tells.get(j).getTellUserId());
						tellUserName=tellUserName+tells.get(j).getTellUserName()+"\n";
					}
					//OPL整体跟踪记录
					List<HProIssueTrackRecord> issueTrackList=HProIssueTrackRecord.dao.find("select * from h_pro_issue_track_record where issue_id="+issueId);
					String issueTracks=" ";
					for (int j = 0; j < issueTrackList.size(); j++) {
						issueTracks=issueTracks+(j+1)+"."+issueTrackList.get(j).getTrackRecord()+"-"+issueTrackList.get(j).getCreateUserName()+"-"+sdf1.format(issueTrackList.get(j).getCreateTime())+"\n";
					}
					//实际完成
					Date realFinishDate=issueList.get(i).getDate("real_finish_time");
					String real_finish_time="";
					if (realFinishDate==null) {
						real_finish_time="进行中";
					}else {
						real_finish_time=sdf.format(realFinishDate);
					}
					//计划完成时间修改记录
					List<HProIssuePlantimeEditRecord> editRecords=HProIssuePlantimeEditRecord.dao.find("select * from h_pro_issue_plantime_edit_record where issue_id="+issueId);
					String planTimeEditRecord=" ";
					for (int j = 0; j < editRecords.size(); j++) {
						planTimeEditRecord=planTimeEditRecord+editRecords.get(j).getOldPlanTime()+"->"+editRecords.get(j).getNewPlanTime()+"-"+editRecords.get(j).getCreateUserName()+"\n";
					}
					issueList.get(i)
					.set("tell_users_name",tellUserName)
					.set("tell_users_id", tellUserId)
					.set("description_target",description_target)
					.set("issue_track_record", issueTracks)
					.set("real_finish_time", real_finish_time)
					.set("caozuo", " ")
					.set("plan_time_edit_record", planTimeEditRecord);
					List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
					//原因分析
					if (!reasonAnalysis.isEmpty()) {
						for (int j = 0; j < reasonAnalysis.size(); j++) {
							//措施状态、内容
							List<HProjectMeasures> measureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId());
							if (!measureList.isEmpty()) {
								for (int k = 0; k < measureList.size(); k++) {
									int measureStatus=measureList.get(k).getStatus();
									String measure=measureList.get(k).getXuhao()+"：";
									if (measureStatus==1) {//进行中
										measure=measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-计划："+sdf.format(measureList.get(k).getPlanFinishDate());
									}else {
										measure=measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-"+measureList.get(k).getRealFinishDate();
										
									}
									//措施跟踪情况 
									List<HProissueTrackHistory> measureTrackList =HProissueTrackHistory.dao.find("select * from h_proissue_track_history where measure_id="+measureList.get(k).getId());
									String measureTracks="";
									for (int l = 0; l < measureTrackList.size(); l++) {
										measureTracks=measureTracks+(l+1)+"."+measureTrackList.get(l).getRemark()+"-"+measureTrackList.get(l).getTrackUserName()+"-"+sdf1.format(measureTrackList.get(l).getCreateTime())+"\n";
									}
									Record issueRecord= new Record();
									issueRecord.setColumns(issueList.get(i));
									issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
									.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
									.set("measure_id", measureList.get(k).getId())
									.set("measure_status",measureStatus)
									.set("meature_track", measureTracks);
									if (measureList.get(k).getMeasureType().intValue()==0) {
										issueRecord.set("ls_measure", measure);
									}else {
										issueRecord.set("yj_measure", measure);
									}
									exportList.add(issueRecord);
								}
							}else {
								Record issueRecord= new Record();
								issueRecord.setColumns(issueList.get(i));							
								issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
								.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
								.set("measure_id", 0)
								.set("measure_status",0)
								.set("ls_measure", "暂无临时措施！")
								.set("yj_measure", "暂无永久措施！")
								.set("meature_track", "");
								exportList.add(issueRecord);
							}
						}
					}else {
						Record issueRecord= new Record();
						issueRecord.setColumns(issueList.get(i));							
						issueRecord.set("reason_id", i)
						.set("reason_analysis", "暂无原因分析！")
						.set("measure_id", 0)
						.set("measure_status",0)
						.set("measure", "暂无措施！")
						.set("meature_track", "");
						exportList.add(issueRecord);
					}
				}		
				int sameOPLNum=0;
				boolean isMax=false;
				for (int j = 0; j < exportList.size(); j++) {
					long oplIdj=exportList.get(j).getLong("id");
					for (int k = j; k < exportList.size(); k++) {
						long oplIdk=exportList.get(k).getLong("id");
						if (oplIdj==oplIdk) {//同OPL继续获取原因等信息
							long reasonIdk=exportList.get(k).getLong("reason_id");
							int sameReasonNum=0;
							for (int l = k; l < exportList.size(); l++) {
								long reasonIdl=exportList.get(l).getLong("reason_id");
								if (reasonIdk==reasonIdl) {//同原因
									sameReasonNum=sameReasonNum+1;
									sameOPLNum=sameOPLNum+1;
									HSSFRow rowIndex = sheet.createRow(l+1);
							        HSSFCell cell_10 = rowIndex.createCell(0);
							        cell_10.setCellStyle(style);
							        cell_10.setCellValue(exportList.get(l).getStr("dep_name"));
							        HSSFCell cell_11 = rowIndex.createCell(1);
							        cell_11.setCellStyle(style);
							        cell_11.setCellValue(exportList.get(l).getStr("pro_code"));
							        HSSFCell cell_12 = rowIndex.createCell(2);
							        cell_12.setCellStyle(style);
							        cell_12.setCellValue(exportList.get(l).getStr("product_name"));
							        HSSFCell cell_13 = rowIndex.createCell(3);
							        cell_13.setCellStyle(style);
							        cell_13.setCellValue(exportList.get(l).getStr("status"));
							        HSSFCell cell_14 = rowIndex.createCell(4);
							        cell_14.setCellStyle(style);
							        cell_14.setCellValue(exportList.get(l).getStr("plan_finish_time"));
							        HSSFCell cell_15 = rowIndex.createCell(5);
							        cell_15.setCellStyle(style);
							        cell_15.setCellValue(exportList.get(l).getStr("real_finish_time"));
							        HSSFCell cell_16 = rowIndex.createCell(6);
							        cell_16.setCellStyle(style);
							        cell_16.setCellValue(exportList.get(l).getStr("stage"));
							        HSSFCell cell_17 = rowIndex.createCell(7);
							        cell_17.setCellStyle(style);
							        cell_17.setCellValue(exportList.get(l).getStr("propose_time"));
							        HSSFCell cell_18 = rowIndex.createCell(8);
							        cell_18.setCellStyle(style);
							        cell_18.setCellValue(exportList.get(l).getStr("severity"));
							        HSSFCell cell_19 = rowIndex.createCell(9);
							        cell_19.setCellStyle(style);
							        cell_19.setCellValue(exportList.get(l).getStr("upgrade_user_name"));
							        HSSFCell cell_110 = rowIndex.createCell(10);
							        cell_110.setCellStyle(style);
							        cell_110.setCellValue(exportList.get(l).getStr("issue_type"));
							        HSSFCell cell_111 = rowIndex.createCell(11);
							        cell_111.setCellStyle(style);
							        cell_111.setCellValue(exportList.get(l).getStr("issue_source"));
							        HSSFCell cell_112 = rowIndex.createCell(12);
							        cell_112.setCellStyle(style);
							        cell_112.setCellValue(exportList.get(l).getStr("description_target"));
							        HSSFCell cell_113 = rowIndex.createCell(13);
							        cell_113.setCellStyle(style);
							        cell_113.setCellValue(exportList.get(l).getStr("reason_analysis"));
							        HSSFCell cell_114 = rowIndex.createCell(14);
							        cell_114.setCellStyle(style);
							        cell_114.setCellValue(exportList.get(l).getStr("ls_measure"));
							        HSSFCell cell_115 = rowIndex.createCell(15);
							        cell_115.setCellStyle(style);
							        cell_115.setCellValue(exportList.get(l).getStr("yj_measure"));
							        HSSFCell cell_116 = rowIndex.createCell(16);
							        cell_116.setCellStyle(style);
							        cell_116.setCellValue(exportList.get(l).getStr("create_user_name"));
							        HSSFCell cell_117 = rowIndex.createCell(17);
							        cell_117.setCellStyle(style);
							        cell_117.setCellValue(exportList.get(l).getStr("duty_user_name"));
							        HSSFCell cell_118 = rowIndex.createCell(18);
							        cell_118.setCellStyle(style);
							        cell_118.setCellValue(exportList.get(l).getStr("confirmer_name"));
							        HSSFCell cell_119 = rowIndex.createCell(19);
							        cell_119.setCellStyle(style);
							        cell_119.setCellValue(exportList.get(l).getStr("tell_users_name"));
							        HSSFCell cell_120 = rowIndex.createCell(20);
							        cell_120.setCellStyle(style);
							        cell_120.setCellValue(exportList.get(l).getStr("issue_track_record"));
							        if (l==exportList.size()-1) {
										isMax=true;
										if (sameReasonNum>1) {
											CellRangeAddress region = new CellRangeAddress(k+1, sameReasonNum+k, 13, 13);
									        sheet.addMergedRegion(region);
										}
									}
								}else {
									if (sameReasonNum>1) {
										CellRangeAddress region = new CellRangeAddress(k+1, sameReasonNum+k, 13, 13);
								        sheet.addMergedRegion(region);
									}
									k=l-1;
									sameReasonNum=0;
									break;
								}
							}
						}else {
							if (sameOPLNum>1) {
								for (int i = 0; i <= 12; i++) {
					        		CellRangeAddress region = new CellRangeAddress(j+1, sameOPLNum+j, i, i);
							        sheet.addMergedRegion(region);
								}
								for (int i = 15; i <= 18; i++) {
					        		CellRangeAddress region = new CellRangeAddress(j+1, sameOPLNum+j, i, i);
							        sheet.addMergedRegion(region);
								}
							}
							j=k-1;
							sameOPLNum=0;
							break;
						}
						if (isMax) {
							if (sameOPLNum>1) {
								for (int i = 0; i <= 12; i++) {
					        		CellRangeAddress region = new CellRangeAddress(j+1, sameOPLNum+j, i, i);
							        sheet.addMergedRegion(region);
								}
								for (int i = 15; i <= 18; i++) {
					        		CellRangeAddress region = new CellRangeAddress(j+1, sameOPLNum+j, i, i);
							        sheet.addMergedRegion(region);
								}
							}
							break;
						}
					}
					if (isMax) {
						break;
					}
				}
				File file = new File("D:\\SysFujianFiles\\openIssue\\upload\\moban\\项目标准OPL.xls");
		        FileOutputStream fout = new FileOutputStream(file);
		        workbook.write(fout);
		        fout.close();
				req.set("code", 0);
				req.set("msg", "成功").set("data",exportList);
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				req.set("code", 1);
				req.set("msg", "失败！");
				return false;
			}
		});
		renderJson(req);
	}
	/**
	 * 获取OPL原因分析
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月14日 下午4:19:08
	 */
	public void getProStandReasonAnalysis() {
		try {
			long issueId=getParaToLong("issueId");
			List<Record> reasonAnalysis=Db.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
			for (int i = 0; i < reasonAnalysis.size(); i++) {
				reasonAnalysis.get(i).set("isDelete", false)
				.set("isLinshi", false);
			}
			renderJson(Ret.ok("data", reasonAnalysis));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取OPL措施列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月14日 下午4:19:08
	 */
	public void getProStandMeasures() {
		try {
			long issueId=getParaToLong("issueId");
			List<Record> measureList=Db.find("select a.*,b.reason_analysis from h_project_measures a,h_pro_issue_reason_analysis b where a.is_del=0 and a.issue_id="+issueId+" and b.id=a.reason_id order by a.xuhao asc");
			for (int i = 0; i < measureList.size(); i++) {
				User measureUser=User.dao.findById(measureList.get(i).getInt("user_id"));
				measureList.get(i).set("user_id", measureUser.getDingUserId());
				measureList.get(i).set("isDelete", false);
			}
			renderJson(Ret.ok("data", measureList));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**opl导入
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月2日 上午10:20:45
	 */
	public void proOPLImport() {
		Record DUser = (Record)getSessionAttr("user");
		Db.tx(()->{
			try {
				UploadFile file = getFile("file", "temp");
				InputStream is = new FileInputStream(file.getFile());
				List<com.ray.ExcelModel.StandardOPLModel> list = DefaultExcelReader
						.of(com.ray.ExcelModel.StandardOPLModel.class).sheet(0).rowFilter(row -> row.getRowNum() > 0)
						.read(is);
				//内容格式校验
				for (int i = 0; i < list.size(); i++) {
					//项目号校验
					String pro_code=list.get(i).getPro_code();
					if (pro_code==null||"".equals(pro_code)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行项目号为空，请核对!"));
						return false;
					}else {
						Record proInfo=Db.use("bud").findFirst("select * from file_pro_info where pro_code='"+pro_code+"'");
						if (proInfo==null) {
							renderJson(Ret.fail("msg","第"+(i+1)+"行项目号不存在，请核对!"));
							return false;
						}
					}
					//项目名称校验
					String pro_name=list.get(i).getPro_name(); 
					if (pro_name==null||"".equals(pro_name)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行项目名称为空，请核对!"));
						return false;
					}
					//阶段
					String stage=list.get(i).getStage();
					if (stage==null||"".equals(stage)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行阶段为空，请核对!"));
						return false;
					}
					//提出时间
					String propose_time=list.get(i).getPropose_time();
					if (propose_time==null||"".equals(propose_time)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行提出时间为空，请核对!"));
						return false;
					}else {
						String[] propose_time1=new String[0];
						propose_time1=propose_time.split("/");
						if (propose_time1.length!=3) {
							renderJson(Ret.fail("msg","第"+(i+1)+"行提出时间格式有误，请核对格式 YYYY-MM-DD "));
							return false;
						}
					}
					//严重度
					String severity=list.get(i).getSeverity();
					if (severity==null||"".equals(severity)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行严重度为空，请核对!"));
						return false;
					}
					//延期升级对象
					String upgrade_user_jobnum=list.get(i).getUpgrade_user_jobnum();
					if (upgrade_user_jobnum==null||"".equals(upgrade_user_jobnum)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行延期升级对象 员工工号为空，请核对!"));
						return false;
					}else {
						User user=User.dao.findFirst("select * from user where job_num='"+upgrade_user_jobnum+"'");
						if (user==null) {
							renderJson(Ret.fail("msg","第"+(i+1)+"行延期升级对象 员工工号不存在，请核对!"));
							return false;
						}
					}
					//问题分类
					String issue_type=list.get(i).getIssue_type();
					if (issue_type==null||"".equals(issue_type)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行问题分类为空，请核对!"));
						return false;
					}
					//问题来源
					String issue_source=list.get(i).getIssue_source();
					if (issue_source==null||"".equals(issue_source)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行问题来源为空，请核对!"));
						return false;
					}
					//问题描述
					String description_target=list.get(i).getDescription_target();
					if (description_target==null||"".equals(description_target)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行问题描述为空，请核对!"));
						return false;
					}
					//责任人
					String duty_user_jobnum=list.get(i).getDuty_user_jobnum();
					if (duty_user_jobnum==null||"".equals(duty_user_jobnum)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行责任人 员工工号为空，请核对!"));
						return false;
					}else {
						User user=User.dao.findFirst("select * from user where job_num='"+duty_user_jobnum+"'");
						if (user==null) {
							renderJson(Ret.fail("msg","第"+(i+1)+"行责任人 员工工号不存在，请核对!"));
							return false;
						}
					}
					//确认人
					String confirmer_jobnum=list.get(i).getConfirmer_jobnum();
					if (confirmer_jobnum==null||"".equals(confirmer_jobnum)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行确认人 员工工号为空，请核对!"));
						return false;
					}else {
						User user=User.dao.findFirst("select * from user where job_num='"+confirmer_jobnum+"'");
						if (user==null) {
							renderJson(Ret.fail("msg","第"+(i+1)+"行确认人 员工工号不存在，请核对!"));
							return false;
						}
					}
					//计划完成时间
					String plan_finish_time=list.get(i).getPlan_finish_time();
					if (plan_finish_time!=null&&!"".equals(plan_finish_time)) {
						String[] plan_finish_time1=new String[0];
						plan_finish_time1=propose_time.split("/");
						if (plan_finish_time1.length!=3) {
							renderJson(Ret.fail("msg","第"+(i+1)+"行计划完成时间格式有误，请核对格式 YYYY-MM-DD !"));
							return false;
						}
					}
					//事业部
					String dep_name=list.get(i).getDep_name();
					long proDepId=0;
					if (dep_name==null||"".equals(dep_name)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行事业部为空，请核对!"));
						return false;
					}else {
						proDepId=com.ray.util.HyCommenMethods.dDepId(dep_name);
						if (proDepId==0) {
							renderJson(Ret.fail("msg","第"+(i+1)+"行事业部不存在，请核对!"));
							return false;
						}
					}
					//产线
					String line_name=list.get(i).getLine_name();
					if (line_name==null||"".equals(line_name)) {
						renderJson(Ret.fail("msg","第"+(i+1)+"行产线为空，请核对!"));
						return false;
					}else {
						HDepLine line=HDepLine.dao.findFirst("select * from h_dep_line where line_name='"+line_name+"' and dep_id="+proDepId);
						if (line==null) {
							renderJson(Ret.fail("msg","第"+(i+1)+"行产线与项目号关联的事业部不匹配，请核对!"));
							return false;
						}
					}
					//最后一行判断
					if (("".equals(pro_code)||pro_code==null)&&("".equals(pro_name)||pro_name==null)&&("".equals(stage)||stage==null)) {
						break;
					}
				}
				//校验完成
				for (int i = 0; i < list.size(); i++) {
					String pro_code=list.get(i).getPro_code();
					Record proInfo=Db.use("bud").findFirst("select * from file_pro_info where pro_code='"+pro_code+"'");
					String depart_name=list.get(i).getDep_name();
					long proDepId=com.ray.util.HyCommenMethods.dDepId(depart_name);
					//升级对象
					String upgrade_user_jobnum=list.get(i).getUpgrade_user_jobnum();
					User upgradeUser=User.dao.findFirst("select * from user where job_num='"+upgrade_user_jobnum+"'");
					//责任人
					String duty_user_jobnum=list.get(i).getDuty_user_jobnum();
					User dutyUser=User.dao.findFirst("select * from user where job_num='"+duty_user_jobnum+"'");
					//确认人
					String confirmer_jobnum=list.get(i).getConfirmer_jobnum();
					User confirmerUser=User.dao.findFirst("select * from user where job_num='"+confirmer_jobnum+"'");
					//时间格式转换
					String proposeTime=list.get(i).getPropose_time().replaceAll("/", "-");
					Date planDate=null;
					if(list.get(i).getPlan_finish_time()!=null&&!"".equals(list.get(i).getPlan_finish_time())) {
						String planFinishTime=list.get(i).getPlan_finish_time().replaceAll("/", "-");
						planDate=new SimpleDateFormat("yyyy-MM-dd").parse(planFinishTime);
					}
					//产线
					String line_name=list.get(i).getLine_name();
					HDepLine line=HDepLine.dao.findFirst("select * from h_dep_line where line_name='"+line_name+"' and dep_id="+proDepId);
					HProjectIssue projectIssue=new HProjectIssue();					
					projectIssue.setProId(proInfo.getInt("id")).setProCode(pro_code).setDepId(proDepId)
					.setLineId(line.getId().intValue())
					.setProductName(proInfo.getStr("category_name"))
					.setStatus("R").setProName(list.get(i).getPro_name())
					.setStage(list.get(i).getStage()).setSeverity(list.get(i).getSeverity())
					.setProposeTime(new SimpleDateFormat("yyyy-MM-dd").parse(proposeTime))
					.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
					.setIssueType(list.get(i).getIssue_type())
					.setIssueSource(list.get(i).getIssue_source())
					.setIssueDescription(list.get(i).getDescription_target())
					.setIssueTarget(list.get(i).getIssue_target())
					.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
					.setConfirmerId(confirmerUser.getId().intValue()).setConfirmerName(confirmerUser.getNickname())
					.setCreateTime(new Date())
					.setCreateUserId(DUser.getInt("id")).setCreateUserName(DUser.get("nickname"))
					.setIsDel(0).setIssueStatus(0)
					.setPlanFinishTime(planDate);
					projectIssue.save();
				}
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg","导入失败，请联系管理员！"));
				return false;
			}
			renderJson(Ret.ok("msg","导入成功！"));
			return true;
		});
	}
	/**模板下载
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年9月22日 上午10:24:19
	 */
	public void downLoadExcelModel() {
		renderFile("/moban/项目标准OPL导入模板.xlsx");
	}
}
