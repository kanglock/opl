package com.ray.controller.admin.StandardProOPL;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.HProissueTrackHistory;
import com.ray.common.model.HProjectIssue;
import com.ray.common.model.HProjectMeasures;
import com.ray.common.model.User;
/**
 * 措施执行情况 proOPLMeasure/toMyMeasureProOPL
 * @author FL00024996
 *
 */
public class MeasureDoneController extends Controller {
	public void toMyMeasureProOPL() {
		render("myMeasureProOPL.html");
	}
	public void getMyMeasureProOPL() {
		StringBuilder sb=new StringBuilder();
		Record loginUser=getSessionAttr("user");
		int loginUserId=loginUser.getInt("id");
		String selStatus=get("selStatus");
		sb.append(" from h_project_issue a,h_project_measures b,h_cause_dep c,h_pro_issue_reason_analysis d where  "
				+ "a.is_del=0 and a.dep_id=c.dep_id  and a.status<>'G' and  a.id=b.issue_id and d.issue_id=a.id and b.reason_id=d.id and b.is_del=0 "
				+ "and b.user_id="+loginUserId);
		String selProductName=get("selProductName");
		String selIssueType=get("selIssueType");
		String selProCode=get("selProCode");
		if (!"".equals(selProCode)&& selProCode!=null) {
			sb.append(" and a.pro_code like '%"+selProCode+"%'");
		}
		if (selProductName!=null&&!"".equals(selProductName)) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		if (selIssueType!=null&&!"".equals(selIssueType)) {
			sb.append(" and a.issue_type='"+selIssueType+"'");
		}
		if (selStatus!=null&&!"".equals(selStatus)) {
			sb.append(" and b.status="+Integer.valueOf(selStatus));
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.measure_type,c.dep_name,d.reason_analysis,b.id as measure_id ",sb.toString()+" order by id desc");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < issueList.getList().size(); i++) {
			//问题、目标
			String issue_description=issueList.getList().get(i).getStr("issue_description");
			if(issue_description==null) {
				issue_description="";
			}
			String issue_target=issueList.getList().get(i).getStr("issue_target");
			if (issue_target==null) {
				issue_target="";
			}
			String description_target="问题："+issue_description+"<br>"+"目标："+issue_target;
			issueList.getList().get(i).set("description_target",description_target);
			//措施
			long measureId=issueList.getList().get(i).getLong("measure_id");
			HProjectMeasures measureDetail=HProjectMeasures.dao.findById(measureId);
			int measureStatus=measureDetail.getStatus();
			String measure=measureDetail.getXuhao()+" ";
			String color="";//措施单元格颜色标注
			if (measureStatus==1) {//进行中
				measure=measure+measureDetail.getMeasures()+"-"+measureDetail.getUserName()+"-计划："+sdf.format(measureDetail.getPlanFinishDate());
				if (measureDetail.getPlanFinishDate().before(new Date())) {
					color="yellow";
				}
			}else {
				measure=measure+measureDetail.getMeasures()+"-"+measureDetail.getUserName()+"-"+sdf.format(measureDetail.getRealFinishDate());
				color="green";
			}
			//措施跟踪情况 
			List<HProissueTrackHistory> measureTrackList =HProissueTrackHistory.dao.find("select * from h_proissue_track_history where measure_id="+measureId);
			String measureTracks="";
			for (int l = 0; l < measureTrackList.size(); l++) {
				measureTracks=measureTracks+(l+1)+"."+measureTrackList.get(l).getRemark()+"-"+measureTrackList.get(l).getTrackUserName()+"-"+sdf1.format(measureTrackList.get(l).getCreateTime())+"<br>";
			}
			issueList.getList().get(i).set("reason_id", measureDetail.getReasonId())
			.set("measure_id", measureId)
			.set("measure_fujian_id", measureDetail.getFujianId())
			.set("measure_status",measureStatus)
			.set("measure", measure)
			.set("meature_track", measureTracks)
			.set("color", color)
			.set("measure_plan_finish_date", measureDetail.getPlanFinishDate())
			.set("measure_real_finish_date", measureDetail.getRealFinishDate());
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", issueList.getList());
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**
	 * 完成执行措施
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月25日 下午1:45:47
	 */
	public void finishMeasure() {
		try {
			boolean flag = Db.tx(() -> {
				long measureId=getParaToLong("measure_id");
				//附件
				JSONArray fileList=JSONArray.parseArray(getPara("fileList"));
				long fileId=0;
				if(fileList.size()!=0) {
					JSONObject fileObject=fileList.getJSONObject(0);
					fileId=fileObject.getLongValue("id");
				}
				HProjectMeasures measures=HProjectMeasures.dao.findById(measureId);
				measures.setStatus(2).setRealFinishDate(new Date())
				.setFujianId(fileId);
				measures.update();
				HProjectIssue issue=HProjectIssue.dao.findById(measures.getIssueId());
				User dutyUser=User.dao.findById(issue.getDutyUserId());
				DingMessage.sendText(dutyUser.getDingUserId(), measures.getUserName()+"已完成项目号下\n【"+issue.getProCode()+"】措施，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "3");//3-标准OPL措施责任人页面
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "操作成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "操作失败：" + e.getMessage()));
		}
	}
}
