package com.ray.controller.admin.StandardProOPL;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
/**
 * 标准OPL统计分析 standardAnalysis
 * @author FL00024996
 *
 */
public class StandardAnalysisController extends Controller {
	public void toStandardAnalysis() {
		render("standardAnalysis.html");
	}
	/**
	 * 获取表格分析数据
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月2日 下午3:28:12
	 */
	public void getAnalysisTableData() {
		String sqlCom="select count(*) as total_num from h_project_issue where is_del=0  ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selLine")!=null&&!"".equals(getPara("selLine"))) {
			sqlCom=sqlCom+" and line_id="+getParaToInt("selLine");
		}
		if (getPara("selProCode")!=null&&!"".equals(getPara("selProCode"))) {
			sqlCom=sqlCom+" and pro_code like '%"+getPara("selProCode")+"%'";
		}
		String[] levels= {"A","B","C","D","合计"};
		String[] types= {"设计","工艺","质量","成本","交付","管理","包装物流"}; 
		List<Record> dataList=new ArrayList<Record>();
		for (int i = 0; i < levels.length; i++) {
			Record analysisData=new Record();
			analysisData.set("level", levels[i]);
			String sqlLevelCom=sqlCom;
			if (i<4) {
				sqlLevelCom=sqlLevelCom+" and severity='"+levels[i]+"'";
			}
			int levelAllNum=0;//同等级问题总数
			int levelOpenNum=0;//同等级开发问题数
			for (int j = 0; j < types.length; j++) {
				String sqlAll=sqlLevelCom+" and issue_type='"+types[j]+"'";
				String sqlOpen=sqlLevelCom+" and issue_type='"+types[j]+"' and status<>'G'";
				Record allissue=Db.findFirst(sqlAll);
				Record openIssue=Db.findFirst(sqlOpen);
				int alltypeNum=allissue.getInt("total_num");
				int opentypeNum=openIssue.getInt("total_num");
				levelAllNum=levelAllNum+alltypeNum;
				levelOpenNum=levelOpenNum+opentypeNum;
				String typeName="";
				if ("设计".equals(types[j])) {
					typeName="sheji";
				}else if ("工艺".equals(types[j])) {
					typeName="gongyi";
				}else if ("质量".equals(types[j])) {
					typeName="zhiliang";
				}else if ("成本".equals(types[j])) {
					typeName="chengben";
				}else if ("交付".equals(types[j])) {
					typeName="jiaofu";
				}else if ("管理".equals(types[j])) {
					typeName="guanli";
				}else if ("包装物流".equals(types[j])) {
					typeName="pack";
				}
				analysisData.set("all_"+typeName, alltypeNum).set("open_"+typeName, opentypeNum);
			}
			//合计
			analysisData.set("all_heji", levelAllNum).set("open_heji", levelOpenNum);
			//关闭率
			if (levelAllNum>0) {
				BigDecimal closeRate=new BigDecimal(levelAllNum-levelOpenNum).divide(new BigDecimal(levelAllNum), 4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
				analysisData.set("close_rate", closeRate+"%");
			}else {
				analysisData.set("close_rate", "0.00%");
			}
			dataList.add(analysisData);
		}
		renderJson(Ret.ok("data",dataList));
	}
	/**
	 * 饼状图数据获取---严重度
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月3日 上午10:02:03
	 */
	public void getPieData() {
		String sqlCom="select count(*) as total_num from h_project_issue where is_del=0  ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selLine")!=null&&!"".equals(getPara("selLine"))) {
			sqlCom=sqlCom+" and line_id="+getParaToInt("selLine");
		}
		if (getPara("selProCode")!=null&&!"".equals(getPara("selProCode"))) {
			sqlCom=sqlCom+" and pro_code like '%"+getPara("selProCode")+"%'";
		}
		//严重度
		String[] levels= {"A","B","C","D"};
		List<Record> levelList=new ArrayList<Record>();
		for (int i = 0; i < levels.length; i++) {
			Record analysisData=new Record();
			analysisData.set("level", levels[i]);
			String sqlLevelCom=sqlCom+" and severity='"+levels[i]+"'";
			Record allLevelIssue=Db.findFirst(sqlLevelCom);
			int allNum=allLevelIssue.getInt("total_num");
			analysisData.set("total_num", allNum);
			levelList.add(analysisData);
		}
		//类型
		String[] types= {"设计","工艺","质量","成本","交付","管理","包装物流"};
		List<Record> typeList=new ArrayList<Record>();
		for (int i = 0; i < types.length; i++) {
			Record analysisData=new Record();
			analysisData.set("type", types[i]);
			String sqlTypeCom=sqlCom+" and issue_type='"+types[i]+"'";
			Record allTypeIssue=Db.findFirst(sqlTypeCom);
			int allNum=allTypeIssue.getInt("total_num");
			analysisData.set("total_num", allNum);
			typeList.add(analysisData);
		}
		Record record=new Record();
		record.set("level_list", levelList).set("type_list", typeList);
		renderJson(Ret.ok("data",record));
	}
}
