package com.ray.controller.admin.StandardProOPL;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.model.HProIssuePlantimeEditRecord;
import com.ray.common.model.HProIssueReasonAnalysis;
import com.ray.common.model.HProIssueTell;
import com.ray.common.model.HProIssueTrackRecord;
import com.ray.common.model.HProjectMeasures;
/**
 * 知会我的标准OPL tellMeProOPL
 * @author FL00024996
 *
 */
public class TellMeProOPLController extends Controller {
	public void toTellMeProOPL() {
		render("tellMeProOPL.html");
	}
	public void getTellMeProOPL() {
		StringBuilder sb=new StringBuilder();
		Record loginUser=getSessionAttr("user");
		int loginUserId=loginUser.getInt("id");
		sb.append(" from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.id in (select issue_id from h_pro_issue_tell where status=0 and tell_user_id="+loginUserId+") ");//我创建的
		String selProductName=get("selProductName");
		String selStatus=get("selStatus");
		String selIssueType=get("selIssueType");
		String selProCode=get("selProCode");
		if (!"".equals(selProCode)&& selProCode!=null) {
			sb.append(" and a.pro_code like '%"+selProCode+"%'");
		}
		if (selProductName!=null&&!"".equals(selProductName)) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		if (selIssueType!=null&&!"".equals(selIssueType)) {
			sb.append(" and a.issue_type='"+selIssueType+"'");
		}
		if (selStatus!=null&&!"".equals(selStatus)) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name ",sb.toString()+" order by id desc");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			//问题、目标
			String issue_description=issueList.getList().get(i).getStr("issue_description");
			if(issue_description==null) {
				issue_description="";
			}
			String issue_target=issueList.getList().get(i).getStr("issue_target");
			if (issue_target==null) {
				issue_target="";
			}
			String description_target="问题："+issue_description+"<br>"+"目标："+issue_target;
			//知会人
			List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueId+" and status=0");
			List<Integer> tellUserId=new ArrayList<Integer>();
			String tellUserName=" ";
			for (int j = 0; j < tells.size(); j++) {
				tellUserId.add(tells.get(j).getTellUserId());
				tellUserName=tellUserName+tells.get(j).getTellUserName()+"<br>";
			}
			//OPL整体跟踪记录
			List<HProIssueTrackRecord> issueTrackList=HProIssueTrackRecord.dao.find("select * from h_pro_issue_track_record where issue_id="+issueId);
			String issueTracks=" ";
			for (int j = 0; j < issueTrackList.size(); j++) {
				issueTracks=issueTracks+(j+1)+"."+issueTrackList.get(j).getTrackRecord()+"-"+issueTrackList.get(j).getCreateUserName()+"-"+issueTrackList.get(j).getCreateTime()+"<br>";
			}
			//实际完成
			Date realFinishDate=issueList.getList().get(i).getDate("real_finish_time");
			String real_finish_time="";
			if (realFinishDate==null) {
				real_finish_time="进行中";
			}else {
				real_finish_time=sdf.format(realFinishDate);
			}
			//计划完成时间修改记录
			List<HProIssuePlantimeEditRecord> editRecords=HProIssuePlantimeEditRecord.dao.find("select * from h_pro_issue_plantime_edit_record where issue_id="+issueId);
			String planTimeEditRecord=" ";
			for (int j = 0; j < editRecords.size(); j++) {
				String oldTime="空";
				String newTime="空";
				if (editRecords.get(j).getOldPlanTime()!=null) {
					oldTime=sdf.format(editRecords.get(j).getOldPlanTime());
				}
				if (editRecords.get(j).getNewPlanTime()!=null) {
					newTime=sdf.format(editRecords.get(j).getNewPlanTime());
				}
				planTimeEditRecord=planTimeEditRecord+oldTime+"->"+newTime+"-"+editRecords.get(j).getCreateUserName()+"<br>";
			}
			issueList.getList().get(i)
			.set("tell_users_name",tellUserName)
			.set("tell_users_id", tellUserId)
			.set("description_target",description_target)
			.set("issue_track_record", issueTracks)
			.set("real_finish_time", real_finish_time)
			.set("plan_time_edit_record", planTimeEditRecord);
			//原因分析
			List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
			String reason_analysis="暂无原因分析";
			String temMeasures="暂无临时措施";
			String perMeasures="暂无永久措施";
			if (!reasonAnalysis.isEmpty()) {
				reason_analysis="";
				boolean haveTemMeasure = false;
				boolean havePerTemMeasure = false;
				String temMeasure1="";
				String perMeasure1="";
				for (int j = 0; j < reasonAnalysis.size(); j++) {
					reason_analysis=reason_analysis+(j+1)+" "+reasonAnalysis.get(j).getReasonAnalysis()+"<br>";
					//临时措施状态、内容
					List<HProjectMeasures> temMeasureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" and measure_type=0 and is_del=0 order by xuhao asc");
					if (!temMeasureList.isEmpty()) {
						haveTemMeasure = true;
						for (int k = 0; k < temMeasureList.size(); k++) {
							int measureStatus=temMeasureList.get(k).getStatus();
							String measure=(j+1)+"."+(k+1)+" ";
							if (measureStatus==1) {//进行中
								measure=measure+temMeasureList.get(k).getMeasures()+"-"+temMeasureList.get(k).getUserName()+"-计划："+sdf.format(temMeasureList.get(k).getPlanFinishDate());
								if (temMeasureList.get(k).getPlanFinishDate().before(new Date())) {
								}
							}else {
								measure=measure+temMeasureList.get(k).getMeasures()+"-"+temMeasureList.get(k).getUserName()+"-"+temMeasureList.get(k).getRealFinishDate();
							}
							temMeasure1=temMeasure1+measure+"<br>";
						}
					}
					//永久
					List<HProjectMeasures> perMeasureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId()+" and measure_type=1 and is_del=0 order by xuhao asc");
					if (!perMeasureList.isEmpty()) {
						havePerTemMeasure = true;
						for (int k = 0; k < perMeasureList.size(); k++) {
							int measureStatus=perMeasureList.get(k).getStatus();
							String measure=(j+1)+"."+(k+1)+" ";
							if (measureStatus==1) {//进行中
								measure=measure+perMeasureList.get(k).getMeasures()+"-"+perMeasureList.get(k).getUserName()+"-计划："+sdf.format(perMeasureList.get(k).getPlanFinishDate());
								if (perMeasureList.get(k).getPlanFinishDate().before(new Date())) {
								}
							}else {
								measure=measure+perMeasureList.get(k).getMeasures()+"-"+perMeasureList.get(k).getUserName()+"-"+perMeasureList.get(k).getRealFinishDate();
							}
							perMeasure1=perMeasure1+measure+"<br>";
						}
					}
				}
				if (haveTemMeasure) {
					temMeasures=temMeasure1;
				}
				if (havePerTemMeasure) {
					perMeasures=perMeasure1;
				}
			}
			issueList.getList().get(i)
			.set("reason_analysis", reason_analysis)
			.set("tem_measure", temMeasures).set("per_measure", perMeasures);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", issueList.getList());
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**
	 * 以措施为单位
	 */
//	public void getTellMeProOPL() {
//		StringBuilder sb=new StringBuilder();
//		Record loginUser=getSessionAttr("user");
//		int loginUserId=loginUser.getInt("id");
//		sb.append(" from h_project_issue a,h_cause_dep b where  a.is_del=0 and a.dep_id=b.dep_id and a.id in (select issue_id from h_pro_issue_tell where status=0 and tell_user_id="+loginUserId+") ");//我创建的
//		String selProductName=get("selProductName");
//		String selStatus=get("selStatus");
//		String selIssueType=get("selIssueType");
//		String selProCode=get("selProCode");
//		if (!"".equals(selProCode)&& selProCode!=null) {
//			sb.append(" and a.pro_code like '%"+selProCode+"%'");
//		}
//		if (selProductName!=null&&!"".equals(selProductName)) {
//			sb.append(" and a.product_name like '%"+selProductName+"%'");
//		}
//		if (selIssueType!=null&&!"".equals(selIssueType)) {
//			sb.append(" and a.issue_type='"+selIssueType+"'");
//		}
//		if (selStatus!=null&&!"".equals(selStatus)) {
//			sb.append(" and a.status='"+selStatus+"'");
//		}
//		int page=getInt("currentPage");
//		int limit=getInt("pageSize");
//		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name ",sb.toString()+" order by id desc");
//		List<Record> proIssueList=new ArrayList<Record>();
//		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//		for (int i = 0; i < issueList.getList().size(); i++) {
//			long issueId=issueList.getList().get(i).getLong("id");
//			//问题、目标
//			String issue_description=issueList.getList().get(i).getStr("issue_description");
//			if(issue_description==null) {
//				issue_description="";
//			}
//			String issue_target=issueList.getList().get(i).getStr("issue_target");
//			if (issue_target==null) {
//				issue_target="";
//			}
//			String description_target="问题："+issue_description+"<br>"+"目标："+issue_target;
//			//知会人
//			List<HProIssueTell> tells=HProIssueTell.dao.find("select * from h_pro_issue_tell where issue_id="+issueId+" and status=0");
//			List<Integer> tellUserId=new ArrayList<Integer>();
//			String tellUserName=" ";
//			for (int j = 0; j < tells.size(); j++) {
//				tellUserId.add(tells.get(j).getTellUserId());
//				tellUserName=tellUserName+tells.get(j).getTellUserName()+"<br>";
//			}
//			//OPL整体跟踪记录
//			List<HProIssueTrackRecord> issueTrackList=HProIssueTrackRecord.dao.find("select * from h_pro_issue_track_record where issue_id="+issueId);
//			String issueTracks=" ";
//			for (int j = 0; j < issueTrackList.size(); j++) {
//				issueTracks=issueTracks+(j+1)+"."+issueTrackList.get(j).getTrackRecord()+"-"+issueTrackList.get(j).getCreateUserName()+"-"+issueTrackList.get(j).getCreateTime()+"<br>";
//			}
//			//实际完成
//			Date realFinishDate=issueList.getList().get(i).getDate("real_finish_time");
//			String real_finish_time="";
//			if (realFinishDate==null) {
//				real_finish_time="进行中";
//			}else {
//				real_finish_time=sdf.format(realFinishDate);
//			}
//			//计划完成时间修改记录
//			List<HProIssuePlantimeEditRecord> editRecords=HProIssuePlantimeEditRecord.dao.find("select * from h_pro_issue_plantime_edit_record where issue_id="+issueId);
//			String planTimeEditRecord=" ";
//			for (int j = 0; j < editRecords.size(); j++) {
//				String oldTime="空";
//				String newTime="空";
//				if (editRecords.get(j).getOldPlanTime()!=null) {
//					oldTime=sdf.format(editRecords.get(j).getOldPlanTime());
//				}
//				if (editRecords.get(j).getNewPlanTime()!=null) {
//					newTime=sdf.format(editRecords.get(j).getNewPlanTime());
//				}
//				planTimeEditRecord=planTimeEditRecord+oldTime+"->"+newTime+"-"+editRecords.get(j).getCreateUserName()+"<br>";
//			}
//			issueList.getList().get(i)
//			.set("tell_users_name",tellUserName)
//			.set("tell_users_id", tellUserId)
//			.set("description_target",description_target)
//			.set("issue_track_record", issueTracks)
//			.set("real_finish_time", real_finish_time)
//			.set("plan_time_edit_record", planTimeEditRecord);
//			List<HProIssueReasonAnalysis> reasonAnalysis=HProIssueReasonAnalysis.dao.find("select * from h_pro_issue_reason_analysis where status=0 and issue_id="+issueId);
//			if (reasonAnalysis.isEmpty()) {
//				Record issueRecord= new Record();
//				issueRecord.setColumns(issueList.getList().get(i));					
//				issueRecord.set("reason_id",0)
//				.set("reason_analysis", "暂无原因分析")
//				.set("measure_id", 0)
//				.set("measure_status",0)
//				.set("measure", "暂无")
//				.set("meature_track", "");
//				proIssueList.add(issueRecord);
//			}else {
//				//原因分析
//				for (int j = 0; j < reasonAnalysis.size(); j++) {
//					//措施状态、内容
//					List<HProjectMeasures> measureList=HProjectMeasures.dao.find("select * from h_project_measures where reason_id="+reasonAnalysis.get(j).getId());
//					if (!measureList.isEmpty()) {
//						for (int k = 0; k < measureList.size(); k++) {
//							int measureStatus=measureList.get(k).getStatus();
//							String measure=measureList.get(k).getXuhao()+" ";
//							String color="";//措施单元格颜色标注
//							if (measureStatus==1) {//进行中
//								measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-计划："+sdf.format(measureList.get(k).getPlanFinishDate());
//								if (measureList.get(k).getPlanFinishDate().before(new Date())) {
//									color="yellow";
//								}
//							}else {
//								measure=measure+measureList.get(k).getMeasures()+"-"+measureList.get(k).getUserName()+"-"+measureList.get(k).getRealFinishDate();
//								color="green";
//							}
//							//措施跟踪情况 
//							List<HProissueTrackHistory> measureTrackList =HProissueTrackHistory.dao.find("select * from h_proissue_track_history where measure_id="+measureList.get(k).getId());
//							String measureTracks="";
//							for (int l = 0; l < measureTrackList.size(); l++) {
//								measureTracks=measureTracks+(l+1)+"."+measureTrackList.get(l).getRemark()+"-"+measureTrackList.get(l).getTrackUserName()+"-"+measureTrackList.get(l).getCreateTime()+"<br>";
//							}
//							Record issueRecord= new Record();
//							issueRecord.setColumns(issueList.getList().get(i));
//							issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
//							.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
//							.set("measure_id", measureList.get(k).getId())
//							.set("measure_status",measureStatus)
//							.set("measure", measure)
//							.set("color", color)
//							.set("meature_track", measureTracks);
//							proIssueList.add(issueRecord); 
//						}
//					}else {
//						Record issueRecord= new Record();
//						issueRecord.setColumns(issueList.getList().get(i));					
//						issueRecord.set("reason_id", reasonAnalysis.get(j).getId())
//						.set("reason_analysis", reasonAnalysis.get(j).getReasonAnalysis())
//						.set("measure_id", 0)
//						.set("measure_status",0)
//						.set("measure", "暂无")
//						.set("meature_track", "");
//						proIssueList.add(issueRecord);
//					}
//				}
//			}
//		}
//		Record record=new Record();
//		record.set("code", 0);
//		record.set("msg", "获取成功");
//		record.set("list", proIssueList);
//		record.set("totalResult", issueList.getTotalRow());
//		renderJson(record);
//	}
}
