package com.ray.controller.admin.ProIssue;

import java.util.List;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.model.User;
/**
 * 项目管理project
 * @author FL00024996
 *
 */
public class ProjectController extends Controller {
	public void toProject() {
		render("projectList.html");
	}
	/**
	 * 获取项目列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月14日 下午5:19:45
	 */
	public void getProList() {
		Record loginUser=getSessionAttr("user");
		User user=User.dao.findById(loginUser.getInt("id"));
		//登陆人在预算管理系统中角色
		String sql_role="select b.role_name from user_role a, roles b where a.user_id='"+user.getDingUserId()+"' and a.role_id=b.id";
		List<Record> user_roleList=Db.use("bud").find(sql_role);
		int user_role=0;
		if (user_roleList.size()>0) {
			for (int i = 0; i < user_roleList.size(); i++) {
				String role_name=user_roleList.get(i).getStr("role_name");
				if (role_name.equals("公司经营层")|| role_name.equals("项目体系")||"财务".equals(role_name)||"管理员".equals(role_name)) {
					user_role=1;
				}
			}
		}
		StringBuilder sb=new StringBuilder();
		if (user_role==0) {//普通用户-判断项目成员
			sb.append(" from file_pro_info   where pro_code in (select pro_code from pro_member where role_member_id='"+user.getDingUserId()+"')");
		}else {
			sb.append(" from file_pro_info where 1=1 ");
		}
		String selProcode=get("selProcode");
		if (!"".equals(selProcode)&& selProcode!=null) {
			sb.append(" and pro_code like '%"+selProcode+"%'");
		}
		String selDepName=get("selDepName");
		if (!"".equals(selDepName)&& selDepName!=null) {
			sb.append(" and depart_name like '%"+selDepName+"%'");
		}
		String selCusName=get("selCusName");
		if (!"".equals(selCusName)&& selCusName!=null) {
			sb.append(" and customer_name like '%"+selCusName+"%'");
		}
		String selProductName=get("selProductName");
		if (!"".equals(selProductName)&& selProductName!=null) {
			sb.append(" and category_name like '%"+selProductName+"%'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> projectList=Db.use("bud").paginate(page, limit, "select * ",sb.toString()+" order by id desc");
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("totalResult", projectList.getTotalRow());
		record.set("list", projectList.getList());
		renderJson(record);
	}

	
}
