package com.ray.controller.admin.ComMethod;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import com.ray.common.model.AndonProblemType;
import com.ray.common.model.AndonReasonType;
import com.ray.common.model.File;
import com.ray.common.model.HCauseDep;
import com.ray.common.model.HDepLine;
import com.ray.common.model.HParts;
import com.ray.common.model.HProCusIssue;
import com.ray.common.model.HQualityIssue;
import com.ray.common.model.HQualityIssueType;
import com.ray.common.model.HUserDep;
import com.ray.common.model.HWare;
import com.ray.common.model.User;

/**
 * 公共方法comMethod
 *
 * @author FL00024996
 */
public class ComMethodController extends Controller {


    /**
     * 获取所有用户
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月8日 上午10:47:35
     */
    public void getAllUser() {
        try {
            List<Record> allUsers = Db.find("select * from user  where status=0 ");
            for (int i = 0; i < allUsers.size(); i++) {
                HUserDep userDep = HUserDep.dao.findFirst("select * from h_user_dep where user_id=" + allUsers.get(i).getInt("id") + " and status=0");
                allUsers.get(i).set("first_dep_name", userDep.getFirstDepName());
            }
            renderJson(Ret.ok("data", allUsers));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取所有用户
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月8日 上午10:47:35
     */
    public void getPageAllUser() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(" from user where status=0 ");
            String userName = get("userName");
            if (userName != null && !"".equals(userName)) {
                sb.append(" and nickname like '%" + userName + "%'");
            }
            int page = getInt("currentPage");
            int limit = getInt("pageSize");
            Page<Record> userList = Db.paginate(page, limit, "select * ", sb.toString());
            for (int i = 0; i < userList.getList().size(); i++) {
                HUserDep userDep = HUserDep.dao.findFirst("select * from h_user_dep where user_id=" + userList.getList().get(i).getInt("id") + " and status=0");
                userList.getList().get(i).set("first_dep_name", userDep.getFirstDepName());
            }
            Record record = new Record();
            record.set("code", 0);
            record.set("msg", "获取成功");
            record.set("list", userList.getList());
            record.set("totalResult", userList.getTotalRow());
            renderJson(record);
            renderJson(Ret.ok("data", record));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 根据选择的用户ID获取用户姓名
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月25日 上午9:13:40
     */
    public void getUserInfo() {
        try {
            int userId = getParaToInt("userId");
            User userInfo = User.dao.findById(userId);
            renderJson(Ret.ok("data", userInfo));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取事业部
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月15日 下午3:54:31
     */
    public void getDep() {
        try {
            List<HCauseDep> depList = HCauseDep.dao.find("select * from h_cause_dep where status=0 ");
            renderJson(Ret.ok("data", depList));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取事业部下 产线列表
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年8月3日 上午9:40:56
     */
    public void getDeplineList() {
        try {
            long dep_id = getParaToLong("selDep");
            List<HDepLine> lineList = HDepLine.dao.find("select * from h_dep_line where status=0 and dep_id=" + dep_id);
            renderJson(Ret.ok("data", lineList));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取事业部下车间
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月15日 下午3:57:17
     */
    public void getDepWare() {
        try {
            long dep_id = getParaToLong("selDep");
            List<HWare> wareList = HWare.dao.find("select * from h_ware where status=0 and dep_id=" + dep_id);
            renderJson(Ret.ok("data", wareList));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 根据值搜索前20条零件列表
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年12月22日 下午4:43:32
     */
    public void getTop20PartList() {
        try {
            String sql = "select * from h_parts where status=0 ";
            if (get("part_no") != null) {
                sql = sql + " and part_no like '%" + get("part_no") + "%'";
            }
            sql = sql + " limit 20";
            List<HParts> partList = HParts.dao.find(sql);
            renderJson(Ret.ok("data", partList));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取零件列表
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月15日 下午4:00:27
     */
    public void getPartList() {
        try {
            String sql = "";
            String id = getPara("id");
            if (!"0".equals(id)) {
                sql = " select * from h_parts where status=0 and part_no like '%" + id + "%'  limit 0,100 ";
            } else {
                sql = " select * from h_parts where status=0  limit 0,100";
            }
            List<HParts> partList = HParts.dao.find(sql);
            renderJson(Ret.ok("data", partList));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 零件信息
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年5月18日 下午8:26:27
     */
    public void getPartInfo() {
        try {
            String partId = getPara("partId");
            Record first = Db.findFirst(" select part_no,part_name from h_parts where id = '" + partId + "' ");
            String part_name = first.getStr("part_name");
            String part_no = first.getStr("part_no");
            renderJson(Ret.ok("part_name", part_name).set("part_no",part_no));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson();
        }
    }

    /**
     * 获取质量问题类型
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月16日 上午10:24:07
     */
    public void getQtyTypeList() {
        try {
            List<HQualityIssueType> typeList = HQualityIssueType.dao.find("select * from h_quality_issue_type where status=0 ");
            renderJson(Ret.ok("data", typeList));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取前20个供应商
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年12月23日 上午10:28:29
     */
    public void getTop20SupplierList() {
        try {
            String sql = "";
            if (get("supplier_name") != null) {
                sql = " SELECT supplier_id,name FROM SUPPLIER_INFO  where  name like '%" + get("supplier_name") + "%'  and rownum<=20 ";
            } else {
                sql = " SELECT supplier_id,name FROM SUPPLIER_INFO where rownum<=20  ";
            }
            List<Record> supplierList = Db.use("oa").find(sql);
            renderJson(Ret.ok("data", supplierList));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取供应商类型
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月16日 上午10:24:07
     */
    public void getSupplierList() {
        List<Record> list = new ArrayList<>();
        String id = getPara("id");
        String sql = "";
        if (!"0".equals(id)) {
            sql = "  Select ROWNUM AS ROWNO,supplier_id,name from SUPPLIER_INFO T where ROWNUM <= 20 and supplier_id like '%" + id + "%'  or  name like  '%" + id + "%' ";
        } else {
            sql = "  Select ROWNUM AS ROWNO,supplier_id,name from SUPPLIER_INFO T where ROWNUM <= 20 ";
        }
        try {
            list = Db.use("oa").find(sql);
            renderJson(Ret.ok("data", list));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取零件类型
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月16日 上午10:24:07
     */
    public void getQualityElementList() throws Exception {
        List<Record> list = new ArrayList<>();
        String id = getPara("id");
        String part_id = getPara("part_id");
        String sql = "";
        if (id == null  && part_id == null){
            sql = "  Select ROWNUM AS ROWNO,part_no,description from INVENTORY_PART T where ROWNUM <= 30 and part_no IN (select COMPONENT_PART FROM  PROD_STRUCTURE )  ";
        }
        if (part_id != null && id == null){
            sql = "  Select ROWNUM AS ROWNO,part_no,description from INVENTORY_PART T where ROWNUM <= 30 and part_no IN (select COMPONENT_PART FROM  PROD_STRUCTURE WHERE PART_NO = '" + part_id + "' )  ";
        }
        if (part_id != null && id != null){
            sql = "  Select ROWNUM AS ROWNO,part_no,description from INVENTORY_PART T where ROWNUM <= 30 and part_no IN (select COMPONENT_PART FROM  PROD_STRUCTURE WHERE PART_NO = '" + part_id + "' ) and part_no like '%" + id + "%' ";
        }
        if (part_id == null && id != null){
            throw new Exception();
        }

        try {
            list = Db.use("oa").find(sql);
            renderJson(Ret.ok("data", list));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：请选择总成号后选择零件号！"));
        }
    }

    /**
     * 获取零件类型
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月16日 上午10:24:07
     */
    public void getElementList() {
        List<Record> list = new ArrayList<>();
        String id = getPara("id");
        String sql = "";
        if (!"0".equals(id)){
        	sql = " SELECT * FROM (Select ROWNUM AS ROWNO,part_no,description from INVENTORY_PART T where ROWNUM <= 20 and part_no like '%"+id+"%') t WHERE t.ROWNO >= 10 ";
        }else{
        	sql = " SELECT * FROM (Select ROWNUM AS ROWNO,part_no,description from INVENTORY_PART T where ROWNUM <= 20 ) t WHERE t.ROWNO >= 10 ";
        }
        try {
            list = Db.use("oa").find(sql);
            renderJson(Ret.ok("data", list));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败："+e.getMessage()));
        }
    }

    /**
     * 上传文件
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月16日 上午10:58:57
     */
    public void upLoadFile() {
        Record resp = new Record();
        Date date = new Date();
        try {
            UploadFile file = getFile("file", "");
            String fileName = file.getFileName();
            File fujian = getModel(File.class);
            fujian.setName(fileName)
                    .setUrl("")
                    .setCreateTime(date);
            fujian.save();
            resp.set("code", 0);
            resp.set("fileId", fujian.getId());
            resp.set("fileName", fileName);
            resp.set("msg", "上传成功！");
        } catch (Exception e) {
            resp.set("code", 1);
            resp.set("msg", "上传失败，原因：" + e.getMessage());
            e.printStackTrace();
        }
        renderJson(resp);
    }

    //附件下载
    public void downLoad() {
        Record rd = new Record();
        long fujianId = getParaToLong(0);
        File fujian = File.dao.findById(fujianId);
        try {
            render(new MyFileRender(fujian.getStr("name")));
        } catch (Exception e) {
            rd.set("code", 1);
            rd.set("msg", "暂无附件下载");
            rd.set("data", "");
            renderJson(rd);
        }
    }

    /**
     * 获取质量OPL激励文件
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年5月27日 下午8:15:05
     */
    public void getQtyUrgePlanFile() {
        long issueId = getParaToLong("issueId");
        HQualityIssue qtyIssue = HQualityIssue.dao.findById(issueId);
        File file = File.dao.findById(qtyIssue.getUrgeFujianId());
        if (file == null) {
            file = new File();
            file.setId(0);
        }
        Record record = new Record();
        record.set("data", file);
        record.set("msg", "获取成功");
        renderJson(record);
    }

    /**
     * 获取某个质量问题的分析报告
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月16日 下午5:33:28
     */
    public void getIssueReportFile() {
        long issueId = getParaToLong("issueId");
        HQualityIssue qtyIssue = HQualityIssue.dao.findById(issueId);
        File file = File.dao.findById(qtyIssue.getFileId());
        if (file == null) {
            file = new File();
            file.setId(0);
        }
        Record record = new Record();
        record.set("data", file);
        record.set("msg", "获取成功");
        renderJson(record);
    }

    /**
     * 获取质量问题PFMEA、WI等文件
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年12月7日 下午3:34:51
     */
    public void getIssueFujianFile() {
        long issueId = getParaToLong("issueId");
        String type = getPara("type");
        HQualityIssue qtyIssue = HQualityIssue.dao.findById(issueId);
        File file = File.dao.findById(qtyIssue.getLong(type));
        if (file == null) {
            file = new File();
            file.setId(0);
        }
        Record record = new Record();
        record.set("data", file);
        record.set("msg", "获取成功");
        renderJson(record);
    }

    /**
     * 获取新品OPL激励文件
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年5月27日 下午8:15:05
     */
    public void getProCusUrgePlanFile() {
        long issueId = getParaToLong("issueId");
        HProCusIssue qtyIssue = HProCusIssue.dao.findById(issueId);
        File file = File.dao.findById(qtyIssue.getUrgeFujianId());
        if (file == null) {
            file = new File();
            file.setId(0);
        }
        Record record = new Record();
        record.set("data", file);
        record.set("msg", "获取成功");
        renderJson(record);
    }

    /**
     * 获取某个项目新品客诉的分析报告
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月16日 下午5:33:28
     */
    public void getProCusIssueReportFile() {
        long issueId = getParaToLong("issueId");
        HProCusIssue qtyIssue = HProCusIssue.dao.findById(issueId);
        File file = File.dao.findById(qtyIssue.getFileId());
        if (file == null) {
            file = new File();
            file.setId(0);
        }
        Record record = new Record();
        record.set("data", file);
        record.set("msg", "获取成功");
        renderJson(record);
    }

    /**
     * 获取所有项目号
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月23日 下午1:59:51
     */
    public void getAllProList() {
        try {
            List<Record> proList = Db.use("bud").find("select * from file_pro_info");
            renderJson(Ret.ok("data", proList));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取项目产品名称
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月23日 下午2:05:53
     */
    public void getProductName() {
        try {
            int proId = getParaToInt("proId");
            Record proInfo = Db.use("bud").findFirst("select * from file_pro_info where id=" + proId);
            renderJson(Ret.ok("data", proInfo));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取项目关联的事业部下产线
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年8月2日 下午4:32:02
     */
    public void getLineList() {
        try {
            int proId = getParaToInt("proId");
            Record proInfo = Db.use("bud").findFirst("select * from file_pro_info where id=" + proId);
            long proDepId = com.ray.util.HyCommenMethods.dDepId(proInfo.getStr("depart_name"));
            List<HDepLine> line = HDepLine.dao.find("select * from h_dep_line where dep_id=" + proDepId + " and status=0");
            renderJson(Ret.ok("data", line));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 标准OPL导出
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月8日 上午10:47:35
     */
    public void proStandOPLDownExcel() {
        Record rd = new Record();
        try {
            renderFile("/moban/项目标准OPL.xls");
        } catch (Exception e) {
            rd.set("code", 1);
            rd.set("msg", "暂无！");
            renderJson(rd);
        }
    }

    /**
     * 新品客诉OPL导出
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年5月6日 上午10:54:22
     */
    public void proNewCusDownExcel() {
        Record rd = new Record();
        try {
            renderFile("/moban/新品客诉OPL.xls");
        } catch (Exception e) {
            rd.set("code", 1);
            rd.set("msg", "暂无！");
            renderJson(rd);
        }
    }

    /**
     * 新品客诉OPL导出
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年5月6日 上午10:54:22
     */
    public void qtyDownExcel() {
        Record rd = new Record();
        try {
            renderFile("/moban/质量OPL.xls");
        } catch (Exception e) {
            rd.set("code", 1);
            rd.set("msg", "暂无！");
            renderJson(rd);
        }
    }

    public void clothesDownExcel() {
        Record rd = new Record();
        try {
            renderFile("/moban/工装OPL.xls");
        } catch (Exception e) {
            rd.set("code", 1);
            rd.set("msg", "暂无！");
            renderJson(rd);
        }
    }

    /**
     * 获取登陆用户
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年4月8日 上午10:47:35
     */
    public void getLoginUser() {
        try {
            Record user = getSessionAttr("user");
            renderJson(Ret.ok("data", user));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }

    }

    /**
     * 获取生产OPL原因类别
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年11月11日 下午5:56:10
     */
    public void getProductReasonType() {
        try {
            List<AndonReasonType> reasonTypes = AndonReasonType.dao.find("select * from andon_reason_type where status=0");
            renderJson(Ret.ok("data", reasonTypes));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }

    /**
     * 获取生产OPL问题属性
     *
     * @author simple
     * @contact 15228717200
     * @time 2021年11月11日 下午5:58:09
     */
    public void getProductProblemType() {
        try {
            List<AndonProblemType> problemTypes = AndonProblemType.dao.find("select * from andon_problem_type where status=0");
            renderJson(Ret.ok("data", problemTypes));
        } catch (Exception e) {
            e.printStackTrace();
            renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
        }
    }
}
