package com.ray.controller.admin.QualityIssue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.HQtyIssue8dPlan;
import com.ray.common.model.HQtyIssue8dReal;
import com.ray.common.model.HQtyIssueReasonAnalysis;
import com.ray.common.model.HQtyIssueTrackRecord;
import com.ray.common.model.HQtyMeasures;
import com.ray.common.model.HQtyTrackHistory;
import com.ray.common.model.HQualityIssue;
import com.ray.common.model.HQualityIssueMonthTrack;
import com.ray.common.model.HQualityIssueTell;
import com.ray.common.model.HQualityopllUpgradeRecord;
import com.ray.common.model.HWare;
import com.ray.common.model.User;
/**
 * 质量问题升级 qtyUpgradeOPL/toMyUpgradeQtyOPL
 * @author FL00024996
 *
 */
public class QtyIssueUpgradeController extends Controller {
	public void toMyUpgradeQtyOPL() {
		render("upgradeQtyOPL.html");
	}
	public void getMyUpgradeQtyOPL() {
		Record loginUser=getSessionAttr("user");
		User user=User.dao.findById(loginUser.getInt("id"));
		StringBuilder sb=new StringBuilder();
		sb.append(" from h_quality_issue a,h_cause_dep b,h_parts d,h_quality_issue_type e where a.dep_id=b.dep_id and a.is_del=0  "
				+ "and a.part_id=d.id and a.type_id=e.id and a.status<>'G'");
		sb.append(" and a.id in (select issue_id from h_qualityopll_upgrade_record where ((new_user_id="+user.getId()+" and sp_status<>2 )or old_user_id="+user.getId()+") )");
		String selOPLdes=get("selOPLdes");
		if (!"".equals(selOPLdes)&& selOPLdes!=null) {
			sb.append(" and a.description like '%"+selOPLdes+"%'");
		}
		String selLevel=get("selLevel");
		if (!"".equals(selLevel)&& selLevel!=null) {
			sb.append(" and a.level='"+selLevel+"'");
		}
		long selDep=getParaToLong("selDep");
		if (selDep!=0) {
			sb.append(" and a.dep_id="+selDep);
		}
		int selWarehouse=getParaToInt("selWarehouse");
		if (selWarehouse!=0) {
			sb.append(" and a.ware_id="+selWarehouse);
		}
		String selPartCode=get("selPartCode");
		if (!"".equals(selPartCode)&& selPartCode!=null) {
			sb.append(" and d.part_no like '%"+selPartCode+"%'");
		}
		String selProductName=get("selProductName");
		if (!"".equals(selProductName)&& selProductName!=null) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		String selCusName=get("selCusName");
		if (!"".equals(selCusName)&& selCusName!=null) {
			sb.append(" and a.cus_name like '%"+selCusName+"%'");
		}
		String selStatus=get("selStatus");
		if (!"".equals(selStatus)&& selStatus!=null) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		String selFrequency=get("selFrequency");
		if (!"".equals(selFrequency)&& selFrequency!=null) {
			sb.append(" and a.happen_frequency='"+selFrequency+"'");
		}
		if (!"".equals(get("selType"))&& get("selType")!=null) {
			sb.append(" and a.type_id="+get("selType"));
		}
		String selSource=get("selSource");
		if (!"".equals(selSource)&& selSource!=null) {
			sb.append(" and a.source='"+selSource+"'");
		}
		String selProductStage=get("selProductStage");
		if (!"".equals(selProductStage)&& selProductStage!=null) {
			sb.append(" and a.product_stage='"+selProductStage+"'");
		}
		String selNextReviewTime=get("selNextReviewTime");
		if (!"".equals(selNextReviewTime)&& selNextReviewTime!=null) {
			sb.append(" and a.next_review_time='"+selNextReviewTime+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit,"select a.*,b.dep_name,d.part_no,e.type ",sb.toString()+" order by id desc");
		List<Record> qtyOPLList=new ArrayList<Record>();
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < issueList.getList().size(); i++) {
			String level = issueList.getList().get(i).getStr("level");
			long issueId=issueList.getList().get(i).getLong("id");
			issueList.getList().get(i).set("caozuo", " ");
			//获取知会人
			List<HQualityIssueTell> tells=HQualityIssueTell.dao.find("select * from h_quality_issue_tell where issue_id="+issueId+" and status=0");
			List<Integer> tellUserId=new ArrayList<Integer>();
			String tellUserName=" ";
			for (int j = 0; j < tells.size(); j++) {
				tellUserId.add(tells.get(j).getTellUserId());
				tellUserName=tellUserName+tells.get(j).getTellUserName()+"，";
			}
			issueList.getList().get(i)
			.set("tell_users_name",tellUserName)
			.set("tell_users_id", tellUserId);
			//车间
			if (issueList.getList().get(i).get("ware_id")!=null) {
				HWare ware=HWare.dao.findById(issueList.getList().get(i).getInt("ware_id"));
				issueList.getList().get(i).set("ware_name", ware.getWareName());
			}else {
				issueList.getList().get(i).set("ware_name", "暂未维护");
			}
			//判断是否拥有升级按钮 \ 审批按钮
			int canUp=0;
			int canPassBack=0;
			HQualityopllUpgradeRecord upgradeRecord=HQualityopllUpgradeRecord.dao.findFirst("select * from h_qualityopll_upgrade_record where status=0 and issue_id="+issueId);
			if (upgradeRecord.getSpStatus()==0) {//待审批
				if (upgradeRecord.getNewUserId().intValue()==user.getId().intValue()) {//审批人为我
					canPassBack=1;//拥有审批按钮
				}
			}else if (upgradeRecord.getSpStatus()==1) {//审批通过
				if (upgradeRecord.getNewUserId().intValue()==user.getId().intValue()) {//审批人为我
					canUp=1;//拥有升级按钮
				}
			}else if (upgradeRecord.getSpStatus()==2) {//审批驳回
				if (upgradeRecord.getOldUserId().intValue()==user.getId().intValue()&&issueList.getList().get(i).getInt("issue_status")==0) {//发起升级人为我
					canUp=1;//拥有升级按钮
				}
			}
			issueList.getList().get(i).set("can_up", canUp).set("can_pass_back", canPassBack).set("upgrade_reason", upgradeRecord.getUpgradeReason());
			//遏制措施 contain_s
			String contain_measures="";
			List<HQtyMeasures> containMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where issue_id="+issueId+" and type=2 and is_del=0");
			if (!containMeasures.isEmpty()) {
				for (int j = 0; j < containMeasures.size(); j++) {
					//遏制措施
					contain_measures=contain_measures+"遏制措施"+(j+1)+":"+containMeasures.get(j).getMeasures()+"-"+containMeasures.get(j).getUserName();
					int measureStatus=containMeasures.get(j).getStatus();
					if (measureStatus==1) {
						contain_measures=contain_measures+"-进行中-"+containMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						contain_measures=contain_measures+"-已完成-"+containMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						contain_measures=contain_measures+"-已关闭-"+containMeasures.get(j).getCloseDate();
					}
					contain_measures=contain_measures+"<br>";
					//遏制结果
					if(containMeasures.get(j).getContainResult()==null) {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":暂无"+"<br>";
					}else {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":"+containMeasures.get(j).getContainResult()+"<br>";
					}
				}
			}else {
				contain_measures="暂无遏制措施"+"<br>";
			}
			//临时措施
			String temporary_measures="";
			List<HQtyMeasures> temporaryMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where issue_id="+issueId+" and type=3 and is_del=0");
			if (!temporaryMeasures.isEmpty()) {
				for (int j = 0; j < temporaryMeasures.size(); j++) {
					//临时措施
					temporary_measures=temporary_measures+"临时措施"+(j+1)+":"+temporaryMeasures.get(j).getMeasures()+"-"+temporaryMeasures.get(j).getUserName();
					int measureStatus=temporaryMeasures.get(j).getStatus();
					if (measureStatus==1) {
						temporary_measures=temporary_measures+"-进行中-"+temporaryMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						temporary_measures=temporary_measures+"-已完成-"+temporaryMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						temporary_measures=temporary_measures+"-已关闭-"+temporaryMeasures.get(j).getCloseDate();
					}
					temporary_measures=temporary_measures+"<br>";
					//有效性跟踪
					List<HQtyTrackHistory> measureTrackRecord=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+temporaryMeasures.get(j).getId());
					if (measureTrackRecord.isEmpty()) {
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":暂无"+"<br>";
					}else {
						String measureTracks="";
						for (int k = 0; k < measureTrackRecord.size(); k++) {
							measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；";
						}
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"<br>";
					}
				}
			}else {
				temporary_measures="暂无临时措施";
			}
			String contain_s=contain_measures+temporary_measures;
			issueList.getList().get(i).set("contain_s", contain_s);
			//根本原因分析 root_s
			String happen_reason="";//产生原因
			String avoid_happen_measures="";//防产生措施
			List<HQtyIssueReasonAnalysis> happenAnalysis=HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
			if (!happenAnalysis.isEmpty()) {
				happen_reason="产生原因："+"<br>";
				avoid_happen_measures="防产生措施："+"<br>";
				for (int j = 0; j < happenAnalysis.size(); j++) {
					happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HQtyMeasures> avoidHappen=HQtyMeasures.dao.find("select * from h_qty_measures where reason_id="+happenAnalysis.get(j).getId()+" and is_del=0 and type=0");//对应防产生措施
					for (int k = 0; k < avoidHappen.size(); k++) {
						avoid_happen_measures=avoid_happen_measures+(j+1)+"."+(k+1)+avoidHappen.get(k).getMeasures()+"-"+avoidHappen.get(k).getUserName();
						int measureStatus=avoidHappen.get(k).getStatus();
						if (measureStatus==1) {
							avoid_happen_measures=avoid_happen_measures+"-进行中"+avoidHappen.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_happen_measures=avoid_happen_measures+"-已完成"+avoidHappen.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_happen_measures=avoid_happen_measures+"-已关闭"+avoidHappen.get(k).getCloseDate();
						}
						avoid_happen_measures=avoid_happen_measures+"<br>";
					}
				}
			}else {
				happen_reason="暂无产生原因"+"<br>";
				avoid_happen_measures="暂无防产生措施"+"<br>";
			}
			String runout_reason="";//流出原因
			String avoid_runout_measures="";//防流出措施
			List<HQtyIssueReasonAnalysis> runOutAnalysis=HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
			if (!runOutAnalysis.isEmpty()) {
				runout_reason="流出原因："+"<br>";
				avoid_runout_measures="防流出措施："+"<br>";
				for (int j = 0; j < runOutAnalysis.size(); j++) {
					runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HQtyMeasures> avoidRunout=HQtyMeasures.dao.find("select * from h_qty_measures where reason_id="+runOutAnalysis.get(j).getId()+" and is_del=0 and type=1");//对应防流出措施
					for (int k = 0; k < avoidRunout.size(); k++) {
						avoid_runout_measures=avoid_runout_measures+(j+1)+"."+(k+1)+avoidRunout.get(k).getMeasures()+"-"+avoidRunout.get(k).getUserName();
						int measureStatus=avoidRunout.get(k).getStatus();
						if (measureStatus==1) {
							avoid_runout_measures=avoid_runout_measures+"-进行中"+avoidRunout.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_runout_measures=avoid_runout_measures+"-已完成"+avoidRunout.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_runout_measures=avoid_runout_measures+"-已关闭"+avoidRunout.get(k).getCloseDate();
						}
						avoid_runout_measures=avoid_runout_measures+"<br>";
					}
				}
			}else {
				runout_reason="暂无流出原因";
				avoid_runout_measures="暂无防流出措施";
			}
			String root_s=happen_reason+runout_reason;
			String avoid_s=avoid_happen_measures+avoid_runout_measures;
			issueList.getList().get(i).set("root_s", root_s).set("avoid_s", avoid_s);
			//永久措施跟踪3月
			List<HQualityIssueMonthTrack> monthTracks=HQualityIssueMonthTrack.dao.find("select * from h_quality_issue_month_track where issue_id="+issueId);
			issueList.getList().get(i).set("month_1", "编辑").set("month_1_remark", "").set("month_1_date", "");
			issueList.getList().get(i).set("month_2", "编辑").set("month_2_remark", "").set("month_2_date", "");
			issueList.getList().get(i).set("month_3", "编辑").set("month_3_remark", "").set("month_3_date", "");
			String monthTrackString="";
			for (int j = 0; j < monthTracks.size(); j++) {
				int month=monthTracks.get(j).getMonth();
				if (month==1) {
					monthTrackString=monthTrackString+"第一月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_1", monthTracks.get(j).getStatus()).set("month_1_remark",  monthTracks.get(j).getRemark()).set("month_1_date",  monthTracks.get(j).getTrackDate());
				}else if (month==2) {
					monthTrackString=monthTrackString+"第二月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_2", monthTracks.get(j).getStatus()).set("month_2_remark", monthTracks.get(j).getRemark()).set("month_2_date",  monthTracks.get(j).getTrackDate());
				}else if (month==3) {
					monthTrackString=monthTrackString+"第三月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_3", monthTracks.get(j).getStatus()).set("month_3_remark", monthTracks.get(j).getRemark()).set("month_3_date",  monthTracks.get(j).getTrackDate());
				}
			}
			//OPL整体跟踪记录
			List<HQtyIssueTrackRecord> trackRecords=HQtyIssueTrackRecord.dao.find("select * from h_qty_issue_track_record where issue_id="+issueId);
			String  oplTrackRecord="";
			for (int j = 0; j < trackRecords.size(); j++) {
				oplTrackRecord=oplTrackRecord+(j+1)+"."+trackRecords.get(j).getTrackRecord()+"-"+trackRecords.get(j).getCreateUserName()+"-"+sdf1.format(trackRecords.get(j).getCreateTime())+"<br>";
			}
			String record=" ";
			if (!"".equals(oplTrackRecord)) {
				record=oplTrackRecord;
			}
			if (!"".equals(monthTrackString)) {
				record=record+monthTrackString;
			}
			issueList.getList().get(i).set("record", record);
			//升级对象钉钉id
			User upgradeUser=User.dao.findById(issueList.getList().get(i).getInt("upgrade_user_id"));
			issueList.getList().get(i).set("upgrade_user_id", upgradeUser.getDingUserId());
			//8D计划
			Record issuePlan = new Record();
			issuePlan.setColumns(issueList.getList().get(i));
			Record issueReal = new Record();
			issueReal.setColumns(issueList.getList().get(i));

			if ("C".equals(level)) {
				HQtyIssue8dPlan plans = HQtyIssue8dPlan.dao.findFirst("select * from h_qty_issue_8d_plan where issue_id=" + issuePlan.getLong("id"));
				issuePlan.set("plan_real", "计划")
						.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
						.set("d_three", plans.getDThree())
						.set("d_one_status", 0).set("d_two_status", 0)
						.set("d_three_status", 0);
				//8D实际
				HQtyIssue8dReal reals = HQtyIssue8dReal.dao.findFirst("select * from h_qty_issue_8d_real where issue_id=" + issueReal.getLong("id"));
				issueReal.set("plan_real", "实际")
						.set("d_one", "点击编辑").set("d_two", "点击编辑")
						.set("d_three", "点击编辑")
						.set("d_one_status", 0).set("d_two_status", 0)
						.set("d_three_status", 0);

				if (reals != null) {
					issuePlan
							.set("d_one_status", reals.getDOneStatus()).set("d_two_status", reals.getDTwoStatus())
							.set("d_three_status", reals.getDThreeStatus());
					issueReal
							.set("d_one_status", reals.getDOneStatus()).set("d_two_status", reals.getDTwoStatus())
							.set("d_three_status", reals.getDThreeStatus());
					if (reals.getDOneStatus() == 2) {
						issueReal.set("d_one", reals.getDOne());
					}
					if (reals.getDTwoStatus() == 2) {
						issueReal.set("d_two", reals.getDTwo());
					}
					if (reals.getDThreeStatus() == 2) {
						issueReal.set("d_three", reals.getDThree());
					}
				}
				MyCreateQtyIssueController.setThreeMonth(issuePlan, reals);
				qtyOPLList.add(issuePlan);
				qtyOPLList.add(issueReal);
			} else {
				HQtyIssue8dPlan plans = HQtyIssue8dPlan.dao.findFirst("select * from h_qty_issue_8d_plan where issue_id=" + issuePlan.getLong("id"));
				issuePlan.set("plan_real", "计划")
						.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
						.set("d_three", plans.getDThree()).set("d_four", plans.getDFour())
						.set("d_five", plans.getDFive()).set("d_six", plans.getDSix())
						.set("d_seven", plans.getDSeven())
						.set("d_one_status", 0).set("d_two_status", 0)
						.set("d_three_status", 0).set("d_four_status", 0)
						.set("d_five_status", 0).set("d_six_status", 0)
						.set("d_seven_status", 0);
				//8D实际
				HQtyIssue8dReal reals = HQtyIssue8dReal.dao.findFirst("select * from h_qty_issue_8d_real where issue_id=" + issueReal.getLong("id"));
				issueReal.set("plan_real", "实际")
						.set("d_one", "点击编辑").set("d_two", "点击编辑")
						.set("d_three", "点击编辑").set("d_four", "点击编辑")
						.set("d_five", "点击编辑").set("d_six", "点击编辑")
						.set("d_seven", "点击编辑")
						.set("d_one_status", 0).set("d_two_status", 0)
						.set("d_three_status", 0).set("d_four_status", 0)
						.set("d_five_status", 0).set("d_six_status", 0)
						.set("d_seven_status", 0);

				if (reals != null) {
					issuePlan
							.set("d_one_status", reals.getDOneStatus()).set("d_two_status", reals.getDTwoStatus())
							.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
							.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
							.set("d_seven_status", reals.getDSevenStatus());
					issueReal
							.set("d_one_status", reals.getDOneStatus()).set("d_two_status", reals.getDTwoStatus())
							.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
							.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
							.set("d_seven_status", reals.getDSevenStatus());
					if (reals.getDOneStatus() == 2) {
						issueReal.set("d_one", reals.getDOne());
					}
					if (reals.getDTwoStatus() == 2) {
						issueReal.set("d_two", reals.getDTwo());
					}
					if (reals.getDThreeStatus() == 2) {
						issueReal.set("d_three", reals.getDThree());
					}
					if (reals.getDFourStatus() == 2) {
						issueReal.set("d_four", reals.getDFour());
					}
					if (reals.getDFiveStatus() == 2) {
						issueReal.set("d_five", reals.getDFive());
					}
					if (reals.getDSixStatus() == 2) {
						issueReal.set("d_six", reals.getDSix());
					}
					if (reals.getDSevenStatus() == 2) {
						issueReal.set("d_seven", reals.getDSeven());
					}
				}
				MyCreateQtyIssueController.setThreeMonth(issuePlan, reals);
				qtyOPLList.add(issuePlan);
				qtyOPLList.add(issueReal);
			}
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", qtyOPLList);
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**
	 * 同意B升级A
	 */
	public void passUpgradeIssueToA() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("issueId");
				HQualityIssue qtyIssue=HQualityIssue.dao.findById(issueId);
				HQualityopllUpgradeRecord upgradeRecord=HQualityopllUpgradeRecord.dao.findFirst("select * from h_qualityopll_upgrade_record where status=0 and issue_id="+issueId);
				//升级次数及新的自动升级日期
				int upgradeNum=qtyIssue.getUpgradeNum()+1;
				qtyIssue
				.setIssueStatus(0).setLevel("A")
				.setUpgradeNum(upgradeNum);
				qtyIssue.update();
				//升级记录
				upgradeRecord.setSpStatus(1);
				upgradeRecord.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "升级成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 同意升级
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月19日 下午4:35:21
	 */
	@SuppressWarnings("rawtypes")
	public void passUpgradeIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				HQualityIssue qtyIssue=HQualityIssue.dao.findById(issueId);
				//相关人员
				String upgrade_user_id=map.get("upgrade_user_id").toString();
				User upUser=User.dao.findFirst("select * from user where ding_user_id='"+upgrade_user_id+"'");
				//严重度
				String level= qtyIssue.getLevel();
				String newLevel="";
				HQualityopllUpgradeRecord upgradeRecord=HQualityopllUpgradeRecord.dao.findFirst("select * from h_qualityopll_upgrade_record where status=0 and issue_id="+issueId);
				if (upgradeRecord.getUpgradeType()==0) {//手动升级
					newLevel=upgradeRecord.getLevel();
				}else {//自动升级
					if ("D".equals(level)) {
						newLevel="C";
					}else if ("C".equals(level)) {
						newLevel="B";
					}else if ("B".equals(level)) {
						newLevel="A";
					}
				}
				//升级次数及新的自动升级日期
				int upgradeNum=qtyIssue.getUpgradeNum()+1;
				qtyIssue.setUpgradeUserId(upUser.getId().intValue()).setUpgradeUserName(upUser.getNickname())
				.setIssueStatus(0).setLevel(newLevel)
				.setUpgradeNum(upgradeNum);
				qtyIssue.update();
				//升级记录
				upgradeRecord.setSpStatus(1);
				upgradeRecord.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "升级成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 驳回升级
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月19日 下午5:03:52
	 */
	public void backUpIssue() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("id");
				HQualityIssue qtyIssue=HQualityIssue.dao.findById(issueId);
				qtyIssue.setIssueStatus(0);
				qtyIssue.update();
				//升级记录
				HQualityopllUpgradeRecord upgradeRecord=HQualityopllUpgradeRecord.dao.findFirst("select * from h_qualityopll_upgrade_record where status=0 and issue_id="+issueId);
				upgradeRecord.setSpStatus(2);
				upgradeRecord.update();
				//钉钉消息通知升级对象
				User oldUpUser=User.dao.findById(upgradeRecord.getOldUserId().intValue());
				DingMessage.sendText(oldUpUser.getDingUserId(),"产品名称\n【"+qtyIssue.getProductName()+"】关联的质量OPL升级被驳回，请确认！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "15");
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "驳回成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}


	public void submitUpgradeLevel(){
		long issueId=getParaToLong("upgradeOPLId");
		String level = getPara("upgradeOPLLevel");

		boolean tx = Db.tx(() -> {
			int update = Db.update(" update h_quality_issue set level = '" + level + "'  where id = '" + issueId + "' ");
			return update == 1;
		});

		if(tx){
			renderJson(Ret.ok("msg", "修改成功！"));
		}else{
			renderJson(Ret.fail("msg", "修改失败，请联系管理员！"));
		}
	}
}
