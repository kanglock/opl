package com.ray.controller.admin.QualityIssue;

import java.text.SimpleDateFormat;
import java.util.Date;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.*;

/**
 * 对接计划仓库质量问题 getWMSissue
 * @author FL00024996
 *
 */
public class GetWMSissueController extends Controller {
	/**
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月31日 上午9:57:46
	 */
	public void getWMSdata() {
		Db.tx(()->{
			String jsonString = HttpKit.readData(getRequest());
			JSONObject jsonObject = JSONObject.parseObject(jsonString);
			String dep_name=jsonObject.getString("dep_name");
			HCauseDep depInfo=HCauseDep.dao.findFirst("select * from h_cause_dep where dep_name='"+dep_name+"' and status=0");
			if (depInfo==null) {
				renderJson(Ret.fail("msg","事业部不存在！"));
				return false;
			}
			String part_no=jsonObject.getString("part_no");
			String part_name=jsonObject.getString("part_name");
			HParts partInfo=HParts.dao.findFirst("select * from h_parts where status=0 and part_no='"+part_no+"'");
			if (partInfo==null) {
				partInfo=new HParts();
				partInfo.setPartNo(part_no).setPartName(part_name);
				partInfo.save();
			}
			String dutyUserId=jsonObject.getString("duty_user_id");
			User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+dutyUserId+"'");
			if (dutyUser==null) {
				renderJson(Ret.fail("msg","责任人不存在！"));
				return false;
			}
			String upgradeUserId=jsonObject.getString("upgrade_user_id");
			User upgradeUser=User.dao.findFirst("select * from user where ding_user_id='"+upgradeUserId+"'");
			if (upgradeUser==null) {
				renderJson(Ret.fail("msg","升级对象不存在！"));
				return false;
			}
			String createUserId=jsonObject.getString("create_user_id");
			User createUser=User.dao.findFirst("select * from user where ding_user_id='"+createUserId+"'");
			if (createUser==null) {
				renderJson(Ret.fail("msg","创建人不存在！"));
				return false;
			}
			String sureUserId=jsonObject.getString("sure_user_id");
			User sureUser=User.dao.findFirst("select * from user where ding_user_id='"+sureUserId+"'");
			if (sureUser==null) {
				renderJson(Ret.fail("msg","跟踪人不存在！"));
				return false;
			}
			Date openTime=jsonObject.getDate("open_time");
			Date dOneDate=com.ray.util.HyCommenMethods.addDate(openTime, 1);
			// TODO 在这里需要判断product_stage 是否是手工样件  如果是就到工装OPL  不是就到质量OPL
			String product_stage=jsonObject.getString("product_stage");
			boolean save = false;
			boolean save1 = false;
			if("手工样件".equals(product_stage)){
				HClothesIssue qualityIssue=new HClothesIssue();
				qualityIssue.setDepId(depInfo.getDepId())
						.setPartId(partInfo.getId().intValue()).setProductName(partInfo.getPartName())
						.setStatus("Y")
						.setReportNo("NA")
						.setLevel(jsonObject.getString("level"))
						.setCusName(jsonObject.getString("cus_name"))
						.setProcedure(jsonObject.getString("procedure"))
						.setSupplierName(jsonObject.getString("supplier_name"))
						.setTypeId(7)//默认供应商类
						.setDescription(jsonObject.getString("description"))
						.setSource("入厂检测")//默认入厂检测
						.setOpenTime(openTime)
						.setHappenFrequency(jsonObject.getString("happen_frequency"))
						.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
						.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
						.setCreateUserId(createUser.getId().intValue()).setCreateUserName(createUser.getNickname())
						.setSureUserId(sureUser.getId().intValue()).setSureUserName(sureUser.getNickname())
						.setCreateTime(new Date())
						.setIsDel(0).setIssueStatus(0)
						.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dOneDate,3))
						.setProductStage(product_stage);
				save = qualityIssue.save();
				DingMessage.sendText(dutyUserId, "有新的手工样件OPL创建并需要你负责，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "13");//13-质量OPL措施责任人页面
				//7D时间
				HClothesIssue8dPlan dayPlan=new HClothesIssue8dPlan();
				dayPlan.setIssueId(qualityIssue.getId())
						.setDOne(com.ray.util.HyCommenMethods.addDate(openTime, 1))
						.setDTwo(com.ray.util.HyCommenMethods.addDate(openTime, 1))
						.setDThree(com.ray.util.HyCommenMethods.addDate(openTime, 2))
						.setDFour(com.ray.util.HyCommenMethods.addDate(openTime, 7))
						.setDFive(com.ray.util.HyCommenMethods.addDate(openTime, 14));
				save1 = dayPlan.save();
			}else{
				HQualityIssue qualityIssue=new HQualityIssue();
				qualityIssue.setDepId(depInfo.getDepId())
						.setPartId(partInfo.getId().intValue()).setProductName(partInfo.getPartName())
						.setStatus("Y")
						.setReportNo("NA")
						.setLevel(jsonObject.getString("level"))
						.setCusName(jsonObject.getString("cus_name"))
						.setProcedure(jsonObject.getString("procedure"))
						.setSupplierName(jsonObject.getString("supplier_name"))
						.setTypeId(7)//默认供应商类
						.setDescription(jsonObject.getString("description"))
						.setSource("入厂检测")//默认入厂检测
						.setOpenTime(openTime)
						.setHappenFrequency(jsonObject.getString("happen_frequency"))
						.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
						.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
						.setCreateUserId(createUser.getId().intValue()).setCreateUserName(createUser.getNickname())
						.setSureUserId(sureUser.getId().intValue()).setSureUserName(sureUser.getNickname())
						.setCreateTime(new Date())
						.setIsDel(0).setIssueStatus(0)
						.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dOneDate,3))
						.setProductStage(product_stage);
				save = qualityIssue.save();
				DingMessage.sendText(dutyUserId, "有新的质量OPL创建并需要你负责，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "13");//13-质量OPL措施责任人页面
				//7D时间
				HQtyIssue8dPlan dayPlan=new HQtyIssue8dPlan();
				dayPlan.setIssueId(qualityIssue.getId())
						.setDOne(com.ray.util.HyCommenMethods.addDate(openTime, 1))
						.setDTwo(com.ray.util.HyCommenMethods.addDate(openTime, 1))
						.setDThree(com.ray.util.HyCommenMethods.addDate(openTime, 2))
						.setDFour(com.ray.util.HyCommenMethods.addDate(openTime, 7))
						.setDFive(com.ray.util.HyCommenMethods.addDate(openTime, 14));
				save1 = dayPlan.save();
			}
			if ( save && save1 ){
				renderJson(Ret.ok("msg","操作成功！"));
			}else{
				renderJson(Ret.fail("msg","操作失败，请联系管理员"));
			}
			return true;
		});
	}
}
