package com.ray.controller.admin.QualityIssue;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.hutool.crypto.SecureUtil;
import com.ray.common.model.*;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import com.alibaba.fastjson.JSONArray;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * 我创建的质量问题 myMreateQtyOPL
 * @author FL00024996
 *
 */
public class MyCreateQtyIssueController extends Controller {

	public void toMyCreateQtyOPL() {
		render("myCreateQualityOPL.html");
	}
	public void getMyCreateQtyOPL() {
		Record loginUser=getSessionAttr("user");
		User user=User.dao.findById(loginUser.getInt("id"));
		StringBuilder sb=new StringBuilder();
		int loginUserId=loginUser.getInt("id");
		UserRole userRole = UserRole.dao.findFirst("select a.* from user_role a, roles b where a.user_id="+loginUserId+" and a.role_id=b.id and (b.role_name='zhiliang_tixi' or b.role_name='admin')");
		if (userRole==null) {
			sb.append(" from h_quality_issue a left join h_quality_issue_type f on a.supplier_type_id = f.id  ,h_cause_dep b,h_parts d,h_quality_issue_type e where a.status<>'G' and "
					+ "a.dep_id=b.dep_id and a.is_del=0 and a.create_user_id="+user.getId()+" and a.part_id=d.id and a.type_id=e.id ");
		}else {
			sb.append(" from h_quality_issue a left join h_quality_issue_type f on a.supplier_type_id = f.id  ,h_cause_dep b,h_parts d,h_quality_issue_type e where a.status<>'G' and "
					+ "a.dep_id=b.dep_id and a.is_del=0 and a.part_id=d.id and a.type_id=e.id  ");
		}
		String selLevel=get("selLevel");
		if (!"".equals(selLevel)&& selLevel!=null) {
			sb.append(" and a.level='"+selLevel+"'");
		}
		long selDep=getParaToLong("selDep");
		if (selDep!=0) {
			sb.append(" and a.dep_id="+selDep);
		}
		int selWarehouse=getParaToInt("selWarehouse");
		if (selWarehouse!=0) {
			sb.append(" and a.ware_id="+selWarehouse);
		}
		String selOPLdes=get("selOPLdes");
		if (!"".equals(selOPLdes)&& selOPLdes!=null) {
			sb.append(" and a.description like '%"+selOPLdes+"%'");
		}
		String selPartCode=get("selPartCode");
		if (!"".equals(selPartCode)&& selPartCode!=null) {
			sb.append(" and d.part_no like '%"+selPartCode+"%'");
		}
		String selProductName=get("selProductName");
		if (!"".equals(selProductName)&& selProductName!=null) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		String selCusName=get("selCusName");
		if (!"".equals(selCusName)&& selCusName!=null) {
			sb.append(" and a.cus_name like '%"+selCusName+"%'");
		}
		String selStatus=get("selStatus");
		if (!"".equals(selStatus)&& selStatus!=null) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		String selFrequency=get("selFrequency");
		if (!"".equals(selFrequency)&& selFrequency!=null) {
			sb.append(" and a.happen_frequency='"+selFrequency+"'");
		}
		if (!"".equals(get("selType"))&& get("selType")!=null) {
			sb.append(" and a.type_id="+get("selType"));
		}
		String selSource=get("selSource");
		if (!"".equals(selSource)&& selSource!=null) {
			sb.append(" and a.source='"+selSource+"'");
		}
		String selProductStage=get("selProductStage");
		if (!"".equals(selProductStage)&& selProductStage!=null) {
			sb.append(" and a.product_stage='"+selProductStage+"'");
		}
		String selNextReviewTime=get("selNextReviewTime");
		if (!"".equals(selNextReviewTime)&& selNextReviewTime!=null) {
			sb.append(" and a.next_review_time='"+selNextReviewTime+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name,d.part_no,e.type,f.type supplier_type ",sb.toString()+" order by id desc");
		List<Record> qtyOPLList=new ArrayList<Record>();
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			issueList.getList().get(i).set("caozuo", " ");
			//获取知会人
			List<HQualityIssueTell> tells=HQualityIssueTell.dao.find("select * from h_quality_issue_tell where issue_id="+issueId+" and status=0");
			List<Integer> tellUserId=new ArrayList<Integer>();
			String tellUserName=" ";
			for (int j = 0; j < tells.size(); j++) {
				tellUserId.add(tells.get(j).getTellUserId());
				tellUserName=tellUserName+tells.get(j).getTellUserName()+"，";
			}
			issueList.getList().get(i)
			.set("tell_users_name",tellUserName)
			.set("tell_users_id", tellUserId);
			//车间
			if (issueList.getList().get(i).get("ware_id")!=null) {
				HWare ware=HWare.dao.findById(issueList.getList().get(i).getInt("ware_id"));
				issueList.getList().get(i).set("ware_name", ware.getWareName());
			}else {
				issueList.getList().get(i).set("ware_name", "暂未维护");
			}
			//遏制措施 contain_s
			String contain_measures="";
			List<HQtyMeasures> containMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where issue_id="+issueId+" and type=2 and is_del=0");
			if (!containMeasures.isEmpty()) {
				for (int j = 0; j < containMeasures.size(); j++) {
					//遏制措施
					contain_measures=contain_measures+"遏制措施"+(j+1)+":"+containMeasures.get(j).getMeasures()+"-"+containMeasures.get(j).getUserName();
					int measureStatus=containMeasures.get(j).getStatus();
					if (measureStatus==1) {
						contain_measures=contain_measures+"-进行中-"+containMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						contain_measures=contain_measures+"-已完成-"+containMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						contain_measures=contain_measures+"-已关闭-"+containMeasures.get(j).getCloseDate();
					}
					contain_measures=contain_measures+"<br>";
					//遏制结果
					if(containMeasures.get(j).getContainResult()==null) {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":暂无"+"<br>";
					}else {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":"+containMeasures.get(j).getContainResult()+"<br>";
					}
					//有效性跟踪
					List<HQtyTrackHistory> measureTrackRecord=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+containMeasures.get(j).getId());
					if (measureTrackRecord.isEmpty()) {
						contain_measures=contain_measures+"有效性跟踪"+(j+1)+":暂无"+"<br>";
					}else {
						String measureTracks="";
						for (int k = 0; k < measureTrackRecord.size(); k++) {
							measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；;";
						}
						contain_measures=contain_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"<br>";
					}
				}
			}else {
				contain_measures="暂无遏制措施"+"<br>";
			}
			//临时措施
			String temporary_measures="";
			List<HQtyMeasures> temporaryMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where issue_id="+issueId+" and type=3 and is_del=0");
			if (!temporaryMeasures.isEmpty()) {
				for (int j = 0; j < temporaryMeasures.size(); j++) {
					//临时措施
					temporary_measures=temporary_measures+"临时措施"+(j+1)+":"+temporaryMeasures.get(j).getMeasures()+"-"+temporaryMeasures.get(j).getUserName();
					int measureStatus=temporaryMeasures.get(j).getStatus();
					if (measureStatus==1) {
						temporary_measures=temporary_measures+"-进行中-"+temporaryMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						temporary_measures=temporary_measures+"-已完成-"+temporaryMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						temporary_measures=temporary_measures+"-已关闭-"+temporaryMeasures.get(j).getCloseDate();
					}
					temporary_measures=temporary_measures+"<br>";
					//有效性跟踪
					List<HQtyTrackHistory> measureTrackRecord=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+temporaryMeasures.get(j).getId());
					if (measureTrackRecord.isEmpty()) {
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":暂无"+"<br>";
					}else {
						String measureTracks="";
						for (int k = 0; k < measureTrackRecord.size(); k++) {
							measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；;";
						}
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"<br>";
					}
				}
			}else {
				temporary_measures="暂无临时措施";
			}
			String contain_s=contain_measures+temporary_measures;
			issueList.getList().get(i).set("contain_s", contain_s);
			//根本原因分析 root_s
			String happen_reason="";//产生原因
			String avoid_happen_measures="";//防产生措施
			List<HQtyIssueReasonAnalysis> happenAnalysis=HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
			if (!happenAnalysis.isEmpty()) {
				happen_reason="产生原因："+"<br>";
				avoid_happen_measures="防产生措施："+"<br>";
				for (int j = 0; j < happenAnalysis.size(); j++) {
					happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HQtyMeasures> avoidHappen=HQtyMeasures.dao.find("select * from h_qty_measures where reason_id="+happenAnalysis.get(j).getId()+" and is_del=0 and type=0");//对应防产生措施
					for (int k = 0; k < avoidHappen.size(); k++) {
						avoid_happen_measures=avoid_happen_measures+(j+1)+"."+(k+1)+avoidHappen.get(k).getMeasures()+"-"+avoidHappen.get(k).getUserName()	;
						int measureStatus=avoidHappen.get(k).getStatus();
						if (measureStatus==1) {
							avoid_happen_measures=avoid_happen_measures+"-进行中"+avoidHappen.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_happen_measures=avoid_happen_measures+"-已完成"+avoidHappen.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_happen_measures=avoid_happen_measures+"-已关闭"+avoidHappen.get(k).getCloseDate();
						}
						avoid_happen_measures=avoid_happen_measures+"<br>";
						// TODO 需要在 防产生措施  后添加防产生措施的跟踪纪律
						String sql = "select IFNULL(GROUP_CONCAT(remark,';'),'') remark  from h_qty_track_history where measure_id= '"+avoidHappen.get(k).get("id")+"'";
						Record first = Db.findFirst(sql);
						if(!"".equals(first.get("remark"))){
							int z = k+1;
							avoid_happen_measures += "跟踪记录"+z+"："+first.get("remark")+"<br>";
						}
					}
				}
			}else {
				happen_reason="暂无产生原因"+"<br>";
				avoid_happen_measures="暂无防产生措施"+"<br>";
			}
			String runout_reason="";//流出原因
			String avoid_runout_measures="";//防流出措施
			List<HQtyIssueReasonAnalysis> runOutAnalysis=HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
			if (!runOutAnalysis.isEmpty()) {
				runout_reason="流出原因："+"<br>";
				avoid_runout_measures="防流出措施："+"<br>";
				for (int j = 0; j < runOutAnalysis.size(); j++) {
					runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HQtyMeasures> avoidRunout=HQtyMeasures.dao.find("select * from h_qty_measures where reason_id="+runOutAnalysis.get(j).getId()+" and is_del=0 and type=1");//对应防流出措施
					for (int k = 0; k < avoidRunout.size(); k++) {
						avoid_runout_measures=avoid_runout_measures+(j+1)+"."+(k+1)+avoidRunout.get(k).getMeasures()+"-"+avoidRunout.get(k).getUserName();
						int measureStatus=avoidRunout.get(k).getStatus();
						if (measureStatus==1) {
							avoid_runout_measures=avoid_runout_measures+"-进行中"+avoidRunout.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_runout_measures=avoid_runout_measures+"-已完成"+avoidRunout.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_runout_measures=avoid_runout_measures+"-已关闭"+avoidRunout.get(k).getCloseDate();
						}
						avoid_runout_measures=avoid_runout_measures+"<br>";
						// TODO 需要在 防产生措施  后添加防产生措施的跟踪纪律
						String sql = "select IFNULL(GROUP_CONCAT(remark,';'),'') remark  from h_qty_track_history where measure_id= '"+avoidRunout.get(k).get("id")+"'";
						Record first = Db.findFirst(sql);
						if(!"".equals(first.get("remark"))){
							int z = k+1;
							avoid_runout_measures += "跟踪记录"+z+"："+first.get("remark")+"<br>";
						}
					}
				}
			}else {
				runout_reason="暂无流出原因";
				avoid_runout_measures="暂无防流出措施";
			}
			String root_s=happen_reason+runout_reason;
			String avoid_s=avoid_happen_measures+avoid_runout_measures;
			issueList.getList().get(i).set("root_s", root_s).set("avoid_s", avoid_s);
			//永久措施跟踪3月
			List<HQualityIssueMonthTrack> monthTracks=HQualityIssueMonthTrack.dao.find("select * from h_quality_issue_month_track where issue_id="+issueId);
			issueList.getList().get(i).set("month_1", " ").set("month_1_remark", "").set("month_1_date", "");
			issueList.getList().get(i).set("month_2", " ").set("month_2_remark", "").set("month_2_date", "");
			issueList.getList().get(i).set("month_3", " ").set("month_3_remark", "").set("month_3_date", "");
			String monthTrackString="";
			for (int j = 0; j < monthTracks.size(); j++) {
				int month=monthTracks.get(j).getMonth();
				if (month==1) {
					monthTrackString=monthTrackString+"第一月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_1", monthTracks.get(j).getStatus()).set("month_1_remark",  monthTracks.get(j).getRemark()).set("month_1_date",  monthTracks.get(j).getTrackDate());
				}else if (month==2) {
					monthTrackString=monthTrackString+"第二月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_2", monthTracks.get(j).getStatus()).set("month_2_remark", monthTracks.get(j).getRemark()).set("month_2_date",  monthTracks.get(j).getTrackDate());
				}else if (month==3) {
					monthTrackString=monthTrackString+"第三月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_3", monthTracks.get(j).getStatus()).set("month_3_remark", monthTracks.get(j).getRemark()).set("month_3_date",  monthTracks.get(j).getTrackDate());
				}
			}
			//OPL整体跟踪记录
			List<HQtyIssueTrackRecord> trackRecords=HQtyIssueTrackRecord.dao.find("select * from h_qty_issue_track_record where issue_id="+issueId);
			String  oplTrackRecord="";
			for (int j = 0; j < trackRecords.size(); j++) {
				oplTrackRecord=oplTrackRecord+(j+1)+"."+trackRecords.get(j).getTrackRecord()+"-"+trackRecords.get(j).getCreateUserName()+"-"+sdf1.format(trackRecords.get(j).getCreateTime())+"<br>";
			}
			String record=" ";
			if (!"".equals(oplTrackRecord)) {
				record=oplTrackRecord;
			}
			if (!"".equals(monthTrackString)) {
				record=record+monthTrackString;
			}
			issueList.getList().get(i).set("record", record);
			//8D计划
			Record issuePlan=new Record();
			issuePlan.setColumns(issueList.getList().get(i));
			Record issueReal=new Record();
			issueReal.setColumns(issueList.getList().get(i));
			HQtyIssue8dPlan plans=HQtyIssue8dPlan.dao.findFirst("select * from h_qty_issue_8d_plan where issue_id="+issuePlan.getLong("id"));
			issuePlan.set("plan_real", "计划")
			.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
			.set("d_three", plans.getDThree()).set("d_four", plans.getDFour())
			.set("d_five", plans.getDFive()).set("d_six", plans.getDSix())
			.set("d_seven", plans.getDSeven())
			.set("d_one_status", 0).set("d_two_status",0)
			.set("d_three_status", 0).set("d_four_status", 0)
			.set("d_five_status", 0).set("d_six_status", 0)
			.set("d_seven_status", 0);
			//8D实际
			HQtyIssue8dReal reals=HQtyIssue8dReal.dao.findFirst("select * from h_qty_issue_8d_real where issue_id="+issueReal.getLong("id"));
			issueReal.set("plan_real", "实际")
			.set("d_one", " ").set("d_two", " ")
			.set("d_three", " ").set("d_four", " ")
			.set("d_five", " ").set("d_six", " ")
			.set("d_seven", " ")
			.set("d_one_status", 0).set("d_two_status",0)
			.set("d_three_status", 0).set("d_four_status", 0)
			.set("d_five_status", 0).set("d_six_status", 0)
			.set("d_seven_status", 0);
			if (reals!=null) {
				issuePlan
				.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
				.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
				.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
				.set("d_seven_status", reals.getDSevenStatus());
				issueReal
				.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
				.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
				.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
				.set("d_seven_status", reals.getDSevenStatus());
				if (reals.getDOneStatus()==2) {
					issueReal.set("d_one", reals.getDOne());
				}
				if (reals.getDTwoStatus()==2) {
					issueReal.set("d_two", reals.getDTwo());
				}
				if (reals.getDThreeStatus()==2) {
					issueReal.set("d_three", reals.getDThree());
				}
				if (reals.getDFourStatus()==2) {
					issueReal.set("d_four", reals.getDFour());
				}
				if (reals.getDFiveStatus()==2) {
					issueReal.set("d_five", reals.getDFive());
				}
				if (reals.getDSixStatus()==2) {
					issueReal.set("d_six", reals.getDSix());
				}
				if (reals.getDSevenStatus()==2) {
					issueReal.set("d_seven", reals.getDSeven());
					Date dSeven = reals.getDSeven();
				}
			}
			setThreeMonth(issuePlan,reals);
			qtyOPLList.add(issuePlan);
			qtyOPLList.add(issueReal);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", qtyOPLList);
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}


	public static void setThreeMonth(Record issuePlan, HQtyIssue8dReal real){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		if(real != null&&real.getDSeven() != null){
			Date dSeven = real.getDSeven();
			calendar.setTime(dSeven);
			calendar.add(Calendar.MONTH, 1);
			String date1 = sdf.format(calendar.getTime());
			issuePlan.set("month_1",date1);
			calendar.add(Calendar.MONTH, 1);
			String date2 = sdf.format(calendar.getTime());
			issuePlan.set("month_2",date2);
			calendar.add(Calendar.MONTH, 1);
			String date3 = sdf.format(calendar.getTime());
			issuePlan.set("month_3",date3);
		}else{
			issuePlan.set("month_1","暂无");
			issuePlan.set("month_2","暂无");
			issuePlan.set("month_3","暂无"
			);
		}
	}

	/**
	 * 指向新增页面
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月14日 下午4:42:22
	 */
	public void toAddQtyOPL() {
		long issueId=getParaToLong("issueid");
		set("issueid", issueId);
		render("addEditQtyIssue.html");
	}
	/**
	 * 获取质量OPL编辑详情
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月18日 下午8:01:46
	 */
	public void getIssueInfo() {
		try {
			long issueId=getParaToLong("issueId");
			HQualityIssue issue=HQualityIssue.dao.findById(issueId);
			//对应用户钉钉id
			User upgradeUser=User.dao.findById(issue.getUpgradeUserId());
			User dutyUser=User.dao.findById(issue.getDutyUserId());
			User sureUser=User.dao.findById(issue.getSureUserId());
			User zjUser=User.dao.findById(issue.getZjUserId());

			String sure_user_id="";
			if (sureUser!=null) {
				sure_user_id=sureUser.getDingUserId();
			}
			//知会人
			List<HQualityIssueTell> tells=HQualityIssueTell.dao.find("select * from h_quality_issue_tell where issue_id="+issueId+" and status=0");
			String tellUserIds="";
			String tellUserName="";
			List<String> selTellUserIds=new ArrayList<String>();
			for (int j = 0; j < tells.size(); j++) {
				User tellUser=User.dao.findById(tells.get(j).getTellUserId());
				tellUserIds=tellUserIds+tellUser.getDingUserId()+",";
				tellUserName=tellUserName+tellUser.getNickname()+",";
				selTellUserIds.add(tellUser.getDingUserId());
			}
			Record record=new Record();
			record.set("issue", issue)
			.set("upgrade_user_id",upgradeUser.getDingUserId())
			.set("duty_user_id",dutyUser.getDingUserId())
			.set("sure_user_id",sure_user_id)
			.set("tell_users_name",tellUserName)
			.set("tell_users_id", tellUserIds)
			.set("sel_tell_user_ids", selTellUserIds);
			if(zjUser != null){
				record.set("zj_user_id",zjUser.getDingUserId());
			}
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}

	/**
	 * 当问题分类为供应商包装类
	 * 插入包装系统
	 *
	 * @param map*/
	public boolean addPackSystem(Map map, JSONArray fujianArray) {
		if(Optional.ofNullable(map.get("type_id")).isPresent()){
            String type_id = map.get("type_id").toString();
            Record first = Db.findFirst("select type from h_quality_issue_type where id = '" + type_id + "' ");
            String type = first.get("type").toString();
            if ("供应商包装类".equals(type)) {
                Record user = (Record) getSessionAttr("user");
                String dep_name = Db.findFirst(" select dep_name from  h_cause_dep where dep_id = '" + map.get("dep_id").toString() + "' ").get("dep_name").toString();
                Record pack = Db.use("pack").findFirst("select id,name from department where 	name = '" + dep_name + "' ");
                Integer dept_no = pack.get("id");//部门id
                String dept = pack.get("name");//部门名称
                String zccode = map.get("zccode").toString();//总成号
                String part_no = Optional.ofNullable(map.get("element_id")).orElse("").toString();//零件号
                String part_name =Optional.ofNullable(map.get("element_name")).orElse("").toString();//零件名称
                String question_level = map.get("level").toString();//问题等级
                String question_source = map.get("source").toString();//问题来源
                String happen_frequency = map.get("happen_frequency").toString();//发生频次
                String open_time = map.get("open_time").toString();//打开时间
                String description = map.get("description").toString();//问题描述
                String supplier_name = map.get("supplier_name").toString();//供应商名称
                String supplier_id = map.get("supplier_id").toString();//供应商id

                //判断是否存在供应商
                String supplier_no = isExist(supplier_name,supplier_id);
                String create_user = user.getStr("nickname");//姓名
				Record packUser = Db.use("pack").findFirst("select id from user where nickname = '" + create_user + "' ");
				if(packUser == null){
					//新增用户表
					String create_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
					String newUser = "insert into user (username,password,nickname,ding_user_id,update_time,create_time,job_num,supplier_id) " +
							"values ('"+user.getStr("username")+"','"+ SecureUtil.md5(user.getStr("password")) +"','"+user.getStr("nickname")+"','"+user.getStr("ding_user_id")+"', " +
							" '"+create_time+"','"+create_time+"','"+user.getStr("job_num")+"','0')";
					int update = Db.use("pack").update(newUser);
					if (update != 0){
						packUser = Db.use("pack").findFirst("select id from user where nickname = '" + create_user + "' ");
					}
				}
				String create_userid = packUser.get("id").toString();//用户id
                String create_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());//创建时间
                String sql ="";
                // 只能使用sql 进行插入数据  判断非合格附件
                if(fujianArray.size() != 0){
                    String add_file = fujianArray.getJSONObject(0).get("name").toString();
                    sql = " insert into  h_supplier_question (dept_no,dept,zccode,part_no,part_name,question_level,question_source,happen_frequency,open_time,description," +
                            " supplier_name,supplier_no,create_time,create_userid,create_user ,add_file)  values " +
                            "( '"+dept_no+"','"+dept+"','"+zccode+"','"+part_no+"','"+part_name+"','"+question_level+"','"+question_source+"','"+happen_frequency+"', " +
                            " '"+open_time+"','"+description+"' ,'"+supplier_name+"' ,'"+supplier_no+"','"+create_time+"','"+create_userid+"','"+create_user+"','"+add_file+"' ) ";
                }else{
                    sql = " insert into  h_supplier_question (dept_no,dept,zccode,part_no,part_name,question_level,question_source,happen_frequency,open_time,description," +
                            " supplier_name,supplier_no,create_time,create_userid,create_user )  values " +
                            "( '"+dept_no+"','"+dept+"','"+zccode+"','"+part_no+"','"+part_name+"','"+question_level+"','"+question_source+"','"+happen_frequency+"', " +
                            " '"+open_time+"','"+description+"' ,'"+supplier_name+"' ,'"+supplier_no+"','"+create_time+"','"+create_userid+"','"+create_user+"' ) ";
                }
                int is = Db.use("pack").update(sql);
                if (is != 0) {
					Record record = Db.use("pack").findFirst(" select  id from h_supplier_question where create_time = '" + create_time + "' ");
					Record nickname = Db.findFirst("select role_nick_name from user u, user_role ur,roles r  where u.id = ur.user_id and ur.role_id = r.id and  u.nickname = '" + user.getStr("nickname") + "'");
					Db.use("pack").update(" insert into h_supplier_history (action_id,username,role,action,actiontime,type) values ('"+record.get("id")+"', '"+user.getStr("nickname")+"','"+nickname.get("role_nick_name")+"','创建任务','"+create_time+"','1') ");
					return true;
				}
			}
		}
        return false;
    }

	//判断供应商是否存在
	private String isExist(String supplier_name, String supplier_id) {
		List<Record> records = Db.use("pack").find("select * from h_supplier where supplier_name = '" + supplier_name + "'  ");
		String supplier_no = "";
		if (records.size()!= 0 ){
			supplier_no = records.get(0).get("id").toString();
		}else{
			//新增供应商表
			String newSupplier = "insert into h_supplier (supplier_name,supplier_no) values ('"+supplier_name+"','"+supplier_id+"') ";
			int pack = Db.use("pack").update(newSupplier);
			if (pack != 0) {
				Record record = Db.use("pack").findFirst("select id from h_supplier where supplier_name = '" + supplier_name + "'");
				String id = record.get("id").toString();
				//新增用户表
				String create_time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
				String newSupplierToUser = "insert into user (username,password,nickname,ding_user_id,update_time,create_time,job_num,supplier_id) " +
						"values ('"+supplier_id+"','"+SecureUtil.md5(supplier_id)+"','"+supplier_name+"','"+supplier_id+"','"+create_time+"','"+create_time+"','"+supplier_id+"','"+id+"')";
				int is = Db.use("pack").update(newSupplierToUser);
				if (is != 0){
					String sql = "select id from user where update_time = '"+create_time+"' ";
					Record first = Db.use("pack").findFirst(sql);
					String userid = first.get("id").toString();
					Db.use("pack").update(" insert into user_role (user_id,role_id) values ('"+userid+"',34) ");
				}
				supplier_no = id;
			}
		}
		return supplier_no;
	}

	/**
	 * 新增质量问题
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月15日 下午2:55:42
	 */
	@SuppressWarnings("rawtypes")
	public void addQtyIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				JSONArray files=JSONArray.parseArray(getPara("fileList"));
				boolean b = addPackSystem(map,files);
				if (b){
					renderJson(Ret.fail("msg", "新增质量OPL失败，请联系管理员！"));
				}
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				Record loginUser=getSessionAttr("user");
				User login=User.dao.findById(loginUser.getInt("id"));
				HQualityIssue qtyIssue=new HQualityIssue();
				String upgrade_user_id=map.get("upgrade_user_id").toString();
				User upgradeUser=User.dao.findFirst("select * from user where ding_user_id='"+upgrade_user_id+"'");
				String duty_user_id=map.get("duty_user_id").toString();
				User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+duty_user_id+"'");
				String sure_user_id=map.get("sure_user_id").toString();
				User sureUser=User.dao.findFirst("select * from user where ding_user_id='"+sure_user_id+"'");
				String zj_user_id=map.get("zj_user_id").toString();
				User zjUser=User.dao.findFirst("select * from user where ding_user_id='"+zj_user_id+"'");

				String productStage=map.get("product_stage").toString();
				try {
					//附件
					JSONArray fujianArray=JSONArray.parseArray(getPara("fileList"));
					long unQualifyFujianId=0;
					if (fujianArray.size()>0) {
						unQualifyFujianId=fujianArray.getJSONObject(0).getLongValue("id");
					}
					Date openTime=sdf.parse(map.get("open_time").toString());
					Date dOneDate=com.ray.util.HyCommenMethods.addDate(openTime, 1);
					Integer part_id = Db.findFirst(" select id from h_parts where part_no = '" + map.get("part_id").toString() + "' ").getInt("id");
					//新增OPL
					qtyIssue
					.setDepId(Long.valueOf(map.get("dep_id").toString()))
					.setWareId(Integer.valueOf(map.get("ware_id").toString()))
					.setPartId(part_id)
					.setStatus("Y")
					.setReportNo("NA")
					.setLevel(map.get("level").toString())
					.setProductName(map.get("product_name").toString())
					.setCusName(map.get("cus_name").toString())
					.setProcedure(map.get("procedure").toString())
					.setSupplierName(Optional.ofNullable(map.get("supplier_name")).orElse("").toString())
					.setTypeId(Integer.valueOf(map.get("type_id").toString()))
					.setSupplierTypeId(map.get("suppliertype_id") != null ?Integer.valueOf(map.get("suppliertype_id").toString()):0)
					.setDescription(map.get("description").toString())
					.setSource(map.get("source").toString())
					.setProductStage(productStage)
					.setOpenTime(sdf.parse(map.get("open_time").toString()))
					.setHappenFrequency(map.get("happen_frequency").toString())
					.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
					.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
					.setCreateUserId(login.getId()).setCreateUserName(login.getNickname())
					.setSureUserId(sureUser.getId().intValue()).setSureUserName(sureUser.getNickname())
					.setZjUserId(zjUser.getId().toString()).setZjUserName(zjUser.getNickname())
					.setCreateTime(new Date())
					.setIsDel(0).setIssueStatus(0)
					.setUnqualifyFujianId(unQualifyFujianId)
					.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dOneDate,3))//默认定时任务执行日期
							//增加字段  零件号  零件id
					.setElementname(map.get("element_name")==null?null:map.get("element_name").toString())
					.setElementid(map.get("element_id")==null?null:map.get("element_id").toString())
					.setSupplierId(Integer.valueOf(Optional.ofNullable(map.get("supplier_id")).orElse("0").toString()));
					//对接项目OPL
					int proId=0;
					if (map.get("pro_id")!=null) {
						proId=Integer.valueOf(map.get("pro_id").toString());
					}
					int lineId=0;
					if (map.get("line_id")!=null) {
						lineId=Integer.valueOf(map.get("line_id").toString());
						qtyIssue.setProId(proId).setLineId(lineId).setProName(map.get("pro_name").toString());
					}
					qtyIssue.save();
					Long id = qtyIssue.getId();
					DingMessage.sendText(duty_user_id, "有新的质量OPL创建并需要你负责,编号："+id+"，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "13");//13-质量OPL措施责任人页面
					DingMessage.sendText(sure_user_id, "有新的质量OPL创建并需要你跟踪,编号："+id+"，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "14");//13-质量OPL措施跟踪人页面
					//7D时间
					HQtyIssue8dPlan dayPlan=new HQtyIssue8dPlan();
					dayPlan.setIssueId(qtyIssue.getId())
					.setDOne(com.ray.util.HyCommenMethods.addDate(openTime, 1))
					.setDTwo(com.ray.util.HyCommenMethods.addDate(openTime, 1))
					.setDThree(com.ray.util.HyCommenMethods.addDate(openTime, 2))
					.setDFour(com.ray.util.HyCommenMethods.addDate(openTime, 7))
					.setDFive(com.ray.util.HyCommenMethods.addDate(openTime, 14));
					dayPlan.save();

					//问题知会
					String tellUserIdstring=getPara("tellUserIds");
					if (tellUserIdstring!=null) {
						String[] tellUserIds=tellUserIdstring.split(",");
						if (tellUserIds.length>0) {
							for (int i = 0; i < tellUserIds.length; i++) {
								String tellUserId=tellUserIds[i];
								User tellUser=User.dao.findFirst("select * from user where ding_user_id='"+tellUserId+"'");
								HQualityIssueTell issueTell=new HQualityIssueTell();
								issueTell.setIssueId(qtyIssue.getId())
								.setTellUserId(tellUser.getId().intValue()).setTellUserName(tellUser.getNickname())
								.setStatus(0);
								issueTell.save();
								DingMessage.sendText(tellUserId, "有新的质量OPL创建并知会了你,编号："+id+"，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "11");//11-质量OPL知会页面
							}
						}
					}
					//增加操作历史记录
					HQtyissueEditRecord editRecord=new HQtyissueEditRecord();
					editRecord.setIssueId(qtyIssue.getId()).setContext(map.toString())
					.setCreateTime(new Date()).setCreateUserId(login.getId()).setCreateUserName(login.getNickname());
					editRecord.save();
					//判断是否非SOP阶段，是则对接项目OPL
					if (!"SOP".equals(productStage)) {
						String stage="";
						if ("手工样件".equals(productStage)) {
							stage="P2";
						}else if ("OTS".equals(productStage)) {
							stage="P3";
						}else {
							stage="P4";
						}
						Record proInfo=Db.use("bud").findById("file_pro_info", proId);
						String proCode=proInfo.getStr("pro_code");
						HProjectIssue projectIssue=new HProjectIssue();
						projectIssue
						.setProId(proId).setDepId(Long.valueOf(map.get("dep_id").toString())).setProCode(proCode)
						.setLineId(lineId)
						.setProName(map.get("pro_name").toString())
						.setProductName(proInfo.get("category_name"))
						.setStatus(qtyIssue.getStatus())
						.setStage(stage)
						.set("propose_time",map.get("open_time").toString())
						.setSeverity(map.get("level").toString())
						.setIssueType("质量")
						.setIssueSource(map.get("source").toString())
						.setIssueDescription(map.get("description").toString())
						.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
						.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
						.setConfirmerId(sureUser.getId().intValue()).setConfirmerName(sureUser.getNickname())
						.setIssueTarget("")
						.setCreateTime(new Date())
						.setCreateUserId(login.getId()).setCreateUserName(login.getNickname())
						.setIsDel(0).setIssueStatus(0)
						.setQualityIssueId(qtyIssue.getId());
						projectIssue.save();
						//回写质量OPL中项目OPL id
						qtyIssue.setProIssueId(projectIssue.getId());
						qtyIssue.update();
						//默认知会项目责任人
						Record proDutyUser=Db.use("bud").findFirst(" select * from pro_member where pro_code='"+proCode+"' and role_name='责任人' ");
						User zhihuiUser=User.dao.findFirst(" select * from user where ding_user_id='"+proDutyUser.getStr("role_member_id")+"' ");
						HProIssueTell issueTell=new HProIssueTell();
						issueTell.setIssueId(projectIssue.getId())
						.setTellUserId(zhihuiUser.getId().intValue()).setTellUserName(zhihuiUser.getNickname())
						.setStatus(0);
						issueTell.save();
						DingMessage.sendText(zhihuiUser.getDingUserId(), "有新的项目标准OPL创建并知会了你，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "1");//1-标准OPL知会页面
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "新增质量OPL成功！"));
			}else {
				renderJson(Ret.fail("msg", "新增质量OPL失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}



	/**
	 * 删除质量问题
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月15日 下午4:11:53
	 */
	public void delQtyIssue() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("id");
				HQualityIssue issue=HQualityIssue.dao.findById(issueId);
				issue.setIsDel(1);
				issue.update();
				HProjectIssue projectIssue=HProjectIssue.dao.findById(issue.getProIssueId());
				if (projectIssue!=null) {
					projectIssue.setIsDel(1);
					projectIssue.update();
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "删除成功！"));
			}else {
				renderJson(Ret.fail("msg", "删除失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 编辑质量问题
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月15日 下午4:11:53
	 */
	@SuppressWarnings("rawtypes")
	public void editQtyIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long editId=getParaToLong("editId");
				HQualityIssue qtyIssue=HQualityIssue.dao.findById(editId);
				String upgrade_user_id=map.get("upgrade_user_id").toString();
				User upgradeUser=User.dao.findFirst("select * from user where ding_user_id='"+upgrade_user_id+"'");
				String duty_user_id=map.get("duty_user_id").toString();
				User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+duty_user_id+"'");
				String sure_user_id=map.get("sure_user_id").toString();
				User sureUser=User.dao.findFirst("select * from user where ding_user_id='"+sure_user_id+"'");
				String zj_user_id=map.get("zj_user_id").toString();
				User zjUser=User.dao.findFirst("select * from user where ding_user_id = '"+zj_user_id+"'");

				//附件
				JSONArray fujianArray=JSONArray.parseArray(getPara("fileList"));
				long unQualifyFujianId=0;
				if (fujianArray.size()>0) {
					unQualifyFujianId=fujianArray.getJSONObject(0).getLongValue("id");
				}
				Integer part_id = Db.findFirst(" select id from h_parts where part_no = '" + map.get("part_id").toString() + "' ").getInt("id");
				//编辑OPL
				qtyIssue
				.setDepId(Long.valueOf(map.get("dep_id").toString()))
				.setWareId(Integer.valueOf(map.get("ware_id").toString()))
				.setPartId(part_id)
				.setLevel(map.get("level").toString())
				.setProductName(map.get("product_name").toString())
				.setCusName(map.get("cus_name").toString())
				.setSupplierName(map.get("supplier_name").toString())
				.setProcedure(map.get("procedure").toString())
				.setTypeId(Integer.valueOf(map.get("type_id").toString()))
				.setSupplierTypeId(map.get("suppliertype_id") != null ?Integer.valueOf(map.get("suppliertype_id").toString()):0)
				.setDescription(map.get("description").toString())
				.setProductStage(map.get("product_stage").toString())
				.setSource(map.get("source").toString())
				.setHappenFrequency(map.get("happen_frequency").toString())
				.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
				.setSureUserId(sureUser.getId().intValue()).setSureUserName(sureUser.getNickname())
				.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
				.setZjUserId(zjUser.getId().toString()).setZjUserName(zjUser.getNickname())
				.setCreateTime(new Date())
				.setUnqualifyFujianId(unQualifyFujianId)
				.setIsDel(0).setIssueStatus(0)
				//增加字段  零件号  零件id  供应商零件号
				.setElementname(map.get("element_name")==null?null:map.get("element_name").toString())
				.setElementid(map.get("element_id")==null?null:map.get("element_id").toString())
				.setSupplierId(Integer.valueOf(Optional.ofNullable(map.get("supplier_id")).orElse("0").toString()));
				//对接项目OPL
				int proId=0;
				if (map.get("pro_id")!=null) {
					proId=Integer.valueOf(map.get("pro_id").toString());
				}
				int lineId=0;
				if (map.get("line_id")!=null) {
					lineId=Integer.valueOf(map.get("line_id").toString());
					qtyIssue.setProId(proId).setLineId(lineId).setProName(map.get("pro_name").toString());
				}

				qtyIssue.update();
				Long id = qtyIssue.getId();
				DingMessage.sendText(duty_user_id, "有新的质量OPL已重新编辑并需要你负责,编号："+id+"，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "13");//13-质量OPL措施责任人页面
				//问题知会
				Db.update("update h_quality_issue_tell set status=1 where issue_id="+qtyIssue.getId());
				String tellUserIdstring=getPara("tellUserIds");
				if (tellUserIdstring!=null) {
					String[] tellUserIds=tellUserIdstring.split(",");
					if (tellUserIds.length>0) {
						for (int i = 0; i < tellUserIds.length; i++) {
							String tellUserId=tellUserIds[i];
							User tellUser=User.dao.findFirst("select * from user where ding_user_id='"+tellUserId+"'");
							HQualityIssueTell issueTell=HQualityIssueTell.dao.findFirst("select * from h_quality_issue_tell where issue_id="+qtyIssue.getId()+" and tell_user_id="+tellUser.getId());
							if (issueTell==null) {
								issueTell=new HQualityIssueTell();
								issueTell.setIssueId(qtyIssue.getId())
								.setTellUserId(tellUser.getId().intValue()).setTellUserName(tellUser.getNickname())
								.setStatus(0);
								issueTell.save();
							//	DingMessage.sendText(tellUserId, "有新的质量OPL创建并知会了你，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "11");//11-质量OPL知会页面
							}else {
								issueTell.setStatus(0);
								issueTell.update();
							}
						}
					}
				}
				//增加操作历史记录
				Record loginUser=getSessionAttr("user");
				User login=User.dao.findById(loginUser.getInt("id"));
				HQtyissueEditRecord editRecord=new HQtyissueEditRecord();
				editRecord.setIssueId(qtyIssue.getId()).setContext(map.toString())
				.setCreateTime(new Date()).setCreateUserId(login.getId()).setCreateUserName(login.getNickname());
				editRecord.save();
				//判断是否非SOP阶段，是则对接项目OPL
				String productStage=map.get("product_stage").toString();
				HProjectIssue projectIssue=HProjectIssue.dao.findById(qtyIssue.getProIssueId());
				if (!"SOP".equals(productStage)) {
					String stage="";
					if ("手工样件".equals(productStage)) {
						stage="P2";
					}else if ("OTS".equals(productStage)) {
						stage="P3";
					}else {
						stage="P4";
					}
					Record proInfo=Db.use("bud").findById("file_pro_info", proId);
					String proCode=proInfo.getStr("pro_code");
					//默认知会项目责任人
					Record proDutyUser=Db.use("bud").findFirst("select * from pro_member where pro_code='"+proCode+"' and role_name='责任人'");
					User zhihuiUser=User.dao.findFirst("select * from user where ding_user_id='"+proDutyUser.getStr("role_member_id")+"'");
					if (projectIssue==null) {
						projectIssue=new HProjectIssue();
						projectIssue
						.setProId(proId).setDepId(Long.valueOf(map.get("dep_id").toString())).setProCode(proCode)
						.setLineId(lineId)
						.setProName(map.get("pro_name").toString())
						.setProductName(proInfo.get("category_name"))
						.setStatus(qtyIssue.getStatus())
						.setStage(stage)
						.set("propose_time",map.get("open_time").toString())
						.setSeverity(map.get("level").toString())
						.setIssueType("质量")
						.setIssueSource(map.get("source").toString())
						.setIssueDescription(map.get("description").toString())
						.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
						.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
						.setConfirmerId(sureUser.getId().intValue()).setConfirmerName(sureUser.getNickname())
						.setCreateTime(new Date())
						.setCreateUserId(qtyIssue.getCreateUserId()).setCreateUserName(qtyIssue.getCreateUserName())
						.setIsDel(0).setIssueStatus(0)
						.setQualityIssueId(qtyIssue.getId());
						projectIssue.save();
						//知会人
						HProIssueTell issueTell=new HProIssueTell();
						issueTell.setIssueId(projectIssue.getId())
						.setTellUserId(zhihuiUser.getId().intValue()).setTellUserName(zhihuiUser.getNickname())
						.setStatus(0);
						issueTell.save();
					}else {
						projectIssue
						.setProId(proId).setDepId(Long.valueOf(map.get("dep_id").toString())).setProCode(proCode)
						.setLineId(lineId)
						.setProName(map.get("pro_name").toString())
						.setProductName(proInfo.get("category_name"))
						.setStage(stage)
						.setSeverity(map.get("level").toString())
						.setIssueSource(map.get("source").toString())
						.setIssueDescription(map.get("description").toString())
						.setUpgradeUserId(upgradeUser.getId().intValue()).setUpgradeUserName(upgradeUser.getNickname())
						.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
						.setConfirmerId(sureUser.getId().intValue()).setConfirmerName(sureUser.getNickname())
						.setIsDel(0);
						projectIssue.update();
						//知会人
						HProIssueTell issueTell=HProIssueTell.dao.findFirst("select * from h_pro_issue_tell where issue_id="+projectIssue.getId()+" and status=0");
						if (issueTell==null) {
							issueTell=new HProIssueTell();
							issueTell.setIssueId(projectIssue.getId())
							.setTellUserId(zhihuiUser.getId().intValue()).setTellUserName(zhihuiUser.getNickname())
							.setStatus(0);
							issueTell.save();
						}else {
							issueTell.setTellUserId(zhihuiUser.getId().intValue()).setTellUserName(zhihuiUser.getNickname())
							.setStatus(0);
							issueTell.update();
						}
					}
					/**
					 * 2022-5-25 防止之前是SOP阶段，编辑了措施、原因分析等之后，再修改阶段-start
					 */
					//原因分析同步
					List<HQtyIssueReasonAnalysis> reasonAnalysis = HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where status=0 and issue_id="+qtyIssue.getId());
					for (int i = 0; i < reasonAnalysis.size(); i++) {
						HProIssueReasonAnalysis proReasonAnalysis=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where qty_reason_id="+reasonAnalysis.get(i).getId());
						if (proReasonAnalysis==null) {
							proReasonAnalysis=new HProIssueReasonAnalysis();
							proReasonAnalysis.setQtyReasonId(reasonAnalysis.get(i).getId())
							.setIssueId(projectIssue.getId())
							.setReasonAnalysis(reasonAnalysis.get(i).getType().intValue()==0?"产生原因：":"根本原因："+reasonAnalysis.get(i).getReasonAnalysis())
							.setStatus(0).setIsXuni(0);
							proReasonAnalysis.save();
							proReasonAnalysis.setLsIndex(proReasonAnalysis.getId());
							proReasonAnalysis.update();
						}else {
							proReasonAnalysis
							.setReasonAnalysis(reasonAnalysis.get(i).getType().intValue()==0?"产生原因：":"根本原因："+reasonAnalysis.get(i).getReasonAnalysis())
							.setStatus(0);
							proReasonAnalysis.update();
						}
					}
					//临时措施抓取“D3遏制/临时措施及措施有效性跟踪”
					//措施同步-2=遏制
					List<HQtyMeasures> ezqtyMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where is_del=0 and issue_id="+qtyIssue.getId()+" and type=2");
					for (int i = 0; i < ezqtyMeasures.size(); i++) {
						HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  qty_measure_id="+ezqtyMeasures.get(i).getId());
						HProIssueReasonAnalysis xuniReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where issue_id="+projectIssue.getId()+" and is_xuni=1 ");
						if (xuniReason==null) {
							xuniReason=new HProIssueReasonAnalysis();
							xuniReason.setIssueId(projectIssue.getId()).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1);
							xuniReason.save();
							xuniReason.setLsIndex(xuniReason.getId());
							xuniReason.update();
						}else {
							xuniReason.setStatus(0);
							xuniReason.update();
						}
						if (proMeasure==null) {
							proMeasure=new HProjectMeasures();
							proMeasure.setIssueId(projectIssue.getId()).setReasonId(xuniReason.getId())
							.setXuhao("1."+(i+1)).setMeasures(ezqtyMeasures.get(i).getMeasures()+"。遏制结果："+ezqtyMeasures.get(i).getContainResult())
							.setUserId(ezqtyMeasures.get(i).getUserId()).setUserName(ezqtyMeasures.get(i).getUserName())
							.setPlanFinishDate(ezqtyMeasures.get(i).getPlanFinishDate())
							.setStatus(1).setIsDel(0).setMeasureType(0)
							.setQtyMeasureId(ezqtyMeasures.get(i).getId());
							proMeasure.save();
						}else {
							proMeasure.setReasonId(xuniReason.getId())
							.setXuhao("1."+(i+1)).setMeasures(ezqtyMeasures.get(i).getMeasures()+"。遏制结果："+ezqtyMeasures.get(i).getContainResult())
							.setUserId(ezqtyMeasures.get(i).getUserId()).setUserName(ezqtyMeasures.get(i).getUserName())
							.setPlanFinishDate(ezqtyMeasures.get(i).getPlanFinishDate())
							.setIsDel(0);
							proMeasure.update();
						}
						//2022-6-16 增加 措施跟踪同步
						List<HQtyTrackHistory> trackHistorys=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+ezqtyMeasures.get(i).getId());
						for (int j = 0; j < trackHistorys.size(); j++) {
							HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where qty_track_history_id="+trackHistorys.get(j).getId());
							if (proMeasureTrack==null) {
								proMeasureTrack = new HProissueTrackHistory();
								proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
								.setQtyTrackHistoryId(trackHistorys.get(j).getId())
								.setMeasureStatus(trackHistorys.get(j).getMeasureStatus()).setRemark(trackHistorys.get(j).getRemark())
								.setTrackUserId(trackHistorys.get(j).getTrackUserId()).setTrackUserName(trackHistorys.get(j).getTrackUserName())
								.setCreateTime(trackHistorys.get(j).getCreateTime());
								proMeasureTrack.save();
							}
						}
					}
					//措施同步-3=临时
					List<HQtyMeasures> lsqtyMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where is_del=0 and issue_id="+qtyIssue.getId()+" and type=3");
					for (int i = 0; i < lsqtyMeasures.size(); i++) {
						HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  qty_measure_id="+lsqtyMeasures.get(i).getId());
						HProIssueReasonAnalysis xuniReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where issue_id="+projectIssue.getId()+" and is_xuni=1 ");
						if (xuniReason==null) {
							xuniReason=new HProIssueReasonAnalysis();
							xuniReason.setIssueId(projectIssue.getId()).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1);
							xuniReason.save();
							xuniReason.setLsIndex(xuniReason.getId());
							xuniReason.update();
						}else {
							xuniReason.setStatus(0);
							xuniReason.update();
						}
						if (proMeasure==null) {
							proMeasure=new HProjectMeasures();
							proMeasure.setIssueId(projectIssue.getId()).setReasonId(xuniReason.getId())
							.setXuhao("1."+(i+1)).setMeasures(lsqtyMeasures.get(i).getMeasures())
							.setUserId(lsqtyMeasures.get(i).getUserId()).setUserName(lsqtyMeasures.get(i).getUserName())
							.setPlanFinishDate(lsqtyMeasures.get(i).getPlanFinishDate())
							.setStatus(1).setIsDel(0).setMeasureType(0)
							.setQtyMeasureId(lsqtyMeasures.get(i).getId());
							proMeasure.save();
						}else {
							proMeasure.setReasonId(xuniReason.getId())
							.setXuhao("1."+(i+1)).setMeasures(lsqtyMeasures.get(i).getMeasures())
							.setUserId(lsqtyMeasures.get(i).getUserId()).setUserName(lsqtyMeasures.get(i).getUserName())
							.setPlanFinishDate(lsqtyMeasures.get(i).getPlanFinishDate())
							.setIsDel(0);
							proMeasure.update();
						}
						//2022-6-16 增加 措施跟踪同步
						List<HQtyTrackHistory> trackHistorys=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+lsqtyMeasures.get(i).getId());
						for (int j = 0; j < trackHistorys.size(); j++) {
							HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where qty_track_history_id="+trackHistorys.get(j).getId());
							if (proMeasureTrack==null) {
								proMeasureTrack = new HProissueTrackHistory();
								proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
								.setQtyTrackHistoryId(trackHistorys.get(j).getId())
								.setMeasureStatus(trackHistorys.get(j).getMeasureStatus()).setRemark(trackHistorys.get(j).getRemark())
								.setTrackUserId(trackHistorys.get(j).getTrackUserId()).setTrackUserName(trackHistorys.get(j).getTrackUserName())
								.setCreateTime(trackHistorys.get(j).getCreateTime());
								proMeasureTrack.save();
							}
						}
					}
					//永久措施抓取“D5/D6永久措施指定&实施”
					//措施同步--0=防产生
					List<HQtyMeasures> csqtyMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where is_del=0 and issue_id="+qtyIssue.getId()+" and type=0");
					for (int i = 0; i < csqtyMeasures.size(); i++) {
						HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  qty_measure_id="+csqtyMeasures.get(i).getId());
						HProIssueReasonAnalysis proReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where qty_reason_id="+csqtyMeasures.get(i).getReasonId());
						if (proMeasure==null) {
							proMeasure=new HProjectMeasures();
							proMeasure.setIssueId(projectIssue.getId()).setReasonId(proReason.getId())
							.setXuhao(String.valueOf(i+1)).setMeasures(csqtyMeasures.get(i).getMeasures())
							.setUserId(csqtyMeasures.get(i).getUserId()).setUserName(csqtyMeasures.get(i).getUserName())
							.setPlanFinishDate(csqtyMeasures.get(i).getPlanFinishDate())
							.setStatus(1).setIsDel(0).setMeasureType(1)
							.setQtyMeasureId(csqtyMeasures.get(i).getId());
							proMeasure.save();
						}else {
							proMeasure.setReasonId(proReason.getId())
							.setXuhao(String.valueOf(i+1)).setMeasures(csqtyMeasures.get(i).getMeasures())
							.setUserId(csqtyMeasures.get(i).getUserId()).setUserName(csqtyMeasures.get(i).getUserName())
							.setPlanFinishDate(csqtyMeasures.get(i).getPlanFinishDate())
							.setIsDel(0);
							proMeasure.update();
						}
						//2022-6-16 增加 措施跟踪同步
						List<HQtyTrackHistory> trackHistorys=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+csqtyMeasures.get(i).getId());
						for (int j = 0; j < trackHistorys.size(); j++) {
							HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where qty_track_history_id="+trackHistorys.get(j).getId());
							if (proMeasureTrack==null) {
								proMeasureTrack = new HProissueTrackHistory();
								proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
								.setQtyTrackHistoryId(trackHistorys.get(j).getId())
								.setMeasureStatus(trackHistorys.get(j).getMeasureStatus()).setRemark(trackHistorys.get(j).getRemark())
								.setTrackUserId(trackHistorys.get(j).getTrackUserId()).setTrackUserName(trackHistorys.get(j).getTrackUserName())
								.setCreateTime(trackHistorys.get(j).getCreateTime());
								proMeasureTrack.save();
							}
						}
					}
					//措施同步--1=防流出
					List<HQtyMeasures> lcqtyMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where is_del=0 and issue_id="+qtyIssue.getId()+" and type=1");
					for (int i = 0; i < lcqtyMeasures.size(); i++) {
						HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  qty_measure_id="+lcqtyMeasures.get(i).getId());
						HProIssueReasonAnalysis proReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where qty_reason_id="+lcqtyMeasures.get(i).getReasonId());
						if (proMeasure==null) {
							proMeasure=new HProjectMeasures();
							proMeasure.setIssueId(projectIssue.getId()).setReasonId(proReason.getId())
							.setXuhao(String.valueOf(i+1)).setMeasures(lcqtyMeasures.get(i).getMeasures())
							.setUserId(lcqtyMeasures.get(i).getUserId()).setUserName(lcqtyMeasures.get(i).getUserName())
							.setPlanFinishDate(lcqtyMeasures.get(i).getPlanFinishDate())
							.setStatus(1).setIsDel(0).setMeasureType(1)
							.setQtyMeasureId(lcqtyMeasures.get(i).getId());
							proMeasure.save();
						}else {
							proMeasure.setReasonId(proReason.getId())
							.setXuhao(String.valueOf(i+1)).setMeasures(lcqtyMeasures.get(i).getMeasures())
							.setUserId(lcqtyMeasures.get(i).getUserId()).setUserName(lcqtyMeasures.get(i).getUserName())
							.setPlanFinishDate(lcqtyMeasures.get(i).getPlanFinishDate())
							.setIsDel(0);
							proMeasure.update();
						}
						//2022-6-16 增加 措施跟踪同步
						List<HQtyTrackHistory> trackHistorys=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+lcqtyMeasures.get(i).getId());
						for (int j = 0; j < trackHistorys.size(); j++) {
							HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where qty_track_history_id="+trackHistorys.get(j).getId());
							if (proMeasureTrack==null) {
								proMeasureTrack = new HProissueTrackHistory();
								proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
								.setQtyTrackHistoryId(trackHistorys.get(j).getId())
								.setMeasureStatus(trackHistorys.get(j).getMeasureStatus()).setRemark(trackHistorys.get(j).getRemark())
								.setTrackUserId(trackHistorys.get(j).getTrackUserId()).setTrackUserName(trackHistorys.get(j).getTrackUserName())
								.setCreateTime(trackHistorys.get(j).getCreateTime());
								proMeasureTrack.save();
							}
						}
					}
					/**
					 * 2022-5-25 防止之前是SOP阶段，编辑了措施、原因分析等之后，再修改阶段-end
					 */
					//回写质量OPL中项目OPL id
					qtyIssue.setProIssueId(projectIssue.getId());
					qtyIssue.update();
					DingMessage.sendText(zhihuiUser.getDingUserId(), "有新的项目标准OPL创建并知会了你，请知晓！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "1");//1-标准OPL知会页面
				}else {
					if (projectIssue!=null) {
						projectIssue.setIsDel(1);
						projectIssue.update();
					}
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "编辑质量OPL成功！"));
			}else {
				renderJson(Ret.fail("msg", "编辑质量OPL失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 导出
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月6日 上午11:22:32
	 */
	@SuppressWarnings("resource")
	public void exportQtyIssue() {
		Record req=new Record();
		Db.tx(() -> {
			try {
				HSSFWorkbook workbook = new HSSFWorkbook();
		        HSSFCellStyle style = workbook.createCellStyle();
		        style.setAlignment(HorizontalAlignment.CENTER);
		        style.setVerticalAlignment(VerticalAlignment.CENTER);
		        HSSFSheet sheet = workbook.createSheet("sheet");

		        HSSFRow title = sheet.createRow(0);
		        HSSFRow title2 = sheet.createRow(1);
		        HSSFCell cell_00 = title.createCell(0);
		        cell_00.setCellStyle(style);
		        cell_00.setCellValue("报告编号");
		        HSSFCell cell_01 = title.createCell(1);
		        cell_01.setCellStyle(style);
		        cell_01.setCellValue("状态");
		        HSSFCell cell_02 = title.createCell(2);
		        cell_02.setCellStyle(style);
		        cell_02.setCellValue("风险等级");
		        HSSFCell cell_03 = title.createCell(3);
		        cell_03.setCellStyle(style);
		        cell_03.setCellValue("事业部");
		        HSSFCell cell_04 = title.createCell(4);
		        cell_04.setCellStyle(style);
		        cell_04.setCellValue("车间");
		        HSSFCell cell_05 = title.createCell(5);
		        cell_05.setCellStyle(style);
		        cell_05.setCellValue("零件号");
		        HSSFCell cell_06 = title.createCell(6);
		        cell_06.setCellStyle(style);
		        cell_06.setCellValue("产品名称");
		        HSSFCell cell_07 = title.createCell(7);
		        cell_07.setCellStyle(style);
		        cell_07.setCellValue("开放时间");
		        HSSFCell cell_08 = title.createCell(8);
		        cell_08.setCellStyle(style);
		        cell_08.setCellValue("时限要求");
		        HSSFCell cell_09 = title.createCell(9);
		        cell_09.setCellStyle(style);
		        cell_09.setCellValue("8D进度状态");
		        //8D-开始
		        HSSFCell cell_19 = title2.createCell(9);
		        cell_19.setCellStyle(style);
		        cell_19.setCellValue("D1");
		        HSSFCell cell_110 = title2.createCell(10);
		        cell_110.setCellStyle(style);
		        cell_110.setCellValue("D2");
		        HSSFCell cell_111 = title2.createCell(11);
		        cell_111.setCellStyle(style);
		        cell_111.setCellValue("D3");
		        HSSFCell cell_112 = title2.createCell(12);
		        cell_112.setCellStyle(style);
		        cell_112.setCellValue("D4");
		        HSSFCell cell_113 = title2.createCell(13);
		        cell_113.setCellStyle(style);
		        cell_113.setCellValue("D5");
		        HSSFCell cell_114 = title2.createCell(14);
		        cell_114.setCellStyle(style);
		        cell_114.setCellValue("D6");
		        HSSFCell cell_115 = title2.createCell(15);
		        cell_115.setCellStyle(style);
		        cell_115.setCellValue("D7");
		        //8D结束
		        HSSFCell cell_016 = title.createCell(16);
		        cell_016.setCellStyle(style);
		        cell_016.setCellValue("客户名称");
		        HSSFCell cell_017 = title.createCell(17);
		        cell_017.setCellStyle(style);
		        cell_017.setCellValue("供应商名称");
		        HSSFCell cell_018 = title.createCell(18);
		        cell_018.setCellStyle(style);
		        cell_018.setCellValue("问题发生工序");
		        HSSFCell cell_019 = title.createCell(19);
		        cell_019.setCellStyle(style);
		        cell_019.setCellValue("问题类型");
		        HSSFCell cell_020 = title.createCell(20);
		        cell_020.setCellStyle(style);
		        cell_020.setCellValue("问题来源");
		        HSSFCell cell_021 = title.createCell(21);
		        cell_021.setCellStyle(style);
		        cell_021.setCellValue("发生频次");
		        HSSFCell cell_022 = title.createCell(22);
		        cell_022.setCellStyle(style);
		        cell_022.setCellValue("问题描述（地点/批次/数量）");
		        HSSFCell cell_023 = title.createCell(23);
		        cell_023.setCellStyle(style);
		        cell_023.setCellValue("遏制/临时措施及措施有效性跟踪");
		        HSSFCell cell_024 = title.createCell(24);
		        cell_024.setCellStyle(style);
		        cell_024.setCellValue("根本原因分析（产生/流出）");
		        HSSFCell cell_025 = title.createCell(25);
		        cell_025.setCellStyle(style);
		        cell_025.setCellValue("永久措施制定（产生/流出）");
		        HSSFCell cell_026 = title.createCell(26);
		        cell_026.setCellStyle(style);
		        cell_026.setCellValue("永久措施节点");
		        HSSFCell cell_027 = title.createCell(27);
		        cell_027.setCellStyle(style);
		        cell_027.setCellValue("永久措施3个月跟踪");
		        //3个月开始
		        HSSFCell cell_127 = title2.createCell(27);
		        cell_127.setCellStyle(style);
		        cell_127.setCellValue("第一月");
		        HSSFCell cell_128 = title2.createCell(28);
		        cell_128.setCellStyle(style);
		        cell_128.setCellValue("第二月");
		        HSSFCell cell_129 = title2.createCell(29);
		        cell_129.setCellStyle(style);
		        cell_129.setCellValue("第三月");
		        //3个月结束
		        HSSFCell cell_030 = title.createCell(30);
		        cell_030.setCellStyle(style);
		        cell_030.setCellValue("责任人");
		        HSSFCell cell_031 = title.createCell(31);
		        cell_031.setCellStyle(style);
		        cell_031.setCellValue("升级对象");
		        HSSFCell cell_032 = title.createCell(32);
		        cell_032.setCellStyle(style);
		        cell_032.setCellValue("关闭时间");
		        HSSFCell cell_033 = title.createCell(33);
		        cell_033.setCellStyle(style);
		        cell_033.setCellValue("激励方案（正/负）");
		        HSSFCell cell_034 = title.createCell(34);
		        cell_034.setCellStyle(style);
		        cell_034.setCellValue("跟踪记录");
		        HSSFCell cell_035 = title.createCell(35);
		        cell_035.setCellStyle(style);
		        cell_035.setCellValue("产品阶段");
		        CellRangeAddress region7d = new CellRangeAddress(0, 0, 9, 15);
		        sheet.addMergedRegion(region7d);
		        CellRangeAddress region3month = new CellRangeAddress(0, 0, 27, 29);
		        sheet.addMergedRegion(region3month);
		        for (int i = 0; i < 9; i++) {
		        	CellRangeAddress regionCon = new CellRangeAddress(0, 1, i, i);
			        sheet.addMergedRegion(regionCon);
				}
		        for (int i = 16; i < 27; i++) {
		        	CellRangeAddress regionCon = new CellRangeAddress(0, 1, i, i);
			        sheet.addMergedRegion(regionCon);
				}
		        for (int i = 30; i <= 35; i++) {
		        	CellRangeAddress regionCon = new CellRangeAddress(0, 1, i, i);
			        sheet.addMergedRegion(regionCon);
				}
		        Record loginUser=getSessionAttr("user");
				User user=User.dao.findById(loginUser.getInt("id"));
				StringBuilder sb=new StringBuilder();
				int downType=getParaToInt("downType");
				if (downType==0) {//我创建
					sb.append("select a.*,b.dep_name,c.ware_name,d.part_no,e.type from h_quality_issue a,h_cause_dep b,h_ware c,h_parts d,h_quality_issue_type e where  "
							+ "a.dep_id=b.dep_id and c.id=a.ware_id and a.is_del=0 and a.create_user_id="+user.getId()+" and a.part_id=d.id and a.type_id=e.id ");
				}else if (downType==1) {//我负责
					sb.append("select a.*,b.dep_name,c.ware_name,d.part_no,e.type from h_quality_issue a,h_cause_dep b,h_ware c,h_parts d,h_quality_issue_type e where  "
							+ "a.dep_id=b.dep_id and c.id=a.ware_id and a.is_del=0 and a.duty_user_id="+user.getId()+" and a.part_id=d.id and a.type_id=e.id ");
				}else if (downType==3){//我跟踪的
					sb.append("select a.*,b.dep_name,c.ware_name,d.part_no,e.type from h_quality_issue a,h_cause_dep b,h_ware c,h_parts d,h_quality_issue_type e where  "
							+ "a.dep_id=b.dep_id and c.id=a.ware_id and a.is_del=0 and a.sure_user_id="+user.getId()+" and a.part_id=d.id and a.type_id=e.id ");
				}else if (downType==2) {//全部
					//判断是否超管、体系等
					List<Record> userRole=Db.use("bud").find("select b.role_name from user_role a, roles b where a.user_id='"+user.getDingUserId()+"' and a.role_id=b.id");
					int user_role=0;
					if (userRole.size()>0) {
						for (int i = 0; i < userRole.size(); i++) {
							String role_name=userRole.get(i).getStr("role_name");
							if ("公司经营层".equals(role_name)|| "项目体系".equals(role_name)||"财务".equals(role_name)||"管理员".equals(role_name)) {
								user_role=1;
							}
						}
					}
					sb.append("select a.*,b.dep_name,d.part_no,e.type from h_quality_issue a,h_cause_dep b,h_parts d,h_quality_issue_type e where  "
							+ "a.dep_id=b.dep_id and a.is_del=0  and a.part_id=d.id and a.type_id=e.id and a.level != 'C' and a.product_stage != '手工样件'  ");
					if (user_role==0) {
						sb.append(" and (a.dep_id in (select first_dep_id from h_user_dep where user_id="+user.getId()+" and status=0  union "
								+ "select dep_id from h_quality_issue where create_user_id="+user.getId().intValue()+" or duty_user_id="+user.getId().intValue()+") ) ");
					}
					String selCreateUser=get("selCreateUser");
					if (!"".equals(selCreateUser)&& selCreateUser!=null) {
						sb.append(" and a.create_user_name like '%"+selCreateUser+"%'");
					}
					String selDutyUser=get("selDutyUser");
					if (!"".equals(selDutyUser)&& selDutyUser!=null) {
						sb.append(" and a.duty_user_name like '%"+selDutyUser+"%'");
					}
				}
				String selLevel=get("selLevel");
				if (!"".equals(selLevel)&& selLevel!=null) {
					sb.append(" and a.level='"+selLevel+"'");
				}
				long selDep=getParaToLong("selDep");
				if (selDep!=0) {
					sb.append(" and a.dep_id="+selDep);
				}
				int selWarehouse=getParaToInt("selWarehouse");
				if (selWarehouse!=0) {
					sb.append(" and a.ware_id="+selWarehouse);
				}
				String selPartCode=get("selPartCode");
				if (!"".equals(selPartCode)&& selPartCode!=null) {
					sb.append(" and d.part_no like '%"+selPartCode+"%'");
				}
				String selProductName=get("selProductName");
				if (!"".equals(selProductName)&& selProductName!=null) {
					sb.append(" and a.product_name like '%"+selProductName+"%'");
				}
				String selCusName=get("selCusName");
				if (!"".equals(selCusName)&& selCusName!=null) {
					sb.append(" and a.cus_name like '%"+selCusName+"%'");
				}
				String selOPLdes=get("selOPLdes");
				if (!"".equals(selOPLdes)&& selOPLdes!=null) {
					sb.append(" and a.description like '%"+selOPLdes+"%'");
				}
				String selStatus=get("selStatus");
				if (!"".equals(selStatus)&& selStatus!=null) {
					sb.append(" and a.status='"+selStatus+"'");
				}
				String selFrequency=get("selFrequency");
				if (!"".equals(selFrequency)&& selFrequency!=null) {
					sb.append(" and a.happen_frequency='"+selFrequency+"'");
				}
				if (!"".equals(get("selType"))&& get("selType")!=null) {
					sb.append(" and a.type_id="+get("selType"));
				}
				String selSource=get("selSource");
				if (!"".equals(selSource)&& selSource!=null) {
					sb.append(" and a.source='"+selSource+"'");
				}
				String selProductStage=get("selProductStage");
				if (!"".equals(selProductStage)&& selProductStage!=null) {
					sb.append(" and a.product_stage='"+selProductStage+"'");
				}
				String selNextReviewTime=get("selNextReviewTime");
				if (!"".equals(selNextReviewTime)&& selNextReviewTime!=null) {
					sb.append(" and a.next_review_time='"+selNextReviewTime+"'");
				}
				List<Record> issueList=Db.find(sb.toString()+" order by id desc");
				SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				List<Record> qtyOPLList=new ArrayList<Record>();
				for (int i = 0; i < issueList.size(); i++) {
					long issueId=issueList.get(i).getLong("id");
					//车间
					if (issueList.get(i).get("ware_id")!=null) {
						HWare ware=HWare.dao.findById(issueList.get(i).getInt("ware_id"));
						issueList.get(i).set("ware_name", ware.getWareName());
					}else {
						issueList.get(i).set("ware_name", "暂无维护");
					}
					//遏制措施 contain_s
					String contain_measures="";
					List<HQtyMeasures> containMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where issue_id="+issueId+" and type=2 and is_del=0");
					if (!containMeasures.isEmpty()) {
						for (int j = 0; j < containMeasures.size(); j++) {
							//遏制措施
							contain_measures=contain_measures+"遏制措施"+(j+1)+":"+containMeasures.get(j).getMeasures()+"-"+containMeasures.get(j).getUserName();
							int measureStatus=containMeasures.get(j).getStatus();
							if (measureStatus==1) {
								contain_measures=contain_measures+"-进行中-"+containMeasures.get(j).getPlanFinishDate();
							}else if (measureStatus==2){
								contain_measures=contain_measures+"-已完成-"+containMeasures.get(j).getRealFinishDate();
							}else if (measureStatus==3){
								contain_measures=contain_measures+"-已关闭-"+containMeasures.get(j).getCloseDate();
							}
							contain_measures=contain_measures+"\n";
							//遏制结果
							if(containMeasures.get(j).getContainResult()==null) {
								contain_measures=contain_measures+"遏制结果"+(j+1)+":暂无"+"\n";
							}else {
								contain_measures=contain_measures+"遏制结果"+(j+1)+":"+containMeasures.get(j).getContainResult()+"\n";
							}
						}
					}else {
						contain_measures="暂无遏制措施"+"\n";
					}
					//临时措施
					String temporary_measures="";
					List<HQtyMeasures> temporaryMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where issue_id="+issueId+" and type=3 and is_del=0");
					if (!temporaryMeasures.isEmpty()) {
						for (int j = 0; j < temporaryMeasures.size(); j++) {
							//临时措施
							temporary_measures=temporary_measures+"临时措施"+(j+1)+":"+temporaryMeasures.get(j).getMeasures()+"-"+temporaryMeasures.get(j).getUserName();
							int measureStatus=temporaryMeasures.get(j).getStatus();
							if (measureStatus==1) {
								temporary_measures=temporary_measures+"-进行中-"+temporaryMeasures.get(j).getPlanFinishDate();
							}else if (measureStatus==2){
								temporary_measures=temporary_measures+"-已完成-"+temporaryMeasures.get(j).getRealFinishDate();
							}else if (measureStatus==3){
								temporary_measures=temporary_measures+"-已关闭-"+temporaryMeasures.get(j).getCloseDate();
							}
							temporary_measures=temporary_measures+"\n";
							//有效性跟踪
							List<HQtyTrackHistory> measureTrackRecord=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+temporaryMeasures.get(j).getId());
							if (measureTrackRecord.isEmpty()) {
								temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":暂无"+"\n";
							}else {
								String measureTracks="";
								for (int k = 0; k < measureTrackRecord.size(); k++) {
									measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；;";
								}
								temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"\n";
							}
						}
					}else {
						temporary_measures="暂无临时措施";
					}
					String contain_s=contain_measures+temporary_measures;
					issueList.get(i).set("contain_s", contain_s);
					//根本原因分析 root_s
					String happen_reason="";//产生原因
					String avoid_happen_measures="";//防产生措施
					List<HQtyIssueReasonAnalysis> happenAnalysis=HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
					if (!happenAnalysis.isEmpty()) {
						happen_reason="产生原因："+"\n";
						avoid_happen_measures="防产生措施："+"\n";
						for (int j = 0; j < happenAnalysis.size(); j++) {
							happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"\n";
							List<HQtyMeasures> avoidHappen=HQtyMeasures.dao.find("select * from h_qty_measures where reason_id="+happenAnalysis.get(j).getId()+" and is_del=0 and type=0");//对应防产生措施
							for (int k = 0; k < avoidHappen.size(); k++) {
								avoid_happen_measures=avoid_happen_measures+(j+1)+"."+(k+1)+avoidHappen.get(k).getMeasures()+"-"+avoidHappen.get(k).getUserName();
								int measureStatus=avoidHappen.get(k).getStatus();
								if (measureStatus==1) {
									avoid_happen_measures=avoid_happen_measures+"-进行中"+avoidHappen.get(k).getPlanFinishDate();
								}else if (measureStatus==2){
									avoid_happen_measures=avoid_happen_measures+"-已完成"+avoidHappen.get(k).getRealFinishDate();
								}else if (measureStatus==3){
									avoid_happen_measures=avoid_happen_measures+"-已关闭"+avoidHappen.get(k).getCloseDate();
								}
								avoid_happen_measures=avoid_happen_measures+"\n";
							}
						}
					}else {
						happen_reason="暂无产生原因"+"\n";
						avoid_happen_measures="暂无防产生措施"+"\n";
					}
					String runout_reason="";//流出原因
					String avoid_runout_measures="";//防流出措施
					List<HQtyIssueReasonAnalysis> runOutAnalysis=HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
					if (!runOutAnalysis.isEmpty()) {
						runout_reason="流出原因："+"\n";
						avoid_runout_measures="防流出措施："+"\n";
						for (int j = 0; j < runOutAnalysis.size(); j++) {
							runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"\n";
							List<HQtyMeasures> avoidRunout=HQtyMeasures.dao.find("select * from h_qty_measures where reason_id="+runOutAnalysis.get(j).getId()+" and is_del=0 and type=1");//对应防流出措施
							for (int k = 0; k < avoidRunout.size(); k++) {
								avoid_runout_measures=avoid_runout_measures+(j+1)+"."+(k+1)+avoidRunout.get(k).getMeasures()+"-"+avoidRunout.get(k).getUserName();
								int measureStatus=avoidRunout.get(k).getStatus();
								if (measureStatus==1) {
									avoid_runout_measures=avoid_runout_measures+"-进行中"+avoidRunout.get(k).getPlanFinishDate();
								}else if (measureStatus==2){
									avoid_runout_measures=avoid_runout_measures+"-已完成"+avoidRunout.get(k).getRealFinishDate();
								}else if (measureStatus==3){
									avoid_runout_measures=avoid_runout_measures+"-已关闭"+avoidRunout.get(k).getCloseDate();
								}
								avoid_runout_measures=avoid_runout_measures+"\n";
							}
						}
					}else {
						runout_reason="暂无流出原因";
						avoid_runout_measures="暂无防流出措施";
					}
					String root_s=happen_reason+runout_reason;
					String avoid_s=avoid_happen_measures+avoid_runout_measures;
					issueList.get(i).set("root_s", root_s).set("avoid_s", avoid_s);
					//永久措施跟踪3月
					List<HQualityIssueMonthTrack> monthTracks=HQualityIssueMonthTrack.dao.find("select * from h_quality_issue_month_track where issue_id="+issueId);
					issueList.get(i).set("month_1", " ").set("month_1_remark", "").set("month_1_date", "");
					issueList.get(i).set("month_2", " ").set("month_2_remark", "").set("month_2_date", "");
					issueList.get(i).set("month_3", " ").set("month_3_remark", "").set("month_3_date", "");
					String monthTrackString="";
					for (int j = 0; j < monthTracks.size(); j++) {
						int month=monthTracks.get(j).getMonth();
						if (month==1) {
							monthTrackString=monthTrackString+"第一月："+monthTracks.get(j).getRemark()+"\n";
							issueList.get(i).set("month_1", monthTracks.get(j).getStatus()).set("month_1_remark",  monthTracks.get(j).getRemark()).set("month_1_date",  monthTracks.get(j).getTrackDate());
						}else if (month==2) {
							monthTrackString=monthTrackString+"第二月："+monthTracks.get(j).getRemark()+"\n";
							issueList.get(i).set("month_2", monthTracks.get(j).getStatus()).set("month_2_remark", monthTracks.get(j).getRemark()).set("month_2_date",  monthTracks.get(j).getTrackDate());
						}else if (month==3) {
							monthTrackString=monthTrackString+"第三月："+monthTracks.get(j).getRemark()+"\n";
							issueList.get(i).set("month_3", monthTracks.get(j).getStatus()).set("month_3_remark", monthTracks.get(j).getRemark()).set("month_3_date",  monthTracks.get(j).getTrackDate());
						}
					}
					//OPL整体跟踪记录
					List<HQtyIssueTrackRecord> trackRecords=HQtyIssueTrackRecord.dao.find("select * from h_qty_issue_track_record where issue_id="+issueId);
					String  oplTrackRecord="";
					for (int j = 0; j < trackRecords.size(); j++) {
						oplTrackRecord=oplTrackRecord+(j+1)+"."+trackRecords.get(j).getTrackRecord()+"-"+trackRecords.get(j).getCreateUserName()+"-"+sdf1.format(trackRecords.get(j).getCreateTime())+"\n";
					}
					String record=" ";
					if (!"".equals(oplTrackRecord)) {
						record=oplTrackRecord;
					}
					if (!"".equals(monthTrackString)) {
						record=record+monthTrackString;
					}
					issueList.get(i).set("record", record);
					//8D计划
					Record issuePlan=new Record();
					issuePlan.setColumns(issueList.get(i));
					Record issueReal=new Record();
					issueReal.setColumns(issueList.get(i));
					HQtyIssue8dPlan plans=HQtyIssue8dPlan.dao.findFirst("select * from h_qty_issue_8d_plan where issue_id="+issuePlan.getLong("id"));
					issuePlan.set("plan_real", "计划")
					.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
					.set("d_three", plans.getDThree()).set("d_four", plans.getDFour())
					.set("d_five", plans.getDFive()).set("d_six", plans.getDSix())
					.set("d_seven", plans.getDSeven());
					//8D实际
					HQtyIssue8dReal reals=HQtyIssue8dReal.dao.findFirst("select * from h_qty_issue_8d_real where issue_id="+issueReal.getLong("id"));
					issueReal.set("plan_real", "实际")
					.set("d_one", " ").set("d_two", " ")
					.set("d_three", " ").set("d_four", " ")
					.set("d_five", " ").set("d_six", " ")
					.set("d_seven", " ")
					.set("d_one_status", 0).set("d_two_status",0)
					.set("d_three_status", 0).set("d_four_status", 0)
					.set("d_five_status", 0).set("d_six_status", 0)
					.set("d_seven_status", 0);
					if (reals!=null) {
						issueReal
						.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
						.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
						.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
						.set("d_seven_status", reals.getDSevenStatus());
						if (reals.getDOneStatus()==2) {
							issueReal.set("d_one", reals.getDOne());
						}
						if (reals.getDTwoStatus()==2) {
							issueReal.set("d_two", reals.getDTwo());
						}
						if (reals.getDThreeStatus()==2) {
							issueReal.set("d_three", reals.getDThree());
						}
						if (reals.getDFourStatus()==2) {
							issueReal.set("d_four", reals.getDFour());
						}
						if (reals.getDFiveStatus()==2) {
							issueReal.set("d_five", reals.getDFive());
						}
						if (reals.getDSixStatus()==2) {
							issueReal.set("d_six", reals.getDSix());
						}
						if (reals.getDSevenStatus()==2) {
							issueReal.set("d_seven", reals.getDSeven());
						}
					}
					qtyOPLList.add(issuePlan);
					qtyOPLList.add(issueReal);
				}
				for (int j = 0; j < qtyOPLList.size(); j++) {
					HSSFRow rowIndex = sheet.createRow(j+2);
			        HSSFCell cell_20 = rowIndex.createCell(0);
			        cell_20.setCellStyle(style);
			        cell_20.setCellValue(qtyOPLList.get(j).getStr("report_no"));
			        HSSFCell cell_21 = rowIndex.createCell(1);
			        cell_21.setCellStyle(style);
			        cell_21.setCellValue(qtyOPLList.get(j).getStr("status"));
			        HSSFCell cell_22 = rowIndex.createCell(2);
			        cell_22.setCellStyle(style);
			        cell_22.setCellValue(qtyOPLList.get(j).getStr("level"));
			        HSSFCell cell_23 = rowIndex.createCell(3);
			        cell_23.setCellStyle(style);
			        cell_23.setCellValue(qtyOPLList.get(j).getStr("dep_name"));
			        HSSFCell cell_24 = rowIndex.createCell(4);
			        cell_24.setCellStyle(style);
			        cell_24.setCellValue(qtyOPLList.get(j).getStr("ware_name"));
			        HSSFCell cell_25 = rowIndex.createCell(5);
			        cell_25.setCellStyle(style);
			        cell_25.setCellValue(qtyOPLList.get(j).getStr("part_no"));
			        HSSFCell cell_26 = rowIndex.createCell(6);
			        cell_26.setCellStyle(style);
			        cell_26.setCellValue(qtyOPLList.get(j).getStr("product_name"));
			        HSSFCell cell_27 = rowIndex.createCell(7);
			        cell_27.setCellStyle(style);
			        cell_27.setCellValue(qtyOPLList.get(j).getStr("open_time"));
			        HSSFCell cell_28 = rowIndex.createCell(8);
			        cell_28.setCellStyle(style);
			        cell_28.setCellValue(qtyOPLList.get(j).getStr("plan_real"));
			        HSSFCell cell_29 = rowIndex.createCell(9);
			        cell_29.setCellStyle(style);
			        cell_29.setCellValue(qtyOPLList.get(j).getStr("d_one"));
					HSSFCell cell_210 = rowIndex.createCell(10);
					cell_210.setCellStyle(style);
					cell_210.setCellValue(qtyOPLList.get(j).getStr("d_two"));
			        HSSFCell cell_211 = rowIndex.createCell(11);
			        cell_211.setCellStyle(style);
			        cell_211.setCellValue(qtyOPLList.get(j).getStr("d_three"));
			        HSSFCell cell_212 = rowIndex.createCell(12);
			        cell_212.setCellStyle(style);
			        cell_212.setCellValue(qtyOPLList.get(j).getStr("d_four"));
			        HSSFCell cell_213 = rowIndex.createCell(13);
			        cell_213.setCellStyle(style);
			        cell_213.setCellValue(qtyOPLList.get(j).getStr("d_five"));
			        HSSFCell cell_214 = rowIndex.createCell(14);
			        cell_214.setCellStyle(style);
			        cell_214.setCellValue(qtyOPLList.get(j).getStr("d_six"));
			        HSSFCell cell_215 = rowIndex.createCell(15);
			        cell_215.setCellStyle(style);
			        cell_215.setCellValue(qtyOPLList.get(j).getStr("d_seven"));
			        HSSFCell cell_216 = rowIndex.createCell(16);
			        cell_216.setCellStyle(style);
			        cell_216.setCellValue(qtyOPLList.get(j).getStr("cus_name"));
			        HSSFCell cell_217 = rowIndex.createCell(17);
			        cell_217.setCellStyle(style);
			        cell_217.setCellValue(qtyOPLList.get(j).getStr("supplier_name"));
			        HSSFCell cell_218 = rowIndex.createCell(18);
			        cell_218.setCellStyle(style);
			        cell_218.setCellValue(qtyOPLList.get(j).getStr("procedure"));
			        HSSFCell cell_219 = rowIndex.createCell(19);
			        cell_219.setCellStyle(style);
			        cell_219.setCellValue(qtyOPLList.get(j).getStr("type"));
			        HSSFCell cell_220 = rowIndex.createCell(20);
			        cell_220.setCellStyle(style);
			        cell_220.setCellValue(qtyOPLList.get(j).getStr("source"));
			        HSSFCell cell_221 = rowIndex.createCell(21);
			        cell_221.setCellStyle(style);
			        cell_221.setCellValue(qtyOPLList.get(j).getStr("happen_frequency"));
			        HSSFCell cell_222 = rowIndex.createCell(22);
			        cell_222.setCellStyle(style);
			        cell_222.setCellValue(qtyOPLList.get(j).getStr("description"));
			        HSSFCell cell_223 = rowIndex.createCell(23);
			        cell_223.setCellStyle(style);
			        cell_223.setCellValue(qtyOPLList.get(j).getStr("contain_s"));
			        HSSFCell cell_224 = rowIndex.createCell(24);
			        cell_224.setCellStyle(style);
			        cell_224.setCellValue(qtyOPLList.get(j).getStr("root_s"));
			        HSSFCell cell_225 = rowIndex.createCell(25);
			        cell_225.setCellStyle(style);
			        cell_225.setCellValue(qtyOPLList.get(j).getStr("avoid_s"));
			        HSSFCell cell_226 = rowIndex.createCell(26);
			        cell_226.setCellStyle(style);
			        cell_226.setCellValue(qtyOPLList.get(j).getStr("permanent_measures_time"));
			        HSSFCell cell_227 = rowIndex.createCell(27);
			        cell_227.setCellStyle(style);
			        cell_227.setCellValue(qtyOPLList.get(j).getStr("month_1"));
			        HSSFCell cell_228 = rowIndex.createCell(28);
			        cell_228.setCellStyle(style);
			        cell_228.setCellValue(qtyOPLList.get(j).getStr("month_2"));
			        HSSFCell cell_229 = rowIndex.createCell(29);
			        cell_229.setCellStyle(style);
			        cell_229.setCellValue(qtyOPLList.get(j).getStr("month_3"));
			        HSSFCell cell_230 = rowIndex.createCell(30);
			        cell_230.setCellStyle(style);
			        cell_230.setCellValue(qtyOPLList.get(j).getStr("duty_user_name"));
			        HSSFCell cell_231 = rowIndex.createCell(31);
			        cell_231.setCellStyle(style);
			        cell_231.setCellValue(qtyOPLList.get(j).getStr("upgrade_user_name"));
			        HSSFCell cell_232 = rowIndex.createCell(32);
			        cell_232.setCellStyle(style);
			        cell_232.setCellValue(qtyOPLList.get(j).getStr("close_time"));
			        HSSFCell cell_233 = rowIndex.createCell(33);
			        cell_233.setCellStyle(style);
			        cell_233.setCellValue(qtyOPLList.get(j).getStr("urge_plan"));
			        HSSFCell cell_234 = rowIndex.createCell(34);
			        cell_234.setCellStyle(style);
			        cell_234.setCellValue(qtyOPLList.get(j).getStr("record"));
			        HSSFCell cell_235 = rowIndex.createCell(35);
			        cell_235.setCellStyle(style);
			        cell_235.setCellValue(qtyOPLList.get(j).getStr("product_stage"));
					if (j%2!=0) {
						for (int i = 0; i < 8; i++) {
				        	CellRangeAddress regionCon = new CellRangeAddress(j+1, j+2, i, i);
					        sheet.addMergedRegion(regionCon);
						}
				        for (int i = 16; i <=35; i++) {
				        	CellRangeAddress regionCon = new CellRangeAddress(j+1, j+2, i, i);
					        sheet.addMergedRegion(regionCon);
						}
					}
				}
				File file = new File("D:\\SysFujianFiles\\openIssue\\upload\\moban\\质量OPL.xls");
		        FileOutputStream fout = new FileOutputStream(file);
		        workbook.write(fout);
		        fout.close();
				req.set("code", 0);
				req.set("msg", "成功");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				req.set("code", 1);
				req.set("msg", "失败！");
				return false;
			}
		});
		renderJson(req);
	}
	/**获取不合格附件
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年10月14日 下午2:23:18
	 */
	public void getUnqualifyFile() {
		long fujianId=getParaToLong("fujianId");
		com.ray.common.model.File file=com.ray.common.model.File.dao.findById(fujianId);
		renderJson(Ret.ok("data",file));
	}









	/**opl详情
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月25日 下午3:31:31
	 */
	public void toQtyOPLdetail() {
		long issueId=getParaToLong("issueid");
		set("issueid", issueId);
		render("detailQtyIssue.html");
	}
	/**获取OPL详情
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月25日 下午4:12:49
	 */
	public void getOPLdetail() {
		long issueId=getParaToLong("issueId");
		Record issue=Db.findFirst("select a.*,b.dep_name,d.part_no,e.type from h_quality_issue a,h_cause_dep b,h_parts d,h_quality_issue_type e where a.id="+issueId
				+ " and a.dep_id=b.dep_id and a.part_id=d.id and a.type_id=e.id ");
		String level = issue.getStr("level");

		//知会人
		List<HQualityIssueTell> tells=HQualityIssueTell.dao.find("select * from h_quality_issue_tell where issue_id="+issueId+" and status=0");
		String tellUserName="";
		for (int j = 0; j < tells.size(); j++) {
			tellUserName=tellUserName+tells.get(j).getTellUserName()+",";
		}
		issue.set("tell_users_name",tellUserName);
		//车间
		if (issue.get("ware_id")!=null) {
			HWare ware=HWare.dao.findById(issue.getInt("ware_id"));
			issue.set("ware_name", ware.getWareName());
		}else {
			issue.set("ware_name", "暂未维护");
		}
		//遏制措施 contain_s
		String contain_measures="";
		List<HQtyMeasures> containMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where issue_id="+issueId+" and type=2 and is_del=0");
		if (!containMeasures.isEmpty()) {
			for (int j = 0; j < containMeasures.size(); j++) {
				//遏制措施
				contain_measures=contain_measures+"遏制措施"+(j+1)+":"+containMeasures.get(j).getMeasures()+"-"+containMeasures.get(j).getUserName();
				int measureStatus=containMeasures.get(j).getStatus();
				if (measureStatus==1) {
					contain_measures=contain_measures+"-进行中-"+containMeasures.get(j).getPlanFinishDate();
				}else if (measureStatus==2){
					contain_measures=contain_measures+"-已完成-"+containMeasures.get(j).getRealFinishDate();
				}else if (measureStatus==3){
					contain_measures=contain_measures+"-已关闭-"+containMeasures.get(j).getCloseDate();
				}
				contain_measures=contain_measures+"\n";
				//遏制结果
				if(containMeasures.get(j).getContainResult()==null) {
					contain_measures=contain_measures+"遏制结果"+(j+1)+":暂无"+"\n";
				}else {
					contain_measures=contain_measures+"遏制结果"+(j+1)+":"+containMeasures.get(j).getContainResult()+"\n";
				}
			}
		}else {
			contain_measures="暂无遏制措施"+"\n";
		}
		//临时措施
		String temporary_measures="";
		List<HQtyMeasures> temporaryMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where issue_id="+issueId+" and type=3 and is_del=0");
		if (!temporaryMeasures.isEmpty()) {
			for (int j = 0; j < temporaryMeasures.size(); j++) {
				//临时措施
				temporary_measures=temporary_measures+"临时措施"+(j+1)+":"+temporaryMeasures.get(j).getMeasures()+"-"+temporaryMeasures.get(j).getUserName();
				int measureStatus=temporaryMeasures.get(j).getStatus();
				if (measureStatus==1) {
					temporary_measures=temporary_measures+"-进行中-"+temporaryMeasures.get(j).getPlanFinishDate();
				}else if (measureStatus==2){
					temporary_measures=temporary_measures+"-已完成-"+temporaryMeasures.get(j).getRealFinishDate();
				}else if (measureStatus==3){
					temporary_measures=temporary_measures+"-已关闭-"+temporaryMeasures.get(j).getCloseDate();
				}
				temporary_measures=temporary_measures+"\n";
				//有效性跟踪
				List<HQtyTrackHistory> measureTrackRecord=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+temporaryMeasures.get(j).getId());
				if (measureTrackRecord.isEmpty()) {
					temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":暂无"+"\n";
				}else {
					String measureTracks="";
					for (int k = 0; k < measureTrackRecord.size(); k++) {
						measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；;";
					}
					temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"\n";
				}
			}
		}else {
			temporary_measures="暂无临时措施";
		}
		String contain_s=contain_measures+temporary_measures;
		issue.set("contain_s", contain_s);
		//根本原因分析 root_s
		String happen_reason="";//产生原因
		String avoid_happen_measures="";//防产生措施
		List<HQtyIssueReasonAnalysis> happenAnalysis=HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
		if (!happenAnalysis.isEmpty()) {
			happen_reason="产生原因："+"\n";
			avoid_happen_measures="防产生措施："+"\n";
			for (int j = 0; j < happenAnalysis.size(); j++) {
				happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"\n";
				List<HQtyMeasures> avoidHappen=HQtyMeasures.dao.find("select * from h_qty_measures where reason_id="+happenAnalysis.get(j).getId()+" and is_del=0 and type=0");//对应防产生措施
				for (int k = 0; k < avoidHappen.size(); k++) {
					avoid_happen_measures=avoid_happen_measures+(j+1)+"."+(k+1)+avoidHappen.get(k).getMeasures()+"-"+avoidHappen.get(k).getUserName();
					int measureStatus=avoidHappen.get(k).getStatus();
					if (measureStatus==1) {
						avoid_happen_measures=avoid_happen_measures+"-进行中"+avoidHappen.get(k).getPlanFinishDate();
					}else if (measureStatus==2){
						avoid_happen_measures=avoid_happen_measures+"-已完成"+avoidHappen.get(k).getRealFinishDate();
					}else if (measureStatus==3){
						avoid_happen_measures=avoid_happen_measures+"-已关闭"+avoidHappen.get(k).getCloseDate();
					}
					avoid_happen_measures=avoid_happen_measures+"\n";
				}
			}
		}else {
			happen_reason="暂无产生原因"+"\n";
			avoid_happen_measures="暂无防产生措施"+"\n";
		}
		String runout_reason="";//流出原因
		String avoid_runout_measures="";//防流出措施
		List<HQtyIssueReasonAnalysis> runOutAnalysis=HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
		if (!runOutAnalysis.isEmpty()) {
			runout_reason="流出原因："+"\n";
			avoid_runout_measures="防流出措施："+"\n";
			for (int j = 0; j < runOutAnalysis.size(); j++) {
				runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"\n";
				List<HQtyMeasures> avoidRunout=HQtyMeasures.dao.find("select * from h_qty_measures where reason_id="+runOutAnalysis.get(j).getId()+" and is_del=0 and type=1");//对应防流出措施
				for (int k = 0; k < avoidRunout.size(); k++) {
					avoid_runout_measures=avoid_runout_measures+(j+1)+"."+(k+1)+avoidRunout.get(k).getMeasures()+"-"+avoidRunout.get(k).getUserName();
					int measureStatus=avoidRunout.get(k).getStatus();
					if (measureStatus==1) {
						avoid_runout_measures=avoid_runout_measures+"-进行中"+avoidRunout.get(k).getPlanFinishDate();
					}else if (measureStatus==2){
						avoid_runout_measures=avoid_runout_measures+"-已完成"+avoidRunout.get(k).getRealFinishDate();
					}else if (measureStatus==3){
						avoid_runout_measures=avoid_runout_measures+"-已关闭"+avoidRunout.get(k).getCloseDate();
					}
					avoid_runout_measures=avoid_runout_measures+"\n";
				}
			}
		}else {
			runout_reason="暂无流出原因";
			avoid_runout_measures="暂无防流出措施";
		}
		String root_s=happen_reason+runout_reason;
		String avoid_s=avoid_happen_measures+avoid_runout_measures;
		issue.set("root_s", root_s).set("avoid_s", avoid_s);
		//永久措施跟踪3月
		List<HQualityIssueMonthTrack> monthTracks=HQualityIssueMonthTrack.dao.find("select * from h_quality_issue_month_track where issue_id="+issueId);
		issue.set("month_1", " ").set("month_1_remark", "").set("month_1_date", "");
		issue.set("month_2", " ").set("month_2_remark", "").set("month_2_date", "");
		issue.set("month_3", " ").set("month_3_remark", "").set("month_3_date", "");
		String monthTrackString="";
		for (int j = 0; j < monthTracks.size(); j++) {
			int month=monthTracks.get(j).getMonth();
			if (month==1) {
				monthTrackString=monthTrackString+"第一月："+monthTracks.get(j).getRemark()+"\n";
				issue.set("month_1", monthTracks.get(j).getStatus()).set("month_1_remark",  monthTracks.get(j).getRemark()).set("month_1_date",  monthTracks.get(j).getTrackDate());
			}else if (month==2) {
				monthTrackString=monthTrackString+"第二月："+monthTracks.get(j).getRemark()+"\n";
				issue.set("month_2", monthTracks.get(j).getStatus()).set("month_2_remark", monthTracks.get(j).getRemark()).set("month_2_date",  monthTracks.get(j).getTrackDate());
			}else if (month==3) {
				monthTrackString=monthTrackString+"第三月："+monthTracks.get(j).getRemark()+"\n";
				issue.set("month_3", monthTracks.get(j).getStatus()).set("month_3_remark", monthTracks.get(j).getRemark()).set("month_3_date",  monthTracks.get(j).getTrackDate());
			}
		}
		//OPL整体跟踪记录
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<HQtyIssueTrackRecord> trackRecords=HQtyIssueTrackRecord.dao.find("select * from h_qty_issue_track_record where issue_id="+issueId);
		String  oplTrackRecord="";
		for (int j = 0; j < trackRecords.size(); j++) {
			oplTrackRecord=oplTrackRecord+(j+1)+"."+trackRecords.get(j).getTrackRecord()+"-"+trackRecords.get(j).getCreateUserName()+"-"+sdf1.format(trackRecords.get(j).getCreateTime())+"\n";
		}
		String record=" ";
		if (!"".equals(oplTrackRecord)) {
			record=oplTrackRecord;
		}
		if (!"".equals(monthTrackString)) {
			record=record+monthTrackString;
		}
		issue.set("record", record);
		//8D计划
		Record issuePlan=new Record();
		Record issueReal=new Record();

		if ("C".equals(level)) {
			HQtyIssue8dPlan plans = HQtyIssue8dPlan.dao.findFirst("select * from h_qty_issue_8d_plan where issue_id=" + issueId);
			issuePlan.set("plan_real", "计划")
					.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
					.set("d_three", plans.getDThree())
					.set("d_one_status", 0).set("d_two_status", 0)
					.set("d_three_status", 0);
			//8D实际
			HQtyIssue8dReal reals = HQtyIssue8dReal.dao.findFirst("select * from h_qty_issue_8d_real where issue_id=" + issueId);
			issueReal.set("plan_real", "实际")
					.set("d_one", "点击编辑").set("d_two", "点击编辑")
					.set("d_three", "点击编辑")
					.set("d_one_status", 0).set("d_two_status", 0)
					.set("d_three_status", 0);

			if (reals != null) {
				issuePlan
						.set("d_one_status", reals.getDOneStatus()).set("d_two_status", reals.getDTwoStatus())
						.set("d_three_status", reals.getDThreeStatus());
				issueReal
						.set("d_one_status", reals.getDOneStatus()).set("d_two_status", reals.getDTwoStatus())
						.set("d_three_status", reals.getDThreeStatus());
				if (reals.getDOneStatus() == 2) {
					issueReal.set("d_one", reals.getDOne());
				}
				if (reals.getDTwoStatus() == 2) {
					issueReal.set("d_two", reals.getDTwo());
				}
				if (reals.getDThreeStatus() == 2) {
					issueReal.set("d_three", reals.getDThree());
				}
			}
			MyCreateQtyIssueController.setThreeMonth(issuePlan, reals);
		} else {
			HQtyIssue8dPlan plans=HQtyIssue8dPlan.dao.findFirst("select * from h_qty_issue_8d_plan where issue_id="+issueId);
			issuePlan.set("plan_real", "计划")
			.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
			.set("d_three", plans.getDThree()).set("d_four", plans.getDFour())
			.set("d_five", plans.getDFive()).set("d_six", plans.getDSix())
			.set("d_seven", plans.getDSeven())
			.set("d_one_status", 0).set("d_two_status",0)
			.set("d_three_status", 0).set("d_four_status", 0)
			.set("d_five_status", 0).set("d_six_status", 0)
			.set("d_seven_status", 0);
			//8D实际
			HQtyIssue8dReal reals=HQtyIssue8dReal.dao.findFirst("select * from h_qty_issue_8d_real where issue_id="+issueId);
			issueReal.set("plan_real", "实际")
			.set("d_one", " ").set("d_two", " ")
			.set("d_three", " ").set("d_four", " ")
			.set("d_five", " ").set("d_six", " ")
			.set("d_seven", " ")
			.set("d_one_status", 0).set("d_two_status",0)
			.set("d_three_status", 0).set("d_four_status", 0)
			.set("d_five_status", 0).set("d_six_status", 0)
			.set("d_seven_status", 0);
			if (reals!=null) {
				issuePlan
				.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
				.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
				.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
				.set("d_seven_status", reals.getDSevenStatus());
				issueReal
				.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
				.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
				.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
				.set("d_seven_status", reals.getDSevenStatus());
				if (reals.getDOneStatus()==2) {
					issueReal.set("d_one", reals.getDOne());
				}
				if (reals.getDTwoStatus()==2) {
					issueReal.set("d_two", reals.getDTwo());
				}
				if (reals.getDThreeStatus()==2) {
					issueReal.set("d_three", reals.getDThree());
				}
				if (reals.getDFourStatus()==2) {
					issueReal.set("d_four", reals.getDFour());
				}
				if (reals.getDFiveStatus()==2) {
					issueReal.set("d_five", reals.getDFive());
				}
				if (reals.getDSixStatus()==2) {
					issueReal.set("d_six", reals.getDSix());
				}
				if (reals.getDSevenStatus()==2) {
					issueReal.set("d_seven", reals.getDSeven());
				}
			}
			MyCreateQtyIssueController.setThreeMonth(issuePlan, reals);
		}
		List<Record> dTimeList=new ArrayList<Record>();
		dTimeList.add(issuePlan);
		dTimeList.add(issueReal);
		Record data=new Record();
		data.set("issue", issue).set("d_time", dTimeList);
		renderJson(Ret.ok("data",data));
	}

	/**
	 * 添加快反追踪
	 */
	public void addReaction() throws ParseException {
		long issueId=getParaToLong("issueId");
		String reaction = getPara("reaction");
		String next_review_time = getPara("next_review_time");
//		String sql = "update h_quality_issue set quick_reaction = '"+reaction+"' , next_review_time = '"+next_review_time+"' where id = '"+issueId+"' ";
//		int update = Db.update(sql);
		HQualityIssue hQualityIssue = new HQualityIssue().dao().findById(issueId);
		Date parse = new SimpleDateFormat("yyyy-MM-dd").parse(next_review_time);
		hQualityIssue.setQuickReaction(reaction);
		hQualityIssue.setNextReviewTime(parse);
		boolean save = hQualityIssue.update();
		if (save){
			renderJson(Ret.ok("msg","修改成功"));
		}else{
			renderJson(Ret.fail("msg","修改失败"));
		}
	}

	/**
	 * 添加快反追踪
	 */
	public void addZJReaction() throws ParseException {
		Subject subject = SecurityUtils.getSubject();
		Record user = (Record)subject.getSession().getAttribute("user");
		String id = user.getStr("id");

		Record first = Db.findFirst(" select * from user_role where user_id = '" + id + "' ");
		String role_id = first.getStr("role_id");
		if ("35".equals(role_id)){
			long issueId=getParaToLong("issueId");
			String reaction = getPara("reaction");
			HQualityIssue hQualityIssue = new HQualityIssue().dao().findById(issueId);
			hQualityIssue.setZJQuickReaction(reaction);
			boolean save = hQualityIssue.update();
			if (save){
				renderJson(Ret.ok("msg","修改成功"));
			}else{
				renderJson(Ret.fail("msg","修改失败"));
			}
		}else{
			renderJson(Ret.fail("msg","只有总监能修改"));
			return;
		}
	}

	/**
	 * 添加快反追踪
	 */
	public void addFZJLReaction() throws ParseException {
		Subject subject = SecurityUtils.getSubject();
		Record user = (Record)subject.getSession().getAttribute("user");
		String id = user.getStr("id");

		Record first = Db.findFirst(" select * from user_role where user_id = '" + id + "' ");
		String role_id = first.getStr("role_id");
		if ("35".equals(role_id)){
			long issueId=getParaToLong("issueId");
			String reaction = getPara("reaction");
			HQualityIssue hQualityIssue = new HQualityIssue().dao().findById(issueId);
			hQualityIssue.setFZJLQuickReaction(reaction);
			boolean save = hQualityIssue.update();
			if (save){
				renderJson(Ret.ok("msg","修改成功"));
			}else{
				renderJson(Ret.fail("msg","修改失败"));
			}
		}else{
			renderJson(Ret.fail("msg","只有副总经理能修改"));
			return;
		}
	}

	/**
	 * 添加快反追踪
	 */
	public void addZJLReaction() throws ParseException {
		Subject subject = SecurityUtils.getSubject();
		Record user = (Record)subject.getSession().getAttribute("user");
		String id = user.getStr("id");

		Record first = Db.findFirst(" select * from user_role where user_id = '" + id + "' ");
		String role_id = first.getStr("role_id");
		if ("35".equals(role_id)){
			long issueId=getParaToLong("issueId");
			String reaction = getPara("reaction");
			HQualityIssue hQualityIssue = new HQualityIssue().dao().findById(issueId);
			hQualityIssue.setZJLQuickReaction(reaction);
			boolean save = hQualityIssue.update();
			if (save){
				renderJson(Ret.ok("msg","修改成功"));
			}else{
				renderJson(Ret.fail("msg","修改失败"));
			}
		}else{
			renderJson(Ret.fail("msg","只有总经理能修改"));
			return;
		}
	}
	public void test(){
		List<Record> user = Db.find(" select * from user  ");
		user.forEach(i->{
			String password = i.getStr("password");
			password = SecureUtil.md5(password);
			Db.update("update user set password = '"+password+"' where username = '"+i.getStr("username")+"'");
		});
		renderJson();
	}

}




