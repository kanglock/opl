package com.ray.controller.admin.QualityIssue;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.model.HQualityIssueType;
import freemarker.template.SimpleDate;

/**
 * 质量板块分析 /qualityIsueAnalysis/toQualityIssusAnalysis
 * @author FL00024996
 *
 */
public class QualityIssueAnalysisController extends Controller {

	public void toQualityIssusAnalysis() {
		render("qualityIssueAnalysis.html");
	}

	/**
	 * 统计清单2
	 */
	public void toQualityIssusAnalysis2() {
		render("qualityIssueAnalysis2.html");
	}

	/**
	 * 问题总体来源分析
	 */
	public void QuestionTotalAnalysis(){
		HashMap<String, Object> totalMap = new HashMap<>();
		HashMap<String, Object> closeMap = new HashMap<>();
		HashMap<String, Object> frequencyMap = new HashMap<>();
		HashMap<String, Object> closeRateMap = new HashMap<>();
		HashMap<String, Object> frequencyRateMap = new HashMap<>();
		List<Record> allDept = Db.find("select * from h_cause_dep where status=0 ");
		LinkedList<HashMap<String,Object>> tableColumn = new LinkedList<>();
		HashMap<String,Object> firstColumn = new HashMap<>();
		firstColumn.put("key",0);
		firstColumn.put("field","project");
		firstColumn.put("title","");
		tableColumn.add(firstColumn);
		HashMap<String,Object> secondColumn = new HashMap<>();
		secondColumn.put("key",1);
		secondColumn.put("field","公司整体");
		secondColumn.put("title","公司整体");
		tableColumn.add(secondColumn);
		for (int i = 0; i < allDept.size(); i++) {
			HashMap<String,Object> map = new HashMap<>();
			map.put("key",i+2);
			map.put("field",allDept.get(i).get("dep_name"));
			map.put("title",allDept.get(i).get("dep_name"));
			tableColumn.add(map);
		}

		List<Record> records = Db.find(" select dep_id,status,happen_frequency from  h_quality_issue  where is_del = 0  and level != 'C' and product_stage != '手工样件' ");
		List<Map> dataList=new ArrayList<Map>();
		//总体数据
		double totalCount = records.size();
		double closeCount = records.stream().filter(s -> "G".equals(s.getStr("status"))).count();
		double frequencyCount = records.stream().filter(s -> !"首发".equals(s.getStr("happen_frequency"))).count();
		String closeRate = divided(closeCount, totalCount);
		String frequencyRate = divided(frequencyCount, totalCount);

		//项目
		totalMap.put("project","问题总数");
		closeMap.put("project","关闭数");
		frequencyMap.put("project","重复发生");
		closeRateMap.put("project","关闭率");
		frequencyRateMap.put("project","重复发生率");

		totalMap.put("公司整体",totalCount);
		closeMap.put("公司整体",closeCount);
		frequencyMap.put("公司整体",frequencyCount);
		closeRateMap.put("公司整体",closeRate);
		frequencyRateMap.put("公司整体",frequencyRate);

		for (int i = 0; i < allDept.size(); i++) {
			String dep_id = allDept.get(i).getStr("dep_id");
			String dep_name = allDept.get(i).getStr("dep_name");

			totalCount = records.stream().filter(s -> dep_id.equals(s.getStr("dep_id")) ).count();
			closeCount = records.stream().filter(s -> dep_id.equals(s.getStr("dep_id")) && "G".equals(s.getStr("status"))).count();
			frequencyCount = records.stream().filter(s -> dep_id.equals(s.getStr("dep_id")) && !"首发".equals(s.getStr("happen_frequency"))).count();
			closeRate = divided(closeCount, totalCount);
			frequencyRate = divided(frequencyCount, totalCount);

			totalMap.put(dep_name,totalCount);
			closeMap.put(dep_name,closeCount);
			frequencyMap.put(dep_name,frequencyCount);
			closeRateMap.put(dep_name,closeRate);
			frequencyRateMap.put(dep_name,frequencyRate);
		}
		dataList.add(totalMap);
		dataList.add(closeMap);
		dataList.add(frequencyMap);
		dataList.add(closeRateMap);
		dataList.add(frequencyRateMap);

		renderJson(Ret.ok("tableColumn",tableColumn).set("dataList",dataList));
	}


	/**
	 * 各部门问题分析
	 */
	public void deptQuestionAnalysis() throws ParseException {
		List<Record> allType = Db.find(" select * from h_quality_issue_type where status=0 ");
		String sql = "select dep_id,type_id from  h_quality_issue  where is_del = 0  and level != 'C' and product_stage != '手工样件'";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sql=sql+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("startTime") != "" && getPara("endTime") !=null ){
			String endTime = getPara("endTime");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Date date=sdf.parse(endTime);
			Calendar rightNow=Calendar.getInstance();
			rightNow.setTime(date);
			rightNow.add(Calendar.DAY_OF_YEAR,1);
			endTime=sdf.format(rightNow.getTime());

			sql = sql + " and create_time between '"+getPara("startTime")+"' and  '"+endTime +"' ";
		}
		List<Record> allOpl = Db.find(sql);

		LinkedList<HashMap<String, Object>> list = new LinkedList<>();
		allType.stream().forEach(type->{
			HashMap<String, Object> map = new HashMap<>();
			String typeId = type.getStr("id");
			String typeName = type.getStr("type");
			int totalCount = allOpl.size();
			long count = allOpl.stream().filter(opl -> typeId.equals(opl.getStr("type_id"))).count();
			map.put("type",typeName);
			map.put("count",count);
			map.put("rate",divided(count,totalCount));
			list.add(map);
		});
		HashMap<String, Object> map = new HashMap<>();
		map.put("type","合计");
		map.put("count",allOpl.size());
		map.put("rate","100%");
		list.add(map);

		renderJson(Ret.ok("deptList",list));
	}

	/**
	 * 供应商问题分析
	 */
	public void supplierQuestionAnalysis() throws ParseException {
		List<Record> allType = Db.find(" select * from h_quality_issue_type where status=0 ");
		String sql = " select dep_id,supplier_type_id from  h_quality_issue  where is_del = 0  and level != 'C' and product_stage != '手工样件'   and supplier_type_id is not null and supplier_type_id  != 0  ";
		if (getPara("startTime") != "" && getPara("endTime") !=null ){
			String endTime = getPara("endTime");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			Date date=sdf.parse(endTime);
			Calendar rightNow=Calendar.getInstance();
			rightNow.setTime(date);
			rightNow.add(Calendar.DAY_OF_YEAR,1);
			endTime=sdf.format(rightNow.getTime());

			sql = sql + " and create_time between '"+getPara("startTime")+"' and  '"+endTime +"' ";
		}
		List<Record> allOpl = Db.find(sql);
		LinkedList<HashMap<String, Object>> list = new LinkedList<>();
		allType.stream().forEach(type->{
			HashMap<String, Object> map = new HashMap<>();
			String typeId = type.getStr("id");
			String typeName = type.getStr("type");
			int totalCount = allOpl.size();
			long count = allOpl.stream().filter(opl -> typeId.equals(opl.getStr("supplier_type_id"))).count();
			map.put("type",typeName);
			map.put("count",count);
			map.put("rate",divided(count,totalCount));
			list.add(map);
		});


		HashMap<String, Object> map = new HashMap<>();
		map.put("type","合计");
		map.put("count",allOpl.size());
		map.put("rate","100%");
		list.add(map);

		renderJson(Ret.ok("supplierList",list));
	}
	/**
	 * 除法保留2位小数  加上百分号
	 * @return
	 */
	private String divided(double countA,double countB){
		String s = "";
		if (countB == 0.00){
			s = "0";
		}else{
			s = new BigDecimal(countA).divide(new BigDecimal(countB),4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100")).setScale(2).toString();
		}
		return  s += "%";
	}

	/**问题来源&问题类型
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年9月23日 上午10:11:40
	 */
	public void getAnalysisTypeSourceTableData() {
		String sqlCom="select count(*) as total_num from h_quality_issue where is_del=0 ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		List<Record> dataList=new ArrayList<Record>();
		List<HQualityIssueType> typeList=HQualityIssueType.dao.find("select * from h_quality_issue_type where status=0");
		String[] sources= {"入厂检测","生产过程","成品入库","客户反馈","其他","合计"};
		for (int i = 0; i < sources.length; i++) {
			Record analysisData=new Record();
			analysisData.set("source", sources[i]);
			String sqlSourceCom=sqlCom;
			if (i<5) {
				sqlSourceCom=sqlSourceCom+" and source='"+sources[i]+"'";
			}
			int sourceAllNum=0;//同等级问题总数
			int sourceCloseNum=0;//同等级关闭问题数
			for (int j = 0; j < typeList.size(); j++) {
				String sqlAll=sqlSourceCom+" and type_id='"+typeList.get(j).getId().intValue()+"'";
				String sqlClose=sqlSourceCom+" and type_id='"+typeList.get(j).getId().intValue()+"' and status='G'";
				Record allissue=Db.findFirst(sqlAll);
				Record closeIssue=Db.findFirst(sqlClose);
				int alltypeNum=allissue.getInt("total_num");
				int closeypeNum=closeIssue.getInt("total_num");
				sourceAllNum=sourceAllNum+alltypeNum;
				sourceCloseNum=sourceCloseNum+closeypeNum;
				analysisData.set("all_"+typeList.get(j).getEnTitle(), alltypeNum).set("close_"+typeList.get(j).getEnTitle(), closeypeNum);
			}
			analysisData.set("all_heji", sourceAllNum).set("close_heji", sourceCloseNum);
			//关闭率
			if (sourceAllNum>0) {
				BigDecimal closeRate=new BigDecimal(sourceCloseNum).divide(new BigDecimal(sourceAllNum), 4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
				analysisData.set("close_rate", closeRate+"%");
			}else {
				analysisData.set("close_rate", "0.00%");
			}
			dataList.add(analysisData);
		}
		renderJson(Ret.ok("data",dataList));
	}
	/**获取分析数据详情---问题类型统计
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月30日 下午5:32:54
	 */
	public void getAnalysisTableData() {
		String sqlCom="select count(*) as total_num from h_quality_issue where is_del=0 ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		List<Record> dataList=new ArrayList<Record>();
		List<HQualityIssueType> typeList=HQualityIssueType.dao.find("select * from h_quality_issue_type where status=0");
		String[] levels= {"A","B","C","合计"};
		for (int i = 0; i < levels.length; i++) {
			Record analysisData=new Record();
			analysisData.set("level", levels[i]);
			String sqlLevelCom=sqlCom;
			if (i<3) {
				sqlLevelCom=sqlLevelCom+" and level='"+levels[i]+"'";
			}else {
				sqlLevelCom=sqlLevelCom+" and (level='A' or level='B' or level='C')";
			}
			int levelAllNum=0;//同等级问题总数
			int levelCloseNum=0;//同等级关闭问题数
			for (int j = 0; j < typeList.size(); j++) {
				String sqlAll=sqlLevelCom+" and type_id='"+typeList.get(j).getId().intValue()+"'";
				String sqlClose=sqlLevelCom+" and type_id='"+typeList.get(j).getId().intValue()+"' and status='G'";
				Record allissue=Db.findFirst(sqlAll);
				Record closeIssue=Db.findFirst(sqlClose);
				int alltypeNum=allissue.getInt("total_num");
				int closeypeNum=closeIssue.getInt("total_num");
				levelAllNum=levelAllNum+alltypeNum;
				levelCloseNum=levelCloseNum+closeypeNum;
				analysisData.set("all_"+typeList.get(j).getEnTitle(), alltypeNum).set("close_"+typeList.get(j).getEnTitle(), closeypeNum);
			}
			analysisData.set("all_heji", levelAllNum).set("close_heji", levelCloseNum);
			//关闭率
			if (levelAllNum>0) {
				BigDecimal closeRate=new BigDecimal(levelCloseNum).divide(new BigDecimal(levelAllNum), 4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
				analysisData.set("close_rate", closeRate+"%");
			}else {
				analysisData.set("close_rate", "0.00%");
			}
			dataList.add(analysisData);
		}
		renderJson(Ret.ok("data",dataList));
	}
	/**
	 * 饼状图数据获取---严重度
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月3日 上午10:02:03
	 */
	public void getPieData() {
		String sqlCom="select count(*) as total_num from h_quality_issue where is_del=0  ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		//严重度
		String[] levels= {"A","B","C"};
		List<Record> levelList=new ArrayList<Record>();
		for (int i = 0; i < levels.length; i++) {
			Record analysisData=new Record();
			analysisData.set("level", levels[i]);
			String sqlLevelCom=sqlCom+" and level='"+levels[i]+"'";
			Record allLevelIssue=Db.findFirst(sqlLevelCom);
			int allNum=allLevelIssue.getInt("total_num");
			analysisData.set("total_num", allNum);
			levelList.add(analysisData);
		}
		//类型
		List<HQualityIssueType> types=HQualityIssueType.dao.find("select * from h_quality_issue_type where status=0");
		List<Record> typeList=new ArrayList<Record>();
		for (int i = 0; i < types.size(); i++) {
			Record analysisData=new Record();
			analysisData.set("type", types.get(i).getType());
			String sqlTypeCom=sqlCom+" and type_id='"+types.get(i).getId().intValue()+"'";
			Record allTypeIssue=Db.findFirst(sqlTypeCom);
			int allNum=allTypeIssue.getInt("total_num");
			analysisData.set("total_num", allNum);
			typeList.add(analysisData);
		}
		Record record=new Record();
		record.set("level_list", levelList).set("type_list", typeList);
		renderJson(Ret.ok("data",record));
	}
	/**问题来源分析
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月31日 上午9:13:06
	 */
	public void getAnalysisSource() {
		String sqlCom="select count(*) as total_num from h_quality_issue where is_del=0 ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		List<Record> dataList=new ArrayList<Record>();
		String[] levels= {"A","B","C","合计"};
		String[] sources= {"入厂检测","生产过程","成品入库","客户反馈","其他","合计"};
		for (int i = 0; i < sources.length; i++) {
			Record analysisData=new Record();
			analysisData.set("source", sources[i]);
			String sqlSourceCom=sqlCom;
			if (i<5) {
				sqlSourceCom=sqlSourceCom+" and source='"+sources[i]+"'";
			}
			int sourceAllNum=0;//同等级问题总数
			int sourceCloseNum=0;//同等级关闭问题数
			for (int j = 0; j < levels.length-1; j++) {
				String sqlAll=sqlSourceCom;
				String sqlClose=sqlSourceCom;
				sqlAll=sqlAll+" and level='"+levels[j]+"'";
				sqlClose=sqlClose+" and level='"+levels[j]+"' and status='G'";
				Record allissue=Db.findFirst(sqlAll);
				Record closeIssue=Db.findFirst(sqlClose);
				int alltypeNum=allissue.getInt("total_num");
				int closeypeNum=closeIssue.getInt("total_num");
				sourceAllNum=sourceAllNum+alltypeNum;
				sourceCloseNum=sourceCloseNum+closeypeNum;
				if ("A".equals(levels[j])) {
					analysisData.set("all_a", alltypeNum).set("close_a", closeypeNum);
				}else if ("B".equals(levels[j])) {
					analysisData.set("all_b", alltypeNum).set("close_b", closeypeNum);
				}else if ("C".equals(levels[j])) {
					analysisData.set("all_c", alltypeNum).set("close_c", closeypeNum);
				}
			}
			analysisData.set("all_heji", sourceAllNum).set("close_heji", sourceCloseNum);
			//关闭率
			if (sourceAllNum>0) {
				BigDecimal closeRate=new BigDecimal(sourceCloseNum).divide(new BigDecimal(sourceAllNum), 4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
				analysisData.set("close_rate", closeRate+"%");
			}else {
				analysisData.set("close_rate", "0.00%");
			}
			dataList.add(analysisData);
		}
		renderJson(Ret.ok("data",dataList));
	}
	/**问题来源
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月31日 上午9:41:59
	 */
	public void getAnalysisSourcePieData() {
		String sqlCom="select count(*) as total_num from h_quality_issue where is_del=0  ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		//严重度
		String[] sources= {"入厂检测","生产过程","成品入库","其他","客户反馈"};
		List<Record> sourceList=new ArrayList<Record>();
		for (int i = 0; i < sources.length; i++) {
			Record analysisData=new Record();
			analysisData.set("source", sources[i]);
			String sqlLevelCom=sqlCom+" and source='"+sources[i]+"'";
			Record allLevelIssue=Db.findFirst(sqlLevelCom);
			int allNum=allLevelIssue.getInt("total_num");
			analysisData.set("total_num", allNum);
			sourceList.add(analysisData);
		}
		Record record=new Record();
		record.set("source_list", sourceList);
		renderJson(Ret.ok("data",record));
	}

	/**
	 * 通过部门获得问题来源统计信息
	 */
	public void getQuestionSource(){
		List<String> sourceList = Arrays.asList("入厂检测-incomingCheck", "生产过程-productProcess", "成品入库-inStorage", "客户反馈-feedback", "其他-other");
		List<String> classifyList = Arrays.asList("1-notgood", "2-change", "3-standard", "4-pack",
				"5-gongyi","6-sheji","7-supplierunpack","8-clear","9-other","12-supplierpack");
		List<String> stageList = Arrays.asList("D1-d_one", "D2-d_two", "D3-d_three", "D4-d_four", "D5-d_five", "D6-d_six", "D7-d_seven");

		String deptId = getPara("selDep");
		String sql = "select i.type_id,i.source,r.* from h_quality_issue i join h_qty_issue_8d_real r on i.id = r.issue_id  where 1 = 1  and i.product_stage != '手工样件' and i.is_del = 0 and level != 'C' ";

		String sql2 = "select i.type_id,i.source,r.* from h_quality_issue i join h_qty_issue_8d_plan r on i.id = r.issue_id  where 1 = 1  and i.product_stage != '手工样件' and i.is_del = 0 and level != 'C' ";


		if (deptId != null) {
			sql += " and  i.dep_id = '"+deptId+"' ";
			sql2 += " and  i.dep_id = '"+deptId+"' ";

		}
		List<Record> list = Db.find(sql);
		List<Record> list2 = Db.find(sql2);


		//问题分类统计
		ArrayList<Record> questionClassifyList = new ArrayList<>();
		//问题来源统计
		ArrayList<Record> questionSourceList = new ArrayList<>();
		for (String stage : stageList) {
			Record questionClassify=new Record();
			long questionClassifyTotalPlan = 0;
			long questionClassifyTotalReal = 0;
			for (String s : classifyList) {
				String[] split1 = stage.split("-");
				questionClassify.set("stage",split1[0]);
				String[] split = s.split("-");
				long source = list.stream().filter(j -> split[0].equals(j.getStr("type_id")) && j.getStr(split1[1])!= null ).count();
				long source2 = list2.stream().filter(j -> split[0].equals(j.getStr("type_id")) && j.getStr(split1[1])!= null ).count();

				questionClassify.set(s.split("-")[1],source+"/"+source2);
				questionClassifyTotalReal += source;
				questionClassifyTotalPlan += source2;

			}
			questionClassify.set("total",questionClassifyTotalReal+"/"+questionClassifyTotalPlan);
			questionClassifyList.add(questionClassify);

			Record questionSource=new Record();
			long questionSourceTotalPlan = 0;
			long questionSourceTotalReal = 0;
			for (String s : sourceList) {
				String[] split1 = stage.split("-");
				questionSource.set("stage",split1[0]);
				String[] split = s.split("-");
				long source = list.stream().filter(j -> split[0].equals(j.getStr("source")) && j.getStr(split1[1])!= null ).count();
				long source2 = list2.stream().filter(j -> split[0].equals(j.getStr("source")) && j.getStr(split1[1])!= null ).count();

				questionSource.set(s.split("-")[1],source+"/"+source2);
				questionSourceTotalReal += source;
				questionSourceTotalPlan += source2;

			}
			questionSource.set("total",questionSourceTotalReal+"/"+questionSourceTotalPlan);
			questionSourceList.add(questionSource);
		}
		renderJson(Ret.ok("questionSourceList",questionSourceList).set("questionClassifyList",questionClassifyList));
	}

}
