package com.ray.controller.admin.ClothesIssue;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 我负责的质量OPL /myDutyQtyOPL/toMyDutyQtyOPL
 * @author FL00024996
 *
 */ 
public class MyDutyClothesController extends Controller {

	public void getMyDutyClothesOPL() {
		Record loginUser=getSessionAttr("user");
		User user=User.dao.findById(loginUser.getInt("id"));
		StringBuilder sb=new StringBuilder();
		sb.append(" from h_clothes_issue a,h_cause_dep b,h_parts d,h_quality_issue_type e where a.status<>'G' and "
				+ "a.dep_id=b.dep_id and a.is_del=0 and a.sure_user_id="+user.getId()+" and a.part_id=d.id and a.type_id=e.id ");
		String selLevel=get("selLevel");
		if (!"".equals(selLevel)&& selLevel!=null) {
			sb.append(" and a.level='"+selLevel+"'");
		}
		long selDep=getParaToLong("selDep");
		if (selDep!=0) {
			sb.append(" and a.dep_id="+selDep);
		}
		int selWarehouse=getParaToInt("selWarehouse");
		if (selWarehouse!=0) {
			sb.append(" and a.ware_id="+selWarehouse);
		}
		String selOPLdes=get("selOPLdes");
		if (!"".equals(selOPLdes)&& selOPLdes!=null) {
			sb.append(" and a.description like '%"+selOPLdes+"%'");
		}
		String selPartCode=get("selPartCode");
		if (!"".equals(selPartCode)&& selPartCode!=null) {
			sb.append(" and d.part_no like '%"+selPartCode+"%'");
		}
		String selProductName=get("selProductName");
		if (!"".equals(selProductName)&& selProductName!=null) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		String selCusName=get("selCusName");
		if (!"".equals(selCusName)&& selCusName!=null) {
			sb.append(" and a.cus_name like '%"+selCusName+"%'");
		}
		String selStatus=get("selStatus");
		if (!"".equals(selStatus)&& selStatus!=null) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		String selFrequency=get("selFrequency");
		if (!"".equals(selFrequency)&& selFrequency!=null) {
			sb.append(" and a.happen_frequency='"+selFrequency+"'");
		}
		if (!"".equals(get("selType"))&& get("selType")!=null) {
			sb.append(" and a.type_id="+get("selType"));
		}
		String selSource=get("selSource");
		if (!"".equals(selSource)&& selSource!=null) {
			sb.append(" and a.source='"+selSource+"'");
		}
		String selProductStage=get("selProductStage");
		if (!"".equals(selProductStage)&& selProductStage!=null) {
			sb.append(" and a.product_stage='"+selProductStage+"'");
		}
		String selNextReviewTime=get("selNextReviewTime");
		if (!"".equals(selNextReviewTime)&& selNextReviewTime!=null) {
			sb.append(" and a.next_review_time='"+selNextReviewTime+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name,d.part_no,e.type ",sb.toString()+" order by id desc");
		List<Record> qtyOPLList=new ArrayList<Record>();
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			issueList.getList().get(i).set("caozuo", " ");
			//获取知会人
			List<HClothesIssueTell> tells=HClothesIssueTell.dao.find("select * from h_clothes_issue_tell where issue_id="+issueId+" and status=0");
			List<Integer> tellUserId=new ArrayList<Integer>();
			String tellUserName=" ";
			for (int j = 0; j < tells.size(); j++) {
				tellUserId.add(tells.get(j).getTellUserId());
				tellUserName=tellUserName+tells.get(j).getTellUserName()+"，";
			}
			issueList.getList().get(i)
			.set("tell_users_name",tellUserName)
			.set("tell_users_id", tellUserId);
			//车间
			if (issueList.getList().get(i).get("ware_id")!=null) {
				HWare ware=HWare.dao.findById(issueList.getList().get(i).getInt("ware_id"));
				issueList.getList().get(i).set("ware_name", ware.getWareName());
			}else {
				issueList.getList().get(i).set("ware_name", "暂未维护");
			}
			int canUp=0;
			HClothesoplUpgradeRecord upgradeRecord=HClothesoplUpgradeRecord.dao.findFirst("select * from h_clothesopl_upgrade_record where status=0 and issue_id="+issueId);
			if (upgradeRecord==null) {//没有升级记录，可升级
				canUp=1;
			}else {//有升级记录
				if (upgradeRecord.getSpStatus()==1&&upgradeRecord.getNewUserId()==user.getId()) {
					canUp=1;
				}else if (upgradeRecord.getSpStatus()==2 && upgradeRecord.getOldUserId().intValue()==user.getId().intValue()&&issueList.getList().get(i).getInt("issue_status")==0) {
					canUp=1;
				}
			}
			issueList.getList().get(i).set("can_up", canUp);
			//遏制措施 contain_s
			String contain_measures="";
			List<HClothesMeasures> containMeasures=HClothesMeasures.dao.find("select * from h_clothes_measures where issue_id="+issueId+" and type=2 and is_del=0");
			if (!containMeasures.isEmpty()) {
				for (int j = 0; j < containMeasures.size(); j++) {
					//遏制措施
					contain_measures=contain_measures+"遏制措施"+(j+1)+":"+containMeasures.get(j).getMeasures()+"-"+containMeasures.get(j).getUserName();
					int measureStatus=containMeasures.get(j).getStatus();
					if (measureStatus==1) {
						contain_measures=contain_measures+"-进行中-"+containMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						contain_measures=contain_measures+"-已完成-"+containMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						contain_measures=contain_measures+"-已关闭-"+containMeasures.get(j).getCloseDate();
					}
					contain_measures=contain_measures+"<br>";
					//遏制结果
					if(containMeasures.get(j).getContainResult()==null) {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":暂无"+"<br>";
					}else {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":"+containMeasures.get(j).getContainResult()+"<br>";
					}
					//有效性跟踪
					List<HClothesTrackHistory> measureTrackRecord=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+containMeasures.get(j).getId());
					if (measureTrackRecord.isEmpty()) {
						contain_measures=contain_measures+"有效性跟踪"+(j+1)+":暂无"+"<br>";
					}else {
						String measureTracks="";
						for (int k = 0; k < measureTrackRecord.size(); k++) {
							measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；;";
						}
						contain_measures=contain_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"<br>";
					}
				}
			}else {
				contain_measures="暂无遏制措施"+"<br>";
			}
			//临时措施
			String temporary_measures="";
			List<HClothesMeasures> temporaryMeasures=HClothesMeasures.dao.find("select * from h_clothes_measures where issue_id="+issueId+" and type=3 and is_del=0");
			if (!temporaryMeasures.isEmpty()) {
				for (int j = 0; j < temporaryMeasures.size(); j++) {
					//临时措施
					temporary_measures=temporary_measures+"临时措施"+(j+1)+":"+temporaryMeasures.get(j).getMeasures()+"-"+temporaryMeasures.get(j).getUserName();
					int measureStatus=temporaryMeasures.get(j).getStatus();
					if (measureStatus==1) {
						temporary_measures=temporary_measures+"-进行中-"+temporaryMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						temporary_measures=temporary_measures+"-已完成-"+temporaryMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						temporary_measures=temporary_measures+"-已关闭-"+temporaryMeasures.get(j).getCloseDate();
					}
					temporary_measures=temporary_measures+"<br>";
					//有效性跟踪
					List<HClothesTrackHistory> measureTrackRecord=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+temporaryMeasures.get(j).getId());
					if (measureTrackRecord.isEmpty()) {
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":暂无"+"<br>";
					}else {
						String measureTracks="";
						for (int k = 0; k < measureTrackRecord.size(); k++) {
							measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；";
						}
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"<br>";
					}
				}
			}else {
				temporary_measures="暂无临时措施";
			}
			String contain_s=contain_measures+temporary_measures;
			issueList.getList().get(i).set("contain_s", contain_s);
			//根本原因分析 root_s
			String happen_reason="";//产生原因
			String avoid_happen_measures="";//防产生措施
			List<HClothesIssueReasonAnalysis> happenAnalysis=HClothesIssueReasonAnalysis.dao.find("select * from h_clothes_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
			if (!happenAnalysis.isEmpty()) {
				happen_reason="产生原因："+"<br>";
				avoid_happen_measures="防产生措施："+"<br>";
				for (int j = 0; j < happenAnalysis.size(); j++) {
					happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HClothesMeasures> avoidHappen=HClothesMeasures.dao.find("select * from h_clothes_measures where reason_id="+happenAnalysis.get(j).getId()+" and is_del=0 and type=0");//对应防产生措施
					for (int k = 0; k < avoidHappen.size(); k++) {
						avoid_happen_measures=avoid_happen_measures+(j+1)+"."+(k+1)+avoidHappen.get(k).getMeasures()+"-"+avoidHappen.get(k).getUserName()+"<br>";
						int measureStatus=avoidHappen.get(k).getStatus();
						if (measureStatus==1) {
							avoid_happen_measures=avoid_happen_measures+"-进行中"+avoidHappen.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_happen_measures=avoid_happen_measures+"-已完成"+avoidHappen.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_happen_measures=avoid_happen_measures+"-已关闭"+avoidHappen.get(k).getCloseDate();
						}
						avoid_happen_measures=avoid_happen_measures+"<br>";
						// TODO 需要在 防产生措施  后添加防产生措施的跟踪纪律
						String sql = "select IFNULL(GROUP_CONCAT(remark,';'),'') remark  from h_clothes_track_history where measure_id= '"+avoidHappen.get(k).get("id")+"'";
						Record first = Db.findFirst(sql);
						if(!"".equals(first.get("remark"))){
							int z = k+1;
							avoid_happen_measures += "跟踪记录"+z+"："+first.get("remark")+"<br>";
						}
					}
				}
			}else {
				happen_reason="暂无产生原因"+"<br>";
				avoid_happen_measures="暂无防产生措施"+"<br>";
			}

			String runout_reason="";//流出原因
			String avoid_runout_measures="";//防流出措施
			List<HClothesIssueReasonAnalysis> runOutAnalysis=HClothesIssueReasonAnalysis.dao.find("select * from h_clothes_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
			if (!runOutAnalysis.isEmpty()) {
				runout_reason="流出原因："+"<br>";
				avoid_runout_measures="防流出措施："+"<br>";
				for (int j = 0; j < runOutAnalysis.size(); j++) {
					runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HClothesMeasures> avoidRunout=HClothesMeasures.dao.find("select * from h_clothes_measures where reason_id="+runOutAnalysis.get(j).getId()+" and is_del=0 and type=1");//对应防流出措施
					for (int k = 0; k < avoidRunout.size(); k++) {
						avoid_runout_measures=avoid_runout_measures+(j+1)+"."+(k+1)+avoidRunout.get(k).getMeasures()+"-"+avoidRunout.get(k).getUserName();
						int measureStatus=avoidRunout.get(k).getStatus();
						if (measureStatus==1) {
							avoid_runout_measures=avoid_runout_measures+"-进行中"+avoidRunout.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_runout_measures=avoid_runout_measures+"-已完成"+avoidRunout.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_runout_measures=avoid_runout_measures+"-已关闭"+avoidRunout.get(k).getCloseDate();
						}
						// TODO 需要在 防产生措施  后添加防产生措施的跟踪纪律
						String sql = "select IFNULL(GROUP_CONCAT(remark,';'),'') remark  from h_clothes_track_history where measure_id= '"+avoidRunout.get(k).get("id")+"'";
						Record first = Db.findFirst(sql);
						if(!"".equals(first.get("remark"))){
							int z = k+1;
							avoid_runout_measures += "跟踪记录"+z+"："+first.get("remark")+"<br>";
						}
					}
				}
			}else {
				runout_reason="暂无流出原因";
				avoid_runout_measures="暂无防流出措施";
			}
			String root_s=happen_reason+runout_reason;
			String avoid_s=avoid_happen_measures+avoid_runout_measures;
			issueList.getList().get(i).set("root_s", root_s).set("avoid_s", avoid_s);
			//永久措施跟踪3月
			List<HClothesIssueMonthTrack> monthTracks=HClothesIssueMonthTrack.dao.find("select * from h_clothes_issue_month_track where issue_id="+issueId);
			issueList.getList().get(i).set("month_1", "编辑").set("month_1_remark", "").set("month_1_date", "");
			issueList.getList().get(i).set("month_2", "编辑").set("month_2_remark", "").set("month_2_date", "");
			issueList.getList().get(i).set("month_3", "编辑").set("month_3_remark", "").set("month_3_date", "");
			String monthTrackString="";
			for (int j = 0; j < monthTracks.size(); j++) {
				int month=monthTracks.get(j).getMonth();
				if (month==1) {
					monthTrackString=monthTrackString+"第一月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_1", monthTracks.get(j).getStatus()).set("month_1_remark",  monthTracks.get(j).getRemark()).set("month_1_date",  monthTracks.get(j).getTrackDate());
				}else if (month==2) {
					monthTrackString=monthTrackString+"第二月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_2", monthTracks.get(j).getStatus()).set("month_2_remark", monthTracks.get(j).getRemark()).set("month_2_date",  monthTracks.get(j).getTrackDate());
				}else if (month==3) {
					monthTrackString=monthTrackString+"第三月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_3", monthTracks.get(j).getStatus()).set("month_3_remark", monthTracks.get(j).getRemark()).set("month_3_date",  monthTracks.get(j).getTrackDate());
				}
			}
			//OPL整体跟踪记录
			List<HClothesIssueTrackRecord> trackRecords=HClothesIssueTrackRecord.dao.find(" select * from h_clothes_issue_track_record where issue_id="+issueId);
			String  oplTrackRecord="";
			for (int j = 0; j < trackRecords.size(); j++) {
				oplTrackRecord=oplTrackRecord+(j+1)+"."+trackRecords.get(j).getTrackRecord()+"-"+trackRecords.get(j).getCreateUserName()+"-"+sdf1.format(trackRecords.get(j).getCreateTime())+"<br>";
			}
			String record=" ";
			if (!"".equals(oplTrackRecord)) {
				record=oplTrackRecord;
			}
			if (!"".equals(monthTrackString)) {
				record=record+monthTrackString;
			}
			issueList.getList().get(i).set("record", record);
			//升级对象钉钉id
			User upgradeUser=User.dao.findById(issueList.getList().get(i).getInt("upgrade_user_id"));
			issueList.getList().get(i).set("upgrade_user_id", upgradeUser.getDingUserId());
			//8D计划
			Record issuePlan=new Record();
			issuePlan.setColumns(issueList.getList().get(i));
			Record issueReal=new Record();
			issueReal.setColumns(issueList.getList().get(i));
			HClothesIssue8dPlan plans=HClothesIssue8dPlan.dao.findFirst("select * from h_clothes_issue_8d_plan where issue_id="+issuePlan.getLong("id"));
			issuePlan.set("plan_real", "计划")
			.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
			.set("d_three", plans.getDThree()).set("d_four", plans.getDFour())
			.set("d_five", plans.getDFive()).set("d_six", plans.getDSix())
			.set("d_seven", plans.getDSeven())
			.set("d_one_status", 0).set("d_two_status",0)
			.set("d_three_status", 0).set("d_four_status", 0)
			.set("d_five_status", 0).set("d_six_status", 0)
			.set("d_seven_status", 0);
			//8D实际
			HClothesIssue8dReal reals=HClothesIssue8dReal.dao.findFirst("select * from h_clothes_issue_8d_real where issue_id="+issueReal.getLong("id"));
			issueReal.set("plan_real", "实际")
			.set("d_one", "点击编辑").set("d_two", "点击编辑")
			.set("d_three", "点击编辑").set("d_four", "点击编辑")
			.set("d_five", "点击编辑").set("d_six", "点击编辑")
			.set("d_seven", "点击编辑")
			.set("d_one_status", 0).set("d_two_status",0)
			.set("d_three_status", 0).set("d_four_status", 0)
			.set("d_five_status", 0).set("d_six_status", 0)
			.set("d_seven_status", 0);
			if (reals!=null) {
				issuePlan
				.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
				.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
				.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
				.set("d_seven_status", reals.getDSevenStatus());
				issueReal
				.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
				.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
				.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
				.set("d_seven_status", reals.getDSevenStatus());
				if (reals.getDOneStatus()==2) {
					issueReal.set("d_one", reals.getDOne());
				}
				if (reals.getDTwoStatus()==2) {
					issueReal.set("d_two", reals.getDTwo());
				}
				if (reals.getDThreeStatus()==2) {
					issueReal.set("d_three", reals.getDThree());
				}
				if (reals.getDFourStatus()==2) {
					issueReal.set("d_four", reals.getDFour());
				}
				if (reals.getDFiveStatus()==2) {
					issueReal.set("d_five", reals.getDFive());
				}
				if (reals.getDSixStatus()==2) {
					issueReal.set("d_six", reals.getDSix());
				}
				if (reals.getDSevenStatus()==2) {
					issueReal.set("d_seven", reals.getDSeven());
					Date dSeven = reals.getDSeven();
				}
			}
			MyCreateClothesController.setThreeMonth(issuePlan,reals);
			qtyOPLList.add(issuePlan);
			qtyOPLList.add(issueReal);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", qtyOPLList);
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**
	 * 激励方案
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月20日 下午3:07:42
	 */
	@SuppressWarnings("rawtypes")
	public void saveUrgePlan() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				//激励附件
				JSONArray fileList=JSONArray.parseArray(getPara("fileList"));
				long fileId=0;
				if(fileList.size()!=0) {
					JSONObject fileObject=fileList.getJSONObject(0);
					fileId=fileObject.getLongValue("id");
				}
				HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
				if (map.get("urge_plan")!=null) {
					qtyIssue.setUrgePlan(map.get("urge_plan").toString());
				}
				qtyIssue.setUrgeFujianId(fileId);
				qtyIssue.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "编辑激励方案成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}

	/**
	 * 升级问题
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月9日 下午3:41:02
	 */
	@SuppressWarnings("rawtypes")
	public void upgradeIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				String upgrade_user_id=map.get("upgrade_user_id").toString();
				User upUser=User.dao.findFirst("select * from user where ding_user_id='"+upgrade_user_id+"'");
				String severity=map.get("severity").toString();
				//OPL状态
				long issueId=Long.valueOf(map.get("issue_id").toString());
				HClothesIssue issue=HClothesIssue.dao.findById(issueId);
				issue.setIssueStatus(1);
				issue.update();
				//将以前升级记录置为失效
				Db.update("update h_clothesopl_upgrade_record set status=1 where issue_id="+issueId);
				//添加处理记录
				Record loginUser=getSessionAttr("user");
				User user=User.dao.findById(loginUser.getInt("id"));
				HClothesoplUpgradeRecord upgradeRecord=new HClothesoplUpgradeRecord();
				upgradeRecord.setIssueId(issueId)
				.setOldUserId(user.getId()).setOldUserName(user.getNickname())
				.setNewUserId(upUser.getId().intValue()).setNewUserName(upUser.getNickname())
				.setLevel(severity)
				.setSpStatus(0).setStatus(0).setCreateTime(new Date())
				.setUpgradeReason(map.get("upgrade_reason").toString())
				.setUpgradeType(0);//0-手动升级
				upgradeRecord.save();
				//钉钉消息通知升级对象
				DingMessage.sendText(upgrade_user_id, "有新的质量OPL已升级，等待您的处理！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "15");//15-质量OPL升级处理				
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "升级成功，等待升级对象确认！"));
			}else {
				renderJson(Ret.fail("msg", "升级失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取OPL遏制、临时措施
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午2:59:46
	 */
	public void getClothesIssueContainMeasures() {
		try {
			long issueId=getParaToLong("id");
			List<Record> containMeasures=Db.find("select * from h_clothes_measures where issue_id="+issueId+" and type=2 and is_del=0");
			for (int i = 0; i < containMeasures.size(); i++) {
				User measureUser=User.dao.findFirst("select * from user where id="+containMeasures.get(i).getInt("user_id"));
				containMeasures.get(i).set("user_id", measureUser.getDingUserId());
			}
			List<Record> temporaryMeasures=Db.find("select * from h_clothes_measures where issue_id="+issueId+" and type=3 and is_del=0");
			for (int i = 0; i < temporaryMeasures.size(); i++) {
				User measureUser=User.dao.findFirst("select * from user where id="+temporaryMeasures.get(i).getInt("user_id"));
				temporaryMeasures.get(i).set("user_id", measureUser.getDingUserId());
			}
			Record record=new Record();
			record.set("contain", containMeasures).set("temporary", temporaryMeasures);
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 新增编辑遏制、临时措施
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午3:20:31
	 */
	public void addEditContainMeasure() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("issueId");
				HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
				JSONArray containMeasureList=JSONArray.parseArray(get("containMeasureList"));
				JSONArray temporaryMeasureList=JSONArray.parseArray(get("temporaryMeasureList"));
				Db.update("update h_clothes_measures set is_del=1 where issue_id="+issueId+" and (type=3 or type=2)");
				/**2021-12-23
				 * 判断是否对接项目OPL临时措施---start
				 */
				boolean isProject=false;
				long proIssueId=0;
				HProjectIssue projectIssue=HProjectIssue.dao.findById(qtyIssue.getProIssueId());
				if (!"SOP".equals(qtyIssue.getProductStage())) {
					if (projectIssue!=null) {
						proIssueId=projectIssue.getId();
						isProject=true;
					}
				}
				long xuniReasonId=0;
				if (isProject) {
					HProIssueReasonAnalysis xuniReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where issue_id="+proIssueId+" and is_xuni=1 ");
					if (xuniReason==null) {
						xuniReason=new HProIssueReasonAnalysis();
						xuniReason.setIssueId(proIssueId).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1);
						xuniReason.save();
						xuniReason.setLsIndex(xuniReason.getId());
						xuniReason.update();
					}else {
						xuniReason.setStatus(0);
						xuniReason.update();
					}
					xuniReasonId=xuniReason.getId();
					//将所有临时措施失效
					Db.update("update h_project_measures set is_del=1 where reason_id="+xuniReasonId);
				}
				/**2021-12-23
				 * 判断是否对接项目OPL临时措施---end
				 */
				//遏制措施
				for (int i = 0; i < containMeasureList.size(); i++) {
					JSONObject containObject=containMeasureList.getJSONObject(i);
					String measureUserId=containObject.getString("user_id");
					User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+measureUserId+"'");
					long measureId=containObject.getLongValue("id");
					if (measureId==0) {
						HClothesMeasures measure=new HClothesMeasures();
						measure.setIssueId(issueId)
						.setMeasures(containObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(containObject.getDate("plan_finish_date"))
						.setStatus(1).setIsDel(0)//措施创建之后默认状态为1---进行中
						.setType(2);//2-遏制措施
						measure.save();
						measureId=measure.getId();
					}else {
						HClothesMeasures measure=HClothesMeasures.dao.findById(measureId);
						measure
						.setMeasures(containObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(containObject.getDate("plan_finish_date"))
						.setIsDel(0);
						measure.update();
					}
					/**2021-12-23
					 * 对接项目OPL临时措施---start
					 */
					if (isProject) {
						HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+measureId);
						if (proMeasure==null) {
							proMeasure=new HProjectMeasures();
							proMeasure.setIssueId(proIssueId).setReasonId(xuniReasonId)
							.setXuhao("1."+(i+1)).setMeasures(containObject.getString("measures"))
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(containObject.getDate("plan_finish_date"))
							.setStatus(1).setIsDel(0).setMeasureType(0)
							.setHandleMeasureId(measureId);
							proMeasure.save();
						}else {
							proMeasure.setXuhao("1."+(i+1)).setMeasures(containObject.getString("measures"))
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(containObject.getDate("plan_finish_date"))
							.setIsDel(0);
							proMeasure.update();
						}
					}
					/**2021-12-23
					 * 对接项目OPL临时措施---end
					 */
					DingMessage.sendText(measureUserId, "有新的质量OPL措施需要你执行，请知悉！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "12");//12-质量OPL措施执行
				}
				//临时措施
				for (int i = 0; i < temporaryMeasureList.size(); i++) {
					JSONObject containObject=temporaryMeasureList.getJSONObject(i);
					String measureUserId=containObject.getString("user_id");
					User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+measureUserId+"'");
					long measureId=containObject.getLongValue("id");
					if (measureId==0) {
						HClothesMeasures measure=new HClothesMeasures();
						measure.setIssueId(issueId)
						.setMeasures(containObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(containObject.getDate("plan_finish_date"))
						.setStatus(1).setIsDel(0)//措施创建之后默认状态为1---进行中
						.setType(3);//2-临时措施
						measure.save();
						measureId=measure.getId();
					}else {
						HClothesMeasures measure=HClothesMeasures.dao.findById(measureId);
						measure
						.setMeasures(containObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(containObject.getDate("plan_finish_date"))
						.setIsDel(0);
						measure.update();
					}
					/**2021-12-23
					 * 对接项目OPL临时措施
					 */
					if (isProject) {
						HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+measureId);
						if (proMeasure==null) {
							proMeasure=new HProjectMeasures();
							proMeasure.setIssueId(proIssueId).setReasonId(xuniReasonId)
							.setXuhao("1."+(containMeasureList.size()+1)).setMeasures(containObject.getString("measures"))
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(containObject.getDate("plan_finish_date"))
							.setStatus(1).setIsDel(0).setMeasureType(0)
							.setHandleMeasureId(measureId);
							proMeasure.save();
						}else {
							proMeasure.setXuhao("1."+(containMeasureList.size()+1)).setMeasures(containObject.getString("measures"))
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(containObject.getDate("plan_finish_date"))
							.setIsDel(0);
							proMeasure.update();
						}
					}
					DingMessage.sendText(measureUserId, "有新的质量OPL措施需要你执行，请知悉！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "12");//12-质量OPL措施执行
				}
				//2022-6-16 修改关键opl状态逻辑，状态与质量opl一致
//				/**
//				 * 项目OPL状态修改---start
//				 */
//				if (isProject) {
//					List<HProjectMeasures> projectMeasures=HProjectMeasures.dao.find("select * from h_project_measures where issue_id="+proIssueId+" and is_del=0");
//					if (!"G".equals(projectIssue.getStatus())) {
//						if (!projectMeasures.isEmpty()) {
//							projectIssue.setStatus("Y");
//						}else {
//							projectIssue.setStatus("R");
//						}
//						projectIssue.update();
//					}
//				}
//				/**
//				 * 项目OPL状态修改---end
//				 */
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "新增或编辑 遏制、临时措施成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取OPL根本原因分析
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午4:20:40
	 */
	public void getQtyIssueRootReason() {
		try {
			long issueId=getParaToLong("id");
			List<HClothesIssueReasonAnalysis> happenAnalysis=HClothesIssueReasonAnalysis.dao.find("select * from h_clothes_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
			List<HClothesIssueReasonAnalysis> runOutAnalysis=HClothesIssueReasonAnalysis.dao.find("select * from h_clothes_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
			Record record=new Record();
			record.set("happen", happenAnalysis).set("runout", runOutAnalysis);
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 新增编辑 产生、流出原因
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午4:22:37
	 */
	public void addEditRootReason() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("issueId");
				HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
				JSONArray happenReasonList=JSONArray.parseArray(get("happenReasonList"));
				JSONArray runoutReasonList=JSONArray.parseArray(get("runoutReasonList"));
				Db.update("update h_clothes_issue_reason_analysis set status=1 where issue_id="+issueId);
				/**2021-12-23
				 * 判断是否对接项目OPL原因分析---start
				 */
				boolean isProject=false;
				long proIssueId=0;
				if (!"SOP".equals(qtyIssue.getProductStage())) {
					HProjectIssue projectIssue=HProjectIssue.dao.findById(qtyIssue.getProIssueId());
					if (projectIssue!=null) {
						proIssueId=projectIssue.getId();
						isProject=true;
						Db.update("update h_pro_issue_reason_analysis set status=1 where issue_id="+proIssueId+" and is_xuni=0");//失效所有问题原因
					}
				}
				/**2021-12-23
				 * 判断是否对接项目OPL原因分析---end
				 */
				for (int i = 0; i < happenReasonList.size(); i++) {//产生
					JSONObject happenObject=happenReasonList.getJSONObject(i);
					long reasonId=happenObject.getLongValue("id");
					if (reasonId==0) {
						HClothesIssueReasonAnalysis reason=new HClothesIssueReasonAnalysis();
						reason.setIssueId(issueId)
						.setReasonAnalysis(happenObject.getString("reason_analysis"))
						.setStatus(0)
						.setType(0);//0-产生
						reason.save();
						reasonId=reason.getId();
					}else {
						HClothesIssueReasonAnalysis reason=HClothesIssueReasonAnalysis.dao.findById(reasonId);
						reason
						.setReasonAnalysis(happenObject.getString("reason_analysis"))
						.setStatus(0);
						reason.update();
					}
					/**2021-12-23
					 * 对接项目OPL原因分析---start
					 */
					if (isProject) {
						HProIssueReasonAnalysis proIssueReason= HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where handle_reason_id="+reasonId);
						if (proIssueReason==null) {
							proIssueReason=new HProIssueReasonAnalysis();
							proIssueReason.setIssueId(proIssueId)
							.setReasonAnalysis("产生原因："+happenObject.getString("reason_analysis"))
							.setStatus(0).setIsXuni(0).setHandleReasonId(reasonId);
							proIssueReason.save();
							proIssueReason.setLsIndex(proIssueReason.getId());
							proIssueReason.update();
						}else {
							proIssueReason
							.setReasonAnalysis("产生原因："+happenObject.getString("reason_analysis"))
							.setStatus(0);
							proIssueReason.update();
						}
					}
					/**2021-12-23
					 * 对接项目OPL原因分析---end
					 */
				}
				for (int i = 0; i < runoutReasonList.size(); i++) {//流出
					JSONObject happenObject=runoutReasonList.getJSONObject(i);
					long reasonId=happenObject.getLongValue("id");
					if (reasonId==0) {
						HClothesIssueReasonAnalysis reason=new HClothesIssueReasonAnalysis();
						reason.setIssueId(issueId)
						.setReasonAnalysis(happenObject.getString("reason_analysis"))
						.setStatus(0)
						.setType(1);//1-流出
						reason.save();
						reasonId=reason.getId();
					}else {
						HClothesIssueReasonAnalysis reason=HClothesIssueReasonAnalysis.dao.findById(reasonId);
						reason
						.setReasonAnalysis(happenObject.getString("reason_analysis"))
						.setStatus(0);
						reason.update();
					}
					/**2021-12-23
					 * 对接项目OPL原因分析---start
					 */
					if (isProject) {
						HProIssueReasonAnalysis proIssueReason= HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where handle_reason_id="+reasonId);
						if (proIssueReason==null) {
							proIssueReason=new HProIssueReasonAnalysis();
							proIssueReason.setIssueId(proIssueId)
							.setReasonAnalysis("根本原因："+happenObject.getString("reason_analysis"))
							.setStatus(0).setIsXuni(0).setHandleReasonId(reasonId);
							proIssueReason.save();
							proIssueReason.setLsIndex(proIssueReason.getId());
							proIssueReason.update();
						}else {
							proIssueReason
							.setReasonAnalysis("根本原因："+happenObject.getString("reason_analysis"))
							.setStatus(0);
							proIssueReason.update();
						}
					}
					/**2021-12-23
					 * 对接项目OPL原因分析---end
					 */
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "新增或编辑 根本原因分析成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取OPL永久措施列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午4:20:40
	 */
	public void getQtyIssueAvoidMeasure() {
		try {
			long issueId=getParaToLong("id");
			List<Record> avoidHappen=Db.find("select a.*,b.reason_analysis from h_clothes_measures a,h_clothes_issue_reason_analysis b where a.issue_id="+issueId+" and a.is_del=0 and a.type=0 and b.id=a.reason_id");//防产生措施
			for (int i = 0; i < avoidHappen.size(); i++) {
				User measureUser=User.dao.findFirst("select * from user where id="+avoidHappen.get(i).getInt("user_id"));
				avoidHappen.get(i).set("user_id", measureUser.getDingUserId());
			}
			List<Record> avoidRunout=Db.find("select a.*,b.reason_analysis from h_clothes_measures a,h_clothes_issue_reason_analysis b where a.issue_id="+issueId+" and a.is_del=0 and a.type=1 and b.id=a.reason_id");//防流出措施
			for (int i = 0; i < avoidRunout.size(); i++) {
				User measureUser=User.dao.findFirst("select * from user where id="+avoidRunout.get(i).getInt("user_id"));
				avoidRunout.get(i).set("user_id", measureUser.getDingUserId());
			}
			Record record=new Record();
			record.set("happen", avoidHappen).set("runout", avoidRunout);
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取原因详情
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午5:18:47
	 */
	public void getReasonInfo() {
		try {
			long reasonId=getParaToLong("reasonId");
			HClothesIssueReasonAnalysis happenAnalysis=HClothesIssueReasonAnalysis.dao.findById(reasonId);
			renderJson(Ret.ok("data", happenAnalysis));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 新增编辑永久措施
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午5:33:31
	 */
	public void addEditAvoidMeasure() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("issueId");
				HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
				JSONArray happenMeasureList=JSONArray.parseArray(get("happenMeasureList"));
				JSONArray runoutMeasureList=JSONArray.parseArray(get("runoutMeasureList"));
				/**2021-12-23
				 * 判断是否对接项目OPL永久措施---start
				 */
				boolean isProject=false;
				long proIssueId=0;
				HProjectIssue projectIssue=HProjectIssue.dao.findById(qtyIssue.getProIssueId());
				if (!"SOP".equals(qtyIssue.getProductStage())) {
					if (projectIssue!=null) {
						proIssueId=projectIssue.getId();
						isProject=true;
						Db.update("update h_project_measures set is_del=1 where issue_id="+proIssueId+" and measure_type=1");//失效所有永久措施
					}
				}
				/**2021-12-23
				 * 判断是否对接项目OPL永久措施---end
				 */
				Db.update("update h_clothes_measures set is_del=1 where issue_id="+issueId+" and (type=0 or type=1)");
				for (int i = 0; i < happenMeasureList.size(); i++) {//防产生
					JSONObject happenObject=happenMeasureList.getJSONObject(i);
					String measureUserId=happenObject.getString("user_id");
					User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+measureUserId+"'");
					long measureId=happenObject.getLongValue("id");
					long reasonId=happenObject.getLongValue("reason_id");
					if (measureId==0) {
						HClothesMeasures measure=new HClothesMeasures();
						measure.setIssueId(issueId)
						.setReasonId(reasonId)
						.setMeasures(happenObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(happenObject.getDate("plan_finish_date"))
						.setStatus(1).setIsDel(0)//措施创建之后默认状态为1---进行中
						.setType(0);//0-防产生
						measure.save();
						measureId=measure.getId();
					}else {
						HClothesMeasures measure=HClothesMeasures.dao.findById(measureId);
						measure
						.setReasonId(reasonId)
						.setMeasures(happenObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(happenObject.getDate("plan_finish_date"))
						.setIsDel(0);
						measure.update();
					}
					/**2021-12-23
					 * 判断是否对接项目OPL永久措施---start
					 */
					if (isProject) {
						HProIssueReasonAnalysis proIssueReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where handle_reason_id="+reasonId);
						HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+measureId);
						if (proMeasure==null) {
							proMeasure=new HProjectMeasures();
							proMeasure.setIssueId(proIssueId).setReasonId(proIssueReason.getId())
							.setXuhao(String.valueOf(i+1)).setMeasures(happenObject.getString("measures"))
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(happenObject.getDate("plan_finish_date"))
							.setStatus(1).setIsDel(0).setMeasureType(1)
							.setHandleMeasureId(measureId);
							proMeasure.save();
						}else {
							proMeasure.setReasonId(proIssueReason.getId())
							.setXuhao(String.valueOf(i+1)).setMeasures(happenObject.getString("measures"))
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(happenObject.getDate("plan_finish_date"))
							.setIsDel(0);
							proMeasure.update();
						}
					}
					/**2021-12-23
					 * 判断是否对接项目OPL永久措施---end
					 */
					DingMessage.sendText(measureUserId, "有新的质量OPL措施需要你执行，请知悉！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "12");//12-质量OPL措施执行
				}
				for (int i = 0; i < runoutMeasureList.size(); i++) {//临时措施
					JSONObject runoutObject=runoutMeasureList.getJSONObject(i);
					String measureUserId=runoutObject.getString("user_id");
					User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+measureUserId+"'");
					long measureId=runoutObject.getLongValue("id");
					long reasonId=runoutObject.getLongValue("reason_id");
					if (measureId==0) {
						HClothesMeasures measure=new HClothesMeasures();
						measure.setIssueId(issueId)
						.setReasonId(reasonId)
						.setMeasures(runoutObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(runoutObject.getDate("plan_finish_date"))
						.setStatus(1).setIsDel(0)//措施创建之后默认状态为1---进行中
						.setType(1);//1-防产生
						measure.save();
						measureId=measure.getId();
					}else {
						HClothesMeasures measure=HClothesMeasures.dao.findById(measureId);
						measure
						.setReasonId(reasonId)
						.setMeasures(runoutObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(runoutObject.getDate("plan_finish_date"))
						.setIsDel(0);
						measure.update();
					}
					/**2021-12-23
					 * 判断是否对接项目OPL永久措施---start
					 */
					if (isProject) {
						HProIssueReasonAnalysis proIssueReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where handle_reason_id="+reasonId);
						HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+measureId);
						if (proMeasure==null) {
							proMeasure=new HProjectMeasures();
							proMeasure.setIssueId(proIssueId).setReasonId(proIssueReason.getId())
							.setXuhao(String.valueOf(i+1)).setMeasures(runoutObject.getString("measures"))
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(runoutObject.getDate("plan_finish_date"))
							.setStatus(1).setIsDel(0).setMeasureType(1)
							.setHandleMeasureId(measureId);
							proMeasure.save();
						}else {
							proMeasure.setReasonId(proIssueReason.getId())
							.setXuhao(String.valueOf(i+1)).setMeasures(runoutObject.getString("measures"))
							.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
							.setPlanFinishDate(runoutObject.getDate("plan_finish_date"))
							.setIsDel(0);
							proMeasure.update();
						}
					}
					/**2021-12-23
					 * 判断是否对接项目OPL永久措施---end
					 */
					DingMessage.sendText(measureUserId, "有新的质量OPL措施需要你执行，请知悉！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "12");//12-质量OPL措施执行
				}
				//2022-6-16 修改关键opl状态逻辑，状态与质量opl一致
//				/**
//				 * 项目OPL状态修改---start
//				 */
//				if (isProject) {					
//					List<HProjectMeasures> projectMeasures=HProjectMeasures.dao.find("select * from h_project_measures where issue_id="+proIssueId+" and is_del=0");
//					if (!"G".equals(projectIssue.getStatus())) {
//						if (!projectMeasures.isEmpty()) {
//							projectIssue.setStatus("Y");
//						}else {
//							projectIssue.setStatus("R");
//						}
//						projectIssue.update();
//					}
//				}
//				/**
//				 * 项目OPL状态修改---end
//				 */
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "新增或编辑 永久措施成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	//2022-6-16 手工opl只有3D
//	/**
//	 * 6D7D编辑
//	 * @author simple
//	 * @contact 15228717200
//	 * @time 2021年5月19日 下午7:41:04
//	 */
//	@SuppressWarnings("rawtypes")
//	public void add8DData() {
//		try {
//			boolean flag = Db.tx(() -> {
//				Map map=FastJson.getJson().parse(get("formData"), Map.class);
//				long issueId=Long.valueOf(map.get("issueId").toString());
//				HClothesIssue8dPlan dPlan=HClothesIssue8dPlan.dao.findFirst("select * from h_qty_issue_8d_plan where issue_id="+issueId);
//				int dNum=Integer.valueOf(map.get("dNum").toString());
//				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//				try {
//					Date dDate=sdf.parse(map.get("ddate").toString());
//					if (dNum==6) {
//						dPlan.setDSix(dDate);
//					}else if (dNum==7) {
//						dPlan.setDSeven(dDate);
//					}
//					dPlan.update();
//					//判断是否有项目OPL，有则修改计划完成时间
//					if (dNum==7) {
//						HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
//						if (!"SOP".equals(qtyIssue.getProductStage())&&qtyIssue.getProIssueId()!=null) {
//							HProjectIssue projectIssue=HProjectIssue.dao.findById(qtyIssue.getProIssueId());
//							if (projectIssue!=null) {
//								projectIssue.setPlanFinishTime(dDate);
//								projectIssue.update();
//							}
//						}
//					}
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//				return true;
//			});
//			if (flag) {
//				renderJson(Ret.ok("msg", "编辑成功！"));
//			}else {
//				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
//		}
//	}
	/**
	 * 获取OPL所有措施列表进行跟踪
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月20日 上午9:35:29
	 */
	public void getQtyIssueAllMeasures() {
		try {
			long issueId=getParaToLong("id");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			List<Record> containMeasures=Db.find("select * from h_clothes_measures where issue_id="+issueId+" and type=2 and is_del=0");
			for (int i = 0; i < containMeasures.size(); i++) {
				List<HClothesTrackHistory> trackHistory=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+containMeasures.get(i).getLong("id"));
				String measureTrack="";
				for (int j = 0; j < trackHistory.size(); j++) {
					measureTrack=measureTrack+trackHistory.get(j).getRemark()+"-"+trackHistory.get(j).getTrackUserName()+"-"+sdf.format(trackHistory.get(j).getCreateTime())+"<br>";
				}
				List<String> newTrack=new ArrayList<String>();
				containMeasures.get(i).set("measure_track", measureTrack).set("new_track", newTrack);
			}
			List<Record> temporaryMeasures=Db.find("select * from h_clothes_measures where issue_id="+issueId+" and type=3 and is_del=0");
			for (int i = 0; i < temporaryMeasures.size(); i++) {
				List<HClothesTrackHistory> trackHistory=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+temporaryMeasures.get(i).getLong("id"));
				String measureTrack="";
				for (int j = 0; j < trackHistory.size(); j++) {
					measureTrack=measureTrack+trackHistory.get(j).getRemark()+"-"+trackHistory.get(j).getTrackUserName()+"-"+sdf.format(trackHistory.get(j).getCreateTime())+"<br>";
				}
				List<String> newTrack=new ArrayList<String>();
				temporaryMeasures.get(i).set("measure_track", measureTrack).set("new_track", newTrack);
			}
			List<Record> avoidHappen=Db.find("select a.*,b.reason_analysis from h_clothes_measures a,h_clothes_issue_reason_analysis b where a.issue_id="+issueId+" and a.is_del=0 and a.type=0 and b.id=a.reason_id");//防产生措施
			for (int i = 0; i < avoidHappen.size(); i++) {
				List<HClothesTrackHistory> trackHistory=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+avoidHappen.get(i).getLong("id"));
				String measureTrack="";
				for (int j = 0; j < trackHistory.size(); j++) {
					measureTrack=measureTrack+trackHistory.get(j).getRemark()+"-"+trackHistory.get(j).getTrackUserName()+"-"+sdf.format(trackHistory.get(j).getCreateTime())+"<br>";
				}
				List<String> newTrack=new ArrayList<String>();
				avoidHappen.get(i).set("measure_track", measureTrack).set("new_track", newTrack);
			}
			List<Record> avoidRunout=Db.find("select a.*,b.reason_analysis from h_clothes_measures a,h_clothes_issue_reason_analysis b where a.issue_id="+issueId+" and a.is_del=0 and a.type=1 and b.id=a.reason_id");//防流出措施
			for (int i = 0; i < avoidRunout.size(); i++) {
				List<HClothesTrackHistory> trackHistory=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+avoidRunout.get(i).getLong("id"));
				String measureTrack="";
				for (int j = 0; j < trackHistory.size(); j++) {
					measureTrack=measureTrack+trackHistory.get(j).getRemark()+"-"+trackHistory.get(j).getTrackUserName()+"-"+sdf.format(trackHistory.get(j).getCreateTime())+"<br>";
				}
				List<String> newTrack=new ArrayList<String>();
				avoidRunout.get(i).set("measure_track", measureTrack).set("new_track", newTrack);
			}
			Record record=new Record();
			record.set("contain", containMeasures).set("temporary", temporaryMeasures);
			record.set("happen", avoidHappen).set("runout", avoidRunout);
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * OPL跟踪
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月25日 下午3:08:44
	 */
	@SuppressWarnings("rawtypes")
	public void saveTrackIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				long issueId=Long.valueOf(map.get("issueId").toString());
				Record loginUser=getSessionAttr("user");
				//OPL整体跟踪情况
				if (map.get("reamrk")!=null&&!"".equals(map.get("reamrk").toString())&&!"null".equals(map.get("reamrk").toString())) {
					HClothesIssueTrackRecord qtyIssueTrackRecord=new HClothesIssueTrackRecord();
					qtyIssueTrackRecord.setIssueId(issueId)
					.setTrackRecord(map.get("reamrk").toString())
					.setCreateTime(new Date())
					.setCreateUserId(loginUser.getInt("id")).setCreateUserName(loginUser.getStr("nickname"));
					qtyIssueTrackRecord.save();
				}
				/**
				 * 是否对接项目OPL
				 */
				boolean isProOpl=false;
				HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
				long proIssueId=0;
				if (!"SOP".equals(qtyIssue.getProductStage())) {
					HProjectIssue projectIssue=HProjectIssue.dao.findById(qtyIssue.getProIssueId());
					if (projectIssue!=null) {
						isProOpl=true;
						proIssueId=projectIssue.getId();
					}
				}
				//遏制措施跟踪
				JSONArray trackContainMeasuresList=JSONArray.parseArray(getPara("trackContainMeasuresList"));
				for (int i = 0; i < trackContainMeasuresList.size(); i++) {
					JSONObject measureObject=trackContainMeasuresList.getJSONObject(i);
					long measureId=measureObject.getLongValue("id");
					HClothesMeasures measure=HClothesMeasures.dao.findById(measureId);
					HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+measureId);
					int status=measureObject.getIntValue("status");
					if (status==3) {//关闭
						measure.setCloseDate(new Date());
					}
					if (status==1) {//回退
						measure.setRealFinishDate(null);
						//旧状态为2-已完成，新状态为1，则为回退
						int oldMeasureStatus=measure.getStatus();
						if (oldMeasureStatus==2) {
							User measureUser=User.dao.findById(measure.getUserId().intValue());
							DingMessage.sendText(measureUser.getDingUserId(), "你完成的措施：\n"+measure.getMeasures()+"\n已被退回，请确认！\n"+sdf.format(new Date()),"12");//12-措施执行人
						}
						/**
						 * 回退需要修改项目OPL措施状态---start
						 */
						if (isProOpl&&proMeasure!=null) {
							proMeasure.setStatus(1).setRealFinishDate(null);
							proMeasure.update();
						}
						/**
						 * 回退需要修改项目OPL措施状态---start
						 */
					}
					measure.setStatus(status);
					measure.update();					
					//措施跟踪
					JSONArray newTrack=measureObject.getJSONArray("new_track");
					for (int j = 0; j < newTrack.size(); j++) {
						HClothesTrackHistory trackHistory=new HClothesTrackHistory();
						trackHistory.setIssueId(issueId).setMeasureId(measureId)
						.setMeasureStatus(status).setRemark(newTrack.getString(j))
						.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
						.setCreateTime(new Date());
						trackHistory.save();
						/**
						 * 项目OPL措施跟踪---start
						 */
						if (isProOpl&&proMeasure!=null) {
							HProissueTrackHistory proTrackHistory=new HProissueTrackHistory();
							proTrackHistory.setIssueId(proIssueId).setMeasureId(proMeasure.getId())
							.setMeasureStatus(proMeasure.getStatus()).setRemark(newTrack.getString(j))
							.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
							.setCreateTime(new Date())
							.setHandleTrackHistoryId(trackHistory.getId());
							proTrackHistory.save();
						}
						/**
						 * 项目OPL措施跟踪---end
						 */
					}
				}
				//临时措施跟踪
				JSONArray trackTemporaryMeasuresList=JSONArray.parseArray(getPara("trackTemporaryMeasuresList"));
				for (int i = 0; i < trackTemporaryMeasuresList.size(); i++) {
					JSONObject measureObject=trackTemporaryMeasuresList.getJSONObject(i);
					long measureId=measureObject.getLongValue("id");
					HClothesMeasures measure=HClothesMeasures.dao.findById(measureId);
					HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+measureId);
					int status=measureObject.getIntValue("status");
					if (status==3) {//关闭
						measure.setCloseDate(new Date());
					}
					if (status==1) {//回退
						measure.setRealFinishDate(null);
						/**
						 * 回退需要修改项目OPL措施状态---start
						 */
						if (isProOpl&&proMeasure!=null) {
							proMeasure.setStatus(1).setRealFinishDate(null);
							proMeasure.update();
						}
						/**
						 * 回退需要修改项目OPL措施状态---start
						 */
					}
					measure.setStatus(status);
					measure.update();
					//措施跟踪
					JSONArray newTrack=measureObject.getJSONArray("new_track");
					for (int j = 0; j < newTrack.size(); j++) {
						HClothesTrackHistory trackHistory=new HClothesTrackHistory();
						trackHistory.setIssueId(issueId).setMeasureId(measureId)
						.setMeasureStatus(status).setRemark(newTrack.getString(j))
						.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
						.setCreateTime(new Date());
						trackHistory.save();
						/**
						 * 项目OPL措施跟踪---start
						 */
						if (isProOpl&&proMeasure!=null) {
							HProissueTrackHistory proTrackHistory=new HProissueTrackHistory();
							proTrackHistory.setIssueId(proIssueId).setMeasureId(proMeasure.getId())
							.setMeasureStatus(proMeasure.getStatus()).setRemark(newTrack.getString(j))
							.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
							.setCreateTime(new Date())
							.setHandleTrackHistoryId(trackHistory.getId());
							proTrackHistory.save();
						}
						/**
						 * 项目OPL措施跟踪---end
						 */
					}
				}
				//防产生跟踪
				JSONArray trackHappenMeasuresList=JSONArray.parseArray(getPara("trackHappenMeasuresList"));
				for (int i = 0; i < trackHappenMeasuresList.size(); i++) {
					JSONObject measureObject=trackHappenMeasuresList.getJSONObject(i);
					long measureId=measureObject.getLongValue("id");
					HClothesMeasures measure=HClothesMeasures.dao.findById(measureId);
					HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+measureId);
					int status=measureObject.getIntValue("status");
					if (status==3) {//关闭
						measure.setCloseDate(new Date());
					}
					if (status==1) {//回退
						measure.setRealFinishDate(null);
						/**
						 * 回退需要修改项目OPL措施状态---start
						 */
						if (isProOpl&&proMeasure!=null) {
							proMeasure.setStatus(1).setRealFinishDate(null);
							proMeasure.update();
						}
						/**
						 * 回退需要修改项目OPL措施状态---start
						 */
					}
					measure.setStatus(status);
					measure.update();
					//措施跟踪
					JSONArray newTrack=measureObject.getJSONArray("new_track");
					for (int j = 0; j < newTrack.size(); j++) {
						HClothesTrackHistory trackHistory=new HClothesTrackHistory();
						trackHistory.setIssueId(issueId).setMeasureId(measureId)
						.setMeasureStatus(status).setRemark(newTrack.getString(j))
						.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
						.setCreateTime(new Date());
						trackHistory.save();
						/**
						 * 项目OPL措施跟踪---start
						 */
						if (isProOpl&&proMeasure!=null) {
							HProissueTrackHistory proTrackHistory=new HProissueTrackHistory();
							proTrackHistory.setIssueId(proIssueId).setMeasureId(proMeasure.getId())
							.setMeasureStatus(proMeasure.getStatus()).setRemark(newTrack.getString(j))
							.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
							.setCreateTime(new Date())
							.setHandleTrackHistoryId(trackHistory.getId());
							proTrackHistory.save();
						}
						/**
						 * 项目OPL措施跟踪---end
						 */
					}
				}
				//防流出
				JSONArray trackRunoutMeasuresList=JSONArray.parseArray(getPara("trackRunoutMeasuresList"));
				for (int i = 0; i < trackRunoutMeasuresList.size(); i++) {
					JSONObject measureObject=trackRunoutMeasuresList.getJSONObject(i);
					long measureId=measureObject.getLongValue("id");
					HClothesMeasures measure=HClothesMeasures.dao.findById(measureId);
					HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+measureId);
					int status=measureObject.getIntValue("status");
					if (status==3) {//关闭
						measure.setCloseDate(new Date());
					}
					if (status==1) {//回退
						measure.setRealFinishDate(null);
						/**
						 * 回退需要修改项目OPL措施状态---start
						 */
						if (isProOpl&&proMeasure!=null) {
							proMeasure.setStatus(1).setRealFinishDate(null);
							proMeasure.update();
						}
						/**
						 * 回退需要修改项目OPL措施状态---start
						 */
					}
					measure.setStatus(status);
					measure.update();
					//措施跟踪
					JSONArray newTrack=measureObject.getJSONArray("new_track");
					for (int j = 0; j < newTrack.size(); j++) {
						HClothesTrackHistory trackHistory=new HClothesTrackHistory();
						trackHistory.setIssueId(issueId).setMeasureId(measureId)
						.setMeasureStatus(status).setRemark(newTrack.getString(j))
						.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
						.setCreateTime(new Date());
						trackHistory.save();
						/**
						 * 项目OPL措施跟踪---start
						 */
						if (isProOpl&&proMeasure!=null) {
							HProissueTrackHistory proTrackHistory=new HProissueTrackHistory();
							proTrackHistory.setIssueId(proIssueId).setMeasureId(proMeasure.getId())
							.setMeasureStatus(proMeasure.getStatus()).setRemark(newTrack.getString(j))
							.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
							.setCreateTime(new Date())
							.setHandleTrackHistoryId(trackHistory.getId());
							proTrackHistory.save();
						}
						/**
						 * 项目OPL措施跟踪---end
						 */
					}
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "操作成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	//2022-6-16 手工opl不需要3个月跟踪
//	/**
//	 * 永久措施3个月跟踪
//	 * @author simple
//	 * @contact 15228717200
//	 * @time 2021年4月19日 下午2:54:04
//	 */
//	@SuppressWarnings("rawtypes")
//	public void saveMonthTrack() {
//		try {
//			Record record=new Record();
//			boolean flag = Db.tx(() -> {
//				Map map=FastJson.getJson().parse(get("formData"), Map.class);
//				long issueId=Long.valueOf(map.get("issueId").toString());
//				Record loginUser=getSessionAttr("user");
//				User user=User.dao.findById(loginUser.getInt("id"));
//				String month_num=map.get("month_num").toString();
//				String monthStatus=map.get("status").toString();
//				String remark=map.get("remark").toString();
//				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//				Date trackDate=null;
//				String track_date=map.get("track_date").toString();
//				if (!"".equals(track_date)) {
//					try {
//						trackDate = sdf.parse(map.get("track_date").toString());
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
//				}
//				//判断7D是否完成
//				HClothesIssue8dReal dReals=HClothesIssue8dReal.dao.findFirst("select * from h_clothes_issue_8d_real where issue_id="+issueId);
//				if (dReals==null||dReals.getDOneStatus()!=2||dReals.getDTwoStatus()!=2||dReals.getDThreeStatus()!=2) {
//					record.set("code", 1).set("msg", "请确保3D已全部完成！");
//					return false;
//				}else {
//					List<HClothesIssueMonthTrack> monthTracks=HClothesIssueMonthTrack.dao.find("select * from h_clothes_issue_month_track where issue_id="+issueId);
//					if (monthTracks.isEmpty()) {//还没有跟踪情况，判断是不是第一月跟踪
//						if ("month_1".equals(month_num)) {
//							HClothesIssueMonthTrack monthTrack=new HClothesIssueMonthTrack();
//							monthTrack.setIssueId(issueId)
//							.setMonth(1).setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
//							.setCreateTime(new Date()).setUpdateTime(new Date());
//							monthTrack.save();
//							HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
//							if ("R".equals(monthStatus)&&!"A".equals(qtyIssue.getLevel())) {//只要为R自动升级
//								qtyIssue.setIssueStatus(1);//待审批
//								qtyIssue.update();
//								//将以前的升级记录置为失效
//								Db.update(" update h_clothesopl_upgrade_record set status=1 and issue_id="+issueId);
//								List<HClothesoplUpgradeRecord> upgradeRecords=HClothesoplUpgradeRecord.dao.find("select * from h_clothesopl_upgrade_record where sp_status=1 and issue_id="+issueId);//审批通过的升级记录为空
//								int oldUserId=0;
//								String oldUserName="";
//								if (upgradeRecords.isEmpty()) {//审批通过的升级记录为空，提出升级者便为责任人
//									oldUserId=qtyIssue.getDutyUserId();
//									oldUserName=qtyIssue.getDutyUserName();
//								}else {
//									oldUserId=upgradeRecords.get(upgradeRecords.size()-1).getNewUserId();
//									oldUserName=upgradeRecords.get(upgradeRecords.size()-1).getNewUserName();
//								}
//								//添加新的记录
//								HClothesoplUpgradeRecord upgradeRecord=new HClothesoplUpgradeRecord();
//								upgradeRecord.setIssueId(issueId)
//								.setOldUserId(oldUserId).setOldUserName(oldUserName)
//								.setNewUserId(qtyIssue.getUpgradeUserId()).setNewUserName(qtyIssue.getUpgradeUserName())
//								.setSpStatus(0).setStatus(0).setCreateTime(new Date())
//								.setUpgradeType(2);//2-3个月跟踪状态为R自动升级
//								upgradeRecord.save();
//								//钉钉通知升级对象
//								User upUser=User.dao.findById(qtyIssue.getUpgradeUserId());
//								DingMessage.sendText(upUser.getDingUserId(), "有新的质量OPL已升级，等待您的处理！\n"+sdf.format(new Date()), "15");//15-质量OPL升级处理				
//							}
//							return true;
//						}else {
//							record.set("code", 1).set("msg", "第1月还没有完成跟踪记录，无法进行其他操作！");
//							return false;
//						}
//					}else {//有记录，看前一月是否为空
//						if ("month_1".equals(month_num)) {
//							HClothesIssueMonthTrack track=HClothesIssueMonthTrack.dao.findFirst("select * from h_clothes_issue_month_track where issue_id="+issueId+" and month=1");
//							track.setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
//							.setUpdateTime(new Date());
//							track.update();
//							//问题状态判断
//							List<HClothesIssueMonthTrack> allMonthTracks=HClothesIssueMonthTrack.dao.find("select * from h_clothes_issue_month_track where issue_id="+issueId);
//							if (allMonthTracks.size()==3) {
//								String issueStatus="R";
//								int Rnum=0;
//								int Ynum=0;
//								int Gnum=0;
//								for (int i = 0; i < allMonthTracks.size(); i++) {
//									String trackStatus=allMonthTracks.get(i).getStatus();
//									if ("R".equals(trackStatus)) {
//										Rnum=Rnum+1;
//									}else if ("Y".equals(trackStatus)) {
//										Ynum=Ynum+1;
//									}else if ("G".equals(trackStatus)) {
//										Gnum=Gnum+1;
//									}
//								}
//								if (Rnum!=0) {
//									issueStatus="R";
//								}else if (Ynum!=0) {
//									issueStatus="Y";
//								}else {
//									issueStatus="G";
//								}
//								HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
//								qtyIssue.setStatus(issueStatus);
//								//问题关闭时间
//								if ("G".equals(issueStatus)) {
//									qtyIssue.setCloseTime(new Date());
//									//添加跟踪记录
//									HClothesTrackHistory trackHistory=new HClothesTrackHistory();
//									trackHistory.setIssueId(issueId)
//									.setMeasureStatus(3).setRemark("问题关闭")
//									.setTrackUserId(user.getId()).setTrackUserName(user.getNickname())
//									.setCreateTime(new Date());
//									trackHistory.save();
//								}else {
//									qtyIssue.setCloseTime(null);
//								}
//								qtyIssue.update();
//							}
//							HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
//							if ("R".equals(monthStatus)&&!"A".equals(qtyIssue.getLevel())) {//只要为R自动升级
//								qtyIssue.setIssueStatus(1);//待审批
//								qtyIssue.update();
//								//将以前的升级记录置为失效
//								Db.update(" update h_clothesopl_upgrade_record set status=1 and issue_id="+issueId);
//								List<HClothesoplUpgradeRecord> upgradeRecords=HClothesoplUpgradeRecord.dao.find("select * from h_clothesopl_upgrade_record where sp_status=1 and issue_id="+issueId);//审批通过的升级记录为空
//								int oldUserId=0;
//								String oldUserName="";
//								if (upgradeRecords.isEmpty()) {//审批通过的升级记录为空，提出升级者便为责任人
//									oldUserId=qtyIssue.getDutyUserId();
//									oldUserName=qtyIssue.getDutyUserName();
//								}else {
//									oldUserId=upgradeRecords.get(upgradeRecords.size()-1).getNewUserId();
//									oldUserName=upgradeRecords.get(upgradeRecords.size()-1).getNewUserName();
//								}
//								//添加新的记录
//								HClothesoplUpgradeRecord upgradeRecord=new HClothesoplUpgradeRecord();
//								upgradeRecord.setIssueId(issueId)
//								.setOldUserId(oldUserId).setOldUserName(oldUserName)
//								.setNewUserId(qtyIssue.getUpgradeUserId()).setNewUserName(qtyIssue.getUpgradeUserName())
//								.setSpStatus(0).setStatus(0).setCreateTime(new Date())
//								.setUpgradeType(2);//2-3个月跟踪状态为R自动升级
//								upgradeRecord.save();
//								//钉钉通知升级对象
//								User upUser=User.dao.findById(qtyIssue.getUpgradeUserId());
//								DingMessage.sendText(upUser.getDingUserId(), "有新的质量OPL已升级，等待您的处理！\n"+sdf.format(new Date()), "15");//15-质量OPL升级处理	
//							}
//							return true;
//						}else if ("month_2".equals(month_num)) {
//							HClothesIssueMonthTrack firstTrack=HClothesIssueMonthTrack.dao.findFirst("select * from h_clothes_issue_month_track where issue_id="+issueId+" and month=1");
//							if (firstTrack==null) {
//								record.set("code", 1).set("msg", "第1月还没有完成跟踪记录，无法进行其他操作！");
//								return false;
//							}else {
//								HClothesIssueMonthTrack secondTrack=HClothesIssueMonthTrack.dao.findFirst("select * from h_clothes_issue_month_track where issue_id="+issueId+" and month=2");
//								if (secondTrack==null) {
//									secondTrack=new HClothesIssueMonthTrack();
//									secondTrack.setIssueId(issueId)
//									.setMonth(2).setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
//									.setCreateTime(new Date()).setUpdateTime(new Date());
//									secondTrack.save();
//								}else {
//									secondTrack.setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
//									.setUpdateTime(new Date());
//									secondTrack.update();
//									//问题状态判断
//									List<HClothesIssueMonthTrack> allMonthTracks=HClothesIssueMonthTrack.dao.find("select * from h_clothes_issue_month_track where issue_id="+issueId);
//									if (allMonthTracks.size()==3) {
//										String issueStatus="R";
//										int Rnum=0;
//										int Ynum=0;
//										int Gnum=0;
//										for (int i = 0; i < allMonthTracks.size(); i++) {
//											String trackStatus=allMonthTracks.get(i).getStatus();
//											if ("R".equals(trackStatus)) {
//												Rnum=Rnum+1;
//											}else if ("Y".equals(trackStatus)) {
//												Ynum=Ynum+1;
//											}else if ("G".equals(trackStatus)) {
//												Gnum=Gnum+1;
//											}
//										}
//										if (Rnum!=0) {
//											issueStatus="R";
//										}else if (Ynum!=0) {
//											issueStatus="Y";
//										}else {
//											issueStatus="G";
//										}
//										HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
//										qtyIssue.setStatus(issueStatus);
//										//问题关闭时间
//										if ("G".equals(issueStatus)) {
//											qtyIssue.setCloseTime(new Date());
//											//添加跟踪记录
//											HClothesTrackHistory trackHistory=new HClothesTrackHistory();
//											trackHistory.setIssueId(issueId)
//											.setMeasureStatus(3).setRemark("问题关闭")
//											.setTrackUserId(user.getId()).setTrackUserName(user.getNickname())
//											.setCreateTime(new Date());
//											trackHistory.save();
//										}else {
//											qtyIssue.setCloseTime(null);
//										}
//										qtyIssue.update();
//									}
//								}
//								HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
//								if ("R".equals(monthStatus)&&!"A".equals(qtyIssue.getLevel())) {//只要为R自动升级
//									qtyIssue.setIssueStatus(1);//待审批
//									qtyIssue.update();
//									//将以前的升级记录置为失效
//									Db.update(" update h_clothesopl_upgrade_record set status=1 and issue_id="+issueId);
//									List<HClothesoplUpgradeRecord> upgradeRecords=HClothesoplUpgradeRecord.dao.find("select * from h_clothesopl_upgrade_record where sp_status=1 and issue_id="+issueId);//审批通过的升级记录为空
//									int oldUserId=0;
//									String oldUserName="";
//									if (upgradeRecords.isEmpty()) {//审批通过的升级记录为空，提出升级者便为责任人
//										oldUserId=qtyIssue.getDutyUserId();
//										oldUserName=qtyIssue.getDutyUserName();
//									}else {
//										oldUserId=upgradeRecords.get(upgradeRecords.size()-1).getNewUserId();
//										oldUserName=upgradeRecords.get(upgradeRecords.size()-1).getNewUserName();
//									}
//									//添加新的记录
//									HClothesoplUpgradeRecord upgradeRecord=new HClothesoplUpgradeRecord();
//									upgradeRecord.setIssueId(issueId)
//									.setOldUserId(oldUserId).setOldUserName(oldUserName)
//									.setNewUserId(qtyIssue.getUpgradeUserId()).setNewUserName(qtyIssue.getUpgradeUserName())
//									.setSpStatus(0).setStatus(0).setCreateTime(new Date())
//									.setUpgradeType(2);//2-3个月跟踪状态为R自动升级
//									upgradeRecord.save();
//									//钉钉通知升级对象
//									User upUser=User.dao.findById(qtyIssue.getUpgradeUserId());
//									DingMessage.sendText(upUser.getDingUserId(), "有新的质量OPL已升级，等待您的处理！\n"+sdf.format(new Date()), "15");//15-质量OPL升级处理	
//								}
//								return true;
//							}
//						}else if ("month_3".equals(month_num)) {
//							HClothesIssueMonthTrack secondTrack=HClothesIssueMonthTrack.dao.findFirst("select * from h_clothes_issue_month_track where issue_id="+issueId+" and month=2");
//							if (secondTrack==null) {
//								record.set("code", 1).set("msg", "第2月还没有完成跟踪记录，无法进行其他操作！");
//								return false;
//							}else {
//								HClothesIssueMonthTrack thirdTrack=HClothesIssueMonthTrack.dao.findFirst("select * from h_clothes_issue_month_track where issue_id="+issueId+" and month=3");
//								if (thirdTrack==null) {
//									thirdTrack=new HClothesIssueMonthTrack();
//									thirdTrack.setIssueId(issueId)
//									.setMonth(3).setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
//									.setCreateTime(new Date()).setUpdateTime(new Date());
//									thirdTrack.save();
//								}else {
//									thirdTrack.setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
//									.setUpdateTime(new Date());
//									thirdTrack.update();
//								}
//								//第三月则判断整个问题状态
//								List<HClothesIssueMonthTrack> allMonthTracks=HClothesIssueMonthTrack.dao.find("select * from h_clothes_issue_month_track where issue_id="+issueId);
//								String issueStatus="R";
//								int Rnum=0;
//								int Ynum=0;
//								int Gnum=0;
//								for (int i = 0; i < allMonthTracks.size(); i++) {
//									String trackStatus=allMonthTracks.get(i).getStatus();
//									if ("R".equals(trackStatus)) {
//										Rnum=Rnum+1;
//									}else if ("Y".equals(trackStatus)) {
//										Ynum=Ynum+1;
//									}else if ("G".equals(trackStatus)) {
//										Gnum=Gnum+1;
//									}
//								}
//								if (Rnum!=0) {
//									issueStatus="R";
//								}else if (Ynum!=0) {
//									issueStatus="Y";
//								}else {
//									issueStatus="G";
//								}
//								HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
//								qtyIssue.setStatus(issueStatus);
//								//问题关闭时间
//								if ("G".equals(issueStatus)) {
//									qtyIssue.setCloseTime(new Date());
//									//添加跟踪记录
//									HClothesTrackHistory trackHistory=new HClothesTrackHistory();
//									trackHistory.setIssueId(issueId)
//									.setMeasureStatus(3).setRemark("问题关闭")
//									.setTrackUserId(user.getId()).setTrackUserName(user.getNickname())
//									.setCreateTime(new Date());
//									trackHistory.save();
//								}else {
//									qtyIssue.setCloseTime(null);
//								}
//								if ("R".equals(monthStatus)&&!"A".equals(qtyIssue.getLevel())) {//只要为R自动升级
//									qtyIssue.setIssueStatus(1);//待审批
//									//将以前的升级记录置为失效
//									Db.update(" update h_clothesopl_upgrade_record set status=1 and issue_id="+issueId);
//									List<HClothesoplUpgradeRecord> upgradeRecords=HClothesoplUpgradeRecord.dao.find("select * from h_clothesopl_upgrade_record where sp_status=1 and issue_id="+issueId);//审批通过的升级记录为空
//									int oldUserId=0;
//									String oldUserName="";
//									if (upgradeRecords.isEmpty()) {//审批通过的升级记录为空，提出升级者便为责任人
//										oldUserId=qtyIssue.getDutyUserId();
//										oldUserName=qtyIssue.getDutyUserName();
//									}else {
//										oldUserId=upgradeRecords.get(upgradeRecords.size()-1).getNewUserId();
//										oldUserName=upgradeRecords.get(upgradeRecords.size()-1).getNewUserName();
//									}
//									//添加新的记录
//									HClothesoplUpgradeRecord upgradeRecord=new HClothesoplUpgradeRecord();
//									upgradeRecord.setIssueId(issueId)
//									.setOldUserId(oldUserId).setOldUserName(oldUserName)
//									.setNewUserId(qtyIssue.getUpgradeUserId()).setNewUserName(qtyIssue.getUpgradeUserName())
//									.setSpStatus(0).setStatus(0).setCreateTime(new Date())
//									.setUpgradeType(2);//2-3个月跟踪状态为R自动升级
//									upgradeRecord.save();
//									//钉钉通知升级对象
//									User upUser=User.dao.findById(qtyIssue.getUpgradeUserId());
//									DingMessage.sendText(upUser.getDingUserId(), "有新的质量OPL已升级，等待您的处理！\n"+sdf.format(new Date()), "15");//15-质量OPL升级处理	
//								}
//								qtyIssue.update();
//								return true;
//							}
//						}else {
//							return true;
//						}
//					}
//				}
//			});
//			if (flag) {
//				renderJson(Ret.ok("msg", "提交成功！"));
//			}else {
//				String msg=record.getStr("msg");
//				renderJson(Ret.fail("msg", "提交失败，"+ msg));
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
//		}
//	}
	/**
	 * 7D编辑
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月19日 上午10:41:40
	 */
	@SuppressWarnings("rawtypes")
	public void save7D() {
		try {
			Record record=new Record();
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				String dname=map.get("dname").toString();
				int dstatus=Integer.valueOf(map.get("status").toString());
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				Date finishDate=null;
				String finish_date=map.get("finish_date").toString();
				if (!"".equals(finish_date)) {
					try {
						finishDate = sdf.parse(map.get("finish_date").toString());
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				HClothesIssue8dReal dreal=HClothesIssue8dReal.dao.findFirst("select * from h_clothes_issue_8d_real where issue_id="+issueId);
				HClothesIssue8dPlan dplan=HClothesIssue8dPlan.dao.findFirst("select * from h_clothes_issue_8d_plan where issue_id="+issueId);
				HClothesIssue issue=HClothesIssue.dao.findById(issueId);
				int upgradeNum=issue.getUpgradeNum();
				if (dreal==null) {//还没有实际7D，则只能编辑D1
					if ("d_one".equals(dname)) {
						dreal=new HClothesIssue8dReal();
						dreal.setDOne(finishDate).setDOneStatus(dstatus).setIssueId(issueId);
						dreal.save();
						//下一个提醒日期
						if (dstatus==2) {
							issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDTwo(),(upgradeNum+1)*3));
							issue.update();
						}
						return true;
					}else {//编辑非done则报错
						record.set("code", 1).set("msg", "D1还未完成，无法进行其他操作！");
						return false;
					}
				}else {//有7D，则看前一项是否为空
					if ("d_one".equals(dname)) {
						dreal.setDOne(finishDate).setDOneStatus(dstatus);
						dreal.update();
						//下一个提醒日期
						if (dstatus==2) {
							issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDTwo(),(upgradeNum+1)*3));
							issue.update();
						}else {
							issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDOne(),(upgradeNum+1)*3));
							issue.update();
						}
						return true;
					}else if ("d_two".equals(dname)) {//看d1是否为空
						int dlastStatus=dreal.getDOneStatus();
						if (dlastStatus!=2) {
							record.set("code", 1).set("msg", "D1还未完成，无法进行其他操作！");
							return false;
						}else {
							dreal.setDTwo(finishDate).setDTwoStatus(dstatus);
							dreal.update();
							//下一个提醒日期
							if (dstatus==2) {
								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDThree(),(upgradeNum+1)*3));
								issue.update();
							}else {
								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDTwo(),(upgradeNum+1)*3));
								issue.update();
							}
							return true;
						}
					}else if ("d_three".equals(dname)) {//看d2是否为空  // 2022-6-16 手工样件opl只有3d，opl状态判定D3是否完成
						int dlastStatus=dreal.getDTwoStatus();
						if (dlastStatus!=2) {
							record.set("code", 1).set("msg", "D2还未完成，无法进行其他操作！");
							return false;
						}else {
							dreal.setDThree(finishDate).setDThreeStatus(dstatus);
							dreal.update();
							if (dstatus==2) {
								//修改质量opl状态为G
								issue.setStatus("G");
								issue.update();
								//2022-6-16 增加关联opl状态与质量opl状态同步
								/**
								 * 项目OPL状态变化---start
								 */
								if (!"SOP".equals(issue.getProductStage())) {
									HProjectIssue projectIssue=HProjectIssue.dao.findById(issue.getProIssueId());
									if (projectIssue!=null) {
										projectIssue.setStatus("G").setRealFinishTime(finishDate);
										projectIssue.update();
									}
								}
								/**
								 * 项目OPL状态变化---end
								 */
							}else {
								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDSeven(),(upgradeNum+1)*3));
								issue.update();
							}
							return true;
						}
					}else {
						return true;
					}
					
//					else if ("d_three".equals(dname)) {//看d2是否为空
//						int dlastStatus=dreal.getDTwoStatus();
//						if (dlastStatus!=2) {
//							record.set("code", 1).set("msg", "D2还未完成，无法进行其他操作！");
//							return false;
//						}else {
//							dreal.setDThree(finishDate).setDThreeStatus(dstatus);
//							dreal.update();
//							//下一个提醒日期
//							if (dstatus==2) {
//								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDFour(),(upgradeNum+1)*3));
//								issue.update();
//							}else {
//								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDThree(),(upgradeNum+1)*3));
//								issue.update();
//							}
//							return true;
//						}
//					}else if ("d_four".equals(dname)) {//看d3是否为空
//						int dlastStatus=dreal.getDThreeStatus();
//						if (dlastStatus!=2) {
//							record.set("code", 1).set("msg", "D3还未完成，无法进行其他操作！");
//							return false;
//						}else {
//							dreal.setDFour(finishDate).setDFourStatus(dstatus);
//							dreal.update();
//							//下一个提醒日期
//							if (dstatus==2) {
//								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDFive(),(upgradeNum+1)*3));
//								issue.update();
//							}else {
//								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDFour(),(upgradeNum+1)*3));
//								issue.update();
//							}
//							return true;
//						}
//					}else if ("d_five".equals(dname)) {//看d4是否为空
//						int dlastStatus=dreal.getDFourStatus();
//						if (dlastStatus!=2) {
//							record.set("code", 1).set("msg", "D4还未完成，无法进行其他操作！");
//							return false;
//						}else {
//							dreal.setDFive(finishDate).setDFiveStatus(dstatus);
//							dreal.update();
//							//下一个提醒日期
//							if (dstatus==2) {
//								if (dplan.getDSix()!=null) {
//									issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDSix(),(upgradeNum+1)*3));
//									issue.update();
//								}else {
//									issue.setDNoteDate(null);
//									issue.update();
//								}
//							}else {
//								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDFive(),(upgradeNum+1)*3));
//								issue.update();
//							}
//							return true;
//						}
//					}else if ("d_six".equals(dname)) {//看d5是否为空
//						int dlastStatus=dreal.getDFiveStatus();
//						if (dlastStatus!=2) {
//							record.set("code", 1).set("msg", "D5还未完成，无法进行其他操作！");
//							return false;
//						}else if (dplan.getDSix()==null) {
//							record.set("code", 1).set("msg", "D6计划时间为空，请先维护D6计划时间点！");
//							return false;
//						}else {
//							dreal.setDSix(finishDate).setDSixStatus(dstatus);
//							dreal.update();
//							//下一个提醒日期
//							if (dstatus==2) {
//								if (dplan.getDSeven()!=null) {
//									issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDSeven(),(upgradeNum+1)*3));
//									issue.update();
//								}else {
//									issue.setDNoteDate(null);
//									issue.update();
//								}
//							}else {
//								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDSix(),(upgradeNum+1)*3));
//								issue.update();
//							}
//							return true;
//						}
//					}else if ("d_seven".equals(dname)) {//看d6是否为空
//						int dlastStatus=dreal.getDSixStatus();
//						if (dlastStatus!=2) {
//							record.set("code", 1).set("msg", "D6还未完成，无法进行其他操作！");
//							return false;
//						}else if (dplan.getDSeven()==null) {
//							record.set("code", 1).set("msg", "D7计划时间为空，请先维护D7计划时间点！");
//							return false;
//						}else {
//							dreal.setDSeven(finishDate).setDSevenStatus(dstatus);
//							dreal.update();
//							if (dstatus==2) {
//								issue.setDNoteDate(null)
//								.setPermanentMeasuresTime(finishDate)//永久措施节点
//								.setFirstMonth(com.ray.util.HyCommenMethods.addDate(finishDate, 30))
//								.setSecondMonth(com.ray.util.HyCommenMethods.addDate(finishDate, 60))
//								.setThirdMonth(com.ray.util.HyCommenMethods.addDate(finishDate, 90));
//								issue.update();
//								//2022-6-16 增加关联opl状态与质量opl状态同步
////								/**
////								 * 项目OPL状态变化---start
////								 */
////								if (!"SOP".equals(issue.getProductStage())) {
////									HProjectIssue projectIssue=HProjectIssue.dao.findById(issue.getProIssueId());
////									if (projectIssue!=null) {
////										projectIssue.setStatus("G").setRealFinishTime(finishDate);
////										projectIssue.update();
////									}
////								}
////								/**
////								 * 项目OPL状态变化---end
////								 */
//							}else {
//								issue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDSeven(),(upgradeNum+1)*3));
//								issue.update();
//							}
//							return true;
//						}
//					}
					
					
				}
			});
			if (flag) {
				renderJson(Ret.ok("msg", "提交成功！"));
			}else {
				String msg=record.getStr("msg");
				renderJson(Ret.fail("msg", "提交失败，"+ msg));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 上传8D报告和编号
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月20日 下午4:34:43
	 */
	@SuppressWarnings("rawtypes")
	public void upQtyReport() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				//分析报告
				JSONArray fileList=JSONArray.parseArray(getPara("fileList"));
				int fileId=0;
				if(fileList.size()!=0) {
					JSONObject fileObject=fileList.getJSONObject(0);
					fileId=fileObject.getIntValue("id");
				}
				HClothesIssue qtyIssue=HClothesIssue.dao.findById(issueId);
				qtyIssue.setFileId(fileId)
				.setReportNo(map.get("report_no").toString());
				qtyIssue.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "上传成功！"));
			}else {
				renderJson(Ret.fail("msg", "上传失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
}
