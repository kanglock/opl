package com.ray.controller.admin.ClothesIssue;

import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.model.HQualityIssueType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 质量板块分析 /qualityIsueAnalysis/toQualityIssusAnalysis
 * @author FL00024996
 *
 */
public class ClothesAnalysisController extends Controller {

	public void toClothesIssusAnalysis() {
		render("clothesIssueAnalysis.html");
	}
	/**问题来源&问题类型
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年9月23日 上午10:11:40
	 */
	public void getAnalysisTypeSourceTableData() {
		String sqlCom="select count(*) as total_num from h_clothes_issue where is_del=0 ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		List<Record> dataList=new ArrayList<Record>();
		List<HQualityIssueType> typeList=HQualityIssueType.dao.find("select * from h_quality_issue_type where status=0");
		String[] sources= {"入厂检测","生产过程","成品入库","客户反馈","其他","合计"}; 
		for (int i = 0; i < sources.length; i++) {
			Record analysisData=new Record();
			analysisData.set("source", sources[i]);
			String sqlSourceCom=sqlCom;
			if (i<5) {
				sqlSourceCom=sqlSourceCom+" and source='"+sources[i]+"'";
			}
			int sourceAllNum=0;//同等级问题总数
			int sourceCloseNum=0;//同等级关闭问题数
			for (int j = 0; j < typeList.size(); j++) {
				String sqlAll=sqlSourceCom+" and type_id='"+typeList.get(j).getId().intValue()+"'";
				String sqlClose=sqlSourceCom+" and type_id='"+typeList.get(j).getId().intValue()+"' and status='G'";
				Record allissue=Db.findFirst(sqlAll);
				Record closeIssue=Db.findFirst(sqlClose);
				int alltypeNum=allissue.getInt("total_num");
				int closeypeNum=closeIssue.getInt("total_num");
				sourceAllNum=sourceAllNum+alltypeNum;
				sourceCloseNum=sourceCloseNum+closeypeNum;
				analysisData.set("all_"+typeList.get(j).getEnTitle(), alltypeNum).set("close_"+typeList.get(j).getEnTitle(), closeypeNum);
			}
			analysisData.set("all_heji", sourceAllNum).set("close_heji", sourceCloseNum);
			//关闭率
			if (sourceAllNum>0) {
				BigDecimal closeRate=new BigDecimal(sourceCloseNum).divide(new BigDecimal(sourceAllNum), 4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
				analysisData.set("close_rate", closeRate+"%");
			}else {
				analysisData.set("close_rate", "0.00%");
			}
			dataList.add(analysisData);
		}
		renderJson(Ret.ok("data",dataList));
	}
	/**获取分析数据详情---问题类型统计
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月30日 下午5:32:54
	 */
	public void getAnalysisTableData() {
		String sqlCom="select count(*) as total_num from h_clothes_issue where is_del=0 ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		List<Record> dataList=new ArrayList<Record>();
		List<HQualityIssueType> typeList=HQualityIssueType.dao.find("select * from h_quality_issue_type where status=0");
		String[] levels= {"A","B","C","合计"};
		for (int i = 0; i < levels.length; i++) {
			Record analysisData=new Record();
			analysisData.set("level", levels[i]);
			String sqlLevelCom=sqlCom;
			if (i<3) {
				sqlLevelCom=sqlLevelCom+" and level='"+levels[i]+"'";
			}else {
				sqlLevelCom=sqlLevelCom+" and (level='A' or level='B' or level='C')";
			}
			int levelAllNum=0;//同等级问题总数
			int levelCloseNum=0;//同等级关闭问题数
			for (int j = 0; j < typeList.size(); j++) {
				String sqlAll=sqlLevelCom+" and type_id='"+typeList.get(j).getId().intValue()+"'";
				String sqlClose=sqlLevelCom+" and type_id='"+typeList.get(j).getId().intValue()+"' and status='G'";
				Record allissue=Db.findFirst(sqlAll);
				Record closeIssue=Db.findFirst(sqlClose);
				int alltypeNum=allissue.getInt("total_num");
				int closeypeNum=closeIssue.getInt("total_num");
				levelAllNum=levelAllNum+alltypeNum;
				levelCloseNum=levelCloseNum+closeypeNum;
				analysisData.set("all_"+typeList.get(j).getEnTitle(), alltypeNum).set("close_"+typeList.get(j).getEnTitle(), closeypeNum);
			}
			analysisData.set("all_heji", levelAllNum).set("close_heji", levelCloseNum);
			//关闭率
			if (levelAllNum>0) {
				BigDecimal closeRate=new BigDecimal(levelCloseNum).divide(new BigDecimal(levelAllNum), 4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
				analysisData.set("close_rate", closeRate+"%");
			}else {
				analysisData.set("close_rate", "0.00%");
			}
			dataList.add(analysisData);
		}
		renderJson(Ret.ok("data",dataList));
	}
	/**
	 * 饼状图数据获取---严重度
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月3日 上午10:02:03
	 */
	public void getPieData() {
		String sqlCom="select count(*) as total_num from h_clothes_issue where is_del=0  ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		//严重度
		String[] levels= {"A","B","C"};
		List<Record> levelList=new ArrayList<Record>();
		for (int i = 0; i < levels.length; i++) {
			Record analysisData=new Record();
			analysisData.set("level", levels[i]);
			String sqlLevelCom=sqlCom+" and level='"+levels[i]+"'";
			Record allLevelIssue=Db.findFirst(sqlLevelCom);
			int allNum=allLevelIssue.getInt("total_num");
			analysisData.set("total_num", allNum);
			levelList.add(analysisData);
		}
		//类型
		List<HQualityIssueType> types=HQualityIssueType.dao.find("select * from h_quality_issue_type where status=0");
		List<Record> typeList=new ArrayList<Record>();
		for (int i = 0; i < types.size(); i++) {
			Record analysisData=new Record();
			analysisData.set("type", types.get(i).getType());
			String sqlTypeCom=sqlCom+" and type_id='"+types.get(i).getId().intValue()+"'";
			Record allTypeIssue=Db.findFirst(sqlTypeCom);
			int allNum=allTypeIssue.getInt("total_num");
			analysisData.set("total_num", allNum);
			typeList.add(analysisData);
		}
		Record record=new Record();
		record.set("level_list", levelList).set("type_list", typeList);
		renderJson(Ret.ok("data",record));
	}
	/**问题来源分析
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月31日 上午9:13:06
	 */
	public void getAnalysisSource() {
		String sqlCom="select count(*) as total_num from h_clothes_issue where is_del=0 ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		List<Record> dataList=new ArrayList<Record>();
		String[] levels= {"A","B","C","合计"};
		String[] sources= {"入厂检测","生产过程","成品入库","客户反馈","其他","合计"};
		for (int i = 0; i < sources.length; i++) {
			Record analysisData=new Record();
			analysisData.set("source", sources[i]);
			String sqlSourceCom=sqlCom;
			if (i<5) {
				sqlSourceCom=sqlSourceCom+" and source='"+sources[i]+"'";
			}
			int sourceAllNum=0;//同等级问题总数
			int sourceCloseNum=0;//同等级关闭问题数
			for (int j = 0; j < levels.length-1; j++) {
				String sqlAll=sqlSourceCom;
				String sqlClose=sqlSourceCom;
				sqlAll=sqlAll+" and level='"+levels[j]+"'";
				sqlClose=sqlClose+" and level='"+levels[j]+"' and status='G'";
				Record allissue=Db.findFirst(sqlAll);
				Record closeIssue=Db.findFirst(sqlClose);
				int alltypeNum=allissue.getInt("total_num");
				int closeypeNum=closeIssue.getInt("total_num");
				sourceAllNum=sourceAllNum+alltypeNum;
				sourceCloseNum=sourceCloseNum+closeypeNum;
				if ("A".equals(levels[j])) {
					analysisData.set("all_a", alltypeNum).set("close_a", closeypeNum);
				}else if ("B".equals(levels[j])) {
					analysisData.set("all_b", alltypeNum).set("close_b", closeypeNum);
				}else if ("C".equals(levels[j])) {
					analysisData.set("all_c", alltypeNum).set("close_c", closeypeNum);
				}
			}
			analysisData.set("all_heji", sourceAllNum).set("close_heji", sourceCloseNum);
			//关闭率
			if (sourceAllNum>0) {
				BigDecimal closeRate=new BigDecimal(sourceCloseNum).divide(new BigDecimal(sourceAllNum), 4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
				analysisData.set("close_rate", closeRate+"%");
			}else {
				analysisData.set("close_rate", "0.00%");
			}
			dataList.add(analysisData);
		}
		renderJson(Ret.ok("data",dataList));
	}
	/**问题来源
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年8月31日 上午9:41:59
	 */
	public void getAnalysisSourcePieData() {
		String sqlCom="select count(*) as total_num from h_clothes_issue where is_del=0  ";
		if (getPara("selDep")!=null&&!"".equals(getPara("selDep"))) {
			sqlCom=sqlCom+" and dep_id="+getParaToLong("selDep");
		}
		if (getPara("selWare")!=null&&!"".equals(getPara("selWare"))) {
			sqlCom=sqlCom+" and ware_id="+getParaToLong("selWare");
		}
		//严重度
		String[] sources= {"入厂检测","生产过程","成品入库","其他","客户反馈"};
		List<Record> sourceList=new ArrayList<Record>();
		for (int i = 0; i < sources.length; i++) {
			Record analysisData=new Record();
			analysisData.set("source", sources[i]);
			String sqlLevelCom=sqlCom+" and source='"+sources[i]+"'";
			Record allLevelIssue=Db.findFirst(sqlLevelCom);
			int allNum=allLevelIssue.getInt("total_num");
			analysisData.set("total_num", allNum);
			sourceList.add(analysisData);
		}
		Record record=new Record();
		record.set("source_list", sourceList);
		renderJson(Ret.ok("data",record));
	}
}
