package com.ray.controller.admin.ProductIssue;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.mysql.jdbc.StringUtils;
import com.ray.common.model.AndonProblemIssueProblem;
import com.ray.common.model.AndonProblemTrackRecord;
import com.ray.controller.admin.BaseInfo.BaseInfoCon;
import com.ray.util.ExportExcelUtil;
import org.apache.commons.collections.MapUtils;

/**
 * 所有生产OPL清单 allProductOPL
 * @author FL00024996
 * @data 2021年11月12日
 */
public class AllProductIssueController extends Controller {
	/**获取所有生产OPL
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月12日 下午4:23:42
	 */
	public void getAllProductOPL() {
		// 行内筛选和排序
		Map<String,Object> queryData = JSON.parseObject(get("queryData"), Map.class);
		StringBuilder sb=new StringBuilder();
		sb.append(" from andon_product_issue a,h_cause_dep b,h_ware c where a.dep_id=b.dep_id and c.id=a.ware_id ");
		String selProductCode=get("selProductCode");
		String selProductName=get("selProductName");
		String selProductDate1=get("selProductDate1");
		String selProductDate2=get("selProductDate2");
		String selReasonType=get("selReasonType");
		if (selProductCode!=null&&!"".equals(selProductCode)) {
			sb.append(" and a.product_code like'%"+selProductCode+"%'");
		}
		if (selProductName!=null&&!"".equals(selProductName)) {
			sb.append(" and a.product_name like'%"+selProductName+"%'");
		}
		if (selProductDate1!=null&&!"".equals(selProductDate1)) {
			sb.append(" and a.product_date>='"+selProductDate1+"'");
		}
		if (selProductDate2!=null&&!"".equals(selProductDate2)) {
			sb.append(" and a.product_date<='"+selProductDate2+"'");
		}
		if (selReasonType!=null&&!"".equals(selReasonType)) {
			sb.append(" and a.reason_type='"+selReasonType+"'");
		}
		long selDep=getParaToLong("selDep");
		if (selDep!=0) {
			sb.append(" and a.dep_id="+selDep);
		}
		int selWarehouse=getParaToInt("selWarehouse");
		if (selWarehouse!=0) {
			sb.append(" and a.ware_id="+selWarehouse);
		}
		String selDutyUser=get("selDutyUser");
		if (!"".equals(selDutyUser)&& selDutyUser!=null) {
			sb.append(" and a.duty_user_name like '%"+selDutyUser+"%'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		//行内排序
		if(!StringUtils.isNullOrEmpty(MapUtils.getString(queryData, "order", ""))&&!StringUtils.isNullOrEmpty(MapUtils.getString(queryData, "prop", ""))) {
			sb.append(" order by ").append(MapUtils.getString(queryData, "prop", "")).append(" ").append(MapUtils.getString(queryData, "order", ""));
		} else {
			sb.append(" order by a.status asc");
		}
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name,c.ware_name ",sb.toString());
		long nowDateTime=new Date().getTime();
		for (int i = 0; i < issueList.getList().size(); i++) {
			String wareName = BaseInfoCon.getWareNameByWareId(issueList.getList().get(i).getInt("ware_id"));
			issueList.getList().get(i).set("ware_name",wareName);
			int isShowRed=0;//不显示红色
			if (issueList.getList().get(i).getInt("status")==0) {
				//判断转opl时间是否超过24小时
				Date updateDate=issueList.getList().get(i).getDate("update_time");
				long updateTime=updateDate.getTime();
				if (nowDateTime-updateTime>1000*60*60*24) {
					isShowRed=1;//显示红色
				}
			}
			issueList.getList().get(i).set("is_show_red", isShowRed);
			//问题属性
			List<AndonProblemIssueProblem> problems=AndonProblemIssueProblem.dao.find("select * from andon_problem_issue_problem where status=0 and opl_id="+issueList.getList().get(i).getLong("id"));
			String problem="暂无";
			if (!problems.isEmpty()) {
				problem="";
				for (int j = 0; j < problems.size(); j++) {
					problem=problem+"严重度："+problems.get(j).getSeverity()+"；原因："+problems.get(j).getReasonType()+"；类别："+problems.get(j).getProblemType()+"： "+problems.get(j).getAffectNum()+"<br>";
				}
			}
			issueList.getList().get(i).set("problem_type", problem);
			//跟踪记录
			List<AndonProblemTrackRecord> trackRecords=AndonProblemTrackRecord.dao.find("select * from andon_problem_track_record where opl_id="+issueList.getList().get(i).getLong("id"));
			String track="暂无";
			if (!trackRecords.isEmpty()) {
				track="";
				for (int j = 0; j < trackRecords.size(); j++) {
					track=track+"跟踪人："+trackRecords.get(j).getCreateUserName()+"，跟踪状态："+trackRecords.get(j).getTrackStatus()+"，跟踪内容："+trackRecords.get(j).getTrackContent()+"，跟踪时间："+trackRecords.get(j).getCreateTime()+"<br>";
				}
			}
			issueList.getList().get(i).set("track", track);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", issueList.getList());
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}

	public void downLoadAllProductOPL(){
		{
			// 行内筛选和排序
			Map<String,Object> queryData = JSON.parseObject(get("queryData"), Map.class);
			StringBuilder sb=new StringBuilder();
			sb.append(" from andon_product_issue a,h_cause_dep b,h_ware c where a.dep_id=b.dep_id and c.id=a.ware_id ");
			String selProductCode=get("selProductCode");
			String selProductName=get("selProductName");
			String selProductDate1=get("selProductDate1");
			String selProductDate2=get("selProductDate2");
			String selReasonType=get("selReasonType");
			if (selProductCode!=null&&!"".equals(selProductCode)) {
				sb.append(" and a.product_code like'%"+selProductCode+"%'");
			}
			if (selProductName!=null&&!"".equals(selProductName)) {
				sb.append(" and a.product_name like'%"+selProductName+"%'");
			}
			if (selProductDate1!=null&&!"".equals(selProductDate1)) {
				sb.append(" and a.product_date>='"+selProductDate1+"'");
			}
			if (selProductDate2!=null&&!"".equals(selProductDate2)) {
				sb.append(" and a.product_date<='"+selProductDate2+"'");
			}
			if (selReasonType!=null&&!"".equals(selReasonType)) {
				sb.append(" and a.reason_type='"+selReasonType+"'");
			}
			long selDep=getParaToLong("selDep");
			if (selDep!=0) {
				sb.append(" and a.dep_id="+selDep);
			}
			int selWarehouse=getParaToInt("selWarehouse");
			if (selWarehouse!=0) {
				sb.append(" and a.ware_id="+selWarehouse);
			}
			String selDutyUser=get("selDutyUser");
			if (!"".equals(selDutyUser)&& selDutyUser!=null) {
				sb.append(" and a.duty_user_name like '%"+selDutyUser+"%'");
			}
			//行内排序
			if(!StringUtils.isNullOrEmpty(MapUtils.getString(queryData, "order", ""))&&!StringUtils.isNullOrEmpty(MapUtils.getString(queryData, "prop", ""))) {
				sb.append(" order by ").append(MapUtils.getString(queryData, "prop", "")).append(" ").append(MapUtils.getString(queryData, "order", ""));
			} else {
				sb.append(" order by a.status asc");
			}
			List<Record> issueList=Db.find( "select a.*,b.dep_name,c.ware_name "+sb.toString());
			long nowDateTime=new Date().getTime();
			for (int i = 0; i < issueList.size(); i++) {
				String wareName = BaseInfoCon.getWareNameByWareId(issueList.get(i).getInt("ware_id"));
				issueList.get(i).set("ware_name",wareName);
				int isShowRed=0;//不显示红色
				if (issueList.get(i).getInt("status")==0) {
					//判断转opl时间是否超过24小时
					Date updateDate=issueList.get(i).getDate("update_time");
					long updateTime=updateDate.getTime();
					if (nowDateTime-updateTime>1000*60*60*24) {
						isShowRed=1;//显示红色
					}
				}
				issueList.get(i).set("is_show_red", isShowRed);
				//问题属性
				List<AndonProblemIssueProblem> problems=AndonProblemIssueProblem.dao.find("select * from andon_problem_issue_problem where status=0 and opl_id="+issueList.get(i).getLong("id"));
				String problem="暂无";
				if (!problems.isEmpty()) {
					problem="";
					for (int j = 0; j < problems.size(); j++) {
						problem=problem+"严重度："+problems.get(j).getSeverity()+"；原因："+problems.get(j).getReasonType()+"；类别："+problems.get(j).getProblemType()+"： "+problems.get(j).getAffectNum()+"<br>";
					}
				}
				issueList.get(i).set("problem_type", problem);
				//跟踪记录
				List<AndonProblemTrackRecord> trackRecords=AndonProblemTrackRecord.dao.find("select * from andon_problem_track_record where opl_id="+issueList.get(i).getLong("id"));
				String track="暂无";
				if (!trackRecords.isEmpty()) {
					track="";
					for (int j = 0; j < trackRecords.size(); j++) {
						track=track+"跟踪人："+trackRecords.get(j).getCreateUserName()+"，跟踪状态："+trackRecords.get(j).getTrackStatus()+"，跟踪内容："+trackRecords.get(j).getTrackContent()+"，跟踪时间："+trackRecords.get(j).getCreateTime()+"<br>";
					}
				}
				issueList.get(i).set("track", track);
			}
			Map headMap = new LinkedHashMap();
			headMap.put("product_code","产品件号");
			headMap.put("product_name","产品名称");
			headMap.put("dep_name","事业部");
			headMap.put("ware_name","车间");
			headMap.put("line_name","产线");
			headMap.put("product_date","生产日期");
			headMap.put("affect_num","影响产出件数");
			headMap.put("duty_user_name","责任人");
			headMap.put("status","状态");
			headMap.put("plan_finish_date","计划完成时间");
			headMap.put("finish_status","完成状态");
			headMap.put("problem_type","问题属性");
			headMap.put("description","具体描述");
			headMap.put("measures","解决措施");
			headMap.put("real_finish_date","实际完成时间");
			headMap.put("update_time","创建时间");
			headMap.put("track","跟踪记录");
			String filename = ExportExcelUtil.export(headMap, "OPL清单", issueList);
			Record record=new Record();
			record.set("code", 0);
			record.set("msg", "获取成功");
			record.set("data", "/"+filename);
			renderJson(record);
		}
	}
	public void orRenderFiler() {
		String paramUrl=get("paramUrl");
		renderFile(paramUrl);
	}
}
