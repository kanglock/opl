package com.ray.controller.admin.ProductIssue;

import java.text.SimpleDateFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.mysql.jdbc.StringUtils;
import com.ray.controller.admin.BaseInfo.BaseInfoCon;
import org.apache.commons.collections.MapUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.model.AndonIssueRefuseRecord;
import com.ray.common.model.AndonProblemEditRecord;
import com.ray.common.model.AndonProblemIssueProblem;
import com.ray.common.model.AndonProblemTrackRecord;
import com.ray.common.model.AndonProductIssue;
import com.ray.common.model.User;
/**
 * 我负责的生产OPL
 * @author FL00024996
 * @data 2021年11月11日
 */
@SuppressWarnings("deprecation")
public class MyDutyProductIssueController extends Controller {
	/**
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月11日 下午1:55:47
	 */
	public void getMyDutyProductOPL() {
		// 行内筛选和排序
		Map<String,Object> queryData = JSON.parseObject(get("queryData"), Map.class);
		StringBuilder sb=new StringBuilder();
		Record loginUser=getSessionAttr("user");
		int loginUserId=loginUser.getInt("id");
		sb.append(" from andon_product_issue a,h_cause_dep b,h_ware c where a.duty_user_id="+loginUserId+" and a.dep_id=b.dep_id and a.status<>2 and c.id=a.ware_id ");
		String selProductCode=get("selProductCode");
		String selProductName=get("selProductName");
		String selProductDate=get("selProductDate");
		String selReasonType=get("selReasonType");
		if (selProductCode!=null&&!"".equals(selProductCode)) {
			sb.append(" and a.product_code like'%"+selProductCode+"%'");
		}
		if (selProductName!=null&&!"".equals(selProductName)) {
			sb.append(" and a.product_name like'%"+selProductName+"%'");
		}
		if (selProductDate!=null&&!"".equals(selProductDate)) {
			sb.append(" and a.product_date='"+selProductDate+"'");
		}
		if (selReasonType!=null&&!"".equals(selReasonType)) {
			sb.append(" and a.reason_type='"+selReasonType+"'");
		}
		//行内排序
		if(!StringUtils.isNullOrEmpty(MapUtils.getString(queryData, "order", ""))&&!StringUtils.isNullOrEmpty(MapUtils.getString(queryData, "prop", ""))) {
			sb.append(" order by ").append(MapUtils.getString(queryData, "prop", "")).append(" ").append(MapUtils.getString(queryData, "order", ""));
		} else {
			sb.append(" order by a.status asc");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name,c.ware_name ",sb.toString());
		long nowDateTime=new Date().getTime();
		for (int i = 0; i < issueList.getList().size(); i++) {
			String wareName = BaseInfoCon.getWareNameByWareId(issueList.getList().get(i).getInt("ware_id"));
			issueList.getList().get(i).set("ware_name",wareName);
			int isShowRed=0;//不显示红色
			if (issueList.getList().get(i).getInt("status")==0) {
				//判断转opl时间是否超过24小时
				Date updateDate=issueList.getList().get(i).getDate("update_time");
				long updateTime=updateDate.getTime();
				if (nowDateTime-updateTime>1000*60*60*24) {
					isShowRed=1;//显示红色
				}
			}
			issueList.getList().get(i).set("is_show_red", isShowRed);
			//责任人钉钉id
			User dutyUser=User.dao.findById(issueList.getList().get(i).getInt("duty_user_id"));
			issueList.getList().get(i).set("ding_user_id", dutyUser.getDingUserId());
			//问题属性
			List<AndonProblemIssueProblem> problems=AndonProblemIssueProblem.dao.find("select * from andon_problem_issue_problem where status=0 and opl_id="+issueList.getList().get(i).getLong("id"));
			String problem="暂无";
			if (!problems.isEmpty()) {
				problem="";
				for (int j = 0; j < problems.size(); j++) {
					problem=problem+"产线编号："+problems.get(j).getLineId()+"；产线名称："+problems.get(j).getLineName()+"；严重度："+problems.get(j).getSeverity()+"；原因："+problems.get(j).getReasonType()+"；类别："+problems.get(j).getProblemType()+"： "+problems.get(j).getAffectNum()+"<br>";
				}
			}
			issueList.getList().get(i).set("problem_type", problem);
			//跟踪记录
			List<AndonProblemTrackRecord> trackRecords=AndonProblemTrackRecord.dao.find("select * from andon_problem_track_record where opl_id="+issueList.getList().get(i).getLong("id"));
			String track="暂无";
			if (!trackRecords.isEmpty()) {
				track="";
				for (int j = 0; j < trackRecords.size(); j++) {
					track=track+"跟踪人："+trackRecords.get(j).getCreateUserName()+"，跟踪状态："+trackRecords.get(j).getTrackStatus()+"，跟踪内容："+trackRecords.get(j).getTrackContent()+"，跟踪时间："+trackRecords.get(j).getCreateTime()+"<br>";
				}
			}
			issueList.getList().get(i).set("track", track);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", issueList.getList());
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**获取某个OPL问题类别列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月22日 下午5:48:47
	 */
	public void getIssueProblemList() {
		long oplId=getParaToLong("opl_id");
		List<AndonProblemIssueProblem> problems=AndonProblemIssueProblem.dao.find("select * from andon_problem_issue_problem where status=0 and opl_id="+oplId);
		List<Record> records = new ArrayList<Record>();
		for(int i = 0; i< problems.size(); i++){
			Record record = problems.get(i).toRecord();
			Map<String, Object> line= new HashMap<String, Object>();
			line.put("id",problems.get(i).getLineId());
			line.put("production_name",problems.get(i).getLineName());
			record.set("line",line);
			records.add(record);
		}
		renderJson(Ret.ok("data",records));
	}
	/**编辑生产OPL
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月12日 上午9:08:53
	 */
	@SuppressWarnings({ "rawtypes"})
	public void editProductOPL() {
		Db.tx(() -> {
			try {
				SimpleDateFormat sdFormat=new SimpleDateFormat("yyyy-MM-dd");
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				JSONArray problemList=JSONArray.parseArray(get("problemAnalysisList"));
				AndonProductIssue productIssue=AndonProductIssue.dao.findById(Long.valueOf(map.get("id").toString()));
				User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+map.get("ding_user_id").toString()+"'");
				//对接andon系统---编辑opl
				Record receiveData=new Record();
				receiveData.set("andon_id", productIssue.getAndonId())
				.set("duty_user_name", map.get("duty_user_name").toString())
				.set("problemList", problemList);
				JSONObject object=JSONObject.parseObject(receiveData.toJson());
				String url=PropKit.get("andon_url")+"/openIssue/editProductOPL";
				try {
					JSONObject backData=JSONObject.parseObject(doPost(url,object));
					if ("ok".equals(backData.get("state"))) {
						//修改OPL数据
						productIssue
						.setPlanFinishDate(sdFormat.parse(map.get("plan_finish_date").toString()))
						.setDescription(map.get("description").toString())
						.setMeasures(map.get("measures").toString())
						.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname());
						productIssue.update();
						//修改问题类别列表数据
						Db.update("update andon_problem_issue_problem set status=1 where opl_id="+productIssue.getId());
						for (int i = 0; i < problemList.size(); i++) {
							JSONObject problem=problemList.getJSONObject(i);
							if (problem.getLongValue("id")==0&&problem.getIntValue("status")==0) {//新增
								AndonProblemIssueProblem issueProblem=new AndonProblemIssueProblem();
								issueProblem.setOplId(productIssue.getId())
								.setSeverity(problem.getString("severity")).setReasonType(problem.getString("reason_type"))
								.setProblemType(problem.getString("problem_type"))
								.setAffectNum(problem.getIntValue("affect_num"))
										.setLineId(problem.getJSONObject("line").getIntValue("id"))
										.setLineName(problem.getJSONObject("line").getString("production_name"));
								issueProblem.save();
							}else if(problem.getLongValue("id")!=0&&problem.getIntValue("status")==0){//编辑
								AndonProblemIssueProblem issueProblem=AndonProblemIssueProblem.dao.findById(problem.getLongValue("id"));
								issueProblem
								.setSeverity(problem.getString("severity")).setReasonType(problem.getString("reason_type"))
								.setProblemType(problem.getString("problem_type"))
								.setAffectNum(problem.getIntValue("affect_num"))
										.setLineId(problem.getJSONObject("line").getIntValue("id"))
										.setLineName(problem.getJSONObject("line").getString("production_name"))
								.setStatus(0);
								issueProblem.update();
							}
						}
						//修改历史
						Record loginUser=getSessionAttr("user");
						AndonProblemEditRecord editRecord=new AndonProblemEditRecord();
						editRecord.setContent(map.toString())
						.setOplId(productIssue.getId()).setCreateTime(new Date())
						.setCreateUserId(loginUser.getInt("id")).setCreateUserName(loginUser.get("nickname"));
						editRecord.save();
						
						renderJson(Ret.ok("msg", "编辑成功！"));
						return true;
					}else {
						renderJson(Ret.fail("msg", "andon系统对接失败，请联系管理员！"));
						return false;
					}
				} catch (Exception e1) {
					e1.printStackTrace();
					renderJson(Ret.fail("msg", "对接失败：" + e1.getMessage()));
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "编辑失败：" + e.getMessage()));
				return false;
			}
		});
	}
	/**OPL转移
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月23日 上午10:26:46
	 */
	@SuppressWarnings("rawtypes")
	public void changeOPL() {
		Db.tx(() -> {
			try {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				AndonProductIssue productIssue=AndonProductIssue.dao.findById(Long.valueOf(map.get("id").toString()));
				User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+map.get("ding_user_id").toString()+"'");
				//对接andon系统---编辑opl
				Record changeData=new Record();
				changeData.set("andon_id", productIssue.getAndonId())
				.set("duty_user_name", map.get("duty_user_name").toString());
				JSONObject object=JSONObject.parseObject(changeData.toJson());
				String url=PropKit.get("andon_url")+"/openIssue/changeOPLDutyUser";
				try {
					JSONObject backData=JSONObject.parseObject(doPost(url,object));
					if ("ok".equals(backData.get("state"))) {
						//修改OPL数据
						productIssue
						.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname());
						productIssue.update();
						//修改历史
						Record loginUser=getSessionAttr("user");
						AndonProblemEditRecord editRecord=new AndonProblemEditRecord();
						editRecord.setContent(map.toString())
						.setOplId(productIssue.getId()).setCreateTime(new Date())
						.setCreateUserId(loginUser.getInt("id")).setCreateUserName(loginUser.get("nickname"));
						editRecord.save();
						
						renderJson(Ret.ok("msg", "移交成功！"));
						return true;
					}else {
						renderJson(Ret.fail("msg", "andon系统对接失败，请联系管理员！"));
						return false;
					}
				} catch (Exception e1) {
					e1.printStackTrace();
					renderJson(Ret.fail("msg", "对接失败：" + e1.getMessage()));
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "操作失败：" + e.getMessage()));
				return false;
			}
		});
	}
	/**OPL跟踪
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月22日 下午9:22:11
	 */
	@SuppressWarnings("rawtypes")
	public void trackIssue() {
		Db.tx(() -> {
			try {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				String finish_status=map.get("finish_status").toString();
				AndonProductIssue productIssue=AndonProductIssue.dao.findById(Long.valueOf(map.get("id").toString()));
				//对接andon系统 修改状态
				Record dataRecord=new Record();
				dataRecord.set("finish_status", finish_status)
				.set("andon_id", productIssue.getAndonId());
				JSONObject object=JSONObject.parseObject(dataRecord.toJson());
				String url=PropKit.get("andon_url")+"/openIssue/trackIssue";
				try {
					JSONObject backData=JSONObject.parseObject(doPost(url,object));
					if ("ok".equals(backData.get("state"))) {
						productIssue.setFinishStatus(finish_status);
						productIssue.update();
						//跟踪内容
						Record loginUser=getSessionAttr("user");
						AndonProblemTrackRecord trackRecord=new AndonProblemTrackRecord();
						trackRecord.setOplId(productIssue.getId())
						.setTrackStatus(finish_status)
						.setTrackContent(map.get("track_content").toString())
						.setCreateTime(new Date())
						.setCreateUserId(loginUser.getInt("id")).setCreateUserName(loginUser.get("nickname"));
						trackRecord.save();
						renderJson(Ret.ok("msg", "操作成功！"));
						return true;
					}else {
						renderJson(Ret.fail("msg", "andon系统对接失败，请联系管理员！"));
						return false;
					}
				} catch (Exception e1) {
					e1.printStackTrace();
					renderJson(Ret.fail("msg", "对接失败：" + e1.getMessage()));
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "操作失败：" + e.getMessage()));
				return false;
			}
		});
	}
	/**完成生产OPL
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月12日 上午9:13:25
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void finishIssue() {
		Db.tx(() -> {
			try {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				String finish_status=map.get("finish_status").toString();
				AndonProductIssue productIssue=AndonProductIssue.dao.findById(Long.valueOf(map.get("id").toString()));
				//判断是否有解决措施
				if (productIssue.getMeasures()!=null&&!"".equals(productIssue.getMeasures())) {
					//对接andon系统
					Record finishRecord=new Record();
					finishRecord.set("measures", productIssue.getMeasures())
					.set("finish_status", finish_status)
					.set("real_finish_date", map.get("real_finish_date").toString())
					.set("andon_id", productIssue.getAndonId());
					JSONObject object=JSONObject.parseObject(finishRecord.toJson());
					String url=PropKit.get("andon_url")+"/openIssue/finishIssue";
					try {
						JSONObject backData=JSONObject.parseObject(doPost(url,object));
						if ("ok".equals(backData.get("state"))) {
							//修改opl状态
							productIssue._setAttrs(map);
							productIssue.setStatus(4);//4-完成
							productIssue.update();
							renderJson(Ret.ok("msg", "操作成功！"));
							return true;
						}else {
							renderJson(Ret.fail("msg", "andon系统对接失败，请联系管理员！"));
							return false;
						}
					} catch (Exception e1) {
						e1.printStackTrace();
						renderJson(Ret.fail("msg", "对接失败：" + e1.getMessage()));
						return false;
					}
				}else {
					renderJson(Ret.fail("msg", "此问题还未维护解决措施，请确认！"));
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "操作失败：" + e.getMessage()));
				return false;
			}
		});
	}
	/**拒绝接受OPL
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月11日 下午5:00:06
	 */
	@SuppressWarnings("rawtypes")
	public void RefuseReceiveOPL() {
		Db.tx(() -> {
			try {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				AndonProductIssue productIssue=AndonProductIssue.dao.findById(Long.valueOf(map.get("id").toString()));
				//对接andon系统
				Record refuseData=new Record();
				refuseData.set("andon_id", productIssue.getAndonId())
				.set("refuse_reason", map.get("refuse_reason").toString());
				JSONObject object=JSONObject.parseObject(refuseData.toJson());
				String url=PropKit.get("andon_url")+"/openIssue/refuseReceiveIssue";
				try {
					JSONObject backData=JSONObject.parseObject(doPost(url,object));
					if ("ok".equals(backData.get("state"))) {
						int backTime=productIssue.getBackTime().intValue()+1;
						productIssue.setBackTime(backTime)
						.setStatus(2);//2-驳回
						productIssue.update();
						//新增驳回记录
						Record loginUser=getSessionAttr("user");
						AndonIssueRefuseRecord refuse=new AndonIssueRefuseRecord();
						refuse.setOplId(Long.valueOf(map.get("id").toString())).setRefuseReason(map.get("refuse_reason").toString())
						.setCreateTime(new Date()).setCreateUserId(loginUser.getInt("id")).setCreateUserName(loginUser.get("nickname"));
						refuse.save();
						
						renderJson(Ret.ok("msg", "拒绝成功！"));
						return true;
					}else {
						renderJson(Ret.fail("msg", "andon系统对接失败，请联系管理员！"));
						return false;
					}
				} catch (Exception e1) {
					e1.printStackTrace();
					renderJson(Ret.fail("msg", "对接失败：" + e1.getMessage()));
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "拒绝失败：" + e.getMessage()));
				return false;
			}

		});
	}
	/**
	 * 接口对接
	 * @param url
	 * @param param
	 * @return
	 */
	@SuppressWarnings("resource")
	public static String doPost(String url, JSONObject param) {
        HttpPost httpPost = null;
        String result = null;
        try {
			HttpClient client = new DefaultHttpClient();
            httpPost = new HttpPost(url);
            if (param != null) {
                StringEntity se = new StringEntity(param.toString(), "utf-8");
                httpPost.setEntity(se); // post方法中，加入json数据
                httpPost.setHeader("Content-Type", "application/json");
            }
            HttpResponse response = client.execute(httpPost);
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {
                    result = EntityUtils.toString(resEntity, "utf-8");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
	}
	
	
	/**确认接收OPL
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月11日 下午4:46:35
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void SureReceiveOPL() {
		Db.tx(() -> {
			try {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				AndonProductIssue productIssue=AndonProductIssue.dao.findById(Long.valueOf(map.get("id").toString()));
				//对接andon系统---接受opl
				Record receiveData=new Record();
				receiveData.set("andon_id", productIssue.getAndonId())
				.set("problem_type", map.get("problem_type").toString())
				.set("reason_type", map.get("reason_type").toString());
				JSONObject object=JSONObject.parseObject(receiveData.toJson());
				String url=PropKit.get("andon_url")+"/openIssue/receiveProductOPL";
				try {
					JSONObject backData=JSONObject.parseObject(doPost(url,object));
					if ("ok".equals(backData.get("state"))) {
						productIssue.setStatus(1);
						productIssue._setAttrs(map);
						productIssue.update();
						renderJson(Ret.ok("msg", "确认接受成功！"));
						return true;
					}else {
						renderJson(Ret.fail("msg", "andon系统对接失败，请联系管理员！"));
						return false;
					}
				} catch (Exception e1) {
					e1.printStackTrace();
					renderJson(Ret.fail("msg", "对接失败：" + e1.getMessage()));
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "接受失败：" + e.getMessage()));
				return false;
			}
		});
	}
	public void getAbnormalReport() {
		Record sendRecord = new Record();
		sendRecord.set("currentPage",get("currentPage"))
				.set("pageSize",get("pageSize"))
				.set("selWareHouse",get("selWareHouse"))
				.set("reasonType",get("reasonType"))
				.set("bindProductCode",get("bindProductCode"))
				.set("planTimeDate",get("planTimeDate"));
		JSONObject object=JSONObject.parseObject(sendRecord.toJson());
		String url= PropKit.get("andon_url")+"/abnormalReport/getAbnormalReport";
		JSONObject backData = JSONObject.parseObject(doPost(url, object));
		if (0==(Integer) backData.get("code")) {
			Record req = new Record();
			req.set("code", 0);
			req.set("msg", "获取成功");
			req.set("totalResult", backData.get("totalResult"));
			req.set("list", backData.get("list"));
			renderJson(req) ;
		}else {
			renderJson(Ret.fail("msg","获取异常填报数据出错"));
		}
	}

}
