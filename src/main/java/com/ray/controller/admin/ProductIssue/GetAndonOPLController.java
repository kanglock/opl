package com.ray.controller.admin.ProductIssue;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.AndonProductIssue;
import com.ray.common.model.User;
/**
 * 获取andon问题数据 getAndonOPL
 * @author FL00024996
 * @data 2021年11月11日
 */
public class GetAndonOPLController extends Controller {
	/**获取andon问题
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月11日 下午2:45:22
	 */
	public void getAndonData() {
		Db.tx(() -> {
			try {
				String jsonString = HttpKit.readData(getRequest());
				JSONObject jsonObject = JSONObject.parseObject(jsonString);
				long opl_id=jsonObject.getLongValue("opl_id");
				long andon_id=jsonObject.getLongValue("andon_id");
				String product_code=jsonObject.getString("product_code");
				String product_name=jsonObject.getString("product_name");
				String line_name=jsonObject.getString("line_name");
				String duty_user_id=jsonObject.getString("duty_user_id");
				String create_user_id=jsonObject.getString("create_user_id");
				Date product_date=jsonObject.getDate("product_date");
				int affect_num=jsonObject.getIntValue("affect_num");
				double affect_hours=jsonObject.getDoubleValue("affect_hours");
				long dep_id=jsonObject.getLongValue("dep_id");
				int ware_id=jsonObject.getIntValue("ware_id");
				User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+duty_user_id+"'");
				User createUser=User.dao.findFirst("select * from user where ding_user_id='"+create_user_id+"'");
				
				AndonProductIssue productIssue=AndonProductIssue.dao.findById(opl_id);
				if (productIssue==null) {
					//新增OPL
					productIssue=new AndonProductIssue();
					productIssue.setStatus(1)//默认已接受---1
					.setAndonId(andon_id).setDepId(dep_id).setWareId(ware_id)
					.setProductCode(product_code).setProductName(product_name)
					.setLineName(line_name)
					.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
					.setCreateTime(new Date()).setUpdateTime(new Date())
					.setCreateUserId(createUser.getId().intValue()).setCreateUserName(createUser.getNickname())
					.setProductDate(product_date)
					.setAffectNum(affect_num).setAffectHours(affect_hours);
					productIssue.save();
				}else {
					//编辑
					productIssue.setStatus(1)//默认已接受---1
					.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
					.setUpdateTime(new Date());
					productIssue.update();
				}
				DingMessage.sendText(dutyUser.getDingUserId(), "有新的生产OPL指派给你，产品号："+product_code+"，产品名称："+product_name+"请注意查收！\n"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "43");
				renderJson(Ret.ok("data", productIssue.getId()));
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
				return false;
			}
		});
	}
	/**被动取消OPL
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年11月11日 下午3:48:29
	 */
	public void getCancleOPL() {
		Db.tx(() -> {
			try {
				String jsonString = HttpKit.readData(getRequest());
				JSONObject jsonObject = JSONObject.parseObject(jsonString);
				long opl_id=jsonObject.getLongValue("opl_id");
				AndonProductIssue productIssue=AndonProductIssue.dao.findById(opl_id);
				productIssue.setStatus(6);//取消
				productIssue.update();
				renderJson(Ret.ok("msg", "成功！"));
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
				return false;
			}

		});
	}
}
