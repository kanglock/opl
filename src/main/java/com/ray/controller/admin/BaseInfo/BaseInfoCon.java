package com.ray.controller.admin.BaseInfo;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import org.apache.commons.collections.MapUtils;

import java.util.List;
import java.util.Map;

import static com.ray.controller.admin.ProductIssue.MyDutyProductIssueController.doPost;

/**
 * 基础信息baseInfo
 * @author FL00024996
 *
 */
public class BaseInfoCon extends Controller {

	/**
	 * 从andon获取车间下的产线列表
	 **/
	public void getProduceLineList() {
		try {
			int currentPage = getParaToInt("currentPage");
			int pageSize = getParaToInt("pageSize");
			int warehouseId = getParaToInt("warehouseId");
			//对接andon系统
			Record receiveData=new Record();
			receiveData.set("currentPage",currentPage).set("pageSize",pageSize).set("warehouseId",warehouseId);
			JSONObject object=JSONObject.parseObject(receiveData.toJson());
			String url= PropKit.get("andon_url")+"/baseInfo/getProduceLineList";
			JSONObject backData = JSONObject.parseObject(doPost(url, object));
			if ("ok".equals(backData.get("state"))) {
				Map map = FastJson.getJson().parse(backData.get("data").toString(), Map.class);
				renderJson(Ret.ok("data",map));
			}
		} catch (Exception e) {
			renderJson(Ret.fail("msg", "获取失败！"));
		}
	}

	/**
	 * 从andon根据车间id获取车间
	 **/
	public static String getWareNameByWareId(Integer wareId){
		Record receiveData=new Record();
		receiveData.set("ware_id",wareId);
		JSONObject object=JSONObject.parseObject(receiveData.toJson());
		String url= PropKit.get("andon_url")+"/baseInfo/getWareNameByWareId";
		JSONObject backData = JSONObject.parseObject(doPost(url, object));
		if ("ok".equals(backData.get("state"))) {
			String wareName = backData.get("data").toString();
			return wareName;
		}else {
			return "";
		}
	}
}