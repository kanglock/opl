package com.ray.controller.admin.NewCusProOPL;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.HProCusIssue;
import com.ray.common.model.HProCusIssue8dPlan;
import com.ray.common.model.HProCusIssue8dReal;
import com.ray.common.model.HProCusIssueMonthTrack;
import com.ray.common.model.HProCusIssueReasonAnalysis;
import com.ray.common.model.HProCusIssueTrackRecord;
import com.ray.common.model.HProCusMeasures;
import com.ray.common.model.HProCusTrackHistory;
import com.ray.common.model.User;
/**
 * 我负责的新品客诉opl myDutyNewCusOPL/toMyDutyNewCusOPL
 * @author FL00024996
 *
 */
public class MyDutyNewCusController extends Controller {
	public void toMyDutyNewCusOPL() {
		render("newCusOPLMyDuty.html");
	}
	public void getMyDutyCusProOPL() {
		Record loginUser=getSessionAttr("user");
		User user=User.dao.findById(loginUser.getInt("id"));
		StringBuilder sb=new StringBuilder();
		sb.append(" from h_pro_cus_issue a,h_cause_dep b,h_quality_issue_type e where a.is_del=0 and a.type_id=e.id  and a.dep_id=b.dep_id ");
		sb.append(" and a.duty_user_id="+user.getId());//我负责的
		String selOPLdes=get("selOPLdes");
		if (!"".equals(selOPLdes)&& selOPLdes!=null) {
			sb.append(" and a.description like '%"+selOPLdes+"%'");
		}
		String selProCode=get("selProCode");
		if (!"".equals(selProCode)&& selProCode!=null) {
			sb.append(" and a.pro_code like '%"+selProCode+"%'");
		}
		String selProductName=get("selProductName");
		if (!"".equals(selProductName)&& selProductName!=null) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		String selCusName=get("selCusName");
		if (!"".equals(selCusName)&& selCusName!=null) {
			sb.append(" and a.cus_name like '%"+selCusName+"%'");
		}
		String selStatus=get("selStatus");
		if (!"".equals(selStatus)&& selStatus!=null) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name,e.type ",sb.toString()+" order by id desc");
		List<Record> cusOPLList=new ArrayList<Record>();
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			issueList.getList().get(i).set("caozuo", " ");
			//遏制措施 contain_s
			String contain_measures="";
			List<HProCusMeasures> containMeasures=HProCusMeasures.dao.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=2 and is_del=0");
			if (!containMeasures.isEmpty()) {
				for (int j = 0; j < containMeasures.size(); j++) {
					//遏制措施
					contain_measures=contain_measures+"遏制措施"+(j+1)+":"+containMeasures.get(j).getMeasures()+"-"+containMeasures.get(j).getUserName();
					int measureStatus=containMeasures.get(j).getStatus();
					if (measureStatus==1) {
						contain_measures=contain_measures+"-进行中-"+containMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						contain_measures=contain_measures+"-已完成-"+containMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						contain_measures=contain_measures+"-已关闭-"+containMeasures.get(j).getCloseDate();
					}
					contain_measures=contain_measures+"<br>";
					//遏制结果
					if(containMeasures.get(j).getContainResult()==null) {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":暂无"+"<br>";
					}else {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":"+containMeasures.get(j).getContainResult()+"<br>";
					}
				}
			}else {
				contain_measures="暂无遏制措施"+"<br>";
			}
			//临时措施
			String temporary_measures="";
			List<HProCusMeasures> temporaryMeasures=HProCusMeasures.dao.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=3 and is_del=0");
			if (!temporaryMeasures.isEmpty()) {
				for (int j = 0; j < temporaryMeasures.size(); j++) {
					//临时措施
					temporary_measures=temporary_measures+"临时措施"+(j+1)+":"+temporaryMeasures.get(j).getMeasures()+"-"+temporaryMeasures.get(j).getUserName();
					int measureStatus=temporaryMeasures.get(j).getStatus();
					if (measureStatus==1) {
						temporary_measures=temporary_measures+"-进行中-"+temporaryMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						temporary_measures=temporary_measures+"-已完成-"+temporaryMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						temporary_measures=temporary_measures+"-已关闭-"+temporaryMeasures.get(j).getCloseDate();
					}
					temporary_measures=temporary_measures+"<br>";
					//有效性跟踪
					List<HProCusTrackHistory> measureTrackRecord=HProCusTrackHistory.dao.find("select * from h_pro_cus_track_history where measure_id="+temporaryMeasures.get(j).getId());
					if (measureTrackRecord.isEmpty()) {
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":暂无"+"<br>";
					}else {
						String measureTracks="";
						for (int k = 0; k < measureTrackRecord.size(); k++) {
							measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；;";
						}
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"<br>";
					}
				}
			}else {
				temporary_measures="暂无临时措施";
			}
			String contain_s=contain_measures+temporary_measures;
			issueList.getList().get(i).set("contain_s", contain_s);
			//根本原因分析 root_s
			String happen_reason="";//产生原因
			String avoid_happen_measures="";//防产生措施
			List<HProCusIssueReasonAnalysis> happenAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
			if (!happenAnalysis.isEmpty()) {
				happen_reason="产生原因："+"<br>";
				avoid_happen_measures="防产生措施："+"<br>";
				for (int j = 0; j < happenAnalysis.size(); j++) {
					happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HProCusMeasures> avoidHappen=HProCusMeasures.dao.find("select * from h_pro_cus_measures where reason_id="+happenAnalysis.get(j).getId()+" and is_del=0 and type=0");//对应防产生措施
					for (int k = 0; k < avoidHappen.size(); k++) {
						avoid_happen_measures=avoid_happen_measures+(j+1)+"."+(k+1)+avoidHappen.get(k).getMeasures()+"-"+avoidHappen.get(k).getUserName();
						int measureStatus=avoidHappen.get(k).getStatus();
						if (measureStatus==1) {
							avoid_happen_measures=avoid_happen_measures+"-进行中"+avoidHappen.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_happen_measures=avoid_happen_measures+"-已完成"+avoidHappen.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_happen_measures=avoid_happen_measures+"-已关闭"+avoidHappen.get(k).getCloseDate();
						}
						avoid_happen_measures=avoid_happen_measures+"<br>";
					}
				}
			}else {
				happen_reason="暂无产生原因"+"<br>";
				avoid_happen_measures="暂无防产生措施"+"<br>";
			}
			String runout_reason="";//流出原因
			String avoid_runout_measures="";//防流出措施
			List<HProCusIssueReasonAnalysis> runOutAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
			if (!runOutAnalysis.isEmpty()) {
				runout_reason="流出原因："+"<br>";
				avoid_runout_measures="防流出措施："+"<br>";
				for (int j = 0; j < runOutAnalysis.size(); j++) {
					runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HProCusMeasures> avoidRunout=HProCusMeasures.dao.find("select * from h_pro_cus_measures where reason_id="+runOutAnalysis.get(j).getId()+" and is_del=0 and type=1");//对应防流出措施
					for (int k = 0; k < avoidRunout.size(); k++) {
						avoid_runout_measures=avoid_runout_measures+(j+1)+"."+(k+1)+avoidRunout.get(k).getMeasures()+"-"+avoidRunout.get(k).getUserName();
						int measureStatus=avoidRunout.get(k).getStatus();
						if (measureStatus==1) {
							avoid_runout_measures=avoid_runout_measures+"-进行中"+avoidRunout.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_runout_measures=avoid_runout_measures+"-已完成"+avoidRunout.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_runout_measures=avoid_runout_measures+"-已关闭"+avoidRunout.get(k).getCloseDate();
						}
						avoid_runout_measures=avoid_runout_measures+"<br>";
					}
				}
			}else {
				runout_reason="暂无流出原因";
				avoid_runout_measures="暂无防流出措施";
			}
			String root_s=happen_reason+runout_reason;
			String avoid_s=avoid_happen_measures+avoid_runout_measures;
			issueList.getList().get(i).set("root_s", root_s).set("avoid_s", avoid_s);
			//永久措施跟踪3月
			List<HProCusIssueMonthTrack> monthTracks=HProCusIssueMonthTrack.dao.find("select * from h_pro_cus_issue_month_track where issue_id="+issueId);
			issueList.getList().get(i).set("month_1", "编辑").set("month_1_remark", "").set("month_1_date", "");
			issueList.getList().get(i).set("month_2", "编辑").set("month_2_remark", "").set("month_2_date", "");
			issueList.getList().get(i).set("month_3", "编辑").set("month_3_remark", "").set("month_3_date", "");
			String monthTrackString="";
			if (!monthTracks.isEmpty()) {
				for (int j = 0; j < monthTracks.size(); j++) {
					int month=monthTracks.get(j).getMonth();
					if (month==1) {
						monthTrackString=monthTrackString+"第一月："+monthTracks.get(j).getRemark()+"<br>";
						issueList.getList().get(i).set("month_1", monthTracks.get(j).getStatus()).set("month_1_remark",  monthTracks.get(j).getRemark()).set("month_1_date",  monthTracks.get(j).getTrackDate());
					}else if (month==2) {
						monthTrackString=monthTrackString+"第二月："+monthTracks.get(j).getRemark()+"<br>";
						issueList.getList().get(i).set("month_2", monthTracks.get(j).getStatus()).set("month_2_remark", monthTracks.get(j).getRemark()).set("month_2_date",  monthTracks.get(j).getTrackDate());
					}else if (month==3) {
						monthTrackString=monthTrackString+"第三月："+monthTracks.get(j).getRemark()+"<br>";
						issueList.getList().get(i).set("month_3", monthTracks.get(j).getStatus()).set("month_3_remark", monthTracks.get(j).getRemark()).set("month_3_date",  monthTracks.get(j).getTrackDate());
					}
				}
			}
			//OPL整体跟踪记录
			List<HProCusIssueTrackRecord> trackRecords=HProCusIssueTrackRecord.dao.find("select * from h_pro_cus_issue_track_record where issue_id="+issueId);
			String  oplTrackRecord="";
			for (int j = 0; j < trackRecords.size(); j++) {
				oplTrackRecord=oplTrackRecord+(j+1)+"."+trackRecords.get(j).getTrackRecord()+"-"+trackRecords.get(j).getCreateUserName()+"-"+sdf1.format(trackRecords.get(j).getCreateTime())+"<br>";
			}
			String record=" ";
			if (!"".equals(oplTrackRecord)) {
				record=oplTrackRecord;
			}
			if (!"".equals(monthTrackString)) {
				record=record+monthTrackString;
			}
			issueList.getList().get(i).set("record", record);
			//8D计划
			Record issuePlan=new Record();
			issuePlan.setColumns(issueList.getList().get(i));
			Record issueReal=new Record();
			issueReal.setColumns(issueList.getList().get(i));
			HProCusIssue8dPlan plans=HProCusIssue8dPlan.dao.findFirst("select * from h_pro_cus_issue_8d_plan where issue_id="+issuePlan.getLong("id"));
			issuePlan.set("plan_real", "计划")
			.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
			.set("d_three", plans.getDThree()).set("d_four", plans.getDFour())
			.set("d_five", plans.getDFive()).set("d_six", plans.getDSix())
			.set("d_seven", plans.getDSeven());
			//8D实际
			HProCusIssue8dReal reals=HProCusIssue8dReal.dao.findFirst("select * from h_pro_cus_issue_8d_real where issue_id="+issueReal.getLong("id"));
			issueReal.set("plan_real", "实际")
			.set("d_one", "点击编辑").set("d_two", "点击编辑")
			.set("d_three", "点击编辑").set("d_four", "点击编辑")
			.set("d_five", "点击编辑").set("d_six", "点击编辑")
			.set("d_seven", "点击编辑")
			.set("d_one_status", 0).set("d_two_status",0)
			.set("d_three_status", 0).set("d_four_status", 0)
			.set("d_five_status", 0).set("d_six_status", 0)
			.set("d_seven_status", 0);
			if (reals!=null) {
				issueReal
				.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
				.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
				.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
				.set("d_seven_status", reals.getDSevenStatus());
				if (reals.getDOneStatus()==2) {
					issueReal.set("d_one", reals.getDOne());
				}
				if (reals.getDTwoStatus()==2) {
					issueReal.set("d_two", reals.getDTwo());
				}
				if (reals.getDThreeStatus()==2) {
					issueReal.set("d_three", reals.getDThree());
				}
				if (reals.getDFourStatus()==2) {
					issueReal.set("d_four", reals.getDFour());
				}
				if (reals.getDFiveStatus()==2) {
					issueReal.set("d_five", reals.getDFive());
				}
				if (reals.getDSixStatus()==2) {
					issueReal.set("d_six", reals.getDSix());
				}
				if (reals.getDSevenStatus()==2) {
					issueReal.set("d_seven", reals.getDSeven());
				}
			}
			cusOPLList.add(issuePlan);
			cusOPLList.add(issueReal);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", cusOPLList);
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**
	 * 7D编辑
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月20日 下午5:11:42
	 */
	@SuppressWarnings("rawtypes")
	public void save7D() {
		try {
			Record record=new Record();
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				String dname=map.get("dname").toString();
				int dstatus=Integer.valueOf(map.get("status").toString());
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				Date finishDate=null;
				String finish_date=map.get("finish_date").toString();
				if (!"".equals(finish_date)) {
					try {
						finishDate = sdf.parse(map.get("finish_date").toString());
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				HProCusIssue8dReal dreal=HProCusIssue8dReal.dao.findFirst("select * from h_pro_cus_issue_8d_real where issue_id="+issueId);
				HProCusIssue8dPlan dplan=HProCusIssue8dPlan.dao.findFirst("select * from h_pro_cus_issue_8d_plan where issue_id="+issueId);
				HProCusIssue cusIssue=HProCusIssue.dao.findById(issueId);
				if (dreal==null) {//还没有实际7D，则只能编辑D1
					if ("d_one".equals(dname)) {
						dreal=new HProCusIssue8dReal();
						dreal.setDOne(finishDate).setDOneStatus(dstatus).setIssueId(issueId);
						dreal.save();
						//下一个提醒日期
						if (dstatus==2) {
							cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDTwo(),3));
							cusIssue.update();
						}
						return true;
					}else {//编辑非done则报错
						record.set("code", 1).set("msg", "D1还未完成，无法进行其他操作！");
						return false;
					}
				}else {//有7D，则看前一项是否为空
					if ("d_one".equals(dname)) {
						dreal.setDOne(finishDate).setDOneStatus(dstatus);
						dreal.update();
						//下一个提醒日期
						if (dstatus==2) {
							cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDTwo(),3));
							cusIssue.update();
						}else {
							cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDOne(),3));
							cusIssue.update();
						}
						return true;
					}else if ("d_two".equals(dname)) {//看d1是否为空
						int dlastStatus=dreal.getDOneStatus();
						if (dlastStatus!=2) {
							record.set("code", 1).set("msg", "D1还未完成，无法进行其他操作！");
							return false;
						}else {
							dreal.setDTwo(finishDate).setDTwoStatus(dstatus);
							dreal.update();
							//下一个提醒日期
							if (dstatus==2) {
								cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDThree(),3));
								cusIssue.update();
							}else {
								cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDTwo(),3));
								cusIssue.update();
							}
							return true;
						}
					}else if ("d_three".equals(dname)) {//看d2是否为空
						int dlastStatus=dreal.getDTwoStatus();
						if (dlastStatus!=2) {
							record.set("code", 1).set("msg", "D2还未完成，无法进行其他操作！");
							return false;
						}else {
							dreal.setDThree(finishDate).setDThreeStatus(dstatus);
							dreal.update();
							//下一个提醒日期
							if (dstatus==2) {
								cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDFour(),3));
								cusIssue.update();
							}else {
								cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDThree(),3));
								cusIssue.update();
							}
							return true;
						}
					}else if ("d_four".equals(dname)) {//看d3是否为空
						int dlastStatus=dreal.getDThreeStatus();
						if (dlastStatus!=2) {
							record.set("code", 1).set("msg", "D3还未完成，无法进行其他操作！");
							return false;
						}else {
							dreal.setDFour(finishDate).setDFourStatus(dstatus);
							dreal.update();
							//下一个提醒日期
							if (dstatus==2) {
								cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDFive(),3));
								cusIssue.update();
							}else {
								cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDFour(),3));
								cusIssue.update();
							}
							return true;
						}
					}else if ("d_five".equals(dname)) {//看d4是否为空
						int dlastStatus=dreal.getDFourStatus();
						if (dlastStatus!=2) {
							record.set("code", 1).set("msg", "D4还未完成，无法进行其他操作！");
							return false;
						}else {
							dreal.setDFive(finishDate).setDFiveStatus(dstatus);
							dreal.update();
							//下一个提醒日期
							if (dstatus==2) {
								if (dplan.getDSix()!=null) {
									cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDSix(),3));
									cusIssue.update();
								}else {
									cusIssue.setDNoteDate(null);
									cusIssue.update();
								}
							}else {
								cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDFive(),3));
								cusIssue.update();
							}
							return true;
						}
					}else if ("d_six".equals(dname)) {//看d5是否为空
						int dlastStatus=dreal.getDFiveStatus();
						if (dlastStatus!=2) {
							record.set("code", 1).set("msg", "D5还未完成，无法进行其他操作！");
							return false;
						}else if (dplan.getDSix()==null) {
							record.set("code", 1).set("msg", "D6计划时间为空，请先维护D6计划时间点！");
							return false;
						}else {
							dreal.setDSix(finishDate).setDSixStatus(dstatus);
							dreal.update();
							//下一个提醒日期
							if (dstatus==2) {
								if (dplan.getDSeven()!=null) {
									cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDSeven(),3));
									cusIssue.update();
								}else {
									cusIssue.setDNoteDate(null);
									cusIssue.update();
								}
							}else {
								cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDSix(),3));
								cusIssue.update();
							}
							return true;
						}
					}else if ("d_seven".equals(dname)) {//看d6是否为空
						int dlastStatus=dreal.getDSixStatus();
						if (dlastStatus!=2) {
							record.set("code", 1).set("msg", "D6还未完成，无法进行其他操作！");
							return false;
						}else if (dplan.getDSeven()==null) {
							record.set("code", 1).set("msg", "D7计划时间为空，请先维护D7计划时间点！");
							return false;
						}else {
							dreal.setDSeven(finishDate).setDSevenStatus(dstatus);
							dreal.update();
							if (dstatus==2) {
								cusIssue.setDNoteDate(null)
								.setPermanentMeasuresTime(finishDate)//永久措施节点
								.setFirstMonth(com.ray.util.HyCommenMethods.addDate(finishDate, 30))
								.setSecondMonth(com.ray.util.HyCommenMethods.addDate(finishDate, 60))
								.setThirdMonth(com.ray.util.HyCommenMethods.addDate(finishDate, 90));
								cusIssue.update();
							}else {
								cusIssue.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dplan.getDSeven(),3));
								cusIssue.update();
							}
							return true;
						}
					}else {
						return true;
					}
				}
			});
			if (flag) {
				renderJson(Ret.ok("msg", "提交成功！"));
			}else {
				String msg=record.getStr("msg");
				renderJson(Ret.fail("msg", "提交失败，"+ msg));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 6D7D编辑
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午7:41:04
	 */
	@SuppressWarnings("rawtypes")
	public void add8DData() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				HProCusIssue8dPlan dPlan=HProCusIssue8dPlan.dao.findFirst("select * from h_pro_cus_issue_8d_plan where issue_id="+issueId);
				int dNum=Integer.valueOf(map.get("dNum").toString());
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date dDate=sdf.parse(map.get("ddate").toString());
					if (dNum==6) {
						dPlan.setDSix(dDate);
					}else if (dNum==7) {
						dPlan.setDSeven(dDate);
					}
					dPlan.update();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "编辑成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 新增编辑永久措施
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午5:33:31
	 */
	public void addEditAvoidMeasure() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("issueId");
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				JSONArray happenMeasureList=JSONArray.parseArray(get("happenMeasureList"));
				JSONArray runoutMeasureList=JSONArray.parseArray(get("runoutMeasureList"));
				Db.update("update h_pro_cus_measures set is_del=1 where issue_id="+issueId+" and (type=0 or type=1)");
				for (int i = 0; i < happenMeasureList.size(); i++) {//防产生
					JSONObject happenObject=happenMeasureList.getJSONObject(i);
					String measureUserId=happenObject.getString("user_id");
					User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+measureUserId+"'");
					long measureId=happenObject.getLongValue("id");
					if (measureId==0) {
						HProCusMeasures measure=new HProCusMeasures();
						measure.setIssueId(issueId)
						.setReasonId(happenObject.getLongValue("reason_id"))
						.setMeasures(happenObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(happenObject.getDate("plan_finish_date"))
						.setStatus(1).setIsDel(0)//措施创建之后默认状态为1---进行中
						.setType(0);//0-防产生
						measure.save();
					}else {
						HProCusMeasures measure=HProCusMeasures.dao.findById(measureId);
						measure
						.setReasonId(happenObject.getLongValue("reason_id"))
						.setMeasures(happenObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(happenObject.getDate("plan_finish_date"))
						.setIsDel(0);
						measure.update();
					}
					DingMessage.sendText(measureUserId, "有新的新品客诉OPL措施需要你执行，请知悉！"+sdf.format(new Date()), "22");//22-新品客诉OPL措施执行
				}
				for (int i = 0; i < runoutMeasureList.size(); i++) {//临时措施
					JSONObject runoutObject=runoutMeasureList.getJSONObject(i);
					String measureUserId=runoutObject.getString("user_id");
					User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+measureUserId+"'");
					long measureId=runoutObject.getLongValue("id");
					if (measureId==0) {
						HProCusMeasures measure=new HProCusMeasures();
						measure.setIssueId(issueId)
						.setReasonId(runoutObject.getLongValue("reason_id"))
						.setMeasures(runoutObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(runoutObject.getDate("plan_finish_date"))
						.setStatus(1).setIsDel(0)//措施创建之后默认状态为1---进行中
						.setType(1);//1-防产生
						measure.save();
					}else {
						HProCusMeasures measure=HProCusMeasures.dao.findById(measureId);
						measure
						.setReasonId(runoutObject.getLongValue("reason_id"))
						.setMeasures(runoutObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(runoutObject.getDate("plan_finish_date"))
						.setIsDel(0);
						measure.update();
					}
					DingMessage.sendText(measureUserId, "有新的新品客诉OPL措施需要你执行，请知悉！"+sdf.format(new Date()), "22");//22-新品客诉OPL措施执行
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "新增或编辑 永久措施成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 新增编辑 产生、流出原因
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午4:22:37
	 */
	public void addEditRootReason() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("issueId");
				JSONArray happenReasonList=JSONArray.parseArray(get("happenReasonList"));
				JSONArray runoutReasonList=JSONArray.parseArray(get("runoutReasonList"));
				Db.update("update h_pro_cus_issue_reason_analysis set status=1 where issue_id="+issueId);
				for (int i = 0; i < happenReasonList.size(); i++) {//产生
					JSONObject happenObject=happenReasonList.getJSONObject(i);
					long reasonId=happenObject.getLongValue("id");
					if (reasonId==0) {
						HProCusIssueReasonAnalysis reason=new HProCusIssueReasonAnalysis();
						reason.setIssueId(issueId)
						.setReasonAnalysis(happenObject.getString("reason_analysis"))
						.setStatus(0)
						.setType(0);//0-产生
						reason.save();
					}else {
						HProCusIssueReasonAnalysis reason=HProCusIssueReasonAnalysis.dao.findById(reasonId);
						reason
						.setReasonAnalysis(happenObject.getString("reason_analysis"))
						.setStatus(0);
						reason.update();
					}
				}
				for (int i = 0; i < runoutReasonList.size(); i++) {//流出
					JSONObject happenObject=runoutReasonList.getJSONObject(i);
					long reasonId=happenObject.getLongValue("id");
					if (reasonId==0) {
						HProCusIssueReasonAnalysis reason=new HProCusIssueReasonAnalysis();
						reason.setIssueId(issueId)
						.setReasonAnalysis(happenObject.getString("reason_analysis"))
						.setStatus(0)
						.setType(1);//1-流出
						reason.save();
					}else {
						HProCusIssueReasonAnalysis reason=HProCusIssueReasonAnalysis.dao.findById(reasonId);
						reason
						.setReasonAnalysis(happenObject.getString("reason_analysis"))
						.setStatus(0);
						reason.update();
					}
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "新增或编辑 根本原因分析成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 新增编辑遏制、临时措施
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午3:20:31
	 */
	public void addEditContainMeasure() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("issueId");
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				JSONArray containMeasureList=JSONArray.parseArray(get("containMeasureList"));
				JSONArray temporaryMeasureList=JSONArray.parseArray(get("temporaryMeasureList"));
				Db.update("update h_pro_cus_measures set is_del=1 where issue_id="+issueId+" and (type=3 or type=2)");
				for (int i = 0; i < containMeasureList.size(); i++) {//遏制措施
					JSONObject containObject=containMeasureList.getJSONObject(i);
					String measureUserId=containObject.getString("user_id");
					User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+measureUserId+"'");
					long measureId=containObject.getLongValue("id");
					if (measureId==0) {
						HProCusMeasures measure=new HProCusMeasures();
						measure.setIssueId(issueId)
						.setMeasures(containObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(containObject.getDate("plan_finish_date"))
						.setStatus(1).setIsDel(0)//措施创建之后默认状态为1---进行中
						.setType(2);//2-遏制措施
						measure.save();
					}else {
						HProCusMeasures measure=HProCusMeasures.dao.findById(measureId);
						measure
						.setMeasures(containObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(containObject.getDate("plan_finish_date"))
						.setIsDel(0);
						measure.update();
					}
					DingMessage.sendText(measureUserId, "有新的新品客诉OPL措施需要你执行，请知悉！"+sdf.format(new Date()), "22");//22-新品客诉OPL措施执行
				}
				for (int i = 0; i < temporaryMeasureList.size(); i++) {//临时措施
					JSONObject containObject=temporaryMeasureList.getJSONObject(i);
					String measureUserId=containObject.getString("user_id");
					User measureUser=User.dao.findFirst("select * from user where ding_user_id='"+measureUserId+"'");
					long measureId=containObject.getLongValue("id");
					if (measureId==0) {
						HProCusMeasures measure=new HProCusMeasures();
						measure.setIssueId(issueId)
						.setMeasures(containObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(containObject.getDate("plan_finish_date"))
						.setStatus(1).setIsDel(0)//措施创建之后默认状态为1---进行中
						.setType(3);//2-临时措施
						measure.save();
					}else {
						HProCusMeasures measure=HProCusMeasures.dao.findById(measureId);
						measure
						.setMeasures(containObject.getString("measures"))
						.setUserId(measureUser.getId().intValue()).setUserName(measureUser.getNickname())
						.setPlanFinishDate(containObject.getDate("plan_finish_date"))
						.setIsDel(0);
						measure.update();
					}
					DingMessage.sendText(measureUserId, "有新的新品客诉OPL措施需要你执行，请知悉！"+sdf.format(new Date()), "22");//22-新品客诉OPL措施执行
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "新增或编辑 遏制、临时措施成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取原因详情
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午5:18:47
	 */
	public void getReasonInfo() {
		try {
			long reasonId=getParaToLong("reasonId");
			HProCusIssueReasonAnalysis happenAnalysis=HProCusIssueReasonAnalysis.dao.findById(reasonId);
			renderJson(Ret.ok("data", happenAnalysis));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取OPL遏制、临时措施
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午2:59:46
	 */
	public void getCusIssueContainMeasures() {
		try {
			long issueId=getParaToLong("id");
			List<Record> containMeasures=Db.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=2 and is_del=0");
			for (int i = 0; i < containMeasures.size(); i++) {
				User measureUser=User.dao.findFirst("select * from user where id="+containMeasures.get(i).getInt("user_id"));
				containMeasures.get(i).set("user_id", measureUser.getDingUserId());
			}
			List<Record> temporaryMeasures=Db.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=3 and is_del=0");
			for (int i = 0; i < temporaryMeasures.size(); i++) {
				User measureUser=User.dao.findFirst("select * from user where id="+temporaryMeasures.get(i).getInt("user_id"));
				temporaryMeasures.get(i).set("user_id", measureUser.getDingUserId());
			}
			Record record=new Record();
			record.set("contain", containMeasures).set("temporary", temporaryMeasures);
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取OPL根本原因分析
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午4:20:40
	 */
	public void getCusIssueRootReason() {
		try {
			long issueId=getParaToLong("id");
			List<HProCusIssueReasonAnalysis> happenAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
			List<HProCusIssueReasonAnalysis> runOutAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
			Record record=new Record();
			record.set("happen", happenAnalysis).set("runout", runOutAnalysis);
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取OPL永久措施列表
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月19日 下午4:20:40
	 */
	public void getCusIssueAvoidMeasure() {
		try {
			long issueId=getParaToLong("id");
			List<Record> avoidHappen=Db.find("select a.*,b.reason_analysis from h_pro_cus_measures a,h_pro_cus_issue_reason_analysis b where a.issue_id="+issueId+" and a.is_del=0 and a.type=0 and b.id=a.reason_id");//防产生措施
			for (int i = 0; i < avoidHappen.size(); i++) {
				User measureUser=User.dao.findFirst("select * from user where id="+avoidHappen.get(i).getInt("user_id"));
				avoidHappen.get(i).set("user_id", measureUser.getDingUserId());
			}
			List<Record> avoidRunout=Db.find("select a.*,b.reason_analysis from h_pro_cus_measures a,h_pro_cus_issue_reason_analysis b where a.issue_id="+issueId+" and a.is_del=0 and a.type=1 and b.id=a.reason_id");//防流出措施
			for (int i = 0; i < avoidRunout.size(); i++) {
				User measureUser=User.dao.findFirst("select * from user where id="+avoidRunout.get(i).getInt("user_id"));
				avoidRunout.get(i).set("user_id", measureUser.getDingUserId());
			}
			Record record=new Record();
			record.set("happen", avoidHappen).set("runout", avoidRunout);
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 永久措施跟踪3个月
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月20日 下午5:18:30
	 */
	@SuppressWarnings("rawtypes")
	public void saveMonthTrack() {
		try {
			Record record=new Record();
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				Record loginUser=getSessionAttr("user");
				User user=User.dao.findById(loginUser.getInt("id"));
				String month_num=map.get("month_num").toString();
				String monthStatus=map.get("status").toString();
				String remark=map.get("remark").toString();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				Date trackDate=null;
				String track_date=map.get("track_date").toString();
				if (!"".equals(track_date)) {
					try {
						trackDate = sdf.parse(map.get("track_date").toString());
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				List<HProCusIssueMonthTrack> monthTracks=HProCusIssueMonthTrack.dao.find("select * from h_pro_cus_issue_month_track where issue_id="+issueId);
				//判断7D是否已经全部完成
				HProCusIssue8dReal dReals=HProCusIssue8dReal.dao.findFirst("select * from h_pro_cus_issue_8d_real where issue_id="+issueId);
				if (dReals==null||dReals.getDOneStatus()!=2||dReals.getDTwoStatus()!=2||dReals.getDThreeStatus()!=2||dReals.getDFourStatus()!=2||dReals.getDFiveStatus()!=2||dReals.getDSixStatus()!=2||dReals.getDSevenStatus()!=2) {
					record.set("code", 1).set("msg", "请确保7D已全部完成！");
					return false;
				}else {
					if (monthTracks.isEmpty()) {//还没有跟踪情况，判断是不是第一月跟踪
						if ("month_1".equals(month_num)) {
							HProCusIssueMonthTrack monthTrack=new HProCusIssueMonthTrack();
							monthTrack.setIssueId(issueId)
							.setMonth(1).setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
							.setCreateTime(new Date()).setUpdateTime(new Date());
							monthTrack.save();
							return true;
						}else {
							record.set("code", 1).set("msg", "第1月还没有完成跟踪记录，无法进行其他操作！");
							return false;
						}
					}else {//有记录，看前一月是否为空
						if ("month_1".equals(month_num)) {
							HProCusIssueMonthTrack track=HProCusIssueMonthTrack.dao.findFirst("select * from h_pro_cus_issue_month_track where issue_id="+issueId+" and month=1");
							track.setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
							.setUpdateTime(new Date());
							track.update();
							//问题状态判断
							List<HProCusIssueMonthTrack> allMonthTracks=HProCusIssueMonthTrack.dao.find("select * from h_pro_cus_issue_month_track where issue_id="+issueId);
							if (allMonthTracks.size()==3) {
								String issueStatus="R";
								int Rnum=0;
								int Ynum=0;
								int Gnum=0;
								for (int i = 0; i < allMonthTracks.size(); i++) {
									String trackStatus=allMonthTracks.get(i).getStatus();
									if ("R".equals(trackStatus)) {
										Rnum=Rnum+1;
									}else if ("Y".equals(trackStatus)) {
										Ynum=Ynum+1;
									}else if ("G".equals(trackStatus)) {
										Gnum=Gnum+1;
									}
								}
								if (Rnum!=0) {
									issueStatus="R";
								}else if (Ynum!=0) {
									issueStatus="Y";
								}else {
									issueStatus="G";
								}
								HProCusIssue cusIssue=HProCusIssue.dao.findById(issueId);
								cusIssue.setStatus(issueStatus);
								//问题关闭时间
								if ("G".equals(issueStatus)) {
									cusIssue.setCloseTime(new Date());
									//添加跟踪记录
									HProCusTrackHistory trackHistory=new HProCusTrackHistory();
									trackHistory.setIssueId(issueId)
									.setMeasureStatus(3).setRemark("问题关闭")
									.setTrackUserId(user.getId()).setTrackUserName(user.getNickname())
									.setCreateTime(new Date());
									trackHistory.save();
								}else {
									cusIssue.setCloseTime(null);
								}
								cusIssue.update();
							}
							return true;
						}else if ("month_2".equals(month_num)) {
							HProCusIssueMonthTrack firstTrack=HProCusIssueMonthTrack.dao.findFirst("select * from h_pro_cus_issue_month_track where issue_id="+issueId+" and month=1");
							if (firstTrack==null) {
								record.set("code", 1).set("msg", "第1月还没有完成跟踪记录，无法进行其他操作！");
								return false;
							}else {
								HProCusIssueMonthTrack secondTrack=HProCusIssueMonthTrack.dao.findFirst("select * from h_pro_cus_issue_month_track where issue_id="+issueId+" and month=2");
								if (secondTrack==null) {
									secondTrack=new HProCusIssueMonthTrack();
									secondTrack.setIssueId(issueId)
									.setMonth(2).setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
									.setCreateTime(new Date()).setUpdateTime(new Date());
									secondTrack.save();
								}else {
									secondTrack.setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
									.setUpdateTime(new Date());
									secondTrack.update();
									//问题状态判断
									List<HProCusIssueMonthTrack> allMonthTracks=HProCusIssueMonthTrack.dao.find("select * from h_pro_cus_issue_month_track where issue_id="+issueId);
									if (allMonthTracks.size()==3) {
										String issueStatus="R";
										int Rnum=0;
										int Ynum=0;
										int Gnum=0;
										for (int i = 0; i < allMonthTracks.size(); i++) {
											String trackStatus=allMonthTracks.get(i).getStatus();
											if ("R".equals(trackStatus)) {
												Rnum=Rnum+1;
											}else if ("Y".equals(trackStatus)) {
												Ynum=Ynum+1;
											}else if ("G".equals(trackStatus)) {
												Gnum=Gnum+1;
											}
										}
										if (Rnum!=0) {
											issueStatus="R";
										}else if (Ynum!=0) {
											issueStatus="Y";
										}else {
											issueStatus="G";
										}
										HProCusIssue cusIssue=HProCusIssue.dao.findById(issueId);
										cusIssue.setStatus(issueStatus);
										//问题关闭时间
										if ("G".equals(issueStatus)) {
											cusIssue.setCloseTime(new Date());
											//添加跟踪记录
											HProCusTrackHistory trackHistory=new HProCusTrackHistory();
											trackHistory.setIssueId(issueId)
											.setMeasureStatus(3).setRemark("问题关闭")
											.setTrackUserId(user.getId()).setTrackUserName(user.getNickname())
											.setCreateTime(new Date());
											trackHistory.save();
										}else {
											cusIssue.setCloseTime(null);
										}
										cusIssue.update();
									}
								}
								return true;
							}
						}else if ("month_3".equals(month_num)) {
							HProCusIssueMonthTrack secondTrack=HProCusIssueMonthTrack.dao.findFirst("select * from h_pro_cus_issue_month_track where issue_id="+issueId+" and month=2");
							if (secondTrack==null) {
								record.set("code", 1).set("msg", "第2月还没有完成跟踪记录，无法进行其他操作！");
								return false;
							}else {
								HProCusIssueMonthTrack thirdTrack=HProCusIssueMonthTrack.dao.findFirst("select * from h_pro_cus_issue_month_track where issue_id="+issueId+" and month=3");
								if (thirdTrack==null) {
									thirdTrack=new HProCusIssueMonthTrack();
									thirdTrack.setIssueId(issueId)
									.setMonth(3).setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
									.setCreateTime(new Date()).setUpdateTime(new Date());
									thirdTrack.save();
								}else {
									thirdTrack.setStatus(monthStatus).setTrackDate(trackDate).setRemark(remark)
									.setUpdateTime(new Date());
									thirdTrack.update();
								}
								//第三月则判断整个问题状态
								List<HProCusIssueMonthTrack> allMonthTracks=HProCusIssueMonthTrack.dao.find("select * from h_pro_cus_issue_month_track where issue_id="+issueId);
								String issueStatus="R";
								int Rnum=0;
								int Ynum=0;
								int Gnum=0;
								for (int i = 0; i < allMonthTracks.size(); i++) {
									String trackStatus=allMonthTracks.get(i).getStatus();
									if ("R".equals(trackStatus)) {
										Rnum=Rnum+1;
									}else if ("Y".equals(trackStatus)) {
										Ynum=Ynum+1;
									}else if ("G".equals(trackStatus)) {
										Gnum=Gnum+1;
									}
								}
								if (Rnum!=0) {
									issueStatus="R";
								}else if (Ynum!=0) {
									issueStatus="Y";
								}else {
									issueStatus="G";
								}
								HProCusIssue cusIssue=HProCusIssue.dao.findById(issueId);
								cusIssue.setStatus(issueStatus);
								//问题关闭时间
								if ("G".equals(issueStatus)) {
									//添加跟踪记录
									HProCusTrackHistory trackHistory=new HProCusTrackHistory();
									trackHistory.setIssueId(issueId)
									.setMeasureStatus(3).setRemark("问题关闭")
									.setTrackUserId(user.getId()).setTrackUserName(user.getNickname())
									.setCreateTime(new Date());
									trackHistory.save();
									cusIssue.setCloseTime(new Date());
								}else {
									cusIssue.setCloseTime(null);
								}
								cusIssue.update();
								return true;
							}
						}else {
							return true;
						}
					}
				}
			});
			if (flag) {
				renderJson(Ret.ok("msg", "提交成功！"));
			}else {
				String msg=record.getStr("msg");
				renderJson(Ret.fail("msg", "提交失败，"+ msg));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * OPL跟踪
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月25日 下午3:08:44
	 */
	@SuppressWarnings("rawtypes")
	public void saveTrackIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				Record loginUser=getSessionAttr("user");
				//OPL整体跟踪情况
				if (map.get("reamrk")!=null&&!"".equals(map.get("reamrk").toString())&&!"null".equals(map.get("reamrk").toString())) {
					HProCusIssueTrackRecord qtyIssueTrackRecord=new HProCusIssueTrackRecord();
					qtyIssueTrackRecord.setIssueId(issueId)
					.setTrackRecord(map.get("reamrk").toString())
					.setCreateTime(new Date())
					.setCreateUserId(loginUser.getInt("id")).setCreateUserName(loginUser.getStr("nickname"));
					qtyIssueTrackRecord.save();
				}
				//遏制措施跟踪
				JSONArray trackContainMeasuresList=JSONArray.parseArray(getPara("trackContainMeasuresList"));
				for (int i = 0; i < trackContainMeasuresList.size(); i++) {
					JSONObject measureObject=trackContainMeasuresList.getJSONObject(i);
					long measureId=measureObject.getLongValue("id");
					HProCusMeasures measure=HProCusMeasures.dao.findById(measureId);
					int status=measureObject.getIntValue("status");
					if (status==3) {//关闭
						measure.setCloseDate(new Date());
					}
					if (status==1) {//回退
						measure.setRealFinishDate(null);
					}
					measure.setStatus(status);
					measure.update();
					//措施跟踪
					JSONArray newTrack=measureObject.getJSONArray("new_track");
					for (int j = 0; j < newTrack.size(); j++) {
						HProCusTrackHistory trackHistory=new HProCusTrackHistory();
						trackHistory.setIssueId(issueId).setMeasureId(measureId)
						.setMeasureStatus(status).setRemark(newTrack.getString(j))
						.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
						.setCreateTime(new Date());
						trackHistory.save();
					}
				}
				//临时措施跟踪
				JSONArray trackTemporaryMeasuresList=JSONArray.parseArray(getPara("trackTemporaryMeasuresList"));
				for (int i = 0; i < trackTemporaryMeasuresList.size(); i++) {
					JSONObject measureObject=trackTemporaryMeasuresList.getJSONObject(i);
					long measureId=measureObject.getLongValue("id");
					HProCusMeasures measure=HProCusMeasures.dao.findById(measureId);
					int status=measureObject.getIntValue("status");
					if (status==3) {//关闭
						measure.setCloseDate(new Date());
					}
					if (status==1) {//回退
						measure.setRealFinishDate(null);
					}
					measure.setStatus(status);
					measure.update();
					//措施跟踪
					JSONArray newTrack=measureObject.getJSONArray("new_track");
					for (int j = 0; j < newTrack.size(); j++) {
						HProCusTrackHistory trackHistory=new HProCusTrackHistory();
						trackHistory.setIssueId(issueId).setMeasureId(measureId)
						.setMeasureStatus(status).setRemark(newTrack.getString(j))
						.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
						.setCreateTime(new Date());
						trackHistory.save();
					}
				}
				//防产生跟踪
				JSONArray trackHappenMeasuresList=JSONArray.parseArray(getPara("trackHappenMeasuresList"));
				for (int i = 0; i < trackHappenMeasuresList.size(); i++) {
					JSONObject measureObject=trackHappenMeasuresList.getJSONObject(i);
					long measureId=measureObject.getLongValue("id");
					HProCusMeasures measure=HProCusMeasures.dao.findById(measureId);
					int status=measureObject.getIntValue("status");
					if (status==3) {//关闭
						measure.setCloseDate(new Date());
					}
					if (status==1) {//回退
						measure.setRealFinishDate(null);
					}
					measure.setStatus(status);
					measure.update();
					//措施跟踪
					JSONArray newTrack=measureObject.getJSONArray("new_track");
					for (int j = 0; j < newTrack.size(); j++) {
						HProCusTrackHistory trackHistory=new HProCusTrackHistory();
						trackHistory.setIssueId(issueId).setMeasureId(measureId)
						.setMeasureStatus(status).setRemark(newTrack.getString(j))
						.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
						.setCreateTime(new Date());
						trackHistory.save();
					}
				}
				//防流出
				JSONArray trackRunoutMeasuresList=JSONArray.parseArray(getPara("trackRunoutMeasuresList"));
				for (int i = 0; i < trackRunoutMeasuresList.size(); i++) {
					JSONObject measureObject=trackRunoutMeasuresList.getJSONObject(i);
					long measureId=measureObject.getLongValue("id");
					HProCusMeasures measure=HProCusMeasures.dao.findById(measureId);
					int status=measureObject.getIntValue("status");
					if (status==3) {//关闭
						measure.setCloseDate(new Date());
					}
					if (status==1) {//回退
						measure.setRealFinishDate(null);
					}
					measure.setStatus(status);
					measure.update();
					//措施跟踪
					JSONArray newTrack=measureObject.getJSONArray("new_track");
					for (int j = 0; j < newTrack.size(); j++) {
						HProCusTrackHistory trackHistory=new HProCusTrackHistory();
						trackHistory.setIssueId(issueId).setMeasureId(measureId)
						.setMeasureStatus(status).setRemark(newTrack.getString(j))
						.setTrackUserId(loginUser.getInt("id")).setTrackUserName(loginUser.getStr("nickname"))
						.setCreateTime(new Date());
						trackHistory.save();
					}
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "操作成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 获取OPL所有措施列表进行跟踪
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月20日 上午9:35:29
	 */
	public void getProCusIssueAllMeasures() {
		try {
			long issueId=getParaToLong("id");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			List<Record> containMeasures=Db.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=2 and is_del=0");
			for (int i = 0; i < containMeasures.size(); i++) {
				List<HProCusTrackHistory> trackHistory=HProCusTrackHistory.dao.find("select * from h_pro_cus_track_history where measure_id="+containMeasures.get(i).getLong("id"));
				String measureTrack="";
				for (int j = 0; j < trackHistory.size(); j++) {
					measureTrack=measureTrack+trackHistory.get(j).getRemark()+"-"+trackHistory.get(j).getTrackUserName()+"-"+sdf.format(trackHistory.get(j).getCreateTime())+"<br>";
				}
				List<String> newTrack=new ArrayList<String>();
				containMeasures.get(i).set("measure_track", measureTrack).set("new_track", newTrack);
			}
			List<Record> temporaryMeasures=Db.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=3 and is_del=0");
			for (int i = 0; i < temporaryMeasures.size(); i++) {
				List<HProCusTrackHistory> trackHistory=HProCusTrackHistory.dao.find("select * from h_pro_cus_track_history where measure_id="+temporaryMeasures.get(i).getLong("id"));
				String measureTrack="";
				for (int j = 0; j < trackHistory.size(); j++) {
					measureTrack=measureTrack+trackHistory.get(j).getRemark()+"-"+trackHistory.get(j).getTrackUserName()+"-"+sdf.format(trackHistory.get(j).getCreateTime())+"<br>";
				}
				List<String> newTrack=new ArrayList<String>();
				temporaryMeasures.get(i).set("measure_track", measureTrack).set("new_track", newTrack);
			}
			List<Record> avoidHappen=Db.find("select a.*,b.reason_analysis from h_pro_cus_measures a,h_pro_cus_issue_reason_analysis b where a.issue_id="+issueId+" and a.is_del=0 and a.type=0 and b.id=a.reason_id");//防产生措施
			for (int i = 0; i < avoidHappen.size(); i++) {
				List<HProCusTrackHistory> trackHistory=HProCusTrackHistory.dao.find("select * from h_pro_cus_track_history where measure_id="+avoidHappen.get(i).getLong("id"));
				String measureTrack="";
				for (int j = 0; j < trackHistory.size(); j++) {
					measureTrack=measureTrack+trackHistory.get(j).getRemark()+"-"+trackHistory.get(j).getTrackUserName()+"-"+sdf.format(trackHistory.get(j).getCreateTime())+"<br>";
				}
				List<String> newTrack=new ArrayList<String>();
				avoidHappen.get(i).set("measure_track", measureTrack).set("new_track", newTrack);
			}
			List<Record> avoidRunout=Db.find("select a.*,b.reason_analysis from h_pro_cus_measures a,h_pro_cus_issue_reason_analysis b where a.issue_id="+issueId+" and a.is_del=0 and a.type=1 and b.id=a.reason_id");//防流出措施
			for (int i = 0; i < avoidRunout.size(); i++) {
				List<HProCusTrackHistory> trackHistory=HProCusTrackHistory.dao.find("select * from h_pro_cus_track_history where measure_id="+avoidRunout.get(i).getLong("id"));
				String measureTrack="";
				for (int j = 0; j < trackHistory.size(); j++) {
					measureTrack=measureTrack+trackHistory.get(j).getRemark()+"-"+trackHistory.get(j).getTrackUserName()+"-"+sdf.format(trackHistory.get(j).getCreateTime())+"<br>";
				}
				List<String> newTrack=new ArrayList<String>();
				avoidRunout.get(i).set("measure_track", measureTrack).set("new_track", newTrack);
			}
			Record record=new Record();
			record.set("contain", containMeasures).set("temporary", temporaryMeasures);
			record.set("happen", avoidHappen).set("runout", avoidRunout);
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 激励方案
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月20日 下午3:07:42
	 */
	@SuppressWarnings("rawtypes")
	public void saveUrgePlan() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				HProCusIssue cusIssue=HProCusIssue.dao.findById(issueId);
				//激励附件
				JSONArray fileList=JSONArray.parseArray(getPara("fileList"));
				long fileId=0;
				if(fileList.size()!=0) {
					JSONObject fileObject=fileList.getJSONObject(0);
					fileId=fileObject.getLongValue("id");
				}
				if (map.get("urge_plan")!=null) {
					cusIssue.setUrgePlan(map.get("urge_plan").toString());
				}
				cusIssue.setUrgeFujianId(fileId);
				cusIssue.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "编辑激励方案成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 上传8D报告和编号
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月20日 下午4:34:43
	 */
	@SuppressWarnings("rawtypes")
	public void upQtyReport() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=Long.valueOf(map.get("issueId").toString());
				//分析报告
				JSONArray fileList=JSONArray.parseArray(getPara("fileList"));
				int fileId=0;
				if(fileList.size()!=0) {
					JSONObject fileObject=fileList.getJSONObject(0);
					fileId=fileObject.getIntValue("id");
				}
				HProCusIssue qtyIssue=HProCusIssue.dao.findById(issueId);
				qtyIssue.setFileId(fileId)
				.setReportNo(map.get("report_no").toString());
				qtyIssue.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "上传成功！"));
			}else {
				renderJson(Ret.fail("msg", "上传失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
}
