package com.ray.controller.admin.NewCusProOPL;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.HProCusIssue;
import com.ray.common.model.HProCusIssue8dPlan;
import com.ray.common.model.HProCusIssue8dReal;
import com.ray.common.model.HProCusIssueMonthTrack;
import com.ray.common.model.HProCusIssueReasonAnalysis;
import com.ray.common.model.HProCusIssueTrackRecord;
import com.ray.common.model.HProCusMeasures;
import com.ray.common.model.HProCusTrackHistory;
import com.ray.common.model.User;
/**
 * 我创建的新品客诉opl  myCreateNewCusOPL/toMyCreateNewCusOPL
 * @author FL00024996
 *
 */
public class MyCreateNewCusController extends Controller {
	public void toMyCreateNewCusOPL() {
		render("newCusOPLMyCreate.html");
	}
	public void getMyCreateCusProOPL() {
		Record loginUser=getSessionAttr("user");
		User user=User.dao.findById(loginUser.getInt("id"));
		StringBuilder sb=new StringBuilder();
		sb.append(" from h_pro_cus_issue a,h_cause_dep b,h_quality_issue_type e where a.is_del=0 and a.type_id=e.id  and a.dep_id=b.dep_id and a.status<>'G' ");
		sb.append(" and a.create_user_id="+user.getId());//我创建的
		String selOPLdes=get("selOPLdes");
		if (!"".equals(selOPLdes)&& selOPLdes!=null) {
			sb.append(" and a.description like '%"+selOPLdes+"%'");
		}
		String selProCode=get("selProCode");
		if (!"".equals(selProCode)&& selProCode!=null) {
			sb.append(" and a.pro_code like '%"+selProCode+"%'");
		}
		String selProductName=get("selProductName");
		if (!"".equals(selProductName)&& selProductName!=null) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		String selCusName=get("selCusName");
		if (!"".equals(selCusName)&& selCusName!=null) {
			sb.append(" and a.cus_name like '%"+selCusName+"%'");
		}
		String selStatus=get("selStatus");
		if (!"".equals(selStatus)&& selStatus!=null) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name,e.type ",sb.toString()+" order by id desc");
		List<Record> cusOPLList=new ArrayList<Record>();
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			issueList.getList().get(i).set("caozuo", " ");
			//遏制措施 contain_s
			String contain_measures="";
			List<HProCusMeasures> containMeasures=HProCusMeasures.dao.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=2 and is_del=0");
			if (!containMeasures.isEmpty()) {
				for (int j = 0; j < containMeasures.size(); j++) {
					//遏制措施
					contain_measures=contain_measures+"遏制措施"+(j+1)+":"+containMeasures.get(j).getMeasures()+"-"+containMeasures.get(j).getUserName();
					int measureStatus=containMeasures.get(j).getStatus();
					if (measureStatus==1) {
						contain_measures=contain_measures+"-进行中-"+containMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						contain_measures=contain_measures+"-已完成-"+containMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						contain_measures=contain_measures+"-已关闭-"+containMeasures.get(j).getCloseDate();
					}
					contain_measures=contain_measures+"<br>";
					//遏制结果
					if(containMeasures.get(j).getContainResult()==null) {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":暂无"+"<br>";
					}else {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":"+containMeasures.get(j).getContainResult()+"<br>";
					}
				}
			}else {
				contain_measures="暂无遏制措施"+"<br>";
			}
			//临时措施
			String temporary_measures="";
			List<HProCusMeasures> temporaryMeasures=HProCusMeasures.dao.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=3 and is_del=0");
			if (!temporaryMeasures.isEmpty()) {
				for (int j = 0; j < temporaryMeasures.size(); j++) {
					//临时措施
					temporary_measures=temporary_measures+"临时措施"+(j+1)+":"+temporaryMeasures.get(j).getMeasures()+"-"+temporaryMeasures.get(j).getUserName();
					int measureStatus=temporaryMeasures.get(j).getStatus();
					if (measureStatus==1) {
						temporary_measures=temporary_measures+"-进行中-"+temporaryMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						temporary_measures=temporary_measures+"-已完成-"+temporaryMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						temporary_measures=temporary_measures+"-已关闭-"+temporaryMeasures.get(j).getCloseDate();
					}
					temporary_measures=temporary_measures+"<br>";
					//有效性跟踪
					List<HProCusTrackHistory> measureTrackRecord=HProCusTrackHistory.dao.find("select * from h_pro_cus_track_history where measure_id="+temporaryMeasures.get(j).getId());
					if (measureTrackRecord.isEmpty()) {
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":暂无"+"<br>";
					}else {
						String measureTracks="";
						for (int k = 0; k < measureTrackRecord.size(); k++) {
							measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；;";
						}
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"<br>";
					}
				}
			}else {
				temporary_measures="暂无临时措施";
			}
			String contain_s=contain_measures+temporary_measures;
			issueList.getList().get(i).set("contain_s", contain_s);
			//根本原因分析 root_s
			String happen_reason="";//产生原因
			String avoid_happen_measures="";//防产生措施
			List<HProCusIssueReasonAnalysis> happenAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
			if (!happenAnalysis.isEmpty()) {
				happen_reason="产生原因："+"<br>";
				avoid_happen_measures="防产生措施："+"<br>";
				for (int j = 0; j < happenAnalysis.size(); j++) {
					happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HProCusMeasures> avoidHappen=HProCusMeasures.dao.find("select * from h_pro_cus_measures where reason_id="+happenAnalysis.get(j).getId()+" and is_del=0 and type=0");//对应防产生措施
					for (int k = 0; k < avoidHappen.size(); k++) {
						avoid_happen_measures=avoid_happen_measures+(j+1)+"."+(k+1)+avoidHappen.get(k).getMeasures()+"-"+avoidHappen.get(k).getUserName();
						int measureStatus=avoidHappen.get(k).getStatus();
						if (measureStatus==1) {
							avoid_happen_measures=avoid_happen_measures+"-进行中"+avoidHappen.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_happen_measures=avoid_happen_measures+"-已完成"+avoidHappen.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_happen_measures=avoid_happen_measures+"-已关闭"+avoidHappen.get(k).getCloseDate();
						}
						avoid_happen_measures=avoid_happen_measures+"<br>";
					}
				}
			}else {
				happen_reason="暂无产生原因"+"<br>";
				avoid_happen_measures="暂无防产生措施"+"<br>";
			}
			String runout_reason="";//流出原因
			String avoid_runout_measures="";//防流出措施
			List<HProCusIssueReasonAnalysis> runOutAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
			if (!runOutAnalysis.isEmpty()) {
				runout_reason="流出原因："+"<br>";
				avoid_runout_measures="防流出措施："+"<br>";
				for (int j = 0; j < runOutAnalysis.size(); j++) {
					runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HProCusMeasures> avoidRunout=HProCusMeasures.dao.find("select * from h_pro_cus_measures where reason_id="+runOutAnalysis.get(j).getId()+" and is_del=0 and type=1");//对应防流出措施
					for (int k = 0; k < avoidRunout.size(); k++) {
						avoid_runout_measures=avoid_runout_measures+(j+1)+"."+(k+1)+avoidRunout.get(k).getMeasures()+"-"+avoidRunout.get(k).getUserName();
						int measureStatus=avoidRunout.get(k).getStatus();
						if (measureStatus==1) {
							avoid_runout_measures=avoid_runout_measures+"-进行中"+avoidRunout.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_runout_measures=avoid_runout_measures+"-已完成"+avoidRunout.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_runout_measures=avoid_runout_measures+"-已关闭"+avoidRunout.get(k).getCloseDate();
						}
						avoid_runout_measures=avoid_runout_measures+"<br>";
					}
				}
			}else {
				runout_reason="暂无流出原因";
				avoid_runout_measures="暂无防流出措施";
			}
			String root_s=happen_reason+runout_reason;
			String avoid_s=avoid_happen_measures+avoid_runout_measures;
			issueList.getList().get(i).set("root_s", root_s).set("avoid_s", avoid_s);
			//永久措施跟踪3月
			List<HProCusIssueMonthTrack> monthTracks=HProCusIssueMonthTrack.dao.find("select * from h_pro_cus_issue_month_track where issue_id="+issueId);
			issueList.getList().get(i).set("month_1", " ").set("month_1_remark", "").set("month_1_date", "");
			issueList.getList().get(i).set("month_2", " ").set("month_2_remark", "").set("month_2_date", "");
			issueList.getList().get(i).set("month_3", " ").set("month_3_remark", "").set("month_3_date", "");
			String monthTrackString="";
			for (int j = 0; j < monthTracks.size(); j++) {
				int month=monthTracks.get(j).getMonth();
				if (month==1) {
					monthTrackString=monthTrackString+"第一月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_1", monthTracks.get(j).getStatus()).set("month_1_remark",  monthTracks.get(j).getRemark()).set("month_1_date",  monthTracks.get(j).getTrackDate());
				}else if (month==2) {
					monthTrackString=monthTrackString+"第二月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_2", monthTracks.get(j).getStatus()).set("month_2_remark", monthTracks.get(j).getRemark()).set("month_2_date",  monthTracks.get(j).getTrackDate());
				}else if (month==3) {
					monthTrackString=monthTrackString+"第三月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_3", monthTracks.get(j).getStatus()).set("month_3_remark", monthTracks.get(j).getRemark()).set("month_3_date",  monthTracks.get(j).getTrackDate());
				}
			}
			//OPL整体跟踪记录
			List<HProCusIssueTrackRecord> trackRecords=HProCusIssueTrackRecord.dao.find("select * from h_pro_cus_issue_track_record where issue_id="+issueId);
			String  oplTrackRecord="";
			for (int j = 0; j < trackRecords.size(); j++) {
				oplTrackRecord=oplTrackRecord+(j+1)+"."+trackRecords.get(j).getTrackRecord()+"-"+trackRecords.get(j).getCreateUserName()+"-"+sdf1.format(trackRecords.get(j).getCreateTime())+"<br>";
			}
			String record=" ";
			if (!"".equals(oplTrackRecord)) {
				record=oplTrackRecord;
			}
			if (!"".equals(monthTrackString)) {
				record=record+monthTrackString;
			}
			issueList.getList().get(i).set("record", record);
			//8D计划
			Record issuePlan=new Record();
			issuePlan.setColumns(issueList.getList().get(i));
			Record issueReal=new Record();
			issueReal.setColumns(issueList.getList().get(i));
			HProCusIssue8dPlan plans=HProCusIssue8dPlan.dao.findFirst("select * from h_pro_cus_issue_8d_plan where issue_id="+issuePlan.getLong("id"));
			issuePlan.set("plan_real", "计划")
			.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
			.set("d_three", plans.getDThree()).set("d_four", plans.getDFour())
			.set("d_five", plans.getDFive()).set("d_six", plans.getDSix())
			.set("d_seven", plans.getDSeven());
			//8D实际
			HProCusIssue8dReal reals=HProCusIssue8dReal.dao.findFirst("select * from h_pro_cus_issue_8d_real where issue_id="+issueReal.getLong("id"));
			issueReal.set("plan_real", "实际")
			.set("d_one", " ").set("d_two", " ")
			.set("d_three", " ").set("d_four", " ")
			.set("d_five", " ").set("d_six", " ")
			.set("d_seven", " ")
			.set("d_one_status", "0").set("d_two_status","0")
			.set("d_three_status", "0").set("d_four_status", "0")
			.set("d_five_status", "0").set("d_six_status", "0")
			.set("d_seven_status", "0");
			if (reals!=null) {
				issueReal
				.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
				.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
				.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
				.set("d_seven_status", reals.getDSevenStatus());
				if (reals.getDOneStatus()==2) {
					issueReal.set("d_one", reals.getDOne());
				}
				if (reals.getDTwoStatus()==2) {
					issueReal.set("d_two", reals.getDTwo());
				}
				if (reals.getDThreeStatus()==2) {
					issueReal.set("d_three", reals.getDThree());
				}
				if (reals.getDFourStatus()==2) {
					issueReal.set("d_four", reals.getDFour());
				}
				if (reals.getDFiveStatus()==2) {
					issueReal.set("d_five", reals.getDFive());
				}
				if (reals.getDSixStatus()==2) {
					issueReal.set("d_six", reals.getDSix());
				}
				if (reals.getDSevenStatus()==2) {
					issueReal.set("d_seven", reals.getDSeven());
				}
			}
			cusOPLList.add(issuePlan);
			cusOPLList.add(issueReal);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", cusOPLList);
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**
	 * 指向新增页面
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月14日 下午4:42:22
	 */
	public void toAddNewCusOPL() {
		long issueId=getParaToLong("issueid");
		set("issueid", issueId);
		render("addEditNewCusIssue.html");
	}
	/**
	 * 获取OPL详情
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月20日 下午5:23:54
	 */
	public void getIssueInfo() {
		try {
			long issueId=getParaToLong("issueId");
			HProCusIssue issue=HProCusIssue.dao.findById(issueId);
			//对应用户钉钉id
			User dutyUser=User.dao.findById(issue.getDutyUserId());
			Record record=new Record();
			record.set("issue", issue)
			.set("duty_user_id",dutyUser.getDingUserId());
			renderJson(Ret.ok("data", record));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 新增新品客诉问题
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月20日 下午4:12:42
	 */
	@SuppressWarnings("rawtypes")
	public void addNewCusIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				int proId=Integer.valueOf(map.get("pro_id").toString());
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				Record loginUser=getSessionAttr("user");
				User login=User.dao.findById(loginUser.getInt("id"));
				HProCusIssue cusIssue=new HProCusIssue();
				try {
					String duty_user_id=map.get("duty_user_id").toString();
					User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+duty_user_id+"'");					
					Date openTime=sdf.parse(map.get("open_time").toString());
					Date dOneDate=com.ray.util.HyCommenMethods.addDate(openTime, 1);
					//项目部门
					Record proInfo=Db.use("bud").findById("file_pro_info", proId);
					String depart_name=proInfo.getStr("depart_name");
					String proCode=proInfo.getStr("pro_code");
					long proDepId=com.ray.util.HyCommenMethods.dDepId(depart_name);
					//新增
					cusIssue
					.setProId(proId).setDepId(proDepId).setProCode(proCode)
					.setProName(map.get("pro_name").toString())
					.setProductName(map.get("product_name").toString())
					.setStatus("Y")
					.setReportNo("NA")
					.setLevel("A")
					.setCusName(map.get("cus_name").toString())
					.setTypeId(Integer.valueOf(map.get("type_id").toString()))
					.setSource(map.get("source").toString())
					.setDescription(map.get("description").toString())
					.setOpenTime(sdf.parse(map.get("open_time").toString()))
					.setHappenFrequency(map.get("happen_frequency").toString())
					.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
					.setCreateUserId(login.getId()).setCreateUserName(login.getNickname())
					.setCreateTime(new Date())
					.setIsDel(0).setIssueStatus(0)
					.setFileId(0)
					.setDNoteDate(com.ray.util.HyCommenMethods.addDate(dOneDate,3));
					cusIssue.save();
					DingMessage.sendText(duty_user_id, "有新的新品客诉OPL创建并需要你负责，请知晓！"+sdf.format(new Date()), "23");//23-新品客诉OPL措施责任人页面
					//7D时间
					HProCusIssue8dPlan dayPlan=new HProCusIssue8dPlan();
					dayPlan.setIssueId(cusIssue.getId())
					.setDOne(com.ray.util.HyCommenMethods.addDate(openTime, 1))
					.setDTwo(com.ray.util.HyCommenMethods.addDate(openTime, 1))
					.setDThree(com.ray.util.HyCommenMethods.addDate(openTime, 2))
					.setDFour(com.ray.util.HyCommenMethods.addDate(openTime, 7))
					.setDFive(com.ray.util.HyCommenMethods.addDate(openTime, 14));
					dayPlan.save();
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "新增新品客诉OPL成功！"));
			}else {
				renderJson(Ret.fail("msg", "新增新品客诉OPL失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	/**
	 * 编辑新品客诉问题
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月20日 下午4:12:42
	 */
	@SuppressWarnings("rawtypes")
	public void editNewCusIssue() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long issueId=getParaToLong("editId");
				int proId=Integer.valueOf(map.get("pro_id").toString());
				Record loginUser=getSessionAttr("user");
				User login=User.dao.findById(loginUser.getInt("id"));
				HProCusIssue cusIssue=HProCusIssue.dao.findById(issueId);
				String duty_user_id=map.get("duty_user_id").toString();
				User dutyUser=User.dao.findFirst("select * from user where ding_user_id='"+duty_user_id+"'");
				//项目部门
				Record proInfo=Db.use("bud").findById("file_pro_info", proId);
				String depart_name=proInfo.getStr("depart_name");
				String proCode=proInfo.getStr("pro_code");
				long proDepId=com.ray.util.HyCommenMethods.dDepId(depart_name);
				//编辑OPL
				cusIssue
				.setProId(proId).setDepId(proDepId).setProCode(proCode)
				.setProName(map.get("pro_name").toString())
				.setProductName(map.get("product_name").toString())
				.setCusName(map.get("cus_name").toString())
				.setTypeId(Integer.valueOf(map.get("type_id").toString()))
				.setSource(map.get("source").toString())
				.setDescription(map.get("description").toString())
				.setHappenFrequency(map.get("happen_frequency").toString())
				.setDutyUserId(dutyUser.getId().intValue()).setDutyUserName(dutyUser.getNickname())
				.setCreateUserId(login.getId()).setCreateUserName(login.getNickname())
				.setCreateTime(new Date())
				.setIsDel(0).setIssueStatus(0);
				cusIssue.update();
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				DingMessage.sendText(duty_user_id, "有新的新品客诉OPL重新编辑并需要你负责，请知晓！"+sdf.format(new Date()), "23");//23-新品客诉OPL措施责任人页面
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "编辑新品客诉OPL成功！"));
			}else {
				renderJson(Ret.fail("msg", "编辑新品客诉OPL失败，请联系管理员！"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
	
	/**
	 * 删除新品客诉
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月20日 下午4:22:17
	 */
	public void delProCusIssue() {
		try {
			boolean flag = Db.tx(() -> {
				long issueId=getParaToLong("id");
				HProCusIssue cusIssue=HProCusIssue.dao.findById(issueId);
				cusIssue.setIsDel(1);
				cusIssue.update();
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "删除成功！"));
			}else {
				renderJson(Ret.fail("msg", "删除失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
}
