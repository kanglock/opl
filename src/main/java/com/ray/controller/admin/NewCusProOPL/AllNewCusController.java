package com.ray.controller.admin.NewCusProOPL;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.model.HProCusIssue8dPlan;
import com.ray.common.model.HProCusIssue8dReal;
import com.ray.common.model.HProCusIssueMonthTrack;
import com.ray.common.model.HProCusIssueReasonAnalysis;
import com.ray.common.model.HProCusIssueTrackRecord;
import com.ray.common.model.HProCusMeasures;
import com.ray.common.model.HProCusTrackHistory;
import com.ray.common.model.User;
/**
 * 所有新品客诉 allNewCusOPL
 * @author FL00024996
 *
 */
public class AllNewCusController extends Controller {
	public void toAllCusProOPL() {
		render("allNewCusProOPL.html");
	}
	public void getAllCusProOPL() {
		Record loginUser=getSessionAttr("user");
		User user=User.dao.findById(loginUser.getInt("id"));
		//判断是否超管、体系等
		List<Record> userRole=Db.use("bud").find("select b.role_name from user_role a, roles b where a.user_id='"+user.getDingUserId()+"' and a.role_id=b.id");
		int user_role=0;
		if (userRole.size()>0) {
			for (int i = 0; i < userRole.size(); i++) {
				String role_name=userRole.get(i).getStr("role_name");
				if ("公司经营层".equals(role_name)|| "项目体系".equals(role_name)||"财务".equals(role_name)||"管理员".equals(role_name)) {
					user_role=1;
				}
			}
		}
		StringBuilder sb=new StringBuilder();
		sb.append(" from h_pro_cus_issue a,h_cause_dep b,h_quality_issue_type e where a.is_del=0 and a.type_id=e.id  and a.dep_id=b.dep_id ");
		if (user_role==0) {
			sb.append(" and (a.dep_id in (select first_dep_id from h_user_dep where user_id="+user.getId()+" and status=0  union "
					+ "select dep_id from h_pro_cus_issue where create_user_id="+user.getId().intValue()+" or duty_user_id="+user.getId().intValue()+") ) ");
		}
		String selOPLdes=get("selOPLdes");
		if (!"".equals(selOPLdes)&& selOPLdes!=null) {
			sb.append(" and a.description like '%"+selOPLdes+"%'");
		}
		String selCreateUser=get("selCreateUser");
		if (!"".equals(selCreateUser)&& selCreateUser!=null) {
			sb.append(" and a.create_user_name like '%"+selCreateUser+"%'");
		}
		String selDutyUser=get("selDutyUser");
		if (!"".equals(selDutyUser)&& selDutyUser!=null) {
			sb.append(" and a.duty_user_name like '%"+selDutyUser+"%'");
		}
		String selProCode=get("selProCode");
		if (!"".equals(selProCode)&& selProCode!=null) {
			sb.append(" and a.pro_code like '%"+selProCode+"%'");
		}
		String selProductName=get("selProductName");
		if (!"".equals(selProductName)&& selProductName!=null) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		String selCusName=get("selCusName");
		if (!"".equals(selCusName)&& selCusName!=null) {
			sb.append(" and a.cus_name like '%"+selCusName+"%'");
		}
		String selStatus=get("selStatus");
		if (!"".equals(selStatus)&& selStatus!=null) {
			sb.append(" and a.status='"+selStatus+"'");
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name,e.type ",sb.toString()+" order by id desc");
		List<Record> cusOPLList=new ArrayList<Record>();
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			issueList.getList().get(i).set("caozuo", " ");
			//遏制措施 contain_s
			String contain_measures="";
			List<HProCusMeasures> containMeasures=HProCusMeasures.dao.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=2 and is_del=0");
			if (!containMeasures.isEmpty()) {
				for (int j = 0; j < containMeasures.size(); j++) {
					//遏制措施
					contain_measures=contain_measures+"遏制措施"+(j+1)+":"+containMeasures.get(j).getMeasures()+"-"+containMeasures.get(j).getUserName();
					int measureStatus=containMeasures.get(j).getStatus();
					if (measureStatus==1) {
						contain_measures=contain_measures+"-进行中-"+containMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						contain_measures=contain_measures+"-已完成-"+containMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						contain_measures=contain_measures+"-已关闭-"+containMeasures.get(j).getCloseDate();
					}
					contain_measures=contain_measures+"<br>";
					//遏制结果
					if(containMeasures.get(j).getContainResult()==null) {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":暂无"+"<br>";
					}else {
						contain_measures=contain_measures+"遏制结果"+(j+1)+":"+containMeasures.get(j).getContainResult()+"<br>";
					}
				}
			}else {
				contain_measures="暂无遏制措施"+"<br>";
			}
			//临时措施
			String temporary_measures="";
			List<HProCusMeasures> temporaryMeasures=HProCusMeasures.dao.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=3 and is_del=0");
			if (!temporaryMeasures.isEmpty()) {
				for (int j = 0; j < temporaryMeasures.size(); j++) {
					//临时措施
					temporary_measures=temporary_measures+"临时措施"+(j+1)+":"+temporaryMeasures.get(j).getMeasures()+"-"+temporaryMeasures.get(j).getUserName();
					int measureStatus=temporaryMeasures.get(j).getStatus();
					if (measureStatus==1) {
						temporary_measures=temporary_measures+"-进行中-"+temporaryMeasures.get(j).getPlanFinishDate();
					}else if (measureStatus==2){
						temporary_measures=temporary_measures+"-已完成-"+temporaryMeasures.get(j).getRealFinishDate();
					}else if (measureStatus==3){
						temporary_measures=temporary_measures+"-已关闭-"+temporaryMeasures.get(j).getCloseDate();
					}
					temporary_measures=temporary_measures+"<br>";
					//有效性跟踪
					List<HProCusTrackHistory> measureTrackRecord=HProCusTrackHistory.dao.find("select * from h_pro_cus_track_history where measure_id="+temporaryMeasures.get(j).getId());
					if (measureTrackRecord.isEmpty()) {
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":暂无"+"<br>";
					}else {
						String measureTracks="";
						for (int k = 0; k < measureTrackRecord.size(); k++) {
							measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；;";
						}
						temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"<br>";
					}
				}
			}else {
				temporary_measures="暂无临时措施";
			}
			String contain_s=contain_measures+temporary_measures;
			issueList.getList().get(i).set("contain_s", contain_s);
			//根本原因分析 root_s
			String happen_reason="";//产生原因
			String avoid_happen_measures="";//防产生措施
			List<HProCusIssueReasonAnalysis> happenAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
			if (!happenAnalysis.isEmpty()) {
				happen_reason="产生原因："+"<br>";
				avoid_happen_measures="防产生措施："+"<br>";
				for (int j = 0; j < happenAnalysis.size(); j++) {
					happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HProCusMeasures> avoidHappen=HProCusMeasures.dao.find("select * from h_pro_cus_measures where reason_id="+happenAnalysis.get(j).getId()+" and is_del=0 and type=0");//对应防产生措施
					for (int k = 0; k < avoidHappen.size(); k++) {
						avoid_happen_measures=avoid_happen_measures+(j+1)+"."+(k+1)+avoidHappen.get(k).getMeasures()+"-"+avoidHappen.get(k).getUserName();
						int measureStatus=avoidHappen.get(k).getStatus();
						if (measureStatus==1) {
							avoid_happen_measures=avoid_happen_measures+"-进行中"+avoidHappen.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_happen_measures=avoid_happen_measures+"-已完成"+avoidHappen.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_happen_measures=avoid_happen_measures+"-已关闭"+avoidHappen.get(k).getCloseDate();
						}
						avoid_happen_measures=avoid_happen_measures+"<br>";
					}
				}
			}else {
				happen_reason="暂无产生原因"+"<br>";
				avoid_happen_measures="暂无防产生措施"+"<br>";
			}
			String runout_reason="";//流出原因
			String avoid_runout_measures="";//防流出措施
			List<HProCusIssueReasonAnalysis> runOutAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
			if (!runOutAnalysis.isEmpty()) {
				runout_reason="流出原因："+"<br>";
				avoid_runout_measures="防流出措施："+"<br>";
				for (int j = 0; j < runOutAnalysis.size(); j++) {
					runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"<br>";
					List<HProCusMeasures> avoidRunout=HProCusMeasures.dao.find("select * from h_pro_cus_measures where reason_id="+runOutAnalysis.get(j).getId()+" and is_del=0 and type=1");//对应防流出措施
					for (int k = 0; k < avoidRunout.size(); k++) {
						avoid_runout_measures=avoid_runout_measures+(j+1)+"."+(k+1)+avoidRunout.get(k).getMeasures()+"-"+avoidRunout.get(k).getUserName();
						int measureStatus=avoidRunout.get(k).getStatus();
						if (measureStatus==1) {
							avoid_runout_measures=avoid_runout_measures+"-进行中"+avoidRunout.get(k).getPlanFinishDate();
						}else if (measureStatus==2){
							avoid_runout_measures=avoid_runout_measures+"-已完成"+avoidRunout.get(k).getRealFinishDate();
						}else if (measureStatus==3){
							avoid_runout_measures=avoid_runout_measures+"-已关闭"+avoidRunout.get(k).getCloseDate();
						}
						avoid_runout_measures=avoid_runout_measures+"<br>";
					}
				}
			}else {
				runout_reason="暂无流出原因";
				avoid_runout_measures="暂无防流出措施";
			}
			String root_s=happen_reason+runout_reason;
			String avoid_s=avoid_happen_measures+avoid_runout_measures;
			issueList.getList().get(i).set("root_s", root_s).set("avoid_s", avoid_s);
			//永久措施跟踪3月
			List<HProCusIssueMonthTrack> monthTracks=HProCusIssueMonthTrack.dao.find("select * from h_pro_cus_issue_month_track where issue_id="+issueId);
			issueList.getList().get(i).set("month_1", " ").set("month_1_remark", "").set("month_1_date", "");
			issueList.getList().get(i).set("month_2", " ").set("month_2_remark", "").set("month_2_date", "");
			issueList.getList().get(i).set("month_3", " ").set("month_3_remark", "").set("month_3_date", "");
			String monthTrackString="";
			for (int j = 0; j < monthTracks.size(); j++) {
				int month=monthTracks.get(j).getMonth();
				if (month==1) {
					monthTrackString=monthTrackString+"第一月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_1", monthTracks.get(j).getStatus()).set("month_1_remark",  monthTracks.get(j).getRemark()).set("month_1_date",  monthTracks.get(j).getTrackDate());
				}else if (month==2) {
					monthTrackString=monthTrackString+"第二月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_2", monthTracks.get(j).getStatus()).set("month_2_remark", monthTracks.get(j).getRemark()).set("month_2_date",  monthTracks.get(j).getTrackDate());
				}else if (month==3) {
					monthTrackString=monthTrackString+"第三月："+monthTracks.get(j).getRemark()+"<br>";
					issueList.getList().get(i).set("month_3", monthTracks.get(j).getStatus()).set("month_3_remark", monthTracks.get(j).getRemark()).set("month_3_date",  monthTracks.get(j).getTrackDate());
				}
			}
			//OPL整体跟踪记录
			List<HProCusIssueTrackRecord> trackRecords=HProCusIssueTrackRecord.dao.find("select * from h_pro_cus_issue_track_record where issue_id="+issueId);
			String  oplTrackRecord="";
			for (int j = 0; j < trackRecords.size(); j++) {
				oplTrackRecord=oplTrackRecord+(j+1)+"."+trackRecords.get(j).getTrackRecord()+"-"+trackRecords.get(j).getCreateUserName()+"-"+sdf1.format(trackRecords.get(j).getCreateTime())+"<br>";
			}
			String record=" ";
			if (!"".equals(oplTrackRecord)) {
				record=oplTrackRecord;
			}
			if (!"".equals(monthTrackString)) {
				record=record+monthTrackString;
			}
			issueList.getList().get(i).set("record", record);
			//8D计划
			Record issuePlan=new Record();
			issuePlan.setColumns(issueList.getList().get(i));
			Record issueReal=new Record();
			issueReal.setColumns(issueList.getList().get(i));
			HProCusIssue8dPlan plans=HProCusIssue8dPlan.dao.findFirst("select * from h_pro_cus_issue_8d_plan where issue_id="+issuePlan.getLong("id"));
			issuePlan.set("plan_real", "计划")
			.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
			.set("d_three", plans.getDThree()).set("d_four", plans.getDFour())
			.set("d_five", plans.getDFive()).set("d_six", plans.getDSix())
			.set("d_seven", plans.getDSeven());
			//8D实际
			HProCusIssue8dReal reals=HProCusIssue8dReal.dao.findFirst("select * from h_pro_cus_issue_8d_real where issue_id="+issueReal.getLong("id"));
			issueReal.set("plan_real", "实际")
			.set("d_one", " ").set("d_two", " ")
			.set("d_three", " ").set("d_four", " ")
			.set("d_five", " ").set("d_six", " ")
			.set("d_seven", " ")
			.set("d_one_status", "0").set("d_two_status","0")
			.set("d_three_status", "0").set("d_four_status", "0")
			.set("d_five_status", "0").set("d_six_status", "0")
			.set("d_seven_status", "0");
			if (reals!=null) {
				issueReal
				.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
				.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
				.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
				.set("d_seven_status", reals.getDSevenStatus());
				if (reals.getDOneStatus()==2) {
					issueReal.set("d_one", reals.getDOne());
				}
				if (reals.getDTwoStatus()==2) {
					issueReal.set("d_two", reals.getDTwo());
				}
				if (reals.getDThreeStatus()==2) {
					issueReal.set("d_three", reals.getDThree());
				}
				if (reals.getDFourStatus()==2) {
					issueReal.set("d_four", reals.getDFour());
				}
				if (reals.getDFiveStatus()==2) {
					issueReal.set("d_five", reals.getDFive());
				}
				if (reals.getDSixStatus()==2) {
					issueReal.set("d_six", reals.getDSix());
				}
				if (reals.getDSevenStatus()==2) {
					issueReal.set("d_seven", reals.getDSeven());
				}
			}
			cusOPLList.add(issuePlan);
			cusOPLList.add(issueReal);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", cusOPLList);
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**
	 * 导出
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月6日 上午11:06:37
	 */
	@SuppressWarnings("resource")
	public void exportNewCus() {
		Record req=new Record();
		Db.tx(() -> {
			try {
				HSSFWorkbook workbook = new HSSFWorkbook();
		        HSSFCellStyle style = workbook.createCellStyle();
		        style.setAlignment(HorizontalAlignment.CENTER);
		        style.setVerticalAlignment(VerticalAlignment.CENTER);
		        HSSFSheet sheet = workbook.createSheet("sheet");
		        
		        HSSFRow title = sheet.createRow(0);
		        HSSFRow title2 = sheet.createRow(1);
		        HSSFCell cell_00 = title.createCell(0);
		        cell_00.setCellStyle(style);
		        cell_00.setCellValue("报告编号");
		        HSSFCell cell_01 = title.createCell(1);
		        cell_01.setCellStyle(style);
		        cell_01.setCellValue("状态");
		        HSSFCell cell_02 = title.createCell(2);
		        cell_02.setCellStyle(style);
		        cell_02.setCellValue("事业部");
		        HSSFCell cell_03 = title.createCell(3);
		        cell_03.setCellStyle(style);
		        cell_03.setCellValue("项目名称");
		        HSSFCell cell_04 = title.createCell(4);
		        cell_04.setCellStyle(style);
		        cell_04.setCellValue("项目号");
		        HSSFCell cell_05 = title.createCell(5);
		        cell_05.setCellStyle(style);
		        cell_05.setCellValue("开放时间");
		        HSSFCell cell_06 = title.createCell(6);
		        cell_06.setCellStyle(style);
		        cell_06.setCellValue("时限要求");
		        HSSFCell cell_07 = title.createCell(7);
		        cell_07.setCellStyle(style);
		        cell_07.setCellValue("8D进度状态");
		        //8D-开始
		        HSSFCell cell_17 = title2.createCell(7);
		        cell_17.setCellStyle(style);
		        cell_17.setCellValue("D1");
		        HSSFCell cell_18 = title2.createCell(8);
		        cell_18.setCellStyle(style);
		        cell_18.setCellValue("D2");
		        HSSFCell cell_19 = title2.createCell(9);
		        cell_19.setCellStyle(style);
		        cell_19.setCellValue("D3");
		        HSSFCell cell_110 = title2.createCell(10);
		        cell_110.setCellStyle(style);
		        cell_110.setCellValue("D4");
		        HSSFCell cell_111 = title2.createCell(11);
		        cell_111.setCellStyle(style);
		        cell_111.setCellValue("D5");
		        HSSFCell cell_112 = title2.createCell(12);
		        cell_112.setCellStyle(style);
		        cell_112.setCellValue("D6");
		        HSSFCell cell_113 = title2.createCell(13);
		        cell_113.setCellStyle(style);
		        cell_113.setCellValue("D7");
		        //8D结束
		        HSSFCell cell_014 = title.createCell(14);
		        cell_014.setCellStyle(style);
		        cell_014.setCellValue("客户名称");
		        HSSFCell cell_015 = title.createCell(15);
		        cell_015.setCellStyle(style);
		        cell_015.setCellValue("问题类型");
		        HSSFCell cell_016 = title.createCell(16);
		        cell_016.setCellStyle(style);
		        cell_016.setCellValue("问题来源");
		        HSSFCell cell_017 = title.createCell(17);
		        cell_017.setCellStyle(style);
		        cell_017.setCellValue("发生频次");
		        HSSFCell cell_018 = title.createCell(18);
		        cell_018.setCellStyle(style);
		        cell_018.setCellValue("问题描述（地点/批次/数量）");
		        HSSFCell cell_019 = title.createCell(19);
		        cell_019.setCellStyle(style);
		        cell_019.setCellValue("遏制/临时措施及措施有效性跟踪");
		        HSSFCell cell_020 = title.createCell(20);
		        cell_020.setCellStyle(style);
		        cell_020.setCellValue("根本原因分析（产生/流出）");
		        HSSFCell cell_021 = title.createCell(21);
		        cell_021.setCellStyle(style);
		        cell_021.setCellValue("永久措施制定（产生/流出）");
		        HSSFCell cell_022 = title.createCell(22);
		        cell_022.setCellStyle(style);
		        cell_022.setCellValue("永久措施节点");
		        HSSFCell cell_023 = title.createCell(23);
		        cell_023.setCellStyle(style);
		        cell_023.setCellValue("永久措施3个月跟踪");
		        //3个月开始
		        HSSFCell cell_123 = title2.createCell(23);
		        cell_123.setCellStyle(style);
		        cell_123.setCellValue("第一月");
		        HSSFCell cell_124 = title2.createCell(24);
		        cell_124.setCellStyle(style);
		        cell_124.setCellValue("第二月");
		        HSSFCell cell_125 = title2.createCell(25);
		        cell_125.setCellStyle(style);
		        cell_125.setCellValue("第三月");
		        //3个月结束
		        HSSFCell cell_026 = title.createCell(26);
		        cell_026.setCellStyle(style);
		        cell_026.setCellValue("责任人");
		        HSSFCell cell_027 = title.createCell(27);
		        cell_027.setCellStyle(style);
		        cell_027.setCellValue("升级对象");
		        HSSFCell cell_028 = title.createCell(28);
		        cell_028.setCellStyle(style);
		        cell_028.setCellValue("关闭时间");
		        HSSFCell cell_029 = title.createCell(29);
		        cell_029.setCellStyle(style);
		        cell_029.setCellValue("激励方案（正/负）");
		        HSSFCell cell_030 = title.createCell(30);
		        cell_030.setCellStyle(style);
		        cell_030.setCellValue("跟踪记录");
		        CellRangeAddress region7d = new CellRangeAddress(0, 0, 7, 13);
		        sheet.addMergedRegion(region7d);
		        CellRangeAddress region3month = new CellRangeAddress(0, 0, 23, 25);
		        sheet.addMergedRegion(region3month);
		        for (int i = 0; i < 7; i++) {
		        	CellRangeAddress regionCon = new CellRangeAddress(0, 1, i, i);
			        sheet.addMergedRegion(regionCon);
				}
		        for (int i = 14; i < 23; i++) {
		        	CellRangeAddress regionCon = new CellRangeAddress(0, 1, i, i);
			        sheet.addMergedRegion(regionCon);
				}
		        for (int i = 26; i <= 30; i++) {
		        	CellRangeAddress regionCon = new CellRangeAddress(0, 1, i, i);
			        sheet.addMergedRegion(regionCon);
				}
		        //获取数据
		        Record loginUser=getSessionAttr("user");
				User user=User.dao.findById(loginUser.getInt("id"));
				StringBuilder sb=new StringBuilder();
				int downType=getParaToInt("downType");
				sb.append("select a.*,b.dep_name,e.type  from h_pro_cus_issue a,h_cause_dep b,h_quality_issue_type e where a.is_del=0 and a.type_id=e.id  and a.dep_id=b.dep_id ");
				if (downType==0) {
					sb.append(" and a.create_user_id="+user.getId());//我创建的
				}else if (downType==1) {
					sb.append(" and a.duty_user_id="+user.getId());//我负责
				}else if (downType==2) {
					//判断是否超管、体系等
					List<Record> userRole=Db.use("bud").find("select b.role_name from user_role a, roles b where a.user_id='"+user.getDingUserId()+"' and a.role_id=b.id");
					int user_role=0;
					if (userRole.size()>0) {
						for (int i = 0; i < userRole.size(); i++) {
							String role_name=userRole.get(i).getStr("role_name");
							if ("公司经营层".equals(role_name)|| "项目体系".equals(role_name)||"财务".equals(role_name)||"管理员".equals(role_name)) {
								user_role=1;
							}
						}
					}
					if (user_role==0) {
						sb.append(" and (a.dep_id in (select first_dep_id from h_user_dep where user_id="+user.getId()+" and status=0  union "
								+ "select dep_id from h_pro_cus_issue where create_user_id="+user.getId().intValue()+" or duty_user_id="+user.getId().intValue()+") ) ");
					}
					String selCreateUser=get("selCreateUser");
					if (!"".equals(selCreateUser)&& selCreateUser!=null) {
						sb.append(" and a.create_user_name like '%"+selCreateUser+"%'");
					}
					String selDutyUser=get("selDutyUser");
					if (!"".equals(selDutyUser)&& selDutyUser!=null) {
						sb.append(" and a.duty_user_name like '%"+selDutyUser+"%'");
					}
				}
				String selOPLdes=get("selOPLdes");
				if (!"".equals(selOPLdes)&& selOPLdes!=null) {
					sb.append(" and a.description like '%"+selOPLdes+"%'");
				}
				String selProCode=get("selProCode");
				if (!"".equals(selProCode)&& selProCode!=null) {
					sb.append(" and a.pro_code like '%"+selProCode+"%'");
				}
				String selProductName=get("selProductName");
				if (!"".equals(selProductName)&& selProductName!=null) {
					sb.append(" and a.product_name like '%"+selProductName+"%'");
				}
				String selCusName=get("selCusName");
				if (!"".equals(selCusName)&& selCusName!=null) {
					sb.append(" and a.cus_name like '%"+selCusName+"%'");
				}
				String selStatus=get("selStatus");
				if (!"".equals(selStatus)&& selStatus!=null) {
					sb.append(" and a.status='"+selStatus+"'");
				}
				List<Record> issueList=Db.find(sb.toString()+" order by id desc");
				List<Record> cusOPLList=new ArrayList<Record>();
				SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				for (int i = 0; i < issueList.size(); i++) {
					long issueId=issueList.get(i).getLong("id");
					//遏制措施 contain_s
					String contain_measures="";
					List<HProCusMeasures> containMeasures=HProCusMeasures.dao.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=2 and is_del=0");
					if (!containMeasures.isEmpty()) {
						for (int j = 0; j < containMeasures.size(); j++) {
							//遏制措施
							contain_measures=contain_measures+"遏制措施"+(j+1)+":"+containMeasures.get(j).getMeasures()+"-"+containMeasures.get(j).getUserName();
							int measureStatus=containMeasures.get(j).getStatus();
							if (measureStatus==1) {
								contain_measures=contain_measures+"-进行中-"+containMeasures.get(j).getPlanFinishDate();
							}else if (measureStatus==2){
								contain_measures=contain_measures+"-已完成-"+containMeasures.get(j).getRealFinishDate();
							}else if (measureStatus==3){
								contain_measures=contain_measures+"-已关闭-"+containMeasures.get(j).getCloseDate();
							}
							contain_measures=contain_measures+"\n";
							//遏制结果
							if(containMeasures.get(j).getContainResult()==null) {
								contain_measures=contain_measures+"遏制结果"+(j+1)+":暂无"+"\n";
							}else {
								contain_measures=contain_measures+"遏制结果"+(j+1)+":"+containMeasures.get(j).getContainResult()+"\n";
							}
						}
					}else {
						contain_measures="暂无遏制措施"+"\n";
					}
					//临时措施
					String temporary_measures="";
					List<HProCusMeasures> temporaryMeasures=HProCusMeasures.dao.find("select * from h_pro_cus_measures where issue_id="+issueId+" and type=3 and is_del=0");
					if (!temporaryMeasures.isEmpty()) {
						for (int j = 0; j < temporaryMeasures.size(); j++) {
							//临时措施
							temporary_measures=temporary_measures+"临时措施"+(j+1)+":"+temporaryMeasures.get(j).getMeasures()+"-"+temporaryMeasures.get(j).getUserName();
							int measureStatus=temporaryMeasures.get(j).getStatus();
							if (measureStatus==1) {
								temporary_measures=temporary_measures+"-进行中-"+temporaryMeasures.get(j).getPlanFinishDate();
							}else if (measureStatus==2){
								temporary_measures=temporary_measures+"-已完成-"+temporaryMeasures.get(j).getRealFinishDate();
							}else if (measureStatus==3){
								temporary_measures=temporary_measures+"-已关闭-"+temporaryMeasures.get(j).getCloseDate();
							}
							temporary_measures=temporary_measures+"\n";
							//有效性跟踪
							List<HProCusTrackHistory> measureTrackRecord=HProCusTrackHistory.dao.find("select * from h_pro_cus_track_history where measure_id="+temporaryMeasures.get(j).getId());
							if (measureTrackRecord.isEmpty()) {
								temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":暂无"+"\n";
							}else {
								String measureTracks="";
								for (int k = 0; k < measureTrackRecord.size(); k++) {
									measureTracks=measureTracks+measureTrackRecord.get(k).getRemark()+"；;";
								}
								temporary_measures=temporary_measures+"有效性跟踪"+(j+1)+":"+measureTracks+"\n";
							}
						}
					}else {
						temporary_measures="暂无临时措施";
					}
					String contain_s=contain_measures+temporary_measures;
					issueList.get(i).set("contain_s", contain_s);
					//根本原因分析 root_s
					String happen_reason="";//产生原因
					String avoid_happen_measures="";//防产生措施
					List<HProCusIssueReasonAnalysis> happenAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
					if (!happenAnalysis.isEmpty()) {
						happen_reason="产生原因："+"\n";
						avoid_happen_measures="防产生措施："+"\n";
						for (int j = 0; j < happenAnalysis.size(); j++) {
							happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"\n";
							List<HProCusMeasures> avoidHappen=HProCusMeasures.dao.find("select * from h_pro_cus_measures where reason_id="+happenAnalysis.get(j).getId()+" and is_del=0 and type=0");//对应防产生措施
							for (int k = 0; k < avoidHappen.size(); k++) {
								avoid_happen_measures=avoid_happen_measures+(j+1)+"."+(k+1)+avoidHappen.get(k).getMeasures()+"-"+avoidHappen.get(k).getUserName();
								int measureStatus=avoidHappen.get(k).getStatus();
								if (measureStatus==1) {
									avoid_happen_measures=avoid_happen_measures+"-进行中"+avoidHappen.get(k).getPlanFinishDate();
								}else if (measureStatus==2){
									avoid_happen_measures=avoid_happen_measures+"-已完成"+avoidHappen.get(k).getRealFinishDate();
								}else if (measureStatus==3){
									avoid_happen_measures=avoid_happen_measures+"-已关闭"+avoidHappen.get(k).getCloseDate();
								}
								avoid_happen_measures=avoid_happen_measures+"\n";
							}
						}
					}else {
						happen_reason="暂无产生原因"+"\n";
						avoid_happen_measures="暂无防产生措施"+"\n";
					}
					String runout_reason="";//流出原因
					String avoid_runout_measures="";//防流出措施
					List<HProCusIssueReasonAnalysis> runOutAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
					if (!runOutAnalysis.isEmpty()) {
						runout_reason="流出原因："+"\n";
						avoid_runout_measures="防流出措施："+"\n";
						for (int j = 0; j < runOutAnalysis.size(); j++) {
							runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"\n";
							List<HProCusMeasures> avoidRunout=HProCusMeasures.dao.find("select * from h_pro_cus_measures where reason_id="+runOutAnalysis.get(j).getId()+" and is_del=0 and type=1");//对应防流出措施
							for (int k = 0; k < avoidRunout.size(); k++) {
								avoid_runout_measures=avoid_runout_measures+(j+1)+"."+(k+1)+avoidRunout.get(k).getMeasures()+"-"+avoidRunout.get(k).getUserName();
								int measureStatus=avoidRunout.get(k).getStatus();
								if (measureStatus==1) {
									avoid_runout_measures=avoid_runout_measures+"-进行中"+avoidRunout.get(k).getPlanFinishDate();
								}else if (measureStatus==2){
									avoid_runout_measures=avoid_runout_measures+"-已完成"+avoidRunout.get(k).getRealFinishDate();
								}else if (measureStatus==3){
									avoid_runout_measures=avoid_runout_measures+"-已关闭"+avoidRunout.get(k).getCloseDate();
								}
								avoid_runout_measures=avoid_runout_measures+"\n";
							}
						}
					}else {
						runout_reason="暂无流出原因";
						avoid_runout_measures="暂无防流出措施";
					}
					String root_s=happen_reason+runout_reason;
					String avoid_s=avoid_happen_measures+avoid_runout_measures;
					issueList.get(i).set("root_s", root_s).set("avoid_s", avoid_s);
					//永久措施跟踪3月
					List<HProCusIssueMonthTrack> monthTracks=HProCusIssueMonthTrack.dao.find("select * from h_pro_cus_issue_month_track where issue_id="+issueId);
					issueList.get(i).set("month_1", " ").set("month_1_remark", "").set("month_1_date", "");
					issueList.get(i).set("month_2", " ").set("month_2_remark", "").set("month_2_date", "");
					issueList.get(i).set("month_3", " ").set("month_3_remark", "").set("month_3_date", "");
					String monthTrackString="";
					for (int j = 0; j < monthTracks.size(); j++) {
						int month=monthTracks.get(j).getMonth();
						if (month==1) {
							monthTrackString=monthTrackString+"第一月："+monthTracks.get(j).getRemark()+"\n";
							issueList.get(i).set("month_1", monthTracks.get(j).getStatus()).set("month_1_remark",  monthTracks.get(j).getRemark()).set("month_1_date",  monthTracks.get(j).getTrackDate());
						}else if (month==2) {
							monthTrackString=monthTrackString+"第二月："+monthTracks.get(j).getRemark()+"\n";
							issueList.get(i).set("month_2", monthTracks.get(j).getStatus()).set("month_2_remark", monthTracks.get(j).getRemark()).set("month_2_date",  monthTracks.get(j).getTrackDate());
						}else if (month==3) {
							monthTrackString=monthTrackString+"第三月："+monthTracks.get(j).getRemark()+"\n";
							issueList.get(i).set("month_3", monthTracks.get(j).getStatus()).set("month_3_remark", monthTracks.get(j).getRemark()).set("month_3_date",  monthTracks.get(j).getTrackDate());
						}
					}
					//OPL整体跟踪记录
					List<HProCusIssueTrackRecord> trackRecords=HProCusIssueTrackRecord.dao.find("select * from h_pro_cus_issue_track_record where issue_id="+issueId);
					String  oplTrackRecord="";
					for (int j = 0; j < trackRecords.size(); j++) {
						oplTrackRecord=oplTrackRecord+(j+1)+"."+trackRecords.get(j).getTrackRecord()+"-"+trackRecords.get(j).getCreateUserName()+"-"+sdf1.format(trackRecords.get(j).getCreateTime())+"\n";
					}
					String record=" ";
					if (!"".equals(oplTrackRecord)) {
						record=oplTrackRecord;
					}
					if (!"".equals(monthTrackString)) {
						record=record+monthTrackString;
					}
					issueList.get(i).set("record", record);
					//8D计划
					Record issuePlan=new Record();
					issuePlan.setColumns(issueList.get(i));
					Record issueReal=new Record();
					issueReal.setColumns(issueList.get(i));
					HProCusIssue8dPlan plans=HProCusIssue8dPlan.dao.findFirst("select * from h_pro_cus_issue_8d_plan where issue_id="+issuePlan.getLong("id"));
					issuePlan.set("plan_real", "计划")
					.set("d_one", plans.getDOne()).set("d_two", plans.getDTwo())
					.set("d_three", plans.getDThree()).set("d_four", plans.getDFour())
					.set("d_five", plans.getDFive()).set("d_six", plans.getDSix())
					.set("d_seven", plans.getDSeven());
					//8D实际
					HProCusIssue8dReal reals=HProCusIssue8dReal.dao.findFirst("select * from h_pro_cus_issue_8d_real where issue_id="+issueReal.getLong("id"));
					issueReal.set("plan_real", "实际")
					.set("d_one", " ").set("d_two", " ")
					.set("d_three", " ").set("d_four", " ")
					.set("d_five", " ").set("d_six", " ")
					.set("d_seven", " ")
					.set("d_one_status", "0").set("d_two_status","0")
					.set("d_three_status", "0").set("d_four_status", "0")
					.set("d_five_status", "0").set("d_six_status", "0")
					.set("d_seven_status", "0");
					if (reals!=null) {
						issueReal
						.set("d_one_status", reals.getDOneStatus()).set("d_two_status",reals.getDTwoStatus())
						.set("d_three_status", reals.getDThreeStatus()).set("d_four_status", reals.getDFourStatus())
						.set("d_five_status", reals.getDFiveStatus()).set("d_six_status", reals.getDSixStatus())
						.set("d_seven_status", reals.getDSevenStatus());
						if (reals.getDOneStatus()==2) {
							issueReal.set("d_one", reals.getDOne());
						}
						if (reals.getDTwoStatus()==2) {
							issueReal.set("d_two", reals.getDTwo());
						}
						if (reals.getDThreeStatus()==2) {
							issueReal.set("d_three", reals.getDThree());
						}
						if (reals.getDFourStatus()==2) {
							issueReal.set("d_four", reals.getDFour());
						}
						if (reals.getDFiveStatus()==2) {
							issueReal.set("d_five", reals.getDFive());
						}
						if (reals.getDSixStatus()==2) {
							issueReal.set("d_six", reals.getDSix());
						}
						if (reals.getDSevenStatus()==2) {
							issueReal.set("d_seven", reals.getDSeven());
						}
					}
					cusOPLList.add(issuePlan);
					cusOPLList.add(issueReal);
				}
				//赋值到excel
				for (int j = 0; j < cusOPLList.size(); j++) {
					HSSFRow rowIndex = sheet.createRow(j+2);
			        HSSFCell cell_20 = rowIndex.createCell(0);
			        cell_20.setCellStyle(style);
			        cell_20.setCellValue(cusOPLList.get(j).getStr("report_no"));
			        HSSFCell cell_21 = rowIndex.createCell(1);
			        cell_21.setCellStyle(style);
			        cell_21.setCellValue(cusOPLList.get(j).getStr("status"));
			        HSSFCell cell_22 = rowIndex.createCell(2);
			        cell_22.setCellStyle(style);
			        cell_22.setCellValue(cusOPLList.get(j).getStr("dep_name"));
			        HSSFCell cell_23 = rowIndex.createCell(3);
			        cell_23.setCellStyle(style);
			        cell_23.setCellValue(cusOPLList.get(j).getStr("pro_name"));
			        HSSFCell cell_24 = rowIndex.createCell(4);
			        cell_24.setCellStyle(style);
			        cell_24.setCellValue(cusOPLList.get(j).getStr("pro_code"));
			        HSSFCell cell_25 = rowIndex.createCell(5);
			        cell_25.setCellStyle(style);
			        cell_25.setCellValue(cusOPLList.get(j).getStr("open_time"));
			        HSSFCell cell_26 = rowIndex.createCell(6);
			        cell_26.setCellStyle(style);
			        cell_26.setCellValue(cusOPLList.get(j).getStr("plan_real"));
			        HSSFCell cell_27 = rowIndex.createCell(7);
			        cell_27.setCellStyle(style);
			        cell_27.setCellValue(cusOPLList.get(j).getStr("d_one"));
			        HSSFCell cell_28 = rowIndex.createCell(8);
			        cell_28.setCellStyle(style);
			        cell_28.setCellValue(cusOPLList.get(j).getStr("d_two"));
			        HSSFCell cell_29 = rowIndex.createCell(9);
			        cell_29.setCellStyle(style);
			        cell_29.setCellValue(cusOPLList.get(j).getStr("d_three"));
					HSSFCell cell_210 = rowIndex.createCell(10);
					cell_210.setCellStyle(style);
					cell_210.setCellValue(cusOPLList.get(j).getStr("d_four"));
			        HSSFCell cell_211 = rowIndex.createCell(11);
			        cell_211.setCellStyle(style);
			        cell_211.setCellValue(cusOPLList.get(j).getStr("d_five"));
			        HSSFCell cell_212 = rowIndex.createCell(12);
			        cell_212.setCellStyle(style);
			        cell_212.setCellValue(cusOPLList.get(j).getStr("d_six"));
			        HSSFCell cell_213 = rowIndex.createCell(13);
			        cell_213.setCellStyle(style);
			        cell_213.setCellValue(cusOPLList.get(j).getStr("d_seven"));
			        HSSFCell cell_214 = rowIndex.createCell(14);
			        cell_214.setCellStyle(style);
			        cell_214.setCellValue(cusOPLList.get(j).getStr("cus_name"));
			        HSSFCell cell_215 = rowIndex.createCell(15);
			        cell_215.setCellStyle(style);
			        cell_215.setCellValue(cusOPLList.get(j).getStr("type"));
			        HSSFCell cell_216 = rowIndex.createCell(16);
			        cell_216.setCellStyle(style);
			        cell_216.setCellValue(cusOPLList.get(j).getStr("source"));
			        HSSFCell cell_217 = rowIndex.createCell(17);
			        cell_217.setCellStyle(style);
			        cell_217.setCellValue(cusOPLList.get(j).getStr("happen_frequency"));
			        HSSFCell cell_218 = rowIndex.createCell(18);
			        cell_218.setCellStyle(style);
			        cell_218.setCellValue(cusOPLList.get(j).getStr("description"));
			        HSSFCell cell_219 = rowIndex.createCell(19);
			        cell_219.setCellStyle(style);
			        cell_219.setCellValue(cusOPLList.get(j).getStr("contain_s"));
			        HSSFCell cell_220 = rowIndex.createCell(20);
			        cell_220.setCellStyle(style);
			        cell_220.setCellValue(cusOPLList.get(j).getStr("root_s"));
			        HSSFCell cell_221 = rowIndex.createCell(21);
			        cell_221.setCellStyle(style);
			        cell_221.setCellValue(cusOPLList.get(j).getStr("avoid_s"));
			        HSSFCell cell_222 = rowIndex.createCell(22);
			        cell_222.setCellStyle(style);
			        cell_222.setCellValue(cusOPLList.get(j).getStr("permanent_measures_time"));
			        HSSFCell cell_223 = rowIndex.createCell(23);
			        cell_223.setCellStyle(style);
			        cell_223.setCellValue(cusOPLList.get(j).getStr("month_1"));
			        HSSFCell cell_224 = rowIndex.createCell(24);
			        cell_224.setCellStyle(style);
			        cell_224.setCellValue(cusOPLList.get(j).getStr("month_2"));
			        HSSFCell cell_225 = rowIndex.createCell(25);
			        cell_225.setCellStyle(style);
			        cell_225.setCellValue(cusOPLList.get(j).getStr("month_3"));
			        HSSFCell cell_226 = rowIndex.createCell(26);
			        cell_226.setCellStyle(style);
			        cell_226.setCellValue(cusOPLList.get(j).getStr("duty_user_name"));
			        HSSFCell cell_227 = rowIndex.createCell(27);
			        cell_227.setCellStyle(style);
			        cell_227.setCellValue(cusOPLList.get(j).getStr("upgrade_user_name"));
			        HSSFCell cell_228 = rowIndex.createCell(28);
			        cell_228.setCellStyle(style);
			        cell_228.setCellValue(cusOPLList.get(j).getStr("close_time"));
			        HSSFCell cell_229 = rowIndex.createCell(29);
			        cell_229.setCellStyle(style);
			        cell_229.setCellValue(cusOPLList.get(j).getStr("urge_plan"));
			        HSSFCell cell_230 = rowIndex.createCell(30);
			        cell_230.setCellStyle(style);
			        cell_230.setCellValue(cusOPLList.get(j).getStr("record"));
					if (j%2!=0) {
						for (int i = 0; i < 6; i++) {
				        	CellRangeAddress regionCon = new CellRangeAddress(j+1, j+2, i, i);
					        sheet.addMergedRegion(regionCon);
						}
				        for (int i = 14; i <=30; i++) {
				        	CellRangeAddress regionCon = new CellRangeAddress(j+1, j+2, i, i);
					        sheet.addMergedRegion(regionCon);
						}
					}
				}
				File file = new File("D:\\SysFujianFiles\\openIssue\\upload\\moban\\新品客诉OPL.xls");
		        FileOutputStream fout = new FileOutputStream(file);
		        workbook.write(fout);
		        fout.close();
				req.set("code", 0);
				req.set("msg", "成功");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				req.set("code", 1);
				req.set("msg", "失败！");
				return false;
			}
		});
		renderJson(req);
	}
}
