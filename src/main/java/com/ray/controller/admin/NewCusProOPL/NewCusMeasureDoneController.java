package com.ray.controller.admin.NewCusProOPL;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import com.jfinal.core.Controller;
import com.jfinal.json.FastJson;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.HProCusIssue;
import com.ray.common.model.HProCusIssueReasonAnalysis;
import com.ray.common.model.HProCusMeasures;
import com.ray.common.model.HProCusTrackHistory;
import com.ray.common.model.User;
/**
 * 新品客诉 措施执行  newCusMeasureDone/toMyMeasureProOPL
 * @author FL00024996
 *
 */
public class NewCusMeasureDoneController extends Controller {
	public void toMyMeasureProOPL() {
		render("newCusMeasureDone.html");
	}
	public void getMyMeasure() {
		Record loginUser=getSessionAttr("user");
		User user=User.dao.findById(loginUser.getInt("id"));
		StringBuilder sb=new StringBuilder();
		sb.append(" from h_pro_cus_issue a,h_cause_dep b,h_pro_cus_measures c,h_quality_issue_type e "
				+ "where a.is_del=0 and a.type_id=e.id  and a.dep_id=b.dep_id and a.id=c.issue_id and c.is_del=0 and c.user_id="+user.getId());
		String selOPLdes=get("selOPLdes");
		if (!"".equals(selOPLdes)&& selOPLdes!=null) {
			sb.append(" and a.description like '%"+selOPLdes+"%'");
		}
		String selProCode=get("selProCode");
		if (!"".equals(selProCode)&& selProCode!=null) {
			sb.append(" and a.pro_code like '%"+selProCode+"%'");
		}
		String selProductName=get("selProductName");
		if (!"".equals(selProductName)&& selProductName!=null) {
			sb.append(" and a.product_name like '%"+selProductName+"%'");
		}
		String selCusName=get("selCusName");
		if (!"".equals(selCusName)&& selCusName!=null) {
			sb.append(" and a.cus_name like '%"+selCusName+"%'");
		}
		String selStatus=get("selStatus");
		if (!"".equals(selStatus)&& selStatus!=null) {
			sb.append(" and c.status="+selStatus);
		}
		int page=getInt("currentPage");
		int limit=getInt("pageSize");
		Page<Record> issueList=Db.paginate(page, limit, "select a.*,b.dep_name,e.type,"
				+ "c.measures,c.status as measure_status,c.plan_finish_date as measure_plan_finish_date,"
				+ "c.real_finish_date as measure_real_finish_date,c.close_date as measure_close_date,c.id as measure_id, "
				+ "c.type as measure_type,c.contain_result ",
				sb.toString()+" order by id desc");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < issueList.getList().size(); i++) {
			long issueId=issueList.getList().get(i).getLong("id");
			//遏制措施
			if (issueList.getList().get(i).getInt("measure_type")==2) {
				issueList.getList().get(i).set("measures", "遏制措施："+issueList.getList().get(i).get("measures"));
			}
			//临时措施
			if (issueList.getList().get(i).getInt("measure_type")==3) {
				issueList.getList().get(i).set("measures", "临时措施："+issueList.getList().get(i).get("measures"));
			}
			//防产生措施
			if (issueList.getList().get(i).getInt("measure_type")==0) {
				issueList.getList().get(i).set("measures", "防产生措施："+issueList.getList().get(i).get("measures"));
			}
			//防流出措施
			if (issueList.getList().get(i).getInt("measure_type")==1) {
				issueList.getList().get(i).set("measures", "防流出措施："+issueList.getList().get(i).get("measures"));
			}
			//跟踪记录or临时措施有效性
			List<HProCusTrackHistory> measureTrackHistory=HProCusTrackHistory.dao.find("select * from h_pro_cus_track_history where measure_id="+issueList.getList().get(i).getLong("measure_id"));
			String measureTrack="";
			for (int j = 0; j < measureTrackHistory.size(); j++) {
				measureTrack=measureTrack+(j+1)+measureTrackHistory.get(j).getRemark()+"-"+measureTrackHistory.get(j).getTrackUserName()+"-"+sdf.format(measureTrackHistory.get(j).getCreateTime())+"<br>";
			}
			issueList.getList().get(i).set("measure_tracks", measureTrack);
			//根本原因分析
			String happen_reason="";//产生原因
			List<HProCusIssueReasonAnalysis> happenAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=0 and issue_id="+issueId+" and status=0");
			if (!happenAnalysis.isEmpty()) {
				happen_reason="产生原因："+"<br>";
				for (int j = 0; j < happenAnalysis.size(); j++) {
					happen_reason=happen_reason+(j+1)+"、"+happenAnalysis.get(j).getReasonAnalysis()+"<br>";
				}
			}else {
				happen_reason="暂无产生原因"+"<br>";
			}
			String runout_reason="";//流出原因
			List<HProCusIssueReasonAnalysis> runOutAnalysis=HProCusIssueReasonAnalysis.dao.find("select * from h_pro_cus_issue_reason_analysis where type=1 and issue_id="+issueId+" and status=0");
			if (!runOutAnalysis.isEmpty()) {
				runout_reason="流出原因："+"<br>";
				for (int j = 0; j < runOutAnalysis.size(); j++) {
					runout_reason=runout_reason+(j+1)+"、"+runOutAnalysis.get(j).getReasonAnalysis()+"<br>";
				}
			}else {
				runout_reason="暂无流出原因";
			}
			String root_s=happen_reason+runout_reason;
			issueList.getList().get(i).set("root_s", root_s);
		}
		Record record=new Record();
		record.set("code", 0);
		record.set("msg", "获取成功");
		record.set("list", issueList.getList());
		record.set("totalResult", issueList.getTotalRow());
		renderJson(record);
	}
	/**
	 * 完成执行措施
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年4月25日 下午1:45:47
	 */
	public void finishMeasure() {
		try {
			boolean flag = Db.tx(() -> {
				long measureId=getParaToLong("measure_id");
				HProCusMeasures measures=HProCusMeasures.dao.findById(measureId);
				measures.setStatus(2).setRealFinishDate(new Date());
				measures.update();
				//钉钉通知责任人
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				HProCusIssue cusIssue=HProCusIssue.dao.findById(measures.getIssueId());
				User dutyUser=User.dao.findById(cusIssue.getDutyUserId().intValue());
				DingMessage.sendText(dutyUser.getDingUserId(), measures.getUserName()+"已完成产品名称为【"+cusIssue.getProductName()+"】的质量OPL措施，请知悉！\n"+sdf.format(new Date()), "23");//23-新品客诉OPL责任人
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "操作成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "操作失败：" + e.getMessage()));
		}
	}
	/**
	 * 遏制措施完成
	 * @author simple
	 * @contact 15228717200
	 * @time 2021年5月20日 下午2:07:54
	 */
	@SuppressWarnings("rawtypes")
	public void saveContainResult() {
		try {
			boolean flag = Db.tx(() -> {
				Map map=FastJson.getJson().parse(get("formData"), Map.class);
				long measureId=Long.valueOf(map.get("measureId").toString());
				HProCusMeasures containMeasure=HProCusMeasures.dao.findById(measureId);
				containMeasure.setStatus(2).setRealFinishDate(new Date())
				.setContainResult(map.get("contain_result").toString());
				containMeasure.update();
				//钉钉通知责任人
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				HProCusIssue cusIssue=HProCusIssue.dao.findById(containMeasure.getIssueId());
				User dutyUser=User.dao.findById(cusIssue.getDutyUserId().intValue());
				DingMessage.sendText(dutyUser.getDingUserId(), containMeasure.getUserName()+"已完成产品名称为【"+cusIssue.getProductName()+"】的质量OPL措施，请知悉！\n"+sdf.format(new Date()), "23");//23-新品客诉OPL责任人
				return true;
			});
			if (flag) {
				renderJson(Ret.ok("msg", "操作成功！"));
			}else {
				renderJson(Ret.fail("msg", "操作失败，请联系管理员"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}
}
