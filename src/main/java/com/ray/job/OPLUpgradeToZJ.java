package com.ray.job;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.User;
import com.ray.common.quartz.AbsJob;
import com.ray.util.HyCommenMethods;
import org.quartz.JobExecutionContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * OPL自动升级
 * @author FL00024996
 *
 */
public class OPLUpgradeToZJ extends AbsJob {
	@Override
	protected void process(JobExecutionContext context) {
		try {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			//质量OPL升级到总监和总经理  时间按照此次更新后的数据
			List<Record> qtyIssues=Db.find("   select i.id,i.is_upgrade_zj,i.zj_user_id,i.zj_user_name,i.is_upgrade_zjl,i.product_name,\n" +
					"\tp.d_one p_one,p.d_two p_two,p.d_three p_three,p.d_four p_four,p.d_five p_five,p.d_six p_six,p.d_seven p_seven,\n" +
					"  r.d_one  ,r.d_two  ,r.d_three  ,r.d_four  ,r.d_five  ,r.d_six  ,r.d_seven  \n" +
					"\tfrom h_quality_issue i,h_qty_issue_8d_plan p,h_qty_issue_8d_real r where \n" +
					"\ti.id = p.issue_id and i.id = r.issue_id\n" +
					"  and i.is_del=0 and i.issue_status=0   and i.level != 'C' and i.status != 'G' \n" +
					"\tand i.zj_user_id is not null and (is_upgrade_zj = 0 or is_upgrade_zjl = 0 ) \n" +
					"  and DATE_SUB(CURDATE(), INTERVAL 40 DAY) <= date(create_time) \n" +
					"  and DATE_SUB(CURDATE(), INTERVAL 40 DAY) <= date(open_time)\n" +
					"  order by i.id desc  ");
			for (int i = 0; i < qtyIssues.size(); i++) {
				int upgrade = judgeIsDelay(qtyIssues.get(i));
				String id = qtyIssues.get(i).getStr("id");
				if (upgrade == 1){
					Db.update(" update h_quality_issue set is_upgrade_zj = 1 where id = "+qtyIssues.get(i).getStr("id"));
					//钉钉消息通知升级对象
					String zjUserId = qtyIssues.get(i).getStr("zj_user_id");
					if (zjUserId != null){
						User upUser=User.dao.findById(qtyIssues.get(i).getStr("zj_user_id"));
						DingMessage.sendText(upUser.getDingUserId(),"产品名称\n【"+qtyIssues.get(i).getStr("product_name")+"】关联的质量OPL已自动升级至总监OPL，等待您的处理！\n"+sdf.format(new Date()), "15");//15-质量OPL措施升级处理页面
					}
				}else if(upgrade == 2){
					Db.update(" update h_quality_issue set is_upgrade_zjl = 1 where id = "+qtyIssues.get(i).getStr("id"));
					//钉钉消息通知 总经理和副总
					try {
						DingMessage.sendText("FL00000103","产品名称\n【"+qtyIssues.get(i).getStr("product_name")+"】关联的质量OPL已自动升级至总经理OPL，等待您的处理！\n"+sdf.format(new Date()), "15");//15-质量OPL措施升级处理页面
						DingMessage.sendText("FL00000102","产品名称\n【"+qtyIssues.get(i).getStr("product_name")+"】关联的质量OPL已自动升级至总经理OPL，等待您的处理！\n"+sdf.format(new Date()), "15");//15-质量OPL措施升级处理页面
					}catch (Exception e){
						System.out.println(e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 判断是否延期升级
	 * @param issue
	 * @return  0 不升级   1升级总监  2升级总经理
	 */
	private int judgeIsDelay(Record issue) throws ParseException {
		int upgrade = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String nowDate = sdf.format(new Date());
		List<String> timeList = Arrays.asList("_four", "_five", "_six", "_seven");
		//当计划时间和实际时间存在，跳过
		//当计划时间存在，实际时间不存在，对比计划时间和当前时间
		//当计划时间不存在，break，当对比计划时间和当前时间之后，break
		for (String s : timeList) {
			String plan = issue.getStr("p" + s);
			String real = issue.getStr("d" + s);
			if (plan != null && real == null){
				Date parse = sdf.parse(plan);
				Date planAdd11 = HyCommenMethods.addDate(parse, 11);
				String plan11 = sdf.format(planAdd11);
				Date planAdd30 = HyCommenMethods.addDate(parse, 30);
				String plan30 = sdf.format(planAdd30);
				//如果当前时间在计划时间之后  =  当前时间大于计划时间
				if (nowDate.compareTo(plan11) > 0 && nowDate.compareTo(plan30) < 0 ){
					if (issue.getInt("is_upgrade_zj") == 1){
						upgrade = 0;
					}else{
						upgrade = 1;
					}
				}else if(nowDate.compareTo(plan30) > 0){
					if (issue.getInt("is_upgrade_zjl") == 1){
						upgrade = 0;
					}else{
						upgrade = 2;
					}
				}
				break;
			}
		}
		return upgrade;
	}

}






