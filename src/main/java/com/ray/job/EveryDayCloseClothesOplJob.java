package com.ray.job;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.model.HClothesIssue;
import com.ray.common.model.HClothesIssue8dPlan;
import com.ray.common.model.HClothesIssueReasonAnalysis;
import com.ray.common.model.HClothesMeasures;
import com.ray.common.model.HClothesTrackHistory;
import com.ray.common.model.HProIssueReasonAnalysis;
import com.ray.common.model.HProIssueTell;
import com.ray.common.model.HProissueTrackHistory;
import com.ray.common.model.HProjectIssue;
import com.ray.common.model.HProjectMeasures;
import com.ray.common.model.HQtyIssue8dPlan;
import com.ray.common.model.HQtyIssueReasonAnalysis;
import com.ray.common.model.HQtyMeasures;
import com.ray.common.model.HQtyTrackHistory;
import com.ray.common.model.HQualityIssue;
import com.ray.common.model.User;
import com.ray.common.quartz.AbsJob;
import java.util.List;

/**
 * @author kangle
 * @version 1.0
 * @description: 每天执行定时任务  关闭手工样件opl
 * @date 2022-6-16 9:41
 */

public class EveryDayCloseClothesOplJob extends AbsJob {

    @Override
    protected void process(org.quartz.JobExecutionContext context) {
    	try {
    		//初始化执行手工OPL状态
            List<Record> AllClothesOpl = Db.find(" select * from h_clothes_issue_8d_real where issue_id in( select id from h_clothes_issue where status = 'Y' )  ");
            AllClothesOpl.stream().forEach(i->{
                String d_one_status = i.getStr("d_one_status");
                String d_two_status = i.getStr("d_two_status");
                String d_three_status = i.getStr("d_three_status");
                if ("2".equals(d_one_status) && "2".equals(d_two_status) && "2".equals(d_three_status)){
                    String issue_id = i.getStr("issue_id");
                    Db.update(" update  h_clothes_issue set status = 'G' where id = '"+issue_id+"' ");
                    //更新关键opl状态
                    Db.update(" update h_project_issue set status = 'G' where quality_issue_id='"+issue_id+"'");
                }
            });
            //2022-6-16 修复所有关键opl数据---simple
            Db.update("update h_project_issue set is_del=1 where (quality_issue_id IS NOT NULL OR handle_issue_id IS NOT NULL)");
            //同步质量OPL
            List<HQualityIssue> qtyIssues = HQualityIssue.dao.find("SELECT * FROM h_quality_issue WHERE (product_stage='手工样件' OR product_stage='OTS' OR product_stage='PPAP') AND is_del=0 AND pro_id IS NOT NULL AND pro_id<>0");
            for (int j = 0; j < qtyIssues.size(); j++) {
            	Record proInfo=Db.use("bud").findById("file_pro_info", qtyIssues.get(j).getProId().intValue());
            	String proCode="";
            	String category_name="";
            	int zhihuiUserId=0;
            	String zhihuiUserName="";
            	if (proInfo!=null) {
            		proCode = proInfo.getStr("pro_code");
            		category_name = proInfo.getStr("category_name");
            		Record proDutyUser=Db.use("bud").findFirst("select * from pro_member where pro_code='"+proCode+"' and role_name='责任人'");
        			User zhihuiUser=User.dao.findFirst("select * from user where ding_user_id='"+proDutyUser.getStr("role_member_id")+"'");
        			zhihuiUserId = zhihuiUser.getId().intValue();
        			zhihuiUserName = zhihuiUser.getNickname();
				}
            	//阶段问题
            	String stage="";
            	if ("手工样件".equals(qtyIssues.get(j).getProductStage())) {
					stage="P2";
				}else if ("OTS".equals(qtyIssues.get(j).getProductStage())) {
					stage="P3";
				}else if ("PPAP".equals(qtyIssues.get(j).getProductStage())) {
					stage="P4";
				}
    			//计划完成时间
    			HQtyIssue8dPlan dPlan=HQtyIssue8dPlan.dao.findFirst("select * from h_qty_issue_8d_plan where issue_id="+qtyIssues.get(j).getId());
    			HProjectIssue projectIssue = HProjectIssue.dao.findFirst("select * from h_project_issue where quality_issue_id="+qtyIssues.get(j).getId());
    			if (projectIssue==null) {//新增
    				projectIssue=new HProjectIssue();
    				projectIssue
    				.setQualityIssueId(qtyIssues.get(j).getId())
    				.setProId(qtyIssues.get(j).getProId().intValue()).setDepId(qtyIssues.get(j).getDepId()).setProCode(proCode)
    				.setLineId(qtyIssues.get(j).getLineId().intValue())
    				.setProName(qtyIssues.get(j).getProName())
    				.setProductName(category_name)
    				.setStatus(qtyIssues.get(j).getStatus())
    				.setStage(stage)
    				.set("propose_time",qtyIssues.get(j).getOpenTime())
    				.setSeverity(qtyIssues.get(j).getLevel())
    				.setIssueType("质量")
    				.setIssueSource(qtyIssues.get(j).getSource())
    				.setIssueDescription(qtyIssues.get(j).getDescription())
    				.setUpgradeUserId(qtyIssues.get(j).getUpgradeUserId()).setUpgradeUserName(qtyIssues.get(j).getUpgradeUserName())
    				.setDutyUserId(qtyIssues.get(j).getDutyUserId()).setDutyUserName(qtyIssues.get(j).getDutyUserName())
    				.setConfirmerId(qtyIssues.get(j).getSureUserId()).setConfirmerName(qtyIssues.get(j).getSureUserName())
    				.setCreateTime(qtyIssues.get(j).getCreateTime())
    				.setCreateUserId(qtyIssues.get(j).getCreateUserId()).setCreateUserName(qtyIssues.get(j).getCreateUserName())
    				.setIsDel(0).setIssueStatus(qtyIssues.get(j).getIssueStatus());
    				//计划完成时间
    				if (dPlan.getDSeven()!=null) {
    					projectIssue.setPlanFinishTime(com.ray.util.HyCommenMethods.addDate(dPlan.getDSeven(), 90));
    				}
    				//状态
    				projectIssue.setStatus(qtyIssues.get(j).getStatus());
    				//实际完成时间
    				projectIssue.setRealFinishTime(qtyIssues.get(j).getCloseTime());
    				projectIssue.save();
    				//知会人
    				HProIssueTell issueTell=new HProIssueTell();
    				issueTell.setIssueId(projectIssue.getId())
    				.setTellUserId(zhihuiUserId).setTellUserName(zhihuiUserName)
    				.setStatus(0);
    				issueTell.save();
    			}else {//修改
    				projectIssue
    				.setQualityIssueId(qtyIssues.get(j).getId())
    				.setProId(qtyIssues.get(j).getProId().intValue()).setDepId(qtyIssues.get(j).getDepId()).setProCode(proCode)
    				.setLineId(qtyIssues.get(j).getLineId().intValue())
    				.setProName(qtyIssues.get(j).getProName())
    				.setProductName(category_name)
    				.setStatus(qtyIssues.get(j).getStatus())
    				.setStage(stage)
    				.set("propose_time",qtyIssues.get(j).getOpenTime())
    				.setSeverity(qtyIssues.get(j).getLevel())
    				.setIssueType("质量")
    				.setIssueSource(qtyIssues.get(j).getSource())
    				.setIssueDescription(qtyIssues.get(j).getDescription())
    				.setUpgradeUserId(qtyIssues.get(j).getUpgradeUserId()).setUpgradeUserName(qtyIssues.get(j).getUpgradeUserName())
    				.setDutyUserId(qtyIssues.get(j).getDutyUserId()).setDutyUserName(qtyIssues.get(j).getDutyUserName())
    				.setConfirmerId(qtyIssues.get(j).getSureUserId()).setConfirmerName(qtyIssues.get(j).getSureUserName())
    				.setCreateTime(qtyIssues.get(j).getCreateTime())
    				.setCreateUserId(qtyIssues.get(j).getCreateUserId()).setCreateUserName(qtyIssues.get(j).getCreateUserName())
    				.setIsDel(0).setIssueStatus(qtyIssues.get(j).getIssueStatus());
    				if (dPlan.getDSeven()!=null) {
    					projectIssue.setPlanFinishTime(com.ray.util.HyCommenMethods.addDate(dPlan.getDSeven(), 90));
    				}
    				//状态
    				projectIssue.setStatus(qtyIssues.get(j).getStatus());
    				//实际完成时间
    				projectIssue.setRealFinishTime(qtyIssues.get(j).getCloseTime());
    				projectIssue.update();
    				//知会人
    				HProIssueTell issueTell=HProIssueTell.dao.findFirst("select * from h_pro_issue_tell where issue_id="+projectIssue.getId()+" and status=0");
    				if (issueTell==null) {
    					issueTell=new HProIssueTell();
    					issueTell.setIssueId(projectIssue.getId())
    					.setTellUserId(zhihuiUserId).setTellUserName(zhihuiUserName)
    					.setStatus(0);
    					issueTell.save();
    				}else {
    					issueTell.setTellUserId(zhihuiUserId).setTellUserName(zhihuiUserName)
    					.setStatus(0);
    					issueTell.update();
    				}
    			}
    			//回写质量OPL中项目OPL id
    			qtyIssues.get(j).setProIssueId(projectIssue.getId());
    			qtyIssues.get(j).update();
    			//原因分析同步
    			List<HQtyIssueReasonAnalysis> reasonAnalysis = HQtyIssueReasonAnalysis.dao.find("select * from h_qty_issue_reason_analysis where status=0 and issue_id="+qtyIssues.get(j).getId());
    			for (int k = 0; k < reasonAnalysis.size();k++) {
    				HProIssueReasonAnalysis proReasonAnalysis=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where qty_reason_id="+reasonAnalysis.get(k).getId());
    				if (proReasonAnalysis==null) {
    					proReasonAnalysis=new HProIssueReasonAnalysis();
    					proReasonAnalysis.setQtyReasonId(reasonAnalysis.get(k).getId()) 
    					.setIssueId(projectIssue.getId())
    					.setReasonAnalysis(reasonAnalysis.get(k).getType().intValue()==0?"产生原因：":"根本原因："+reasonAnalysis.get(k).getReasonAnalysis())
    					.setStatus(0).setIsXuni(0);
    					proReasonAnalysis.save();
    					proReasonAnalysis.setLsIndex(proReasonAnalysis.getId());
    					proReasonAnalysis.update();
    				}else {
    					proReasonAnalysis.setQtyReasonId(reasonAnalysis.get(k).getId()) 
    					.setIssueId(projectIssue.getId())
    					.setReasonAnalysis(reasonAnalysis.get(k).getType().intValue()==0?"产生原因：":"根本原因："+reasonAnalysis.get(k).getReasonAnalysis())
    					.setStatus(0).setIsXuni(0)
    					.setLsIndex(proReasonAnalysis.getId());
    					proReasonAnalysis.update();
    				}
    			}
    			//临时措施抓取“D3遏制/临时措施及措施有效性跟踪”
    			//措施同步-2=遏制
    			List<HQtyMeasures> ezqtyMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where is_del=0 and issue_id="+qtyIssues.get(j).getId()+" and type=2");
    			for (int k = 0; k < ezqtyMeasures.size(); k++) {
    				HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  qty_measure_id="+ezqtyMeasures.get(k).getId());
    				HProIssueReasonAnalysis xuniReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where issue_id="+projectIssue.getId()+" and is_xuni=1 ");
    				if (xuniReason==null) {
    					xuniReason=new HProIssueReasonAnalysis();
    					xuniReason.setIssueId(projectIssue.getId()).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1);
    					xuniReason.save();
    					xuniReason.setLsIndex(xuniReason.getId());
    					xuniReason.update();
    				}else {
    					xuniReason.setIssueId(projectIssue.getId()).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1)
    					.setLsIndex(xuniReason.getId());
    					xuniReason.update();
    				}
    				int status=1;
    				if (ezqtyMeasures.get(k).getStatus().intValue()==2||ezqtyMeasures.get(k).getStatus().intValue()==3) {
    					status=2;
					}
    				if (proMeasure==null) {
    					proMeasure=new HProjectMeasures();
    					proMeasure.setIssueId(projectIssue.getId()).setReasonId(xuniReason.getId())
    					.setXuhao("1."+(k+1)).setMeasures(ezqtyMeasures.get(k).getMeasures()+"。遏制结果："+ezqtyMeasures.get(k).getContainResult())
    					.setUserId(ezqtyMeasures.get(k).getUserId()).setUserName(ezqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(ezqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(0)
    					.setQtyMeasureId(ezqtyMeasures.get(k).getId())
    					.setRealFinishDate(ezqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.save();
    				}else {
    					proMeasure.setIssueId(projectIssue.getId()).setReasonId(xuniReason.getId())
    					.setXuhao("1."+(k+1)).setMeasures(ezqtyMeasures.get(k).getMeasures()+"。遏制结果："+ezqtyMeasures.get(k).getContainResult())
    					.setUserId(ezqtyMeasures.get(k).getUserId()).setUserName(ezqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(ezqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(0)
    					.setQtyMeasureId(ezqtyMeasures.get(k).getId())
    					.setRealFinishDate(ezqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.update();
    				}
    				//2022-6-16 增加 措施跟踪同步
    				List<HQtyTrackHistory> trackHistorys=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+ezqtyMeasures.get(k).getId());
    				for (int l = 0; l < trackHistorys.size(); l++) {
    					HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where qty_track_history_id="+trackHistorys.get(l).getId());
    					if (proMeasureTrack==null) {
    						proMeasureTrack = new HProissueTrackHistory();
    						proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
    						.setQtyTrackHistoryId(trackHistorys.get(l).getId())
    						.setMeasureStatus(trackHistorys.get(l).getMeasureStatus()).setRemark(trackHistorys.get(l).getRemark())
    						.setTrackUserId(trackHistorys.get(l).getTrackUserId()).setTrackUserName(trackHistorys.get(l).getTrackUserName())
    						.setCreateTime(trackHistorys.get(l).getCreateTime());
    						proMeasureTrack.save();
    					}
    				}
    			}
    			//措施同步-3=临时
    			List<HQtyMeasures> lsqtyMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where is_del=0 and issue_id="+qtyIssues.get(j).getId()+" and type=3");
    			for (int k = 0; k < lsqtyMeasures.size(); k++) {
    				HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  qty_measure_id="+lsqtyMeasures.get(k).getId());
    				HProIssueReasonAnalysis xuniReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where issue_id="+projectIssue.getId()+" and is_xuni=1 ");
    				if (xuniReason==null) {
    					xuniReason=new HProIssueReasonAnalysis();
    					xuniReason.setIssueId(projectIssue.getId()).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1);
    					xuniReason.save();
    					xuniReason.setLsIndex(xuniReason.getId());
    					xuniReason.update();
    				}else {
    					xuniReason.setIssueId(projectIssue.getId()).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1)
    					.setLsIndex(xuniReason.getId());
    					xuniReason.update();
    				}
    				int status=1;
    				if (lsqtyMeasures.get(k).getStatus().intValue()==2||lsqtyMeasures.get(k).getStatus().intValue()==3) {
    					status=2;
					}
    				if (proMeasure==null) {
    					proMeasure=new HProjectMeasures();
    					proMeasure.setIssueId(projectIssue.getId()).setReasonId(xuniReason.getId())
    					.setXuhao("1."+(k+1)).setMeasures(lsqtyMeasures.get(k).getMeasures())
    					.setUserId(lsqtyMeasures.get(k).getUserId()).setUserName(lsqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(lsqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(0)
    					.setQtyMeasureId(lsqtyMeasures.get(k).getId())
    					.setRealFinishDate(lsqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.save();
    				}else {
    					proMeasure.setReasonId(xuniReason.getId())
    					.setXuhao("1."+(k+1)).setMeasures(lsqtyMeasures.get(k).getMeasures())
    					.setUserId(lsqtyMeasures.get(k).getUserId()).setUserName(lsqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(lsqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(0)
    					.setRealFinishDate(lsqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.update();
    				}
    				//2022-6-16 增加 措施跟踪同步
    				List<HQtyTrackHistory> trackHistorys=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+lsqtyMeasures.get(k).getId());
    				for (int l = 0; l < trackHistorys.size(); l++) {
    					HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where qty_track_history_id="+trackHistorys.get(l).getId());
    					if (proMeasureTrack==null) {
    						proMeasureTrack = new HProissueTrackHistory();
    						proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
    						.setQtyTrackHistoryId(trackHistorys.get(l).getId())
    						.setMeasureStatus(trackHistorys.get(l).getMeasureStatus()).setRemark(trackHistorys.get(l).getRemark())
    						.setTrackUserId(trackHistorys.get(l).getTrackUserId()).setTrackUserName(trackHistorys.get(l).getTrackUserName())
    						.setCreateTime(trackHistorys.get(l).getCreateTime());
    						proMeasureTrack.save();
    					}
    				}
    			}
    			//永久措施抓取“D5/D6永久措施指定&实施”
    			//措施同步--0=防产生
    			List<HQtyMeasures> csqtyMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where is_del=0 and issue_id="+qtyIssues.get(j).getId()+" and type=0");
    			for (int k = 0; k < csqtyMeasures.size(); k++) {
    				HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  qty_measure_id="+csqtyMeasures.get(k).getId());
    				HProIssueReasonAnalysis proReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where qty_reason_id="+csqtyMeasures.get(k).getReasonId());
    				int status=1;
    				if (csqtyMeasures.get(k).getStatus().intValue()==2||csqtyMeasures.get(k).getStatus().intValue()==3) {
    					status=2;
					}
    				if (proMeasure==null) {
    					proMeasure=new HProjectMeasures();
    					proMeasure.setIssueId(projectIssue.getId()).setReasonId(proReason.getId())
    					.setXuhao(String.valueOf(k+1)).setMeasures(csqtyMeasures.get(k).getMeasures())
    					.setUserId(csqtyMeasures.get(k).getUserId()).setUserName(csqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(csqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(1)
    					.setQtyMeasureId(csqtyMeasures.get(k).getId())
    					.setRealFinishDate(csqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.save();
    				}else {
    					proMeasure.setReasonId(proReason.getId())
    					.setXuhao(String.valueOf(k+1)).setMeasures(csqtyMeasures.get(k).getMeasures())
    					.setUserId(csqtyMeasures.get(k).getUserId()).setUserName(csqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(csqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(1)
    					.setRealFinishDate(csqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.update();
    				}
    				//2022-6-16 增加 措施跟踪同步
    				List<HQtyTrackHistory> trackHistorys=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+csqtyMeasures.get(k).getId());
    				for (int l = 0; l < trackHistorys.size(); l++) {
    					HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where qty_track_history_id="+trackHistorys.get(l).getId());
    					if (proMeasureTrack==null) {
    						proMeasureTrack = new HProissueTrackHistory();
    						proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
    						.setQtyTrackHistoryId(trackHistorys.get(l).getId())
    						.setMeasureStatus(trackHistorys.get(l).getMeasureStatus()).setRemark(trackHistorys.get(l).getRemark())
    						.setTrackUserId(trackHistorys.get(l).getTrackUserId()).setTrackUserName(trackHistorys.get(l).getTrackUserName())
    						.setCreateTime(trackHistorys.get(l).getCreateTime());
    						proMeasureTrack.save();
    					}
    				}
    			}
    			//措施同步--1=防流出
    			List<HQtyMeasures> lcqtyMeasures=HQtyMeasures.dao.find("select * from h_qty_measures where is_del=0 and issue_id="+qtyIssues.get(j).getId()+" and type=1");
    			for (int k = 0; k < lcqtyMeasures.size(); k++) {
    				HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  qty_measure_id="+lcqtyMeasures.get(k).getId());
    				HProIssueReasonAnalysis proReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where qty_reason_id="+lcqtyMeasures.get(k).getReasonId());
    				int status=1;
    				if (lcqtyMeasures.get(k).getStatus().intValue()==2||lcqtyMeasures.get(k).getStatus().intValue()==3) {
    					status=2;
					}
    				if (proMeasure==null) {
    					proMeasure=new HProjectMeasures();
    					proMeasure.setIssueId(projectIssue.getId()).setReasonId(proReason.getId())
    					.setXuhao(String.valueOf(k+1)).setMeasures(lcqtyMeasures.get(k).getMeasures())
    					.setUserId(lcqtyMeasures.get(k).getUserId()).setUserName(lcqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(lcqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(1)
    					.setQtyMeasureId(lcqtyMeasures.get(k).getId())
    					.setRealFinishDate(lcqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.save();
    				}else {
    					proMeasure.setReasonId(proReason.getId())
    					.setXuhao(String.valueOf(k+1)).setMeasures(lcqtyMeasures.get(k).getMeasures())
    					.setUserId(lcqtyMeasures.get(k).getUserId()).setUserName(lcqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(lcqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(1)
    					.setRealFinishDate(lcqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.update();
    				}
    				//2022-6-16 增加 措施跟踪同步
    				List<HQtyTrackHistory> trackHistorys=HQtyTrackHistory.dao.find("select * from h_qty_track_history where measure_id="+lcqtyMeasures.get(k).getId());
    				for (int l = 0; l < trackHistorys.size(); l++) {
    					HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where qty_track_history_id="+trackHistorys.get(l).getId());
    					if (proMeasureTrack==null) {
    						proMeasureTrack = new HProissueTrackHistory();
    						proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
    						.setQtyTrackHistoryId(trackHistorys.get(l).getId())
    						.setMeasureStatus(trackHistorys.get(l).getMeasureStatus()).setRemark(trackHistorys.get(l).getRemark())
    						.setTrackUserId(trackHistorys.get(l).getTrackUserId()).setTrackUserName(trackHistorys.get(l).getTrackUserName())
    						.setCreateTime(trackHistorys.get(l).getCreateTime());
    						proMeasureTrack.save();
    					}
    				}
    			}
    		}
            //同步手工样件OPL
            List<HClothesIssue> clothIssues = HClothesIssue.dao.find("SELECT * FROM h_clothes_issue WHERE (product_stage='手工样件' OR product_stage='OTS' OR product_stage='PPAP') AND is_del=0 AND pro_id IS NOT NULL AND pro_id<>0");
            for (int j = 0; j < clothIssues.size(); j++) {
            	Record proInfo=Db.use("bud").findById("file_pro_info", clothIssues.get(j).getProId().intValue());
            	String proCode="";
            	String category_name="";
            	int zhihuiUserId=0;
            	String zhihuiUserName="";
            	if (proInfo!=null) {
            		proCode = proInfo.getStr("pro_code");
            		category_name = proInfo.getStr("category_name");
            		Record proDutyUser=Db.use("bud").findFirst("select * from pro_member where pro_code='"+proCode+"' and role_name='责任人'");
        			User zhihuiUser=User.dao.findFirst("select * from user where ding_user_id='"+proDutyUser.getStr("role_member_id")+"'");
        			zhihuiUserId = zhihuiUser.getId().intValue();
        			zhihuiUserName = zhihuiUser.getNickname();
				}
            	//阶段问题
            	String stage="";
            	if ("手工样件".equals(clothIssues.get(j).getProductStage())) {
					stage="P2";
				}else if ("OTS".equals(clothIssues.get(j).getProductStage())) {
					stage="P3";
				}else if ("PPAP".equals(clothIssues.get(j).getProductStage())) {
					stage="P4";
				}
    			HClothesIssue8dPlan dPlan=HClothesIssue8dPlan.dao.findFirst("select * from h_clothes_issue_8d_plan where issue_id="+clothIssues.get(j).getId());
    			HProjectIssue projectIssue = HProjectIssue.dao.findFirst("select * from h_project_issue where handle_issue_id="+clothIssues.get(j).getId());
    			if (projectIssue==null) {//新增
    				projectIssue=new HProjectIssue();
    				projectIssue
    				.setHandleIssueId(clothIssues.get(j).getId())
    				.setProId(clothIssues.get(j).getProId().intValue()).setDepId(clothIssues.get(j).getDepId()).setProCode(proCode)
    				.setLineId(clothIssues.get(j).getLineId().intValue())
    				.setProName(clothIssues.get(j).getProName())
    				.setProductName(category_name)
    				.setStatus(clothIssues.get(j).getStatus())
    				.setStage(stage)
    				.set("propose_time",clothIssues.get(j).getOpenTime())
    				.setSeverity(clothIssues.get(j).getLevel())
    				.setIssueType("质量")
    				.setIssueSource(clothIssues.get(j).getSource())
    				.setIssueDescription(clothIssues.get(j).getDescription())
    				.setUpgradeUserId(clothIssues.get(j).getUpgradeUserId()).setUpgradeUserName(clothIssues.get(j).getUpgradeUserName())
    				.setDutyUserId(clothIssues.get(j).getDutyUserId()).setDutyUserName(clothIssues.get(j).getDutyUserName())
    				.setConfirmerId(clothIssues.get(j).getSureUserId()).setConfirmerName(clothIssues.get(j).getSureUserName())
    				.setCreateTime(clothIssues.get(j).getCreateTime())
    				.setCreateUserId(clothIssues.get(j).getCreateUserId()).setCreateUserName(clothIssues.get(j).getCreateUserName())
    				.setIsDel(0).setIssueStatus(clothIssues.get(j).getIssueStatus());
    				//计划完成时间
    				if (dPlan!=null) {
        				projectIssue.setPlanFinishTime(dPlan.getDThree());
					}
    				//状态
    				projectIssue.setStatus(clothIssues.get(j).getStatus());
    				//实际完成时间
    				projectIssue.setRealFinishTime(clothIssues.get(j).getCloseTime());
    				projectIssue.save();
    				//知会人
    				HProIssueTell issueTell=new HProIssueTell();
    				issueTell.setIssueId(projectIssue.getId())
    				.setTellUserId(zhihuiUserId).setTellUserName(zhihuiUserName)
    				.setStatus(0);
    				issueTell.save();
    			}else {//修改
    				projectIssue
    				.setHandleIssueId(clothIssues.get(j).getId())
    				.setProId(clothIssues.get(j).getProId().intValue()).setDepId(clothIssues.get(j).getDepId()).setProCode(proCode)
    				.setLineId(clothIssues.get(j).getLineId().intValue())
    				.setProName(clothIssues.get(j).getProName())
    				.setProductName(category_name)
    				.setStatus(clothIssues.get(j).getStatus())
    				.setStage(stage)
    				.set("propose_time",clothIssues.get(j).getOpenTime())
    				.setSeverity(clothIssues.get(j).getLevel())
    				.setIssueType("质量")
    				.setIssueSource(clothIssues.get(j).getSource())
    				.setIssueDescription(clothIssues.get(j).getDescription())
    				.setUpgradeUserId(clothIssues.get(j).getUpgradeUserId()).setUpgradeUserName(clothIssues.get(j).getUpgradeUserName())
    				.setDutyUserId(clothIssues.get(j).getDutyUserId()).setDutyUserName(clothIssues.get(j).getDutyUserName())
    				.setConfirmerId(clothIssues.get(j).getSureUserId()).setConfirmerName(clothIssues.get(j).getSureUserName())
    				.setCreateTime(clothIssues.get(j).getCreateTime())
    				.setCreateUserId(clothIssues.get(j).getCreateUserId()).setCreateUserName(clothIssues.get(j).getCreateUserName())
    				.setIsDel(0).setIssueStatus(clothIssues.get(j).getIssueStatus());
    				//计划完成时间
    				if (dPlan!=null) {
        				projectIssue.setPlanFinishTime(dPlan.getDThree());
					}
    				//状态
    				projectIssue.setStatus(clothIssues.get(j).getStatus());
    				//实际完成时间
    				projectIssue.setRealFinishTime(clothIssues.get(j).getCloseTime());
    				projectIssue.update();
    				//知会人
    				HProIssueTell issueTell=HProIssueTell.dao.findFirst("select * from h_pro_issue_tell where issue_id="+projectIssue.getId()+" and status=0");
    				if (issueTell==null) {
    					issueTell=new HProIssueTell();
    					issueTell.setIssueId(projectIssue.getId())
        				.setTellUserId(zhihuiUserId).setTellUserName(zhihuiUserName)
    					.setStatus(0);
    					issueTell.save();
    				}else {
    					issueTell
        				.setTellUserId(zhihuiUserId).setTellUserName(zhihuiUserName)
    					.setStatus(0);
    					issueTell.update();
    				}
    			}
    			//回写质量OPL中项目OPL id
    			clothIssues.get(j).setProIssueId(projectIssue.getId());
    			clothIssues.get(j).update();
    			//原因分析同步
    			List<HClothesIssueReasonAnalysis> reasonAnalysis = HClothesIssueReasonAnalysis.dao.find("select * from h_clothes_issue_reason_analysis where status=0 and issue_id="+clothIssues.get(j).getId());
    			for (int k = 0; k < reasonAnalysis.size();k++) {
    				HProIssueReasonAnalysis proReasonAnalysis=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where handle_reason_id="+reasonAnalysis.get(k).getId());
    				if (proReasonAnalysis==null) {
    					proReasonAnalysis=new HProIssueReasonAnalysis();
    					proReasonAnalysis.setHandleReasonId(reasonAnalysis.get(k).getId())
    					.setIssueId(projectIssue.getId())
    					.setReasonAnalysis(reasonAnalysis.get(k).getType().intValue()==0?"产生原因：":"根本原因："+reasonAnalysis.get(k).getReasonAnalysis())
    					.setStatus(0).setIsXuni(0);
    					proReasonAnalysis.save();
    					proReasonAnalysis.setLsIndex(proReasonAnalysis.getId());
    					proReasonAnalysis.update();
    				}else {
    					proReasonAnalysis
    					.setIssueId(projectIssue.getId())
    					.setReasonAnalysis(reasonAnalysis.get(k).getType().intValue()==0?"产生原因：":"根本原因："+reasonAnalysis.get(k).getReasonAnalysis())
    					.setStatus(0).setIsXuni(0)
    					.setLsIndex(proReasonAnalysis.getId());
    					proReasonAnalysis.update();
    				}
    			}
    			//临时措施抓取“D3遏制/临时措施及措施有效性跟踪”
    			//措施同步-2=遏制
    			List<HClothesMeasures> ezqtyMeasures=HClothesMeasures.dao.find("select * from h_clothes_measures where is_del=0 and issue_id="+clothIssues.get(j).getId()+" and type=2");
    			for (int k = 0; k < ezqtyMeasures.size(); k++) {
    				HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+ezqtyMeasures.get(k).getId());
    				HProIssueReasonAnalysis xuniReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where issue_id="+projectIssue.getId()+" and is_xuni=1 ");
    				if (xuniReason==null) {
    					xuniReason=new HProIssueReasonAnalysis();
    					xuniReason.setIssueId(projectIssue.getId()).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1);
    					xuniReason.save();
    					xuniReason.setLsIndex(xuniReason.getId());
    					xuniReason.update();
    				}else {
    					xuniReason.setIssueId(projectIssue.getId()).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1)
    					.setLsIndex(xuniReason.getId());
    					xuniReason.update();
    				}
    				int status=1;
    				if (ezqtyMeasures.get(k).getStatus().intValue()==2||ezqtyMeasures.get(k).getStatus().intValue()==3) {
    					status=2;
					}
    				if (proMeasure==null) {
    					proMeasure=new HProjectMeasures();
    					proMeasure.setIssueId(projectIssue.getId()).setReasonId(xuniReason.getId())
    					.setXuhao("1."+(k+1)).setMeasures(ezqtyMeasures.get(k).getMeasures()+"。遏制结果："+ezqtyMeasures.get(k).getContainResult())
    					.setUserId(ezqtyMeasures.get(k).getUserId()).setUserName(ezqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(ezqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(0)
    					.setHandleMeasureId(ezqtyMeasures.get(k).getId())
    					.setRealFinishDate(ezqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.save();
    				}else {
    					proMeasure.setReasonId(xuniReason.getId())
    					.setXuhao("1."+(k+1)).setMeasures(ezqtyMeasures.get(k).getMeasures()+"。遏制结果："+ezqtyMeasures.get(k).getContainResult())
    					.setUserId(ezqtyMeasures.get(k).getUserId()).setUserName(ezqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(ezqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(0)
    					.setRealFinishDate(ezqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.update();
    				}
    				//2022-6-16 增加 措施跟踪同步
    				List<HClothesTrackHistory> trackHistorys=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+ezqtyMeasures.get(k).getId());
    				for (int l = 0; l < trackHistorys.size(); l++) {
    					HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where handle_track_history_id="+trackHistorys.get(l).getId());
    					if (proMeasureTrack==null) {
    						proMeasureTrack = new HProissueTrackHistory();
    						proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
    						.setHandleTrackHistoryId(trackHistorys.get(l).getId())
    						.setMeasureStatus(trackHistorys.get(l).getMeasureStatus()).setRemark(trackHistorys.get(l).getRemark())
    						.setTrackUserId(trackHistorys.get(l).getTrackUserId()).setTrackUserName(trackHistorys.get(l).getTrackUserName())
    						.setCreateTime(trackHistorys.get(l).getCreateTime());
    						proMeasureTrack.save();
    					}
    				}
    			}
    			//措施同步-3=临时
    			List<HClothesMeasures> lsqtyMeasures=HClothesMeasures.dao.find("select * from h_clothes_measures where is_del=0 and issue_id="+clothIssues.get(j).getId()+" and type=3");
    			for (int k = 0; k < lsqtyMeasures.size(); k++) {
    				HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+lsqtyMeasures.get(k).getId());
    				HProIssueReasonAnalysis xuniReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where issue_id="+projectIssue.getId()+" and is_xuni=1 ");
    				if (xuniReason==null) {
    					xuniReason=new HProIssueReasonAnalysis();
    					xuniReason.setIssueId(projectIssue.getId()).setStatus(0).setReasonAnalysis("暂无").setIsXuni(1);
    					xuniReason.save();
    					xuniReason.setLsIndex(xuniReason.getId());
    					xuniReason.update();
    				}else {
    					xuniReason.setStatus(0);
    					xuniReason.update();
    				}
    				int status=1;
    				if (lsqtyMeasures.get(k).getStatus().intValue()==2||lsqtyMeasures.get(k).getStatus().intValue()==3) {
    					status=2;
					}
    				if (proMeasure==null) {
    					proMeasure=new HProjectMeasures();
    					proMeasure.setIssueId(projectIssue.getId()).setReasonId(xuniReason.getId())
    					.setXuhao("1."+(k+1)).setMeasures(lsqtyMeasures.get(k).getMeasures())
    					.setUserId(lsqtyMeasures.get(k).getUserId()).setUserName(lsqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(lsqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(0)
    					.setHandleMeasureId(lsqtyMeasures.get(k).getId())
    					.setRealFinishDate(lsqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.save();
    				}else {
    					proMeasure.setReasonId(xuniReason.getId())
    					.setXuhao("1."+(k+1)).setMeasures(lsqtyMeasures.get(k).getMeasures())
    					.setUserId(lsqtyMeasures.get(k).getUserId()).setUserName(lsqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(lsqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(0)
    					.setRealFinishDate(lsqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.update();
    				}
    				//2022-6-16 增加 措施跟踪同步
    				List<HClothesTrackHistory> trackHistorys=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+lsqtyMeasures.get(k).getId());
    				for (int l = 0; l < trackHistorys.size(); l++) {
    					HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where handle_track_history_id="+trackHistorys.get(l).getId());
    					if (proMeasureTrack==null) {
    						proMeasureTrack = new HProissueTrackHistory();
    						proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
    						.setHandleTrackHistoryId(trackHistorys.get(l).getId())
    						.setMeasureStatus(trackHistorys.get(l).getMeasureStatus()).setRemark(trackHistorys.get(l).getRemark())
    						.setTrackUserId(trackHistorys.get(l).getTrackUserId()).setTrackUserName(trackHistorys.get(l).getTrackUserName())
    						.setCreateTime(trackHistorys.get(l).getCreateTime());
    						proMeasureTrack.save();
    					}
    				}
    			}
    			//永久措施抓取“D5/D6永久措施指定&实施”
    			//措施同步--0=防产生
    			List<HClothesMeasures> csqtyMeasures=HClothesMeasures.dao.find("select * from h_clothes_measures where is_del=0 and issue_id="+clothIssues.get(j).getId()+" and type=0");
    			for (int k = 0; k < csqtyMeasures.size(); k++) {
    				HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+csqtyMeasures.get(k).getId());
    				HProIssueReasonAnalysis proReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where handle_reason_id="+csqtyMeasures.get(k).getReasonId());
    				int status=1;
    				if (csqtyMeasures.get(k).getStatus().intValue()==2||csqtyMeasures .get(k).getStatus().intValue()==3) {
    					status=2;
					}
    				if (proMeasure==null) {
    					proMeasure=new HProjectMeasures();
    					proMeasure.setIssueId(projectIssue.getId()).setReasonId(proReason.getId())
    					.setXuhao(String.valueOf(k+1)).setMeasures(csqtyMeasures.get(k).getMeasures())
    					.setUserId(csqtyMeasures.get(k).getUserId()).setUserName(csqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(csqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(1)
    					.setHandleMeasureId(csqtyMeasures.get(k).getId())
    					.setRealFinishDate(csqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.save();
    				}else {
    					proMeasure.setReasonId(proReason.getId())
    					.setXuhao(String.valueOf(k+1)).setMeasures(csqtyMeasures.get(k).getMeasures())
    					.setUserId(csqtyMeasures.get(k).getUserId()).setUserName(csqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(csqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(1)
    					.setRealFinishDate(csqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.update();
    				}
    				//2022-6-16 增加 措施跟踪同步
    				List<HClothesTrackHistory> trackHistorys=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+csqtyMeasures.get(k).getId());
    				for (int l = 0; l < trackHistorys.size(); l++) {
    					HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where handle_track_history_id="+trackHistorys.get(l).getId());
    					if (proMeasureTrack==null) {
    						proMeasureTrack = new HProissueTrackHistory();
    						proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
    						.setHandleTrackHistoryId(trackHistorys.get(l).getId())
    						.setMeasureStatus(trackHistorys.get(l).getMeasureStatus()).setRemark(trackHistorys.get(l).getRemark())
    						.setTrackUserId(trackHistorys.get(l).getTrackUserId()).setTrackUserName(trackHistorys.get(l).getTrackUserName())
    						.setCreateTime(trackHistorys.get(l).getCreateTime());
    						proMeasureTrack.save();
    					}
    				}
    			}
    			//措施同步--1=防流出
    			List<HClothesMeasures> lcqtyMeasures=HClothesMeasures.dao.find("select * from h_clothes_measures where is_del=0 and issue_id="+clothIssues.get(j).getId()+" and type=1");
    			for (int k = 0; k < lcqtyMeasures.size(); k++) {
    				HProjectMeasures proMeasure=HProjectMeasures.dao.findFirst("select * from h_project_measures where  handle_measure_id="+lcqtyMeasures.get(k).getId());
    				HProIssueReasonAnalysis proReason=HProIssueReasonAnalysis.dao.findFirst("select * from h_pro_issue_reason_analysis where handle_reason_id="+lcqtyMeasures.get(k).getReasonId());
    				int status=1;
    				if (lcqtyMeasures.get(k).getStatus().intValue()==2||lcqtyMeasures.get(k).getStatus().intValue()==3) {
    					status=2;
					}
    				if (proMeasure==null) {
    					proMeasure=new HProjectMeasures();
    					proMeasure.setIssueId(projectIssue.getId()).setReasonId(proReason.getId())
    					.setXuhao(String.valueOf(k+1)).setMeasures(lcqtyMeasures.get(k).getMeasures())
    					.setUserId(lcqtyMeasures.get(k).getUserId()).setUserName(lcqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(lcqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(1)
    					.setHandleMeasureId(lcqtyMeasures.get(k).getId())
    					.setRealFinishDate(lcqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.save();
    				}else {
    					proMeasure.setReasonId(proReason.getId())
    					.setXuhao(String.valueOf(k+1)).setMeasures(lcqtyMeasures.get(k).getMeasures())
    					.setUserId(lcqtyMeasures.get(k).getUserId()).setUserName(lcqtyMeasures.get(k).getUserName())
    					.setPlanFinishDate(lcqtyMeasures.get(k).getPlanFinishDate())
    					.setStatus(status).setIsDel(0).setMeasureType(1)
    					.setRealFinishDate(lcqtyMeasures.get(k).getRealFinishDate());
    					proMeasure.update();
    				}
    				//2022-6-16 增加 措施跟踪同步
    				List<HClothesTrackHistory> trackHistorys=HClothesTrackHistory.dao.find("select * from h_clothes_track_history where measure_id="+lcqtyMeasures.get(k).getId());
    				for (int l = 0; l < trackHistorys.size(); l++) {
    					HProissueTrackHistory proMeasureTrack = HProissueTrackHistory.dao.findFirst("select * from h_proissue_track_history where handle_track_history_id="+trackHistorys.get(l).getId());
    					if (proMeasureTrack==null) {
    						proMeasureTrack = new HProissueTrackHistory();
    						proMeasureTrack.setIssueId(projectIssue.getId()).setMeasureId(proMeasure.getId())
    						.setHandleTrackHistoryId(trackHistorys.get(l).getId())
    						.setMeasureStatus(trackHistorys.get(l).getMeasureStatus()).setRemark(trackHistorys.get(l).getRemark())
    						.setTrackUserId(trackHistorys.get(l).getTrackUserId()).setTrackUserName(trackHistorys.get(l).getTrackUserName())
    						.setCreateTime(trackHistorys.get(l).getCreateTime());
    						proMeasureTrack.save();
    					}
    				}
    			}
    		}
		} catch (Exception e) {
			e.printStackTrace();
		}
    };
}

