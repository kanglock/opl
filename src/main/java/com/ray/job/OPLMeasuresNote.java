package com.ray.job;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;

import com.ray.common.ding.DingMessage;
import com.ray.common.model.HProCusIssue;
import com.ray.common.model.HProCusIssueMonthTrack;
import com.ray.common.model.HProCusMeasures;
import com.ray.common.model.HProjectIssue;
import com.ray.common.model.HProjectMeasures;
import com.ray.common.model.HQtyMeasures;
import com.ray.common.model.HQualityIssue;
import com.ray.common.model.HQualityIssueMonthTrack;
import com.ray.common.model.User;
import com.ray.common.quartz.AbsJob;

/**
 * 永久措施时间节点提醒
 * @author FL00024996
 *
 */
public class OPLMeasuresNote extends AbsJob{
	@Override
	protected void process(JobExecutionContext context) {
		try {
			Date nowDate =new Date();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			String today=sdf.format(nowDate);
			/**
			 * 措施提前两天开始提醒-开始
			 */
			Date lastTwoDate=com.ray.util.HyCommenMethods.addDate(nowDate, -2);//两天前
			//标准OPL整个事件节点提前2天开始提醒
			List<HProjectIssue> notFinishProjectIssues=HProjectIssue.dao.find("select * from h_project_issue where is_del=0 and isnull(real_finish_time) and  plan_finish_time<='"+sdf.format(lastTwoDate)+"' and quality_issue_id is null and handle_issue_id is null ");//没有完成的所有标准OPL
			for (int i = 0; i < notFinishProjectIssues.size(); i++) {
				User dutyUser=User.dao.findById(notFinishProjectIssues.get(i).getDutyUserId().intValue());
				User sureUser=User.dao.findById(notFinishProjectIssues.get(i).getConfirmerId().intValue());
				DingMessage.sendText(dutyUser.getDingUserId(), "项目号【"+notFinishProjectIssues.get(i).getProCode()+"】下关联的标准OPL还未关闭\n请及时确认OPL进度！\n"+sdf.format(new Date()), "2");//2-标准OPL措施执行页面
				DingMessage.sendText(sureUser.getDingUserId(), "项目号【"+notFinishProjectIssues.get(i).getProCode()+"】下关联的标准OPL还未关闭\n请及时确认OPL进度！\n"+sdf.format(new Date()), "2");//2-标准OPL措施执行页面

			}
			//标准OPL措施
			List<HProjectIssue> projectIssues=HProjectIssue.dao.find("select * from h_project_issue where is_del=0 and isnull(real_finish_time)  and quality_issue_id is null and handle_issue_id is null ");//没有完成的所有标准OPL
			for (int i = 0; i < projectIssues.size(); i++) {
				//计划完成时间前两天提醒
				List<HProjectMeasures> measures=HProjectMeasures.dao.find("select * from h_project_measures where issue_id="+projectIssues.get(i).getId()+" and is_del=0 and status=1 and plan_finish_date<='"+sdf.format(lastTwoDate)+"'");
				for (int j = 0; j < measures.size(); j++) {
					User measureUser=User.dao.findById(measures.get(j).getUserId().intValue());
					DingMessage.sendText(measureUser.getDingUserId(), "项目号【"+projectIssues.get(i).getProCode()+"】下关联的标准OPL措施即将到期或已到期\n请及时确认已完成！\n"+sdf.format(new Date()), "2");//2-标准OPL措施执行页面					
				}
			}
			//质量OPL措施
			List<HQualityIssue> qtyIssues=HQualityIssue.dao.find("select * from h_quality_issue where is_del=0 and isnull(close_time)   ");//没有完成的所有质量OPL
			for (int i = 0; i < qtyIssues.size(); i++) {
				//计划完成时间前两天提醒
				List<HQtyMeasures> measures=HQtyMeasures.dao.find("select * from h_qty_measures where issue_id="+qtyIssues.get(i).getId()+" and is_del=0 and status=1 and plan_finish_date<='"+sdf.format(lastTwoDate)+"'");
				for (int j = 0; j < measures.size(); j++) {
					User measureUser=User.dao.findById(measures.get(j).getUserId().intValue());
					DingMessage.sendText(measureUser.getDingUserId(), "产品名称【"+qtyIssues.get(i).getProductName()+"】下关联的质量OPL措施即将到期或已到期\n请及时确认已完成！\n"+sdf.format(new Date()), "12");//12-质量OPL措施执行页面					
				}
			}
			//新品客诉OPL措施
			List<HProCusIssue> cusIssues=HProCusIssue.dao.find("select * from h_pro_cus_issue where is_del=0 and isnull(close_time)   ");//没有完成的所有新品客诉OPL
			for (int i = 0; i < cusIssues.size(); i++) {
				//计划完成时间前两天提醒
				List<HProCusMeasures> measures=HProCusMeasures.dao.find("select * from h_pro_cus_measures where issue_id="+cusIssues.get(i).getId()+" and is_del=0 and status=1 and plan_finish_date<='"+sdf.format(lastTwoDate)+"'");
				for (int j = 0; j < measures.size(); j++) {
					User measureUser=User.dao.findById(measures.get(j).getUserId().intValue());
					DingMessage.sendText(measureUser.getDingUserId(), "项目号【"+cusIssues.get(i).getProCode()+"】下关联的新品客诉OPL措施即将到期或已到期\n请及时确认已完成！\n"+sdf.format(new Date()), "22");//22-新品客诉OPL措施执行页面					
				}
			}
			/**
			 * 措施提前两天开始提醒-结束
			 */
			/**
			 * 永久措施节点通知-开始
			 */
			//质量OPL
			List<HQualityIssue> firstQualityIssues=HQualityIssue.dao.find("select * from h_quality_issue where status<>'G' and is_del=0 and first_month='"+today+"' ");
			for (int i = 0; i < firstQualityIssues.size(); i++) {
				HQualityIssueMonthTrack monthTrack=HQualityIssueMonthTrack.dao.findFirst("select * from h_quality_issue_month_track where issue_id="+firstQualityIssues.get(i).getId()+" and month=1");
				if (monthTrack==null) {//没有跟踪记录
					//钉钉消息通知责任人
					User dutyUser=User.dao.findById(firstQualityIssues.get(i).getDutyUserId().intValue());
					DingMessage.sendText(dutyUser.getDingUserId(), "产品名称【"+firstQualityIssues.get(i).getProductName()+"】下关联的质量OPL第一个月跟踪已到期\n请及时确认第一个月跟踪情况！\n"+sdf.format(new Date()), "13");//13-质量OPL责任人					
				}
			}
			List<HQualityIssue> secondQualityIssues=HQualityIssue.dao.find("select * from h_quality_issue where status<>'G' and is_del=0 and second_month='"+today+"' ");
			for (int i = 0; i < secondQualityIssues.size(); i++) {
				HQualityIssueMonthTrack monthTrack=HQualityIssueMonthTrack.dao.findFirst("select * from h_quality_issue_month_track where issue_id="+secondQualityIssues.get(i).getId()+" and month=2");
				if (monthTrack==null) {//没有跟踪记录
					//钉钉消息通知责任人
					User dutyUser=User.dao.findById(secondQualityIssues.get(i).getDutyUserId().intValue());
					DingMessage.sendText(dutyUser.getDingUserId(), "产品名称【"+secondQualityIssues.get(i).getProductName()+"】下关联的质量OPL第二个月跟踪已到期\n请及时确认第二个月跟踪情况！\n"+sdf.format(new Date()), "13");//13-质量OPL责任人	
				}
			}
			List<HQualityIssue> thirdQualityIssues=HQualityIssue.dao.find("select * from h_quality_issue where status<>'G' and is_del=0 and third_month='"+today+"' ");
			for (int i = 0; i < thirdQualityIssues.size(); i++) {
				HQualityIssueMonthTrack monthTrack=HQualityIssueMonthTrack.dao.findFirst("select * from h_quality_issue_month_track where issue_id="+thirdQualityIssues.get(i).getId()+" and month=3");
				if (monthTrack==null) {//没有跟踪记录
					//钉钉消息通知责任人
					User dutyUser=User.dao.findById(thirdQualityIssues.get(i).getDutyUserId().intValue());
					DingMessage.sendText(dutyUser.getDingUserId(), "产品名称【"+thirdQualityIssues.get(i).getProductName()+"】下关联的质量OPL第三个月跟踪已到期\n请及时确认第三个月跟踪情况！\n"+sdf.format(new Date()), "13");//13-质量OPL责任人
				}
			}
			//新品客诉
			List<HProCusIssue> firstCusIssues=HProCusIssue.dao.find("select * from h_pro_cus_issue where status<>'G' and is_del=0 and first_month='"+today+"' ");
			for (int i = 0; i < firstCusIssues.size(); i++) {
				HProCusIssueMonthTrack monthTrack=HProCusIssueMonthTrack.dao.findFirst("select * from h_pro_cus_issue_month_track where issue_id="+firstCusIssues.get(i).getId()+" and month=1");
				if (monthTrack==null) {//没有跟踪记录
					//钉钉消息通知责任人
					User dutyUser=User.dao.findById(firstCusIssues.get(i).getDutyUserId().intValue());
					DingMessage.sendText(dutyUser.getDingUserId(), "产品名称【"+firstCusIssues.get(i).getProductName()+"】下关联的新品客诉OPL第一个月跟踪已到期\n请及时确认第一个月跟踪情况！\n"+sdf.format(new Date()), "23");//23-新品客诉OPL责任人
				}
			}
			List<HProCusIssue> secondCusIssues=HProCusIssue.dao.find("select * from h_pro_cus_issue where status<>'G' and is_del=0 and second_month='"+today+"' ");
			for (int i = 0; i < secondCusIssues.size(); i++) {
				HProCusIssueMonthTrack monthTrack=HProCusIssueMonthTrack.dao.findFirst("select * from h_pro_cus_issue_month_track where issue_id="+secondCusIssues.get(i).getId()+" and month=2");
				if (monthTrack==null) {//没有跟踪记录
					//钉钉消息通知责任人
					User dutyUser=User.dao.findById(secondCusIssues.get(i).getDutyUserId().intValue());
					DingMessage.sendText(dutyUser.getDingUserId(), "产品名称【"+secondCusIssues.get(i).getProductName()+"】下关联的新品客诉OPL第二个月跟踪已到期\n请及时确认第二个月跟踪情况！\n"+sdf.format(new Date()), "23");//23-新品客诉OPL责任人
				}
			}
			List<HProCusIssue> thirdCusIssues=HProCusIssue.dao.find("select * from h_pro_cus_issue where status<>'G' and is_del=0 and third_month='"+today+"' ");
			for (int i = 0; i < thirdCusIssues.size(); i++) {
				HProCusIssueMonthTrack monthTrack=HProCusIssueMonthTrack.dao.findFirst("select * from h_pro_cus_issue_month_track where issue_id="+thirdCusIssues.get(i).getId()+" and month=3");
				if (monthTrack==null) {//没有跟踪记录
					//钉钉消息通知责任人
					User dutyUser=User.dao.findById(thirdCusIssues.get(i).getDutyUserId().intValue());
					DingMessage.sendText(dutyUser.getDingUserId(), "产品名称【"+thirdCusIssues.get(i).getProductName()+"】下关联的新品客诉OPL第三个月跟踪已到期\n请及时确认第三个月跟踪情况！\n"+sdf.format(new Date()), "23");//23-新品客诉OPL责任人
				}
			}
			//质量OPL下次回顾时间通知
			List<HQualityIssue> nextTimeQualityIssues=HQualityIssue.dao.find("select * from h_quality_issue where status<>'G' and is_del=0 and next_review_time IS NOT NULL and next_review_time='"+sdf.format(nowDate)+"' ");
			for (int i = 0; i < nextTimeQualityIssues.size(); i++) {
				User dutyUser=User.dao.findById(nextTimeQualityIssues.get(i).getDutyUserId().intValue());
				DingMessage.sendText(dutyUser.getDingUserId(), "产品名称【"+nextTimeQualityIssues.get(i).getProductName()+"】下关联的质量OPL已到下次回顾时间，请及时参加快反会议！\n"+sdf.format(new Date()), "13");//13-质量OPL责任人				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
