package com.ray.job;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.*;
import com.ray.common.quartz.AbsJob;
import com.ray.util.HyCommenMethods;
import org.quartz.JobExecutionContext;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * opl中 D4为红色之后  将其状态改成R
 * @author FL00024996
 *
 */
public class ChangeOPLToR extends AbsJob {
	@Override
	protected void process(JobExecutionContext context) {
		try {
			List<Record> qtyIssues=Db.find("   select i.id,i.is_upgrade_zj,i.zj_user_id,i.zj_user_name,i.is_upgrade_zjl,i.product_name,\n" +
					"\tp.d_one p_one,p.d_two p_two,p.d_three p_three,p.d_four p_four,p.d_five p_five,p.d_six p_six,p.d_seven p_seven,\n" +
					"  r.d_one  ,r.d_two  ,r.d_three  ,r.d_four  ,r.d_five  ,r.d_six  ,r.d_seven  \n" +
					"\tfrom h_quality_issue i,h_qty_issue_8d_plan p,h_qty_issue_8d_real r where \n" +
					"\ti.id = p.issue_id and i.id = r.issue_id\n" +
					"  and i.is_del=0 and i.issue_status=0   and i.level != 'C' and i.status != 'R' and i.status != 'G' \n" +
					"\tand i.zj_user_id is not null and (is_upgrade_zj = 0 or is_upgrade_zjl = 0 ) \n" +
					"  order by i.id desc  ");

			List<String> list = new ArrayList<>();
			Date now = new Date();
			qtyIssues.stream().forEach(i->{
				//当实际大于计划D4  或者   今天大于计划D4且实际D4没有数据的时候  将id加入到list中
				String p_four = i.getStr("p_four");
				String d_four = i.getStr("d_four");

				String p_five = i.getStr("p_five");
				String d_five = i.getStr("d_five");

				String p_six = i.getStr("p_six");
				String d_six = i.getStr("d_six");

				String p_seven = i.getStr("p_seven");
				String d_seven = i.getStr("d_seven");


				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				System.out.println(p_six +"---------"+d_six);
				try {
					Date plan_four = sdf.parse(p_four);
					Date real_four = sdf.parse(d_four);
					if (plan_four.before(real_four)){
						String id = i.getStr("id");
						list.add(id);
					}

					Date plan_five = sdf.parse(p_five);
					Date real_five = sdf.parse(d_five);
					if (plan_five.before(real_five)){
						String id = i.getStr("id");
						list.add(id);
					}

					Date plan_six = sdf.parse(p_six);
					Date real_six = sdf.parse(d_six);
					if (plan_six.before(real_six)){
						String id = i.getStr("id");
						list.add(id);
					}

					Date plan_seven = sdf.parse(p_seven);
					Date real_seven = sdf.parse(d_seven);
					if (plan_seven.before(real_seven)){
						String id = i.getStr("id");
						list.add(id);
					}
				}catch (NullPointerException | ParseException e){

				}
			});
			List<String> collect = list.stream().distinct().collect(Collectors.toList());
			Db.tx(()->{
				boolean isSuccess = true;
				for (String id : collect) {
					int update = Db.update(" update h_quality_issue  set status = 'R' where id = '" + id + "' ");
					if (update == 0 ){
						isSuccess =  false;
					}
				}
				return isSuccess;
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}


