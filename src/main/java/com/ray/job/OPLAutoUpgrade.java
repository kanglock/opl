package com.ray.job;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import com.jfinal.plugin.activerecord.Record;
import com.ray.util.HyCommenMethods;
import org.quartz.JobExecutionContext;
import com.jfinal.plugin.activerecord.Db;
import com.ray.common.ding.DingMessage;
import com.ray.common.model.HProjectIssue;
import com.ray.common.model.HProoplUpgradeRecord;
import com.ray.common.model.HQualityIssue;
import com.ray.common.model.HQualityopllUpgradeRecord;
import com.ray.common.model.User;
import com.ray.common.quartz.AbsJob;
/**
 * OPL自动升级
 * @author FL00024996
 *
 */
public class OPLAutoUpgrade extends AbsJob {
	@Override
	protected void process(JobExecutionContext context) {
		try {
			Date nowDate =new Date();
			long nowTime=nowDate.getTime();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			//项目OPL升级
			List<HProjectIssue> projectIssues=HProjectIssue.dao.find("select * from h_project_issue where is_del=0 and issue_status=0 and isnull(real_finish_time) and severity<>'A' and quality_issue_id is null and handle_issue_id is null ");//2022-7-14 增加非关键opl判定
			for (int i = 0; i < projectIssues.size(); i++) {
				Date issuePlanFinishDate=projectIssues.get(i).getPlanFinishTime();
				long planTime=issuePlanFinishDate.getTime();
				long time=nowTime-planTime;//现在与计划时间差
				if (time>0) {//现在大于计划时间
					BigDecimal days=new BigDecimal(time).divide(new BigDecimal(1000).multiply(new BigDecimal(60)).multiply(new BigDecimal(60)).multiply(new BigDecimal(24)),1,BigDecimal.ROUND_HALF_UP);
					if (days.doubleValue()>=3) {//超过3天自动升级
						projectIssues.get(i).setIssueStatus(1);//1-待审批
						projectIssues.get(i).update();
						//将以前升级记录置为失效
						long issueId=projectIssues.get(i).getId();
						Db.update("update h_proopl_upgrade_record set status=1 where issue_id="+issueId);
						List<HProoplUpgradeRecord> upgradeRecords=HProoplUpgradeRecord.dao.find("select * from h_proopl_upgrade_record where sp_status=1 and issue_id="+issueId);//审批通过的升级记录为空
						int oldUserId=0;
						String oldUserName="";
						if (upgradeRecords.isEmpty()) {//审批通过的升级记录为空，提出升级者便为责任人
							oldUserId=projectIssues.get(i).getDutyUserId();
							oldUserName=projectIssues.get(i).getDutyUserName();
						}else {
							oldUserId=upgradeRecords.get(upgradeRecords.size()-1).getNewUserId();
							oldUserName=upgradeRecords.get(upgradeRecords.size()-1).getNewUserName();
						}
						//添加处理记录
						HProoplUpgradeRecord upgradeRecord=new HProoplUpgradeRecord();
						upgradeRecord.setIssueId(issueId)
						.setOldUserId(oldUserId).setOldUserName(oldUserName)
						.setNewUserId(projectIssues.get(i).getUpgradeUserId()).setNewUserName(projectIssues.get(i).getUpgradeUserName())
						.setSpStatus(0).setStatus(0).setCreateTime(new Date())
						.setUpgradeReason("自动升级")
						.setUpgradeType(1);//1-自动升级
						upgradeRecord.save();
						//钉钉消息通知升级对象
						User upUser=User.dao.findById(projectIssues.get(i).getUpgradeUserId().intValue());
						DingMessage.sendText(upUser.getDingUserId(),"项目号\n【"+projectIssues.get(i).getProCode()+"】关联的标准OPL已自动升级，等级您的处理！\n"+sdf.format(new Date()), "5");//5-标准OPL措施升级处理页面
					}
				}
			}
			//质量OPL升级
			List<HQualityIssue> qtyIssues2=HQualityIssue.dao.find("  select * from h_quality_issue where is_del=0 and issue_status=0 and not isnull(d_note_date) and level<>'A'  order by id desc");
			for (int i = 0; i < qtyIssues2.size(); i++) {
				Date noteDate=qtyIssues2.get(i).getDNoteDate();
				long noteTime=noteDate.getTime();
				long time=nowTime-noteTime;
				long issueId=qtyIssues2.get(i).getId();
				if (time>0) {//现在大于应该升级时间
					qtyIssues2.get(i).setIssueStatus(1);//1-待审批
					qtyIssues2.get(i).update();
					//将以前的升级记录置为失效
					Db.update(" update h_qualityopll_upgrade_record set status=1 and issue_id="+issueId);
					List<HQualityopllUpgradeRecord> upgradeRecords=HQualityopllUpgradeRecord.dao.find("select * from h_qualityopll_upgrade_record where sp_status=1 and issue_id="+issueId);//审批通过的升级记录为空
					int oldUserId=0;
					String oldUserName="";
					if (upgradeRecords.isEmpty()) {//审批通过的升级记录为空，提出升级者便为责任人
						oldUserId=qtyIssues2.get(i).getDutyUserId();
						oldUserName=qtyIssues2.get(i).getDutyUserName();
					}else {
						oldUserId=upgradeRecords.get(upgradeRecords.size()-1).getNewUserId();
						oldUserName=upgradeRecords.get(upgradeRecords.size()-1).getNewUserName();
					}
					//添加新的记录
					HQualityopllUpgradeRecord upgradeRecord=new HQualityopllUpgradeRecord();
					upgradeRecord.setIssueId(issueId)
					.setOldUserId(oldUserId).setOldUserName(oldUserName)
					.setNewUserId(qtyIssues2.get(i).getUpgradeUserId()).setNewUserName(qtyIssues2.get(i).getUpgradeUserName())
					.setSpStatus(0).setStatus(0).setCreateTime(new Date())
					.setUpgradeReason("自动升级")
					.setUpgradeType(1);//1-自动升级
					upgradeRecord.save();
					//钉钉消息通知升级对象
					User upUser=User.dao.findById(qtyIssues2.get(i).getUpgradeUserId().intValue());
					DingMessage.sendText(upUser.getDingUserId(),"产品名称\n【"+qtyIssues2.get(i).getProductName()+"】关联的质量OPL已自动升级，等级您的处理！\n"+sdf.format(new Date()), "15");//15-质量OPL措施升级处理页面
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
