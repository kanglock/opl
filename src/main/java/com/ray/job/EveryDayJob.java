package com.ray.job;

import java.util.List;
import org.quartz.JobExecutionContext;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiDepartmentGetRequest;
import com.dingtalk.api.request.OapiDepartmentListParentDeptsByDeptRequest;
import com.dingtalk.api.request.OapiDepartmentListRequest;
import com.dingtalk.api.request.OapiUserListbypageRequest;
import com.dingtalk.api.response.OapiDepartmentGetResponse;
import com.dingtalk.api.response.OapiDepartmentListParentDeptsByDeptResponse;
import com.dingtalk.api.response.OapiDepartmentListResponse;
import com.dingtalk.api.response.OapiUserListbypageResponse;
import com.dingtalk.api.response.OapiDepartmentListResponse.Department;
import com.dingtalk.api.response.OapiUserListbypageResponse.Userlist;
import com.jfinal.plugin.activerecord.Db;
import com.ray.common.ding.AccessTokenUtil;
import com.ray.common.model.HUserDep;
import com.ray.common.model.User;
import com.ray.common.model.UserRole;
import com.ray.common.quartz.AbsJob;

/**
 * 每天执行 组织架构更新
 *
 * @author Ray
 * @date 2010-08-03
 */
public class EveryDayJob extends AbsJob {
	@Override
	protected void process(JobExecutionContext context) {
		try {
			Db.update("update user set status=1");//将所有用户置为失效
			Db.update("update h_user_dep set status=1");//将所有部门置为失效
			//所有部门列表
			List<Department> depList=depList();
			for (int i = 0; i < depList.size(); i++) {
				long depId=depList.get(i).getId();
				String depName=depList.get(i).getName();
				int offset=0;
				for (int j = 0; j < 40; j++) {
					List<Userlist> depUserlists=userlists(depId,offset);
					offset=offset+depUserlists.size();
					for (int k = 0; k < depUserlists.size(); k++) {
						//用户表是否有数据
						User user=User.dao.findFirst("select * from user where ding_user_id = '"+depUserlists.get(k).getUserid()+"'");
						if (user==null) {//没有用户则新增
							if (depUserlists.get(k).getMobile()==null) {
								System.err.println(depUserlists.get(k).getUserid());
							}
							user=new User();
							user.setUsername(depUserlists.get(k).getMobile())
							.setPassword(depUserlists.get(k).getMobile())
							.setNickname(depUserlists.get(k).getName())
							.setDingUserId(depUserlists.get(k).getUserid())
							.setPosition(depUserlists.get(k).getPosition())
							.setJobNum(depUserlists.get(k).getJobnumber());
							user.save();
							UserRole userRole=new UserRole();
							userRole.setUserId(user.getId().intValue()).setRoleId(2);
							userRole.save();
						}else {//有用户则修改状态 
							user.setStatus(0)
							.setPosition(depUserlists.get(k).getPosition())
							.setJobNum(depUserlists.get(k).getJobnumber());
							user.update();
							UserRole userRole=UserRole.dao.findFirst("select * from user_role where user_id="+user.getId().intValue());
							if (userRole==null) {
								userRole=new UserRole();
								userRole.setUserId(user.getId().intValue()).setRoleId(2);
								userRole.save();
							}
						}
						//员工部门信息
						HUserDep userDep=HUserDep.dao.findFirst("select * from h_user_dep where user_id="+user.getId()+" and dep_id="+depId);
						long firstDepId=firstDepId(String.valueOf(depId));
						if (userDep==null) {
							userDep=new HUserDep();
							userDep.setUserId(user.getId()).setDepId(depId).setDepName(depName)
							.setStatus(0)
							.setFirstDepId(firstDepId).setFirstDepName(depInfo(String.valueOf(firstDepId)).getName());
							userDep.save();
						}else {
							userDep.setUserId(user.getId()).setDepId(depId).setDepName(depName)
							.setStatus(0)
							.setFirstDepId(firstDepId).setFirstDepName(depInfo(String.valueOf(firstDepId)).getName());
							userDep.update();
						}
					}
					if (depUserlists.size()<100) {
						break;
					}
				}
			}
			//将失效人员置为删除
			Db.update("UPDATE USER SET is_del=1 WHERE STATUS=1 ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//获取根部门部门列表
	public List<Department> depList(){
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/department/list");
		OapiDepartmentListRequest request = new OapiDepartmentListRequest();
		request.setId("1");
		request.setHttpMethod("GET");
		OapiDepartmentListResponse response = null;
		try {
			response=client.execute(request, AccessTokenUtil.getToken());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response.getDepartment();		
	}
	//获取部门下人员信息列表
	public List<Userlist> userlists(long depId,long page){
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/user/listbypage");
		OapiUserListbypageRequest request = new OapiUserListbypageRequest();
		request.setDepartmentId(depId);
		request.setOffset(page);
		request.setSize(100L);
		request.setOrder("entry_desc");
		request.setHttpMethod("GET");
		OapiUserListbypageResponse execute = null;
		try {
			execute=client.execute(request,AccessTokenUtil.getToken());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return execute.getUserlist();	
	}
	//获取用户根部门
	public long firstDepId(String depId) {
		long firstDep=0;
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/department/list_parent_depts_by_dept");
		OapiDepartmentListParentDeptsByDeptRequest request = new OapiDepartmentListParentDeptsByDeptRequest();
		request.setId(depId);
		request.setHttpMethod("GET");
		OapiDepartmentListParentDeptsByDeptResponse response = null;
		try {
			response=client.execute(request, AccessTokenUtil.getToken());
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<Long>	depList	=response.getParentIds();
		if (depList.size()==1) {
			firstDep=depList.get(0);
		}else if (depList.size()==2) {
			firstDep=depList.get(1);
		}else {
			firstDep=depList.get(depList.size()-3);
		}
		return firstDep;
	}
	//获取部门信息
	public OapiDepartmentGetResponse depInfo(String dep_id) {
		DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/department/get");
		OapiDepartmentGetRequest request = new OapiDepartmentGetRequest();
		request.setId(dep_id);
		request.setHttpMethod("GET");
		OapiDepartmentGetResponse response = null;
		try {
			response=client.execute(request, AccessTokenUtil.getToken());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
}
