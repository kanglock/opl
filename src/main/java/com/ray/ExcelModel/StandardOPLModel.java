package com.ray.ExcelModel;

import com.github.liaochong.myexcel.core.annotation.ExcelColumn;

public class StandardOPLModel {
	@ExcelColumn(index = 0)
    private String pro_code;
	public String getPro_code() {
		return pro_code;
	}
	public void setPro_code(String pro_code) {
		this.pro_code=pro_code;
	}
	@ExcelColumn(index = 1)
    private String pro_name;
	public String getPro_name() {
		return pro_name;
	}
	public void setPro_name(String pro_name) {
		this.pro_name=pro_name;
	}
	@ExcelColumn(index = 2)
    private String stage;
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage=stage;
	}
	@ExcelColumn(index = 3)
    private String propose_time;
	public String getPropose_time() {
		return propose_time;
	}
	public void setPropose_time(String propose_time) {
		this.propose_time=propose_time;
	}
	@ExcelColumn(index = 4)
    private String severity;
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity=severity;
	}
	@ExcelColumn(index = 5)
    private String upgrade_user_jobnum;
	public String getUpgrade_user_jobnum() {
		return upgrade_user_jobnum;
	}
	public void setUpgrade_user_jobnum(String upgrade_user_jobnum) {
		this.upgrade_user_jobnum=upgrade_user_jobnum;
	}
	@ExcelColumn(index = 6)
    private String upgrade_user_name;
	public String getUpgrade_user_name() {
		return upgrade_user_name;
	}
	public void setUpgrade_user_name(String upgrade_user_name) {
		this.upgrade_user_name=upgrade_user_name;
	}
	@ExcelColumn(index = 7)
    private String issue_type;
	public String getIssue_type() {
		return issue_type;
	}
	public void setIssue_type(String issue_type) {
		this.issue_type=issue_type;
	}
	@ExcelColumn(index = 8)
    private String issue_source;
	public String getIssue_source() {
		return issue_source;
	}
	public void setIssue_source(String issue_source) {
		this.issue_source=issue_source;
	}
	@ExcelColumn(index = 9)
    private String description_target;
	public String getDescription_target() {
		return description_target;
	}
	public void setDescription_target(String description_target) {
		this.description_target=description_target;
	}
	@ExcelColumn(index = 10)
    private String issue_target;
	public String getIssue_target() {
		return issue_target;
	}
	public void setIssue_target(String issue_target) {
		this.issue_target=issue_target;
	}
	@ExcelColumn(index = 11)
    private String duty_user_jobnum;
	public String getDuty_user_jobnum() {
		return duty_user_jobnum;
	}
	public void setDuty_user_jobnum(String duty_user_jobnum) {
		this.duty_user_jobnum=duty_user_jobnum;
	}
	@ExcelColumn(index = 12)
    private String duty_user_name;
	public String getDuty_user_name() {
		return duty_user_name;
	}
	public void setDuty_user_name(String duty_user_name) {
		this.duty_user_name=duty_user_name;
	}
	@ExcelColumn(index = 13)
    private String confirmer_jobnum;
	public String getConfirmer_jobnum() {
		return confirmer_jobnum;
	}
	public void setConfirmer_jobnum(String confirmer_jobnum) {
		this.confirmer_jobnum=confirmer_jobnum;
	}
	@ExcelColumn(index = 14)
    private String confirmer_name;
	public String getConfirmer_name() {
		return confirmer_name;
	}
	public void setConfirmer_name(String confirmer_name) {
		this.confirmer_name=confirmer_name;
	}
	@ExcelColumn(index = 15)
    private String plan_finish_time;
	public String getPlan_finish_time() {
		return plan_finish_time;
	}
	public void setPlan_finish_time(String plan_finish_time) {
		this.plan_finish_time=plan_finish_time;
	}
	@ExcelColumn(index = 16)
    private String dep_name;
	public String getDep_name() {
		return dep_name;
	}
	public void setDep_name(String dep_name) {
		this.dep_name=dep_name;
	}
	@ExcelColumn(index = 17)
    private String line_name;
	public String getLine_name() {
		return line_name;
	}
	public void setLine_name(String line_name) {
		this.line_name=line_name;
	}
}
