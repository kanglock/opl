package com.ray.common.config;

import com.jfinal.config.Routes;
import com.ray.controller.admin.BaseInfo.BaseInfoCon;
import com.ray.controller.admin.ClothesIssue.*;
import com.ray.controller.admin.ComMethod.ComMethodController;
import com.ray.controller.admin.NewCusProOPL.AllNewCusController;
import com.ray.controller.admin.NewCusProOPL.MyCreateNewCusController;
import com.ray.controller.admin.NewCusProOPL.MyDutyNewCusController;
import com.ray.controller.admin.NewCusProOPL.NewCusMeasureDoneController;
import com.ray.controller.admin.ProIssue.ProjectController;
import com.ray.controller.admin.ProductIssue.AllProductIssueController;
import com.ray.controller.admin.ProductIssue.GetAndonOPLController;
import com.ray.controller.admin.ProductIssue.MyDutyProductIssueController;
import com.ray.controller.admin.QualityIssue.*;
import com.ray.controller.admin.StandardProOPL.AllProOPLController;
import com.ray.controller.admin.StandardProOPL.MeasureDoneController;
import com.ray.controller.admin.StandardProOPL.MyCreateProOPLController;
import com.ray.controller.admin.StandardProOPL.MyDutyProOPLController;
import com.ray.controller.admin.StandardProOPL.MySureProOPLController;
import com.ray.controller.admin.StandardProOPL.ProAOPLController;
import com.ray.controller.admin.StandardProOPL.ProBOPLController;
import com.ray.controller.admin.StandardProOPL.StandardAnalysisController;
import com.ray.controller.admin.StandardProOPL.TellMeProOPLController;
import com.ray.controller.admin.StandardProOPL.UpgradeProOPLController;

public class PCRoutes extends Routes {

	@Override
	public void config() {
		this.setBaseViewPath("/page");
		this.add("baseInfo", BaseInfoCon.class,"/openIssue/baseInfo");//基本信息维护
		this.add("comMethod",ComMethodController.class);
		this.add("project",ProjectController.class,"/openIssue/proIssue");//项目列表

		this.add("myCreateProOPL",MyCreateProOPLController.class,"/openIssue/standardProIssue");//我创建的标准项目问题
		this.add("myDutyProOPL",MyDutyProOPLController.class,"/openIssue/standardProIssue");//我负责的标准项目问题
		this.add("tellMeProOPL",TellMeProOPLController.class,"/openIssue/standardProIssue");//知会我的标准项目问题
		this.add("mySureProOPL",MySureProOPLController.class,"/openIssue/standardProIssue");//知会我的标准项目问题
		this.add("proOPLMeasure",MeasureDoneController.class,"/openIssue/standardProIssue");//措施执行情况
		this.add("upgradeProOPL",UpgradeProOPLController.class,"/openIssue/standardProIssue");//OPL升级
		this.add("aProOPL",ProAOPLController.class,"/openIssue/standardProIssue");//A级问题
		this.add("bProOPL",ProBOPLController.class,"/openIssue/standardProIssue");//B级问题
		this.add("allProOPL",AllProOPLController.class,"/openIssue/standardProIssue");//所有项目OPL
		this.add("standardAnalysis",StandardAnalysisController.class,"/openIssue/standardProIssue");//标准OPL统计分析

		this.add("myCreateNewCusOPL",MyCreateNewCusController.class,"/openIssue/newCusProIssue");//我创建的新品客诉
		this.add("myDutyNewCusOPL",MyDutyNewCusController.class,"/openIssue/newCusProIssue");//我负责的新品客诉
		this.add("newCusMeasureDone",NewCusMeasureDoneController.class,"/openIssue/newCusProIssue");//措施执行情况
		this.add("allNewCusOPL",AllNewCusController.class,"/openIssue/newCusProIssue");//所有新品客诉

		this.add("myMreateQtyOPL",MyCreateQtyIssueController.class,"/openIssue/qualityIssue");//我创建的质量问题
		this.add("myDutyQtyOPL",MyDutyQtyIssueController.class,"/openIssue/qualityIssue");//我负责的质量问题
		this.add("mySureQtyOpl",MySureQtyIssueController.class,"/openIssue/qualityIssue");//我跟踪的质量问题
		this.add("myMeasureQtyOPL",MyMeasureQtyIssueController.class,"/openIssue/qualityIssue");//措施执行质量问题
		this.add("qtyUpgradeOPL",QtyIssueUpgradeController.class,"/openIssue/qualityIssue");//升级质量问题
		this.add("allQtyOPL",AllQtyIssueController.class,"/openIssue/qualityIssue");//所有质量问题
		this.add("finishOPL",FinishRequestController.class);//对接计划仓库质量问题
		this.add("qualityIssueAnalysis",QualityIssueAnalysisController.class,"/openIssue/qualityIssue");//质量问题分析
		this.add("tellMeQtyIssue",TellMeQtyIssueController.class,"/openIssue/qualityIssue");//质量只会我的
		this.add("getWMSissue",GetWMSissueController.class);//对接计划仓库质量问题


		this.add("myDutyProductOPL",MyDutyProductIssueController.class,"/openIssue/productIssue");//我负责的生产OPL
		this.add("allProductOPL",AllProductIssueController.class,"/openIssue/productIssue");//所有生产OPL
		this.add("getAndonOPL",GetAndonOPLController.class);//与andon系统交互

		//工装opl
		this.add("myCreateClothes", MyCreateClothesController.class,"/openIssue/clothesOpl");
		this.add("myDutyClothes", MyDutyClothesController.class,"/openIssue/clothesOpl");
		this.add("mySureClothes", MySureClothesController.class,"/openIssue/clothesOpl");
		this.add("myMeasureClothes", MyMeasureClothesController.class,"/openIssue/clothesOpl");
		this.add("myUpgradeClothes", MyClothesUpgradeController.class,"/openIssue/clothesOpl");
		this.add("tellMeClothesIssue", TellMeClothesController.class,"/openIssue/clothesOpl");
		this.add("allClothesOPL", AllClothesController.class,"/openIssue/clothesOpl");
		this.add("clothesIssueAnalysis", ClothesAnalysisController.class,"/openIssue/clothesOpl");

		//升级到总监  总经理
		this.add("allQtyOPLToZJ", AllQtyIssueUpgradeController.class,"/openIssue/qualityIssue");

	}

}
