package com.ray.common.ding;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.dingtalk.api.response.OapiMessageCorpconversationAsyncsendV2Response;
import com.taobao.api.ApiException;
/**
 * 钉钉消息推送工具类
 * @author Ray
 *
 */
public class DingMessage {
	public static String sendText(String useridList,String message,String urlType) {
		String accessToken = AccessTokenUtil.getToken();
		DingTalkClient client = new DefaultDingTalkClient(Env.URL_MESSAGE_ASYNCSEND_V2);
		
		OapiMessageCorpconversationAsyncsendV2Request request = new OapiMessageCorpconversationAsyncsendV2Request();
		request.setUseridList(useridList);
		request.setAgentId(Env.AGENT_ID);
		request.setToAllUser(false);
		
		OapiMessageCorpconversationAsyncsendV2Request.Msg msg = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
		msg.setOa(new OapiMessageCorpconversationAsyncsendV2Request.OA());
		msg.getOa().setHead(new OapiMessageCorpconversationAsyncsendV2Request.Head());
		msg.getOa().getHead().setText("OPL管理系统");
		msg.getOa().setBody(new OapiMessageCorpconversationAsyncsendV2Request.Body());
		msg.getOa().getBody().setContent(message);
		String com="dingtalk://dingtalkclient/action/openapp?corpid=ding1410adc97eed08c335c2f4657eb6378f&container_type=work_platform&app_id=0_1195999606&redirect_type=jump&redirect_url=";
		int type=Integer.valueOf(urlType);
		String url="http%3A%2F%2F110.186.68.166%3A7711%2FdingLogin%3Ftype%3D"+type;
		msg.getOa().setPcMessageUrl(com+url);
		msg.setMsgtype("oa");
		request.setMsg(msg);
		
		OapiMessageCorpconversationAsyncsendV2Response response = null;
		try {
			response = client.execute(request,accessToken);
		} catch (ApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response.getBody();
	}
}
