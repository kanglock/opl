package com.ray.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * OPL编辑历史记录
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseAndonProblemEditRecord<M extends BaseAndonProblemEditRecord<M>> extends Model<M> implements IBean {

	public M setId(java.lang.Long id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	public M setOplId(java.lang.Long oplId) {
		set("opl_id", oplId);
		return (M)this;
	}
	
	public java.lang.Long getOplId() {
		return getLong("opl_id");
	}

	public M setContent(java.lang.String content) {
		set("content", content);
		return (M)this;
	}
	
	public java.lang.String getContent() {
		return getStr("content");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return getDate("create_time");
	}

	public M setCreateUserId(java.lang.Integer createUserId) {
		set("create_user_id", createUserId);
		return (M)this;
	}
	
	public java.lang.Integer getCreateUserId() {
		return getInt("create_user_id");
	}

	public M setCreateUserName(java.lang.String createUserName) {
		set("create_user_name", createUserName);
		return (M)this;
	}
	
	public java.lang.String getCreateUserName() {
		return getStr("create_user_name");
	}

}

