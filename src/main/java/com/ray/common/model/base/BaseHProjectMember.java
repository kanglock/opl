package com.ray.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseHProjectMember<M extends BaseHProjectMember<M>> extends Model<M> implements IBean {

	/**
	 * id
	 */
	public M setId(java.lang.Long id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * id
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 项目id
	 */
	public M setProId(java.lang.Integer proId) {
		set("pro_id", proId);
		return (M)this;
	}
	
	/**
	 * 项目id
	 */
	public java.lang.Integer getProId() {
		return getInt("pro_id");
	}

	/**
	 * 角色名称
	 */
	public M setMemberRoleName(java.lang.String memberRoleName) {
		set("member_role_name", memberRoleName);
		return (M)this;
	}
	
	/**
	 * 角色名称
	 */
	public java.lang.String getMemberRoleName() {
		return getStr("member_role_name");
	}

	/**
	 * 成员
	 */
	public M setMemberUserId(java.lang.Integer memberUserId) {
		set("member_user_id", memberUserId);
		return (M)this;
	}
	
	/**
	 * 成员
	 */
	public java.lang.Integer getMemberUserId() {
		return getInt("member_user_id");
	}

	/**
	 * 成员姓名
	 */
	public M setMemberUserName(java.lang.String memberUserName) {
		set("member_user_name", memberUserName);
		return (M)this;
	}
	
	/**
	 * 成员姓名
	 */
	public java.lang.String getMemberUserName() {
		return getStr("member_user_name");
	}

	/**
	 * 1-删除
	 */
	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	/**
	 * 1-删除
	 */
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

}
