package com.ray.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 质量问题
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseHQualityIssue<M extends BaseHQualityIssue<M>> extends Model<M> implements IBean {

	/**
	 * id
	 */
	public M setId(java.lang.Long id) {
		set("id", id);
		return (M)this;
	}

	/**
	 * id
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 总成号id
	 */
	public M setPartId(java.lang.Integer partId) {
		set("part_id", partId);
		return (M)this;
	}

	/**
	 * 总成号id
	 */
	public java.lang.Integer getPartId() {
		return getInt("part_id");
	}

	/**
	 * 事业部id
	 */
	public M setDepId(java.lang.Long depId) {
		set("dep_id", depId);
		return (M)this;
	}

	/**
	 * 事业部id
	 */
	public java.lang.Long getDepId() {
		return getLong("dep_id");
	}

	/**
	 * 车间id
	 */
	public M setWareId(java.lang.Integer wareId) {
		set("ware_id", wareId);
		return (M)this;
	}

	/**
	 * 车间id
	 */
	public java.lang.Integer getWareId() {
		return getInt("ware_id");
	}

	/**
	 * 报告编号
	 */
	public M setReportNo(java.lang.String reportNo) {
		set("report_no", reportNo);
		return (M)this;
	}

	/**
	 * 报告编号
	 */
	public java.lang.String getReportNo() {
		return getStr("report_no");
	}

	/**
	 * 状态RGY
	 */
	public M setStatus(java.lang.String status) {
		set("status", status);
		return (M)this;
	}

	/**
	 * 状态RGY
	 */
	public java.lang.String getStatus() {
		return getStr("status");
	}

	/**
	 * 风险等级
	 */
	public M setLevel(java.lang.String level) {
		set("level", level);
		return (M)this;
	}

	/**
	 * 风险等级
	 */
	public java.lang.String getLevel() {
		return getStr("level");
	}

	/**
	 * 产品名称
	 */
	public M setProductName(java.lang.String productName) {
		set("product_name", productName);
		return (M)this;
	}

	/**
	 * 产品名称
	 */
	public java.lang.String getProductName() {
		return getStr("product_name");
	}

	/**
	 * 客户名称
	 */
	public M setCusName(java.lang.String cusName) {
		set("cus_name", cusName);
		return (M)this;
	}

	/**
	 * 客户名称
	 */
	public java.lang.String getCusName() {
		return getStr("cus_name");
	}

	/**
	 * 问题分类id
	 */
	public M setTypeId(java.lang.Integer typeId) {
		set("type_id", typeId);
		return (M)this;
	}

	/**
	 * 问题分类id
	 */
	public java.lang.Integer getTypeId() {
		return getInt("type_id");
	}

	/**
	 * 供应商问题分类id
	 */
	public M setSupplierTypeId(java.lang.Integer supplierTypeId) {
		set("supplier_type_id", supplierTypeId);
		return (M)this;
	}

	/**
	 * 供应商问题分类id
	 */
	public java.lang.Integer getSupplierTypeId() {
		return getInt("supplier_type_id");
	}

	/**
	 * 问题来源
	 */
	public M setSource(java.lang.String source) {
		set("source", source);
		return (M)this;
	}

	/**
	 * 问题来源
	 */
	public java.lang.String getSource() {
		return getStr("source");
	}

	/**
	 * 开发时间
	 */
	public M setOpenTime(java.util.Date openTime) {
		set("open_time", openTime);
		return (M)this;
	}

	/**
	 * 开发时间
	 */
	public java.util.Date getOpenTime() {
		return getDate("open_time");
	}

	/**
	 * 发生频次
	 */
	public M setHappenFrequency(java.lang.String happenFrequency) {
		set("happen_frequency", happenFrequency);
		return (M)this;
	}

	/**
	 * 发生频次
	 */
	public java.lang.String getHappenFrequency() {
		return getStr("happen_frequency");
	}

	/**
	 * 升级对象
	 */
	public M setUpgradeUserId(java.lang.Integer upgradeUserId) {
		set("upgrade_user_id", upgradeUserId);
		return (M)this;
	}

	/**
	 * 升级对象
	 */
	public java.lang.Integer getUpgradeUserId() {
		return getInt("upgrade_user_id");
	}

	public M setUpgradeUserName(java.lang.String upgradeUserName) {
		set("upgrade_user_name", upgradeUserName);
		return (M)this;
	}

	public java.lang.String getUpgradeUserName() {
		return getStr("upgrade_user_name");
	}

	/**
	 * 责任人
	 */
	public M setDutyUserId(java.lang.Integer dutyUserId) {
		set("duty_user_id", dutyUserId);
		return (M)this;
	}

	/**
	 * 责任人
	 */
	public java.lang.Integer getDutyUserId() {
		return getInt("duty_user_id");
	}

	public M setDutyUserName(java.lang.String dutyUserName) {
		set("duty_user_name", dutyUserName);
		return (M)this;
	}

	public java.lang.String getDutyUserName() {
		return getStr("duty_user_name");
	}

	/**
	 * 问题描述（地点/批次/数量）
	 */
	public M setDescription(java.lang.String description) {
		set("description", description);
		return (M)this;
	}

	/**
	 * 问题描述（地点/批次/数量）
	 */
	public java.lang.String getDescription() {
		return getStr("description");
	}

	/**
	 * 永久措施节点
	 */
	public M setPermanentMeasuresTime(java.util.Date permanentMeasuresTime) {
		set("permanent_measures_time", permanentMeasuresTime);
		return (M)this;
	}

	/**
	 * 永久措施节点
	 */
	public java.util.Date getPermanentMeasuresTime() {
		return getDate("permanent_measures_time");
	}

	/**
	 * 关闭时间
	 */
	public M setCloseTime(java.util.Date closeTime) {
		set("close_time", closeTime);
		return (M)this;
	}

	/**
	 * 关闭时间
	 */
	public java.util.Date getCloseTime() {
		return getDate("close_time");
	}

	/**
	 * 创建人
	 */
	public M setCreateUserId(java.lang.Integer createUserId) {
		set("create_user_id", createUserId);
		return (M)this;
	}

	/**
	 * 创建人
	 */
	public java.lang.Integer getCreateUserId() {
		return getInt("create_user_id");
	}

	public M setCreateUserName(java.lang.String createUserName) {
		set("create_user_name", createUserName);
		return (M)this;
	}

	public java.lang.String getCreateUserName() {
		return getStr("create_user_name");
	}

	/**
	 * 创建时间
	 */
	public M setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
		return (M)this;
	}

	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return getDate("create_time");
	}

	/**
	 * 激励方案
	 */
	public M setUrgePlan(java.lang.String urgePlan) {
		set("urge_plan", urgePlan);
		return (M)this;
	}

	/**
	 * 激励方案
	 */
	public java.lang.String getUrgePlan() {
		return getStr("urge_plan");
	}

	/**
	 * 8D分析报告
	 */
	public M setFileId(java.lang.Integer fileId) {
		set("file_id", fileId);
		return (M)this;
	}

	/**
	 * 8D分析报告
	 */
	public java.lang.Integer getFileId() {
		return getInt("file_id");
	}

	/**
	 * 定时任务执行时间
	 */
	public M setDNoteDate(java.util.Date dNoteDate) {
		set("d_note_date", dNoteDate);
		return (M)this;
	}

	/**
	 * 定时任务执行时间
	 */
	public java.util.Date getDNoteDate() {
		return getDate("d_note_date");
	}

	/**
	 * 同意升级次数
	 */
	public M setUpgradeNum(java.lang.Integer upgradeNum) {
		set("upgrade_num", upgradeNum);
		return (M)this;
	}

	/**
	 * 同意升级次数
	 */
	public java.lang.Integer getUpgradeNum() {
		return getInt("upgrade_num");
	}

	/**
	 * 0审批通过；1-待审批；
	 */
	public M setIssueStatus(java.lang.Integer issueStatus) {
		set("issue_status", issueStatus);
		return (M)this;
	}

	/**
	 * 0审批通过；1-待审批；
	 */
	public java.lang.Integer getIssueStatus() {
		return getInt("issue_status");
	}

	/**
	 * 1-删除
	 */
	public M setIsDel(java.lang.Integer isDel) {
		set("is_del", isDel);
		return (M)this;
	}

	/**
	 * 1-删除
	 */
	public java.lang.Integer getIsDel() {
		return getInt("is_del");
	}

	/**
	 * 第一月应该跟踪日期
	 */
	public M setFirstMonth(java.util.Date firstMonth) {
		set("first_month", firstMonth);
		return (M)this;
	}

	/**
	 * 第一月应该跟踪日期
	 */
	public java.util.Date getFirstMonth() {
		return getDate("first_month");
	}

	/**
	 * 第2月应该跟踪日期
	 */
	public M setSecondMonth(java.util.Date secondMonth) {
		set("second_month", secondMonth);
		return (M)this;
	}

	/**
	 * 第2月应该跟踪日期
	 */
	public java.util.Date getSecondMonth() {
		return getDate("second_month");
	}

	/**
	 * 第3月应该跟踪日期
	 */
	public M setThirdMonth(java.util.Date thirdMonth) {
		set("third_month", thirdMonth);
		return (M)this;
	}

	/**
	 * 第3月应该跟踪日期
	 */
	public java.util.Date getThirdMonth() {
		return getDate("third_month");
	}

	/**
	 * 激励方案附件
	 */
	public M setUrgeFujianId(java.lang.Long urgeFujianId) {
		set("urge_fujian_id", urgeFujianId);
		return (M)this;
	}

	/**
	 * 激励方案附件
	 */
	public java.lang.Long getUrgeFujianId() {
		return getLong("urge_fujian_id");
	}

	/**
	 * 供应商编号
	 */
	public M setSupplierId(java.lang.Integer supplierId) {
		set("supplier_id", supplierId);
		return (M)this;
	}

	/**
	 * 供应商编号
	 */
	public java.lang.Integer getSupplierId() {
		return getInt("supplier_id");
	}

	/**
	 * 供应商
	 */
	public M setSupplierName(java.lang.String supplierName) {
		set("supplier_name", supplierName);
		return (M)this;
	}

	/**
	 * 供应商
	 */
	public java.lang.String getSupplierName() {
		return getStr("supplier_name");
	}

	/**
	 * 问题发生工序
	 */
	public M setProcedure(java.lang.String procedure) {
		set("procedure", procedure);
		return (M)this;
	}

	/**
	 * 问题发生工序
	 */
	public java.lang.String getProcedure() {
		return getStr("procedure");
	}

	/**
	 * 快反跟踪
	 */
	public M setQuickReaction(java.lang.String quickReaction) {
		set("quick_reaction", quickReaction);
		return (M)this;
	}

	/**
	 * 快反跟踪
	 */
	public java.lang.String getQuickReaction() {
		return getStr("quick_reaction");
	}

	/**
	 * 总监快反跟踪
	 */
	public M setZJQuickReaction(java.lang.String zjQuickReaction) {
		set("zj_quick_reaction", zjQuickReaction);
		return (M)this;
	}

	/**
	 * 快反跟踪
	 */
	public java.lang.String getZJQuickReaction() {
		return getStr("zj_quick_reaction");
	}


	/**
	 * 快反跟踪
	 */
	public M setFZJLQuickReaction(java.lang.String fzjl_quick_reaction) {
		set("fzjl_quick_reaction", fzjl_quick_reaction);
		return (M)this;
	}

	/**
	 * 快反跟踪
	 */
	public java.lang.String getFZJLQuickReaction() {
		return getStr("fzjl_quick_reaction");
	}


	/**
	 * 快反跟踪
	 */
	public M setZJLQuickReaction(java.lang.String zjl_quick_reaction) {
		set("zjl_quick_reaction", zjl_quick_reaction);
		return (M)this;
	}

	/**
	 * 快反跟踪
	 */
	public java.lang.String getZJLQuickReaction() {
		return getStr("zjl_quick_reaction");
	}

	/**
	 * 产品阶段
	 */
	public M setProductStage(java.lang.String productStage) {
		set("product_stage", productStage);
		return (M)this;
	}

	/**
	 * 产品阶段
	 */
	public java.lang.String getProductStage() {
		return getStr("product_stage");
	}

	/**
	 * 下次回顾时间
	 */
	public M setNextReviewTime(java.util.Date nextReviewTime) {
		set("next_review_time", nextReviewTime);
		return (M)this;
	}

	/**
	 * 下次回顾时间
	 */
	public java.util.Date getNextReviewTime() {
		return getDate("next_review_time");
	}

	/**
	 * 不合格附件
	 */
	public M setUnqualifyFujianId(java.lang.Long unqualifyFujianId) {
		set("unqualify_fujian_id", unqualifyFujianId);
		return (M)this;
	}

	/**
	 * 不合格附件
	 */
	public java.lang.Long getUnqualifyFujianId() {
		return getLong("unqualify_fujian_id");
	}

	/**
	 * 跟踪人
	 */
	public M setSureUserId(java.lang.Integer sureUserId) {
		set("sure_user_id", sureUserId);
		return (M)this;
	}

	/**
	 * 跟踪人
	 */
	public java.lang.Integer getSureUserId() {
		return getInt("sure_user_id");
	}

	/**
	 * 跟踪人
	 */
	public M setSureUserName(java.lang.String sureUserName) {
		set("sure_user_name", sureUserName);
		return (M)this;
	}

	/**
	 * 跟踪人
	 */
	public java.lang.String getSureUserName() {
		return getStr("sure_user_name");
	}

	/**
	 * PFMEA文件
	 */
	public M setPfmeaFujianId(java.lang.Long pfmeaFujianId) {
		set("pfmea_fujian_id", pfmeaFujianId);
		return (M)this;
	}

	/**
	 * PFMEA文件
	 */
	public java.lang.Long getPfmeaFujianId() {
		return getLong("pfmea_fujian_id");
	}

	/**
	 * CP文件
	 */
	public M setCpFujianId(java.lang.Long cpFujianId) {
		set("cp_fujian_id", cpFujianId);
		return (M)this;
	}

	/**
	 * CP文件
	 */
	public java.lang.Long getCpFujianId() {
		return getLong("cp_fujian_id");
	}

	/**
	 * WI文件
	 */
	public M setWiFujianId(java.lang.Long wiFujianId) {
		set("wi_fujian_id", wiFujianId);
		return (M)this;
	}

	/**
	 * WI文件
	 */
	public java.lang.Long getWiFujianId() {
		return getLong("wi_fujian_id");
	}

	/**
	 * 流程文件
	 */
	public M setProcessFujianId(java.lang.Long processFujianId) {
		set("process_fujian_id", processFujianId);
		return (M)this;
	}

	/**
	 * 流程文件
	 */
	public java.lang.Long getProcessFujianId() {
		return getLong("process_fujian_id");
	}

	/**
	 * 同类产品横向排查
	 */
	public M setSameProductFujianId(java.lang.Long sameProductFujianId) {
		set("same_product_fujian_id", sameProductFujianId);
		return (M)this;
	}

	/**
	 * 同类产品横向排查
	 */
	public java.lang.Long getSameProductFujianId() {
		return getLong("same_product_fujian_id");
	}


	/**
	 * 零件号
	 */
	public M setElementid(java.lang.String elementid) {
		set("elementid", elementid);
		return (M)this;
	}

	/**
	 * 零件号
	 */
	public java.lang.String getElementid() {
		return getStr("elementid");
	}

	/**
	 * 零件名称
	 */
	public M setElementname(java.lang.String elementname) {
		set("elementname", elementname);
		return (M)this;
	}

	/**
	 * 零件名称
	 */
	public java.lang.String getElementname() {
		return getStr("elementname");
	}

	/**
	 * 产线
	 */
	public M setLineId(java.lang.Integer lineId) {
		set("line_id", lineId);
		return (M)this;
	}

	/**
	 * 产线
	 */
	public java.lang.Integer getLineId() {
		return getInt("line_id");
	}

	/**
	 * 项目号
	 */
	public M setProId(java.lang.Integer proId) {
		set("pro_id", proId);
		return (M)this;
	}

	/**
	 * 项目号
	 */
	public java.lang.Integer getProId() {
		return getInt("pro_id");
	}

	/**
	 * 项目名称
	 */
	public M setProName(java.lang.String proName) {
		set("pro_name", proName);
		return (M)this;
	}

	/**
	 * 项目名称
	 */
	public java.lang.String getProName() {
		return getStr("pro_name");
	}

	/**
	 * 项目OPL
	 */
	public M setProIssueId(java.lang.Long proIssueId) {
		set("pro_issue_id", proIssueId);
		return (M)this;
	}

	/**
	 * 项目OPL
	 */
	public java.lang.Long getProIssueId() {
		return getLong("pro_issue_id");
	}

	/**
	 * 是否上升到总监  0未上升  1已上升
	 */
	public M setIsUpgradeZj(java.lang.Integer isUpgradeZj) {
		set("is_upgrade_zj", isUpgradeZj);
		return (M)this;
	}

	/**
	 * 是否上升到总监  0未上升  1已上升
	 */
	public java.lang.Integer getIsUpgradeZj() {
		return getInt("is_upgrade_zj");
	}

	public M setZjUserId(java.lang.String zjUserId) {
		set("zj_user_id", zjUserId);
		return (M)this;
	}

	public java.lang.String getZjUserId() {
		return getStr("zj_user_id");
	}

	public M setZjUserName(java.lang.String zjUserName) {
		set("zj_user_name", zjUserName);
		return (M)this;
	}

	public java.lang.String getZjUserName() {
		return getStr("zj_user_name");
	}

	/**
	 * 是否升级到总经理 0否  1是
	 */
	public M setIsUpgradeZjl(java.lang.Integer isUpgradeZjl) {
		set("is_upgrade_zjl", isUpgradeZjl);
		return (M)this;
	}

	/**
	 * 是否升级到总经理 0否  1是
	 */
	public java.lang.Integer getIsUpgradeZjl() {
		return getInt("is_upgrade_zjl");
	}

}

