package com.ray.common.model.base;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 零件列表
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseHParts<M extends BaseHParts<M>> extends Model<M> implements IBean {

	/**
	 * id
	 */
	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * id
	 */
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 零件号
	 */
	public M setPartNo(java.lang.String partNo) {
		set("part_no", partNo);
		return (M)this;
	}
	
	/**
	 * 零件号
	 */
	public java.lang.String getPartNo() {
		return getStr("part_no");
	}

	/**
	 * 零件名称
	 */
	public M setPartName(java.lang.String partName) {
		set("part_name", partName);
		return (M)this;
	}
	
	/**
	 * 零件名称
	 */
	public java.lang.String getPartName() {
		return getStr("part_name");
	}

	/**
	 * 备注
	 */
	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}
	
	/**
	 * 备注
	 */
	public java.lang.String getRemark() {
		return getStr("remark");
	}

	/**
	 * 状态
	 */
	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	/**
	 * 状态
	 */
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

}

