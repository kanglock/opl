package com.ray.common.model;

import com.ray.common.model.base.BaseHProissueEditRecord;

/**
 * 项目问题操作记录
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
public class HProissueEditRecord extends BaseHProissueEditRecord<HProissueEditRecord> {
	//建议将dao放在Service中只用作查询 
	public static final HProissueEditRecord dao = new HProissueEditRecord().dao();
	//在Service中声明 可直接复制过去使用
	//private HProissueEditRecord dao = new HProissueEditRecord().dao();  
}

