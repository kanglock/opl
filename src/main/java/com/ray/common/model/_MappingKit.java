package com.ray.common.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JBolt, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {
	
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("andon_issue_refuse_record", "id", AndonIssueRefuseRecord.class);
		arp.addMapping("andon_problem_edit_record", "id", AndonProblemEditRecord.class);
		arp.addMapping("andon_problem_issue_problem", "id", AndonProblemIssueProblem.class);
		arp.addMapping("andon_problem_track_record", "id", AndonProblemTrackRecord.class);
		arp.addMapping("andon_problem_type", "id", AndonProblemType.class);
		arp.addMapping("andon_product_issue", "id", AndonProductIssue.class);
		arp.addMapping("andon_reason_type", "id", AndonReasonType.class);
		arp.addMapping("data_button", "id", DataButton.class);
		arp.addMapping("data_field", "id", DataField.class);
		arp.addMapping("data_object", "id", DataObject.class);
		arp.addMapping("data_task", "id", DataTask.class);
		arp.addMapping("dicts", "id", Dicts.class);
		arp.addMapping("file", "id", File.class);
		arp.addMapping("h_cause_dep", "dep_id", HCauseDep.class);
		arp.addMapping("h_clothes_issue", "id", HClothesIssue.class);
		arp.addMapping("h_clothes_issue_8d_plan", "id", HClothesIssue8dPlan.class);
		arp.addMapping("h_clothes_issue_8d_real", "id", HClothesIssue8dReal.class);
		arp.addMapping("h_clothes_issue_month_track", "id", HClothesIssueMonthTrack.class);
		arp.addMapping("h_clothes_issue_reason_analysis", "id", HClothesIssueReasonAnalysis.class);
		arp.addMapping("h_clothes_issue_tell", "id", HClothesIssueTell.class);
		arp.addMapping("h_clothes_issue_track_record", "id", HClothesIssueTrackRecord.class);
		arp.addMapping("h_clothes_measures", "id", HClothesMeasures.class);
		arp.addMapping("h_clothes_track_history", "id", HClothesTrackHistory.class);
		arp.addMapping("h_clothesissue_edit_record", "id", HClothesissueEditRecord.class);
		arp.addMapping("h_clothesopl_upgrade_record", "id", HClothesoplUpgradeRecord.class);
		arp.addMapping("h_dep_line", "id", HDepLine.class);
		arp.addMapping("h_parts", "id", HParts.class);
		arp.addMapping("h_pro_cus_issue", "id", HProCusIssue.class);
		arp.addMapping("h_pro_cus_issue_8d_plan", "id", HProCusIssue8dPlan.class);
		arp.addMapping("h_pro_cus_issue_8d_real", "id", HProCusIssue8dReal.class);
		arp.addMapping("h_pro_cus_issue_month_track", "id", HProCusIssueMonthTrack.class);
		arp.addMapping("h_pro_cus_issue_reason_analysis", "id", HProCusIssueReasonAnalysis.class);
		arp.addMapping("h_pro_cus_issue_tell", "id", HProCusIssueTell.class);
		arp.addMapping("h_pro_cus_issue_track_record", "id", HProCusIssueTrackRecord.class);
		arp.addMapping("h_pro_cus_measures", "id", HProCusMeasures.class);
		arp.addMapping("h_pro_cus_track_history", "id", HProCusTrackHistory.class);
		arp.addMapping("h_pro_cus_upgrade_record", "id", HProCusUpgradeRecord.class);
		arp.addMapping("h_pro_issue_plantime_edit_record", "id", HProIssuePlantimeEditRecord.class);
		arp.addMapping("h_pro_issue_reason_analysis", "id", HProIssueReasonAnalysis.class);
		arp.addMapping("h_pro_issue_tell", "id", HProIssueTell.class);
		arp.addMapping("h_pro_issue_track_record", "id", HProIssueTrackRecord.class);
		arp.addMapping("h_proissue_edit_record", "id", HProissueEditRecord.class);
		arp.addMapping("h_proissue_track_history", "id", HProissueTrackHistory.class);
		arp.addMapping("h_project_issue", "id", HProjectIssue.class);
		arp.addMapping("h_project_measures", "id", HProjectMeasures.class);
		arp.addMapping("h_proopl_upgrade_record", "id", HProoplUpgradeRecord.class);
		arp.addMapping("h_qty_issue_8d_plan", "id", HQtyIssue8dPlan.class);
		arp.addMapping("h_qty_issue_8d_real", "id", HQtyIssue8dReal.class);
		arp.addMapping("h_qty_issue_reason_analysis", "id", HQtyIssueReasonAnalysis.class);
		arp.addMapping("h_qty_issue_track_record", "id", HQtyIssueTrackRecord.class);
		arp.addMapping("h_qty_measures", "id", HQtyMeasures.class);
		arp.addMapping("h_qty_track_history", "id", HQtyTrackHistory.class);
		arp.addMapping("h_qtyissue_edit_record", "id", HQtyissueEditRecord.class);
		arp.addMapping("h_quality_issue", "id", HQualityIssue.class);
		arp.addMapping("h_quality_issue_month_track", "id", HQualityIssueMonthTrack.class);
		arp.addMapping("h_quality_issue_tell", "id", HQualityIssueTell.class);
		arp.addMapping("h_quality_issue_type", "id", HQualityIssueType.class);
		arp.addMapping("h_qualityopll_upgrade_record", "id", HQualityopllUpgradeRecord.class);
		arp.addMapping("h_user_dep", "id", HUserDep.class);
		arp.addMapping("h_ware", "id", HWare.class);
		arp.addMapping("menu", "id", Menu.class);
		arp.addMapping("permissions", "id", Permissions.class);
		arp.addMapping("role_permission", "id", RolePermission.class);
		arp.addMapping("roles", "id", Roles.class);
		arp.addMapping("user", "id", User.class);
		arp.addMapping("user_role", "id", UserRole.class);
	}
}


