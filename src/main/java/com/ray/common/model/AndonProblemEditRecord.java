package com.ray.common.model;

import com.ray.common.model.base.BaseAndonProblemEditRecord;

/**
 * OPL编辑历史记录
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
public class AndonProblemEditRecord extends BaseAndonProblemEditRecord<AndonProblemEditRecord> {
	//建议将dao放在Service中只用作查询 
	public static final AndonProblemEditRecord dao = new AndonProblemEditRecord().dao();
	//在Service中声明 可直接复制过去使用
	//private AndonProblemEditRecord dao = new AndonProblemEditRecord().dao();  
}

