package com.ray.common.model;

import com.ray.common.model.base.BaseHClothesIssue8dPlan;
import com.ray.common.model.base.BaseHQtyIssue8dPlan;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class HClothesIssue8dPlan extends BaseHClothesIssue8dPlan<HClothesIssue8dPlan> {
	public static final HClothesIssue8dPlan dao = new HClothesIssue8dPlan().dao();
}
