package com.ray.common.model;

import com.ray.common.model.base.BaseHClothesIssueReasonAnalysis;
import com.ray.common.model.base.BaseHQtyIssueReasonAnalysis;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class HClothesIssueReasonAnalysis extends BaseHClothesIssueReasonAnalysis<HClothesIssueReasonAnalysis> {
	public static final HClothesIssueReasonAnalysis dao = new HClothesIssueReasonAnalysis().dao();
}
