package com.ray.common.model;

import com.ray.common.model.base.BaseAndonProductIssue;

/**
 * 生产OPL
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
public class AndonProductIssue extends BaseAndonProductIssue<AndonProductIssue> {
	//建议将dao放在Service中只用作查询 
	public static final AndonProductIssue dao = new AndonProductIssue().dao();
	//在Service中声明 可直接复制过去使用
	//private AndonProductIssue dao = new AndonProductIssue().dao();  
}

