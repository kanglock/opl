package com.ray.common.model;

import com.ray.common.model.base.BaseAndonIssueRefuseRecord;

/**
 * 拒绝记录
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
public class AndonIssueRefuseRecord extends BaseAndonIssueRefuseRecord<AndonIssueRefuseRecord> {
	//建议将dao放在Service中只用作查询 
	public static final AndonIssueRefuseRecord dao = new AndonIssueRefuseRecord().dao();
	//在Service中声明 可直接复制过去使用
	//private AndonIssueRefuseRecord dao = new AndonIssueRefuseRecord().dao();  
}

