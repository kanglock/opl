package com.ray.common.controller;

import cn.hutool.crypto.SecureUtil;
import com.jfinal.core.NotAction;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.redis.Redis;
import com.taobao.api.ApiException;
import java.util.List;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

public class MainController extends BaseController{

	/**
	 * 开发用
	 */
	public void a() throws ApiException{
			UsernamePasswordToken token = new UsernamePasswordToken("18382223446", "18382223446");
			Subject subject = SecurityUtils.getSubject();
			subject.login(token);
			Record user = Db.findFirst("select * from user where username = '18382223446'");
			Record record = Db.findFirst("SELECT GROUP_CONCAT(role_name) AS roles FROM roles WHERE id IN (SELECT role_id FROM user_role WHERE user_id = '"+user.get("id")+"')");
			user.set("roles", record.getStr("roles"));
			subject.getSession().setAttribute("user", user);
			subject.getSession().setAttribute("loginurltype", 0);//加
			redirect("/index");
	}

	public void dingLogin(){
		int urlType=getParaToInt("type");//加
		Subject subject = SecurityUtils.getSubject();
		subject.getSession().setAttribute("loginurltype", urlType);//加
		render("dingLogin.html");
	}
	public void index(){
		set("request",getRequest());
		render("index.html");
	}
	public void loginInit() {
		if(getPara("code")!=null){
			setAttr("code", getPara("code"));
			setAttr("icon", getPara("icon"));
		}else{
			setAttr("code", 0);
			setAttr("icon", 1);
		}
		render("login.html");
	}


	public void login() {
		UsernamePasswordToken token = new UsernamePasswordToken(getPara("username"), getPara("password"));
		Subject subject = SecurityUtils.getSubject();
		try {
			subject.login(token);
			Record user = Db.findFirst("select * from user where username = '" + getPara("username") + "'");
			Record record = Db.findFirst("SELECT GROUP_CONCAT(role_name) AS roles FROM roles WHERE id IN (SELECT role_id FROM user_role WHERE user_id = '"+user.get("id")+"')");
			user.set("roles", record.getStr("roles"));
			subject.getSession().setAttribute("user", user);
			subject.getSession().setAttribute("loginurltype", 0);//加
			renderJson(Ret.ok("msg", "登录成功"));
		} catch (IncorrectCredentialsException ice) {
			renderJson(Ret.fail("msg", "密码错误"));
		} catch (UnknownAccountException uae) {
			renderJson(Ret.fail("msg", "用户不存在"));
		} catch (ExcessiveAttemptsException eae) {
			renderJson(Ret.fail("msg", "错误登录过多"));
		}
	}

	public void logout() {
		Subject currentUser = SecurityUtils.getSubject();
		currentUser.logout();
		setAttr("code", 0);
		setAttr("icon", 1);
		render("login.html");
	}

	public void mainInfo() {
		int urlType=getSessionAttr("loginurltype");//加
		//1-标准OPL知会页面；2-标准OPL措施执行页面；3-标准OPL措施责任人页面；4-标准OPL措施确认人页面；5-标准OPL措施升级处理页面
		if (urlType==0) {
			render("sys/home/main.html");
		}else if (urlType==1) {
			render("/page/openIssue/standardProIssue/tellMeProOPL.html");
		}else if (urlType==2) {
			render("/page/openIssue/standardProIssue/myMeasureProOPL.html");
		}else if (urlType==3) {
			render("/page/openIssue/standardProIssue/myDutyProOPL.html");
		}else if (urlType==4) {
			render("/page/openIssue/standardProIssue/mySureProOPL.html");
		}else if (urlType==5) {
			render("/page/openIssue/standardProIssue/upgradeIssues.html");
		}else if (urlType==11) {//质量OPL知会
			render("/page/openIssue/qualityIssue/tellMeQtyOPL.html");
		}else if (urlType==13) {//13-质量OPL责任人
			render("/page/openIssue/qualityIssue/myDutyQualityOPL.html");
		}else if (urlType==15) {//质量OPL升级
			render("/page/openIssue/qualityIssue/upgradeQtyOPL.html");
		}else if (urlType==14) {//质量OPL跟踪
			render("/page/openIssue/qualityIssue/mySureQualityOPL.html");
		}else if (urlType==12) {//质量OPL措施执行
			render("/page/openIssue/qualityIssue/qtyMeasureDone.html");
		}else if (urlType==23) {//23-新品客诉OPL责任人
			render("/page/openIssue/newCusProIssue/newCusOPLMyDuty.html");
		}else if (urlType==22) {//新品客诉OPL措施执行
			render("/page/openIssue/newCusProIssue/newCusMeasureDone.html");
		}else if (urlType==43) {//生产OPL责任人页面
			render("/page/openIssue/productIssue/myDutyProductOPL.html");
		}
	}

	public void menus() {
		try {
			List<Record> top_menu = Db.find("select * from menu where parent_menu = 0 and is_hide = 0 order by seq_num");
			top_menu = menuAuth(top_menu);
			List<Record> menu = getMenus(top_menu);
			renderJson(Ret.ok("menus", menu));
		} catch (Exception e) {
			e.printStackTrace();
			renderJson(Ret.fail("msg", "失败：" + e.getMessage()));
		}
	}

	/**
	 * 递归获取菜单
	 * @param menus
	 * @return
	 */
	public List<Record> getMenus(List<Record> menus){
		for (int i = 0; i < menus.size(); i++) {
			List<Record> children_menu = Db.find("select * from menu where parent_menu = "+menus.get(i).getInt("id")+" and is_hide = 0 order by seq_num");
			if(children_menu.size()>0) {
				children_menu = getMenus(menuAuth(children_menu));
			}
			menus.get(i).set("children", menuAuth(children_menu));
		}
		return menus;
	}
	@NotAction
	public List<Record> menuAuth(List<Record> menu){
		Record user = (Record)getSessionAttr("user");
		String sql = "SELECT gl_id FROM permissions WHERE id IN (SELECT permission_id FROM role_permission WHERE role_id IN (SELECT role_id FROM user_role WHERE user_id = '"
				+ user.get("id") + "')) AND TYPE = 1";
		List<Record> menuPermissions = Db.find(sql);
		for (int i = 0; i < menu.size(); i++) {
			boolean flag = true;
			for (int j = 0; j < menuPermissions.size(); j++) {
				if (((Record) menu.get(i)).get("id").equals(((Record) menuPermissions.get(j)).get("gl_id"))) {
					flag = false;
				}
			}
			if (flag) {
				menu.remove(i);
				i--;
			}
		}
		return menu;
	}

	@NotAction
	public static void main(String[] args) {
		//Redis.use("test").lpush("ray", new Object[] { "1" });
		String s = SecureUtil.md5("18382223446");
		System.out.println(s);

	}
}
