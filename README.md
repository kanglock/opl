# Raybase

#### 介绍
基于元对象概念的快速建模开发平台

#### 架构
前端：vue elementUI layui
后端：JFinal
数据库：mysql

#### 安装教程

1.  git clone https://gitee.com/eddie_ray/Raybase.git
2.  eclipse导入maven项目
3.  导入Raybase.sql
3.  MainConfig main方法 start

#### 使用说明

自己研究
